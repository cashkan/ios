//
//  FirstViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 23/06/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController
{

    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var cameraView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor(red: 205/255, green: 205/255, blue:205/255, alpha: 1.0)

        //kl;ksdfl;skgls
        //  label.textAlignment = NSTextAlignment.Center
        label.text = "Pick a currency denomination,slide and click to Cashkan"
        label.lineBreakMode = NSLineBreakMode.byWordWrapping;
        label.numberOfLines = 0
        label.textColor = darkGray_color
        //ghhghfgfg
        cameraView.backgroundColor = UIColor(red: 205/255, green: 205/255, blue:205/255, alpha: 1.0)
        cameraView.layer.borderWidth = CGFloat(customButtonBorderWidth)
        cameraView.layer.borderColor = UIColor(red: 28/255, green: 128/255, blue:122/255, alpha: 1.0).cgColor
        cameraView.layer.cornerRadius = 16
        
        
        
        let mySwitch = SevenSwitch()
        mySwitch.addTarget(self, action: #selector(FirstViewController.switchValueDidChange(_:)), for: UIControlEvents.valueChanged)
        
        
        //  mySwitch.offImage = UIImage(named: "cross.png")
        //   mySwitch.onImage = UIImage(named: "check.png")
        //   mySwitch.thumbImage = UIImage(named: "Forward-25-2.png")
        mySwitch.offLabel.textColor = UIColor.white
        mySwitch.offLabel.font = UIFont .systemFont(ofSize: 20)//UIFont(name: label.font.fontName, size: 20)
        
        mySwitch.onLabel.textColor = UIColor.white
        mySwitch.onLabel.font = UIFont .systemFont(ofSize: 20)//UIFont(name: label.font.fontName, size: 20)
        
        mySwitch.offLabel.text = "On"
        mySwitch.onLabel.text = "Off"
        
        mySwitch.thumbTintColor = UIColor.white
        mySwitch.activeColor =  UIColor(red: 204/255, green: 104/255, blue:77/255, alpha: 1.0)
        mySwitch.inactiveColor = UIColor(red: 204/255, green: 104/255, blue:77/255, alpha: 1.0)   ;mySwitch.onTintColor =  UIColor(red: 204/255, green: 104/255, blue:77/255, alpha: 1.0)
        mySwitch.borderColor = UIColor.clear
        //    mySwitch.shadowColor = UIColor.blackColor()
        
        mySwitch.frame = CGRect(x: 80, y: 95, width: 140, height: 50)
        cameraView.addSubview(mySwitch)

        //mySwitch.isRounded = false


    }

    
    func switchValueDidChange(_ sender:UISwitch!)
    {
        if (sender.isOn == true)
        {
            //print("ON")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "billInfo") 
            self.modalPresentationStyle =  UIModalPresentationStyle.overCurrentContext
            self.present(vc, animated:false, completion: nil)

        }
        else{
            //print("off")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
