//
//  BorderViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 16/06/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class BorderViewController: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    let _border = CAShapeLayer()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        setup()
    }
    
    func setup() {
        _border.strokeColor =  UIColor(red: 190/255, green: 165/255, blue: 144/255, alpha: 1.0).cgColor

        _border.fillColor = nil;
        _border.lineDashPattern = [9,9 ]
        _border.lineWidth = 3
        
        self.layer.addSublayer(_border)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        _border.path = UIBezierPath(roundedRect: self.bounds, cornerRadius:32).cgPath
       // _border.borderWidth = 60
        //   _border.frame = self.bounds
    }


}
