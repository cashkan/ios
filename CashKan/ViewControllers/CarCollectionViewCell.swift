//
//  CarCollectionViewCell.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 18/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class CarCollectionViewCell: UICollectionViewCell {
    @IBOutlet var rankLabel: UILabel!
    @IBOutlet var currencyLabel: UILabel!
    
    @IBOutlet var inactiveLabel: UILabel!
    @IBOutlet var activeLabel: UILabel!
    
    
    @IBOutlet var carButton: UIButton!
    
}
