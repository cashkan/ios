//
//  CashTableView.swift
//  cashkan3
//
//  Created by emtarang on 19/06/16.
//  Copyright © 2016 emtarang. All rights reserved.
//

import UIKit


protocol CustomCellDelegate {
    func cellImgTapped(cell: UIImageView)
}
class CustomCashTableViewCell: UITableViewCell  {
    
    @IBOutlet var firstView: UIView!
    
    
    @IBOutlet var secondView: UIView!
    //@IBOutlet var circularItemImage: UIImageView!
    @IBOutlet var ItemName: UILabel!
    @IBOutlet var DateAndTime: UILabel!
    @IBOutlet var CashPrice: UILabel!
    @IBOutlet var ReceiptPrice: UILabel!
    @IBOutlet var cameraImage: UIImageView!
    @IBOutlet var lastReceiptUploadedDate: UILabel!
    @IBOutlet var showReceiptsImage: UIImageView!
    
    let receipPriceLabel : [UILabel] = []
    let spendObjectLabel : [UILabel] = []
    
    var receiptsArray : [(String,String)] = []
    
  
    
    let cellIdentifier = "Cell"

    
    var delegate: CustomCellDelegate?
    
    func addCustomlabels() {
    
        let Height: CGFloat = 20.0
        var TopMargin: CGFloat = 0
        
        
        for i in 0  ..< receiptsArray.count{
            
            if i == 0 {
                TopMargin = 3
            }
            else {
                TopMargin = TopMargin + Height + 0.1
            }
        
        let receiptPrice = UILabel()
        let spendObjective = UILabel()
            
        
        receiptPrice.translatesAutoresizingMaskIntoConstraints = false
        spendObjective.translatesAutoresizingMaskIntoConstraints = false
        
        
        self.contentView.addSubview(receiptPrice)
        self.contentView.addSubview(spendObjective)

        var (price,name) = receiptsArray[i]
            
        receiptPrice.text = price
        spendObjective.text = name
            
        receiptPrice.font = receiptPrice.font.fontWithSize(12)
        spendObjective.font = spendObjective.font.fontWithSize(12)
  
        let heightConstraint = NSLayoutConstraint(item: receiptPrice,attribute: NSLayoutAttribute.Height,
                                                      relatedBy: NSLayoutRelation.Equal,toItem: nil,attribute: NSLayoutAttribute.NotAnAttribute ,multiplier: 1,constant: Height)
        
        let topConstraint = NSLayoutConstraint(item: receiptPrice, attribute: .Top, relatedBy: .Equal, toItem: self.cameraImage, attribute: .Bottom, multiplier: 1, constant: TopMargin)
        
        let leadingConstraint = NSLayoutConstraint(item: receiptPrice, attribute: .Leading, relatedBy: .Equal, toItem: self.lastReceiptUploadedDate, attribute: .Leading, multiplier: 1, constant: 0)
        
        self.contentView.addConstraints([heightConstraint, topConstraint, leadingConstraint])
            
            let heightConstraint2 = NSLayoutConstraint(item: spendObjective,attribute: NSLayoutAttribute.Height,
                                                      relatedBy: NSLayoutRelation.Equal,toItem: nil,attribute: NSLayoutAttribute.NotAnAttribute ,multiplier: 1,constant: Height)
        
        let topConstraint2 = NSLayoutConstraint(item: spendObjective, attribute: .Top, relatedBy: .Equal, toItem: self.cameraImage, attribute: .Bottom, multiplier: 1, constant: TopMargin)
        
        let trailingConstraint = NSLayoutConstraint(item: spendObjective, attribute: .Trailing, relatedBy: .Equal, toItem: self.cameraImage, attribute: .Trailing, multiplier: 1, constant: 0)
        
         self.contentView.addConstraints([heightConstraint2,topConstraint2, trailingConstraint])
        }
        
        

    }
    
    
    func loadItem( itemName:String, creationDate : String, receiptDate : String,amount :NSNumber, actualSpend : NSNumber) {
        
        
        //circularItemImage.image = UIImage(named: circularImage)
        
        //self.circularItemImage.layer.cornerRadius = self.circularItemImage.frame.size.height / 2;
        //self.circularItemImage.clipsToBounds = true
        //self.circularItemImage.layer.borderWidth = 3.0
        //self.circularItemImage.layer.borderColor = UIColor.orangeColor().CGColor
        
        
        ItemName.text = itemName
        DateAndTime.text = creationDate
        CashPrice.text = HELPER.currencySym+amount.stringValue
        ReceiptPrice.text = HELPER.currencySym+actualSpend.stringValue
        lastReceiptUploadedDate.text = receiptDate
        
        self.cameraImage.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: Selector("imageTapped:"))
        //Add the recognizer to your view.
        self.cameraImage.addGestureRecognizer(tapRecognizer)
        
    }
    
    func imageTapped(sender: UITapGestureRecognizer) {
        delegate?.cellImgTapped(self.cameraImage)
    }
}




class CashTableView: UIViewController {}
//    
//    @IBOutlet var tableView: UITableView!
//    
//    @IBOutlet var spendObjBtn: UIButton!
//    
//    var responseObj : AnyObject! = nil

//    var items: [(String,String,String,NSNumber,NSNumber)] = []
//        //[
////        ("comn_icon010","Shoes","1 June 2016","100","150"),
////        ("comn_icon09","Watch Movie","5 June 2016","120","130"),
////        ("comn_icon08","Buy Dinner","1 June 2016","100","150"),
////        ("comn_icon011","Groceries","1 June 2016","100","150")]
//    
//    var uploadedReceiptsArray : [(String,String)] = [("120","Movie"),("100","dinner")]
//
//    var actualSpend     : [NSNumber]! = []
//    var creationDate    : [String]! = []
//    var receiptDate     : [String]! = []
//    var spendObjTitle   : [String]! = []
//    var totalvalue      : [NSNumber]! = []

//
//    override func viewDidLoad() {
//        super.viewDidLoad()

//        let nib = UINib(nibName: "CustomCashTableCell", bundle: nil)
//        
//        tableView.registerNib(nib, forCellReuseIdentifier: "customCell")
//        tableView.dataSource = self
//        tableView.rowHeight = 150
        
//        self.tableView.setNeedsLayout()
//        self.tableView.layoutIfNeeded()
       // NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadList:",name:"load", object: nil)


//    //}
//    
//  
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    override func viewWillAppear(animated: Bool) {
//        super.viewWillAppear(animated)
////print(responseObj)
//        if responseObj != nil{
//        
//            reloadCashTableView()
//            self.tableView.reloadData()
//        }
        
//        self.tableView.reloadData()
//    }
////    
//      func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        
//            return items.count;
//    }
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let cell:CustomCashTableViewCell = self.tableView.dequeueReusableCellWithIdentifier("customCell") as! CustomCashTableViewCell
//        cell.delegate = self
//
//        
//       // cell.receiptsArray = uploadedReceiptsArray
//        //cell.addCustomlabels()
//        
//        let (title,date,rcptDate,cashPrice,receiptPrice) = items[indexPath.row]
//        
//        cell.loadItem(title,creationDate:date,receiptDate : rcptDate,amount:cashPrice,actualSpend:receiptPrice)
//        
//               return cell
//    }
  
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        tableView.deselectRowAtIndexPath(indexPath, animated: true)
//        
//        
//    }
//    
//    
//    func cellImgTapped(cell: UIImageView) {
//        let indexPath = self.tableView.indexPathForRowAtPoint(cell.center)!
//        let selectedItem = items[indexPath.row]
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let secondViewController = storyboard.instantiateViewControllerWithIdentifier("ReceiptControllerId") 
//        
//        
//        
//                self.navigationController!.pushViewController(secondViewController, animated: false)
//     }
    
//    func loadList(notification: NSNotification){
//        //load data here
//        self.tableView.reloadData()
//    }

//    func reloadCashTableView(){
//    
//    print(responseObj["data"] as! NSArray)
//        
//        if let parseJSON = responseObj![data]{
//            
//            print(parseJSON!.count)
//            
//            if parseJSON!.count > 0{
//                
//                for var i in 0 ..< parseJSON!.count  {
//                    let obj = (parseJSON as! NSArray)[i] as! NSDictionary
//                    
//                    let amount = obj["totalValue"] as! NSNumber
//                    let objTitle = obj["spendObjTitle"] as! String
//                    let crDate = obj["createDt"] as! String  //NSString).doubleValue
//                    var rcptDate : String = crDate
////                    if let date =  obj["lastReceiptDate"] as! String{
////                         rcptDate = obj["lastReceiptDate"] as! String
////                    }
////                    else{
////                        rcptDate = ""
////                    }
//                    let actualSpend = (obj["actualSpend"] as! NSNumber)
//                    
//                    items.insert((objTitle,crDate,rcptDate,amount,actualSpend), atIndex: i)
//
//                }
//                print(items)
//                //self.activityIndicator.hideActivityIndicator(self.view)
//                //self.tableView.reloadData()
//
//                
//            }
//                
//            else{
////                self.activityIndicator.hideActivityIndicator(self.view)
////                self.topRewardsImage.hidden = true
////                self.tableView.hidden = true
////                self.rulesButton.hidden = true
////                self.NoRewardsLabel.hidden = false
//                
//            }
//          //  print(tableView)
//
//            
//        }
//
//    
//    }
//    
//}
