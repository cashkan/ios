//
//  ViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 14/06/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit


class ViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var checkBox: UIButton!
    @IBOutlet weak var rewardsView: UIView!
    @IBOutlet weak var cashkansView: UIView!
    @IBOutlet weak var receiptsView: UIView!
    @IBOutlet weak var cashView: UIView!
    
    @IBOutlet weak var viewOne: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    @IBOutlet weak var bottomView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        emailTextfield.backgroundColor = UIColor(red: 141/255, green: 107/255, blue: 82/255, alpha: 1.0)
        passwordTextfield.backgroundColor = UIColor(red: 141/255, green: 107/255, blue: 82/255, alpha: 1.0)
        
        
        
        emailTextfield.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        emailTextfield.layer.masksToBounds = true
        emailTextfield.layer.borderColor = UIColor(red: 141/255, green: 107/255, blue: 82/255, alpha: 1.0).cgColor
        passwordTextfield.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        passwordTextfield.layer.masksToBounds = true
        
        
        let placeholder = NSAttributedString(string: "  Email", attributes: [NSForegroundColorAttributeName : UIColor.white])
        emailTextfield.attributedPlaceholder = placeholder;
        //set Placeholder to password TextField
        let placeholder2 = NSAttributedString(string: "  Password", attributes: [NSForegroundColorAttributeName : UIColor.white])
        passwordTextfield.attributedPlaceholder = placeholder2;
        
        
        self.emailTextfield.delegate = self
        self.passwordTextfield.delegate = self

        
   //     NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
        
   //     NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)
        
       /* [checkBox setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
        [checkBox setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];    //using quartcore framework we will make a round button
        checkBox.layer.cornerRadius = 10;
        checkBox.layer.masksToBounds = YES;
        checkBox.tag = 1;
        [checkBox addTarget:self action:@selector(radioButtonClicked:) forControlEvents:UIControlEventTouchUpInside];*/

        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

        
       }

    
       //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   /*  func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent) 
    {
        
        super.touchesBegan(touches, withEvent: event)
        
      
    }*/
    

}

