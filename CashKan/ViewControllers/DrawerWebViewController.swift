//
//  WebViewController.swift
//  CashKan
//
//  Created by emtarang on 26/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class DrawerWebViewController: UIViewController, UIWebViewDelegate, SlideMenuDelegate, NetworkFailureDelegate {
    
    @IBOutlet var webView: UIWebView!
    var drawerBtn : UIButton!
    
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()
    
    var urlToVisit:String!
    var menuVC : MenuViewController!
    var selectedItemIndex : Int = 3
    
    
    var bgTapRecognizer:UITapGestureRecognizer!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        webView.delegate = self
        self.navigationItem.hidesBackButton = true
        view.tag = 200
        
        //Drawer button start
        self.drawerBtn = UIButton(type: UIButtonType.custom)
        //set image for button
        drawerBtn.setImage(UIImage(named: "iosdrawericon"), for: UIControlState())
        //add function for button
        drawerBtn.addTarget(self, action: #selector(self.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        drawerBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 25)
        drawerBtn.backgroundColor = UIColor.clear
        let barButton3 = UIBarButtonItem(customView: drawerBtn)
        self.navigationItem.leftBarButtonItem = barButton3;
        //Drawer button end
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.closeDrawer), name: NSNotification.Name(rawValue: "popDrawerController"), object: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.post(name: Notification.Name(rawValue:"changeTabBarColor"), object: nil)
        HELPER.setFontFamily( self.view, andSubViews: true)
        RequestResponceHelper.delegate = self
        if(HELPER.isDrawerOpen == true && menuVC != nil){
            self.dismissContainer()
        }
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 42 / 255, green: 150 / 255,blue: 99 / 255, alpha: 1.0)
        
        self.navigationItem.title = Drawer_titles[selectedItemIndex]
        
        if(Reachability.isConnectedToNetwork() == false){
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                
                self.navigationController!.pushViewController(secondViewController, animated: false)
            }
            
        }
        else{
            webView.delegate = self
            let url = URL(string: urlToVisit);
            let requestObj = URLRequest(url: url!);
            webView.loadRequest(requestObj);
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    //#MARK: Web vie delegate methods
    
    public func webViewDidStartLoad(_ webView: UIWebView){
        
        self.activityIndicator.showActivityIndicatory(self.webView)
        //self.view.isUserInteractionEnabled = false
        //self.webView.isUserInteractionEnabled = false
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        
        
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView){
        HELPER.iswebviewPresent=true //webviewIssue
        
        self.activityIndicator.hideActivityIndicator(self.webView)
        //self.view.isUserInteractionEnabled = true
        self.webView.isUserInteractionEnabled = true
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
    }
    
    //MARK: left drawer method start
    
       func dismissContainer(){
        
        self.view.removeGestureRecognizer(bgTapRecognizer)
        
        //        self.slideMenuItemSelectedAtIndex(-1);
        let viewMenuBack : UIView = view.subviews.last!
        
        if viewMenuBack.tag == 123 {
            self.drawerBtn.tag = 0
            HELPER.isDrawerOpen = false
            HELPER.iswebviewPresent = true
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * (UIScreen.main.bounds.size.width-drawerOffset)
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                 viewMenuBack.removeFromSuperview()
            })
        }
        
        let subViews = self.view.subviews
        for subview in subViews{
            if subview.tag == 111 {
                subview.removeFromSuperview()
            }
        }
        self.tabBarController?.tabBar.isUserInteractionEnabled = true

        return
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        if index > 2 {
            self.navigationItem.title = Drawer_titles[Int(index)]
        }
    }
    
    func onSlideMenuButtonPressed(_ sender : UIButton){
        
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            //            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            HELPER.isDrawerOpen = false
            HELPER.iswebviewPresent = true
            let viewMenuBack : UIView = view.subviews.last!
            if viewMenuBack.tag == 123 {
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    var frameMenu : CGRect = viewMenuBack.frame
                    frameMenu.origin.x = -1 * (UIScreen.main.bounds.size.width-drawerOffset)
                    viewMenuBack.frame = frameMenu
                    viewMenuBack.layoutIfNeeded()
                    viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
                })
            }
            
            let subViews = self.view.subviews
            for subview in subViews{
                if subview.tag == 111 {
                    subview.removeFromSuperview()
                }
            }
            self.tabBarController?.tabBar.isUserInteractionEnabled = true

            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        HELPER.isDrawerOpen = true
        HELPER.iswebviewPresent = false
        
        if(menuVC == nil){
            menuVC  = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        }
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.view.bringSubview(toFront: menuVC.view)
        
        self.addChildViewController(menuVC)
        let dummyDisabledView: UIView = UIView.init(frame: self.view.frame)
        dummyDisabledView.tag = 111
        dummyDisabledView.alpha = 0.5
        dummyDisabledView.backgroundColor = darkGray_color
        
        self.view.insertSubview(dummyDisabledView, belowSubview: menuVC.view)
        
        menuVC.view.layoutIfNeeded()
        bgTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(bgTapped(_:)))
        
        self.view.addGestureRecognizer(bgTapRecognizer)
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-drawerOffset-40, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.menuVC.view.frame=CGRect(x: 0, y: 64, width: UIScreen.main.bounds.size.width-drawerOffset-40, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
        }, completion:nil)
        
        self.tabBarController?.tabBar.isUserInteractionEnabled = false

    }
    
    //MARK: left drawer method end
    func bgTapped(_ sender: UITapGestureRecognizer) {
                dismissContainer()
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) { }
    
    func closeDrawer(){
        self.navigationController?.popToRootViewController(animated: false)
        HELPER.isDrawerOpen = false
        HELPER.iswebviewPresent = false
    }
    
    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
            
            self.navigationController!.pushViewController(secondViewController, animated: false)
        }
        
    }


     
}
