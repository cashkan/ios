//
//  CropperViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 24/08/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

// MARK: - AKImageCropperDelegate
//         _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

extension CropperViewController: AKImageCropperViewDelegate {
    

    func cropRectChanged(_ rect: CGRect) {
        
       // print("New crop rectangle: \(rect)")
    }
}

class CropperViewController: UIViewController {

    var _image: UIImage!
    var receipt :ReceiptController = ReceiptController()
    var register : RegsiterViewController! // = RegsiterViewController()
    var cam :CaptureReceiptViewController!
    var picker : UIImagePickerController!
    var isFromReceipt : Bool = false
    var isfromRegister : Bool = false
    
    @IBOutlet var cropView: AKImageCropperView!
    @IBOutlet var cropBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HELPER.setFontFamily( self.view, andSubViews: true)
        // Do any additional setup after loading the view.
        self.view.layoutIfNeeded()
        self.cropView.layoutIfNeeded()
        cropView.image = _image
        cropView.delegate = self
        
        cropView.showOverlayViewAnimated(true, withCropFrame: CGRect(x:cropView.imageView.frame.width/5 , y: cropView.imageView.frame.height/5, width: cropView.imageView.frame.width/2, height: cropView.imageView.frame.width/2), completion: { () -> Void in
        })
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isFromReceipt{
            self.isfromRegister = false
        }
        else if isfromRegister{
            isFromReceipt = false
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        /**
         
         If you use programmatically initalization
         Switch 'cropView' to 'cropViewProgrammatically'
         Example: cropViewProgrammatically.refresh()
         
         */
        cropView.refresh()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cropBtnClicked(_ sender: AnyObject) {
        
        if( self.isFromReceipt){
        
            _image = cropView.croppedImage()
            self.receipt.imageProf = _image
        
            self.cam.img = _image
            self.dismiss(animated: false, completion:{
                self.cam.onCancelClick(self.cam.cancelButton)

            })
        }
        else if isfromRegister{
            _image = cropView.croppedImage()
            self.register.imageProf = _image
            self.dismiss(animated: false, completion: nil )
        }
}

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "crop"){
            
            if isFromReceipt{
                let vc = segue.destination as! ReceiptController
                vc.imageProf = cropView.croppedImage()
            }
            else if isfromRegister{
                let vc = segue.destination as! RegsiterViewController
                vc.imageProf = cropView.croppedImage()
            }
        }

    }

   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
