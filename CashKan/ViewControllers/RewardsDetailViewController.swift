//
//  RewardsDetailViewController.swift
//  CashKan
//
//  Created by emtarang on 21/10/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class RewardsDetailViewController : UIViewController, NetworkFailureDelegate{

    
    @IBOutlet var outerView: UIView!
    @IBOutlet var rewardsImageBtn: UIButton!
    @IBOutlet var descriptionLbl: UILabel!
    @IBOutlet var requiredPointsValueLbl: UILabel!
    @IBOutlet var redeemBtn: UIButton!
    @IBOutlet var requiredPointsLbl: UILabel!
    @IBOutlet var merchantName: UILabel!
    @IBOutlet var bigDescrTextView: UITextView!
    
    var short_descr : String!
    var big_descr : String!
    var merchant_name : String!
    var required_points : String!
    var isenabledRedeembtn : Bool = false
    var rewards_id : NSNumber!
    var logoImageUrl : String!
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()

    var rewardsViewController : RewardsViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RequestResponceHelper.delegate = self
        self.redeemBtn.layer.borderColor = rewardsPink_color.cgColor
        self.redeemBtn.layer.borderWidth = 3
        self.redeemBtn.layer.cornerRadius = 10
        self.redeemBtn.layer.masksToBounds = true
        
        self.outerView.layer.borderColor = rewardsPink_color.cgColor
        self.outerView.layer.borderWidth = 2
        self.outerView.layer.cornerRadius = 10
        self.outerView.layer.masksToBounds = true
        
        HELPER.updateWithSpacing(lineSpacing: 2, textView: descriptionLbl)
        
        self.descriptionLbl.textColor = textGray_color
        self.descriptionLbl.textAlignment = .center
        self.requiredPointsValueLbl.textColor = textGray_color
        self.requiredPointsLbl.textColor = textGray_color
        self.merchantName.textColor = rewardsPink_color
        self.requiredPointsLbl.text = "Points Required :"
        self.redeemBtn.setTitleColor(rewardsPink_color, for: .normal)

        self.bigDescrTextView.font = UIFont(name: customAppFont, size: 14)

        self.descriptionLbl.text = self.short_descr
        self.requiredPointsValueLbl.text = self.required_points
        self.merchantName.text = self.merchant_name
        self.bigDescrTextView.text = self.big_descr
        self.bigDescrTextView.textColor = textGray_color
        HELPER.updateWithSpacing(lineSpacing: 2, textView: self.bigDescrTextView)
        self.bigDescrTextView.textAlignment = .center
        
        if(logoImageUrl != ""){
            if let urlString : String = logoImageUrl{
                // create NSURL instance
                if let url = NSURL(string: urlString) {
                    // check if your application can open the NSURL instance
                    if UIApplication.shared.canOpenURL(url as URL){
                        self.loadImageFromUrl(urlString, btn: self.rewardsImageBtn)
                        self.rewardsImageBtn.isSelected = false
                    }
                    else{
                        self.rewardsImageBtn.setImage(nil, for: .normal)
                    }
                }
            }
        }
        else{
            self.rewardsImageBtn.setImage(nil, for: .normal)
        }

        
        self.navigationItem.hidesBackButton = true
        
        let button: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        button.setImage(UIImage(named: "Cashkans_Back arrow"), for: UIControlState())
        //add function for button
        button.addTarget(self, action: #selector(ForgotPasswordViewController.back(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton;
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
        HELPER.setFontFamily( self.view, andSubViews: true)
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        self.navigationItem.title = "Reward Details"
        self.bigDescrTextView.textColor = textGray_color
        self.bigDescrTextView.font = UIFont(name: customAppFont, size: 14)
        if(self.bigDescrTextView.contentSize.height > self.bigDescrTextView.frame.height){
            self.bigDescrTextView.isScrollEnabled = true
        }
        else{
            self.bigDescrTextView.isScrollEnabled = false

        }
        if HELPER.backToRoot == true{
            HELPER.backToRoot = false
            self.navigationController?.popViewController(animated: false)
        }

        if self.isenabledRedeembtn {
            
            self.redeemBtn.isUserInteractionEnabled = true
            self.redeemBtn.setTitleColor(rewardsPink_color, for: .normal)
            self.redeemBtn.layer.borderColor = rewardsPink_color.cgColor
        }
        else{
            self.redeemBtn.isUserInteractionEnabled = false
            self.redeemBtn.setTitleColor(rewardsLightPink_color, for: .normal)
            self.redeemBtn.layer.borderColor = rewardsLightPink_color.cgColor
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if HELPER.isConnectedToNetwork {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    func loadImageFromUrl(_ url: String, btn: UIButton){
        
        // Create Url from string
        let url = URL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.sync(execute: { () -> Void in
                    btn.setImage( UIImage(data: data), for: .normal)
                    // self.myActivityIndicator.stopAnimating()
                })
            }
        })
        // Run task
        task.resume()
    }

    func back(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        //dismissViewControllerAnimated(false, completion: nil)
        
//        let transition = CATransition()
//        transition.duration = 0.2
//        transition.type = kCATransitionPush
//        transition.subtype = kCATransitionFromLeft
//        view.window!.layer.add(transition, forKey: kCATransition);
        self.navigationController?.popViewController(animated: true)
    
    }

    @IBAction func redeemBtnClick(_ sender: AnyObject) {
        
        let alertController = UIAlertController(title: alert_redeemRewrad, message: redeemRewardMessage as? String, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Redeem", style: .cancel) { (action:UIAlertAction!) in
            if(Reachability.isConnectedToNetwork() == false){
                
                DispatchQueue.main.async {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                    HELPER.isConnectedToNetwork = false
                    self.navigationController!.pushViewController(secondViewController, animated: false)
                }
            }
            else{
                self.redeemPoints(rewardId: self.rewards_id)
            }
        }
        
        let cancelAction = UIAlertAction(title: alert_cancel, style: .default) { (action:UIAlertAction!) in
            
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
        
    }
    
    func redeemPoints(rewardId : NSNumber){
        let user_id = HELPER.user_id.stringValue
        
        // Password Encryption
        
        self.activityIndicator.showActivityIndicatory(self.view) //(self.view)
        
        self.RedeemRewardPoints(rewardId)
        {
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            //print("got back: \(message)")
            
            if( success == true){
                
                if let response = object as? HTTPURLResponse{
                    
                    // You can print out response object
                    //print("response = \(response)")
                }
                
                do {
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                    
                    if let successMsg = myJSON![display_msg]{
                        //print(successMsg)
                        self.activityIndicator.hideActivityIndicator(self.view)
                        let alertController = UIAlertController(title: alert_cashkan, message: successMsg as? String, preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                            self.rewardsViewController.refreshRewards()
                            self.navigationController?.popViewController(animated: true)
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                    
                }
                catch {
                    //print(error)
                }
                
            }
                
            else{
                
                // If registration failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            let error = myJSON![display_msg]
                            self.activityIndicator.hideActivityIndicator(self.view)
                            
                            let alertController = UIAlertController(title: alert_cashkan, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        //print(error)
                    }
                    
                }
            }
            
            
        }
    }
    
    
    func RedeemRewardPoints(_ rewardId : NSNumber, completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
        
        let id =  HELPER.user_id.stringValue
        var rewardObject: NSDictionary = ["rewardId" : String(describing: rewardId)]
        
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest("/users/"+id+redeemRewardPoints_url, params: rewardObject as! Dictionary<String, AnyObject> )
        //print("/users/"+id+redeemRewardPoints_url)
        RequestResponceHelper.post(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.sync(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? Data{
                        //message = passedMessage
                        completion(success, object ,dataObject ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }
    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
            
            self.navigationController!.pushViewController(secondViewController, animated: false)
        }
        
    }


}
