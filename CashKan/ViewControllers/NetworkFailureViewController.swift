//
//  NetworkFailureViewController.swift
//  CashKan
//
//  Created by emtarang on 19/10/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import Foundation

class NetworkFailureViewController: UIViewController {

    @IBOutlet var noNetworkTextLbl: UILabel!
    @IBOutlet var refreshImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshImageView.isUserInteractionEnabled = true
        
        self.navigationItem.hidesBackButton = true
        
        self.navigationItem.title = "No Network"
        
        HELPER.setFontFamily( self.view, andSubViews: true)
//        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
//
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(self.refreshNetwork(_:)))
        self.refreshImageView.addGestureRecognizer(tap)
        
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        
//        self.navigationItem.title = "Network connection"
//        self.navigationItem.hidesBackButton = true
//        
//        let button: UIButton = UIButton(type: UIButtonType.custom)
//        //set image for button
//        button.setImage(UIImage(named: "Cashkans_Back arrow"), for: UIControlState())
//        //add function for button
//        button.addTarget(self, action: #selector(self.back(_:)), for: UIControlEvents.touchUpInside)
//        //set frame
//        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
//        
//        let barButton = UIBarButtonItem(customView: button)
//        self.navigationItem.leftBarButtonItem = barButton;
//        
//        self.noNetworkTextLbl.textColor = textGray_color
//
//    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        //super.viewWillAppear(animated)
//        
//           }
//
//    func back(_ sender: UIBarButtonItem){
//        self.navigationController?.popViewController(animated: true)
//    }
    
    func refreshNetwork(_ sender: UITapGestureRecognizer) {
        let duration = 2.0
        let delay = 0.0
        let options = UIViewKeyframeAnimationOptions.calculationModePaced
        
        let fullRotation = CGFloat(M_PI * 2)

        
        UIView.animateKeyframes(withDuration: duration, delay: delay, options: options, animations: {
            
            // note that we've set relativeStartTime and relativeDuration to zero.
            // Because we're using `CalculationModePaced` these values are ignored
            // and iOS figures out values that are needed to create a smooth constant transition
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0, animations: {
                self.refreshImageView.transform = CGAffineTransform(rotationAngle: 1/3 * fullRotation)
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0, animations: {
                self.refreshImageView.transform = CGAffineTransform(rotationAngle: 2/3 * fullRotation)
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0, animations: {
                self.refreshImageView.transform = CGAffineTransform(rotationAngle: 3/3 * fullRotation)
            })
            
            }, completion: { finished in
                
                if Reachability.isConnectedToNetwork() {
                    HELPER.isConnectedToNetwork = true
                    self.navigationController?.popViewController(animated: true)
                    
                }
        })
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if HELPER.isConnectedToNetwork == false {
                HELPER.backToRoot = true
               self.navigationController?.popViewController(animated: true)
        }

    }
    

}
