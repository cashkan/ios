//
//  RulesViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 14/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

protocol RulesViewDelegate {
    func cellImgTapped()
}

class RulesViewController: UIViewController {

    var rewards :  RewardsViewController!

    @IBOutlet var label: UILabel!
    @IBOutlet var viewOne: UIView!
    @IBOutlet var ruleLabel: UILabel!
    
    var delegate : RulesViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HELPER.setFontFamily( self.view, andSubViews: true)

        // Do any additional setup after loading the view.
        
        ruleLabel.text = rulesString
        label.textColor = UIColor(red: 187 / 255,
            green: 92 / 255,
            blue: 222 / 255,
            alpha: 1.0)

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonClicked(_ sender: AnyObject) {
        delegate?.cellImgTapped()

        rewards.view.alpha = 1
        rewards.navigationController?.navigationBar.alpha = 1
         rewards.tabBarController?.tabBar.alpha = 1
        rewards.tabBarController?.tabBar.isUserInteractionEnabled = true


        self.dismiss(animated: false, completion: nil)
       // rewards viewWillAppear:YES];

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
