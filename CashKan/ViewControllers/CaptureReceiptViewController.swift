//
//  ViewController.swift
//  AVCamSwift
//
//  Created by sunset on 14-11-9.
//  Copyright (c) 2014年 sunset. All rights reserved.
//

import UIKit
import AVFoundation
import AssetsLibrary



var SessionRunningAndDeviceAuthorizedContext = "SessionRunningAndDeviceAuthorizedContext"
var CapturingStillImageContext = "CapturingStillImageContext"
var RecordingContext = "RecordingContext"

class CaptureReceiptViewController: UIViewController, AVCaptureFileOutputRecordingDelegate {
    
    // MARK: property
    
    var sessionQueue: DispatchQueue!
    var session: AVCaptureSession?
    var videoDeviceInput: AVCaptureDeviceInput?
    var movieFileOutput: AVCaptureMovieFileOutput?
    var stillImageOutput: AVCaptureStillImageOutput?
    @IBOutlet var cancelButton: UIButton!
    
    var img: UIImage!
    
    var deviceAuthorized: Bool  = false
    var backgroundRecordId: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    //OfferDetailsViewController * offerDetails;
    
    var receipt :ReceiptController!
    
    var sessionRunningAndDeviceAuthorized: Bool {
        get {
            return (self.session?.isRunning != nil && self.deviceAuthorized )
        }
    }
    
    var runtimeErrorHandlingObserver: AnyObject?
    var lockInterfaceRotation: Bool = false
    
    @IBOutlet weak var previewView: AVCamPreviewView!
    // @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var snapButton: UIButton!
    //@IBOutlet weak var cameraButton: UIButton!
    
    
    
    
    
    
    // MARK: Override methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib
        
        
        snapButton.layer.cornerRadius = snapButton.frame.width / 2
        snapButton.backgroundColor  = UIColor.white
        
        let session: AVCaptureSession = AVCaptureSession()
        self.session = session
        
        self.previewView.session = session
        self.previewView.frame = self.view.bounds
        
        self.checkDeviceAuthorizationStatus()
        
        
        
        let sessionQueue: DispatchQueue = DispatchQueue(label: "session queue",attributes: [])
        
        self.sessionQueue = sessionQueue
        sessionQueue.async(execute: {
            self.backgroundRecordId = UIBackgroundTaskInvalid
            
            let videoDevice: AVCaptureDevice! = CaptureReceiptViewController.deviceWithMediaType(AVMediaTypeVideo, preferringPosition: AVCaptureDevicePosition.back)
            var error: NSError? = nil
            
            
            
            var videoDeviceInput: AVCaptureDeviceInput?
            do {
                videoDeviceInput = try AVCaptureDeviceInput(device: videoDevice)
            } catch let error1 as NSError {
                error = error1
                videoDeviceInput = nil
            } catch {
                fatalError()
            }
            
            if (error != nil) {
                
                let alertController = UIAlertController (title: "CashKan", message: "Go to Settings to enable camera access?", preferredStyle: .alert)
                
                let settingsAction = UIAlertAction(title: alert_ok, style: .default) { (_) -> Void in
                    
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    
                    if settingsUrl != nil {
                        
                        //UIApplication.shared.openURL(URL(string:"App-Prefs:root=Settings&path=CashKan")!)
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                        
                    }
                }
                let cancelAction = UIAlertAction(title: alert_cancel, style: .default) { (_) -> Void in
                    self.dismiss(animated: false, completion: nil)
                }
                
                
                alertController.addAction(cancelAction)
                alertController.addAction(settingsAction)
                
                self.present(alertController, animated: true, completion: nil)
                return
                
            }
            
            if session.canAddInput(videoDeviceInput){
                session.addInput(videoDeviceInput)
                self.videoDeviceInput = videoDeviceInput
                
                DispatchQueue.main.async(execute: {
                    // Why are we dispatching this to the main queue?
                    // Because AVCaptureVideoPreviewLayer is the backing layer for AVCamPreviewView and UIView can only be manipulated on main thread.
                    // Note: As an exception to the above rule, it is not necessary to serialize video orientation changes on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
                    
                    //                    let orientation: AVCaptureVideoOrientation =  AVCaptureVideoOrientation(rawValue: UIDevice.currentDevice().orientation.rawValue)!
                    
                    let orientation: AVCaptureVideoOrientation =  AVCaptureVideoOrientation.portrait
                    
                    
                    (self.previewView.layer as! AVCaptureVideoPreviewLayer).connection.videoOrientation = orientation
                    
                })
                
            }
            
            
            //            let audioDevice: AVCaptureDevice = AVCaptureDevice.devices(withMediaType: AVMediaTypeAudio).first as! AVCaptureDevice
            //
            //            var audioDeviceInput: AVCaptureDeviceInput?
            //
            //            do {
            //                audioDeviceInput = try AVCaptureDeviceInput(device: audioDevice)
            //            } catch let error2 as NSError {
            //                error = error2
            //                audioDeviceInput = nil
            //            } catch {
            //                fatalError()
            //            }
            //
            //            if error != nil{
            //                print(error)
            //                let alert = UIAlertController(title: "Error", message: error!.localizedDescription
            //                    , preferredStyle: .alert)
            //                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            //                self.present(alert, animated: true, completion: nil)
            //            }
            //            if session.canAddInput(audioDeviceInput){
            //                session.addInput(audioDeviceInput)
            //            }
            //
            
            
            //            let movieFileOutput: AVCaptureMovieFileOutput = AVCaptureMovieFileOutput()
            //            if session.canAddOutput(movieFileOutput){
            //                session.addOutput(movieFileOutput)
            //
            //
            //                let connection: AVCaptureConnection? = movieFileOutput.connection(withMediaType: AVMediaTypeVideo)
            //                let stab = connection?.isVideoStabilizationSupported
            //                if (stab != nil) {
            //                    connection!.enablesVideoStabilizationWhenAvailable = true
            //                }
            //
            //                self.movieFileOutput = movieFileOutput
            //
            //            }
            
            let stillImageOutput: AVCaptureStillImageOutput = AVCaptureStillImageOutput()
            if session.canAddOutput(stillImageOutput){
                stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
                session.addOutput(stillImageOutput)
                
                self.stillImageOutput = stillImageOutput
            }
            
            
        })
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.receipt.imageProf = img
        
        self.sessionQueue.async(execute: {
            
            self.addObserver(self, forKeyPath: "sessionRunningAndDeviceAuthorized", options: [.old , .new] , context: &SessionRunningAndDeviceAuthorizedContext)
            self.addObserver(self, forKeyPath: "stillImageOutput.capturingStillImage", options:[.old , .new], context: &CapturingStillImageContext)
            self.addObserver(self, forKeyPath: "movieFileOutput.recording", options: [.old , .new], context: &RecordingContext)
            
            NotificationCenter.default.addObserver(self, selector: #selector(CaptureReceiptViewController.subjectAreaDidChange(_:)), name: NSNotification.Name.AVCaptureDeviceSubjectAreaDidChange, object: self.videoDeviceInput?.device)
            
            
            weak var weakSelf = self
            
            self.runtimeErrorHandlingObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name.AVCaptureSessionRuntimeError, object: self.session, queue: nil, using: {
                (note: Notification?) in
                let strongSelf: CaptureReceiptViewController = weakSelf!
                strongSelf.sessionQueue.async(execute: {
                    //                    strongSelf.session?.startRunning()
                    if let sess = strongSelf.session{
                        sess.startRunning()
                    }
                    //                    strongSelf.recordButton.title  = NSLocalizedString("Record", "Recording button record title")
                })
                
            })
            
            self.session?.startRunning()
            
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.sessionQueue.async(execute: {
            
            if let sess = self.session{
                sess.stopRunning()
                
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVCaptureDeviceSubjectAreaDidChange, object: self.videoDeviceInput?.device)
                NotificationCenter.default.removeObserver(self.runtimeErrorHandlingObserver!)
                
                self.removeObserver(self, forKeyPath: "sessionRunningAndDeviceAuthorized", context: &SessionRunningAndDeviceAuthorizedContext)
                
                self.removeObserver(self, forKeyPath: "stillImageOutput.capturingStillImage", context: &CapturingStillImageContext)
                self.removeObserver(self, forKeyPath: "movieFileOutput.recording", context: &RecordingContext)
                
                
            }
            
            
            
        })
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func onCancelClick(_ sender: UIButton) {
        
        //self.navigationController?.popViewControllerAnimated(true)
        self.dismiss(animated: false, completion: nil)
        
    }
    
    
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        
        (self.previewView.layer as! AVCaptureVideoPreviewLayer).connection.videoOrientation = AVCaptureVideoOrientation(rawValue: toInterfaceOrientation.rawValue)!
        
        //        if let layer = self.previewView.layer as? AVCaptureVideoPreviewLayer{
        //            layer.connection.videoOrientation = self.convertOrientation(toInterfaceOrientation)
        //        }
        
    }
    
    override var shouldAutorotate : Bool {
        return !self.lockInterfaceRotation
    }
    //    observeValueForKeyPath:ofObject:change:context:
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        
        
        if context == &CapturingStillImageContext{
            let isCapturingStillImage: Bool = (change![NSKeyValueChangeKey.newKey]! as AnyObject).boolValue
            if isCapturingStillImage {
                self.runStillImageCaptureAnimation()
            }
            
        }
            //           else if context  == &RecordingContext{
            //            let isRecording: Bool = change![NSKeyValueChangeNewKey]!.boolValue
            //
            //            dispatch_async(dispatch_get_main_queue(), {
            //
            //                if isRecording {
            //                    self.recordButton.titleLabel!.text = "Stop"
            //                    self.recordButton.enabled = true
            //                    //                    self.snapButton.enabled = false
            //                    self.cameraButton.enabled = false
            //
            //                }else{
            //                    //self.snapButton.enabled = true
            //
            //                    self.recordButton.titleLabel!.text = "Record"
            //                    self.recordButton.enabled = true
            //                    self.cameraButton.enabled = true
            //
            //                }
            //
            //
            //            })
            
            
            //       }
            
        else{
            return super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
        
    }
    
    
    // MARK: Selector
    func subjectAreaDidChange(_ notification: Notification){
        let devicePoint: CGPoint = CGPoint(x: 0.5, y: 0.5)
        self.focusWithMode(AVCaptureFocusMode.continuousAutoFocus, exposureMode: AVCaptureExposureMode.continuousAutoExposure, point: devicePoint, monitorSubjectAreaChange: false)
    }
    
    // MARK:  Custom Function
    
    func focusWithMode(_ focusMode:AVCaptureFocusMode, exposureMode:AVCaptureExposureMode, point:CGPoint, monitorSubjectAreaChange:Bool){
        
        self.sessionQueue.async(execute: {
            let device: AVCaptureDevice! = self.videoDeviceInput!.device
            
            do {
                try device.lockForConfiguration()
                
                if device.isFocusPointOfInterestSupported && device.isFocusModeSupported(focusMode){
                    device.focusMode = focusMode
                    device.focusPointOfInterest = point
                }
                if device.isExposurePointOfInterestSupported && device.isExposureModeSupported(exposureMode){
                    device.exposurePointOfInterest = point
                    device.exposureMode = exposureMode
                }
                device.isSubjectAreaChangeMonitoringEnabled = monitorSubjectAreaChange
                device.unlockForConfiguration()
                
            }catch{
                //print(error)
            }
            
            
            
            
        })
        
    }
    
    
    
    class func setFlashMode(_ flashMode: AVCaptureFlashMode, device: AVCaptureDevice){
        
        if device.hasFlash && device.isFlashModeSupported(flashMode) {
            var error: NSError? = nil
            do {
                try device.lockForConfiguration()
                device.flashMode = flashMode
                device.unlockForConfiguration()
                
            } catch let error1 as NSError {
                error = error1
                //print(error)
            }
        }
        
    }
    
    func runStillImageCaptureAnimation(){
        DispatchQueue.main.async(execute: {
            self.previewView.layer.opacity = 0.0
            //print("opacity 0")
            UIView.animate(withDuration: 0.25, animations: {
                self.previewView.layer.opacity = 1.0
                //print("opacity 1")
            })
        })
    }
    
    class func deviceWithMediaType(_ mediaType: String, preferringPosition:AVCaptureDevicePosition)->AVCaptureDevice{
        
        var devices = AVCaptureDevice.devices(withMediaType: mediaType);
        var captureDevice: AVCaptureDevice = devices![0] as! AVCaptureDevice;
        
        for device in devices!{
            if (device as AnyObject).position == preferringPosition{
                captureDevice = device as! AVCaptureDevice
                break
            }
        }
        
        return captureDevice
        
        
    }
    
    func checkDeviceAuthorizationStatus(){
        let mediaType:String =  AVMediaTypeVideo;
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized {
            // Already Authorized
            self.deviceAuthorized = true;
            
        } else {

        
        AVCaptureDevice.requestAccess(forMediaType: mediaType, completionHandler: { (granted: Bool) in
            
            if granted{
                self.deviceAuthorized = true;
            }else{
               
                self.deviceAuthorized = false;
            }
        })
        }
    }
    
    
    // MARK: File Output Delegate
    func capture(_ captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL: URL!, fromConnections connections: [Any]!, error: Error!) {
        
        if(error != nil){
            //print(error)
        }
        
        self.lockInterfaceRotation = false
        
        // Note the backgroundRecordingID for use in the ALAssetsLibrary completion handler to end the background task associated with this recording. This allows a new recording to be started, associated with a new UIBackgroundTaskIdentifier, once the movie file output's -isRecording is back to NO — which happens sometime after this method returns.
        
        let backgroundRecordId: UIBackgroundTaskIdentifier = self.backgroundRecordId
        self.backgroundRecordId = UIBackgroundTaskInvalid
        
        
        ALAssetsLibrary().writeVideoAtPath(toSavedPhotosAlbum: outputFileURL, completionBlock: {
            (assetURL, error) in
            if error != nil{
                //print(error)
                
            }
            
            do {
                try FileManager.default.removeItem(at: outputFileURL)
            } catch _ {
            }
            
            if backgroundRecordId != UIBackgroundTaskInvalid {
                UIApplication.shared.endBackgroundTask(backgroundRecordId)
            }
            
        })
        
        
    }
    
      @IBAction func snapStillImage(_ sender: AnyObject) {
        //print("snapStillImage")
        self.sessionQueue.async(execute: {
            // Update the orientation on the still image output video connection before capturing.
            
            let videoOrientation =  (self.previewView.layer as! AVCaptureVideoPreviewLayer).connection.videoOrientation
            
            self.stillImageOutput!.connection(withMediaType: AVMediaTypeVideo).videoOrientation = videoOrientation
            
            // Flash set to Auto for Still Capture
            CaptureReceiptViewController.setFlashMode(AVCaptureFlashMode.off, device: self.videoDeviceInput!.device)
            
            
            self.stillImageOutput!.captureStillImageAsynchronously(from: self.stillImageOutput!.connection(withMediaType: AVMediaTypeVideo), completionHandler: {
                (imageDataSampleBuffer, error) in
                
                if error == nil {
                    let data:Data = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                    let image:UIImage = UIImage( data: data)!
                    
                    //let libaray:ALAssetsLibrary = ALAssetsLibrary()
                    //let orientation: ALAssetOrientation = ALAssetOrientation(rawValue: image.imageOrientation.rawValue)!
                    //libaray.writeImageToSavedPhotosAlbum(image.CGImage, orientation: orientation, completionBlock: nil)
                    
                    //print("save to album")
                    
                    self.receipt.imageProf = image
                    self.dismiss(animated: false, completion: nil)
                    
                    
                    
                }else{
                    //print(error)
                }
                
                
            })
            
            
        })
    }
    @IBAction func changeCamera(_ sender: AnyObject) {
        
        
        
        //print("change camera")
        
        //self.cameraButton.enabled = false
        //self.recordButton.enabled = false
        self.snapButton.isEnabled = false
        
        self.sessionQueue.async(execute: {
            
            let currentVideoDevice:AVCaptureDevice = self.videoDeviceInput!.device
            let currentPosition: AVCaptureDevicePosition = currentVideoDevice.position
            var preferredPosition: AVCaptureDevicePosition = AVCaptureDevicePosition.unspecified
            
            switch currentPosition{
            case AVCaptureDevicePosition.front:
                preferredPosition = AVCaptureDevicePosition.back
            case AVCaptureDevicePosition.back:
                preferredPosition = AVCaptureDevicePosition.front
            case AVCaptureDevicePosition.unspecified:
                preferredPosition = AVCaptureDevicePosition.back
                
            }
            
            
            
            let device:AVCaptureDevice = CaptureReceiptViewController.deviceWithMediaType(AVMediaTypeVideo, preferringPosition: preferredPosition)
            
            var videoDeviceInput: AVCaptureDeviceInput?
            
            do {
                videoDeviceInput = try AVCaptureDeviceInput(device: device)
            } catch _ as NSError {
                videoDeviceInput = nil
            } catch {
                fatalError()
            }
            
            self.session!.beginConfiguration()
            
            self.session!.removeInput(self.videoDeviceInput)
            
            if self.session!.canAddInput(videoDeviceInput){
                
                NotificationCenter.default.removeObserver(self, name:NSNotification.Name.AVCaptureDeviceSubjectAreaDidChange, object:currentVideoDevice)
                
                CaptureReceiptViewController.setFlashMode(AVCaptureFlashMode.off, device: device)
                
                NotificationCenter.default.addObserver(self, selector: #selector(CaptureReceiptViewController.subjectAreaDidChange(_:)), name: NSNotification.Name.AVCaptureDeviceSubjectAreaDidChange, object: device)
                
                self.session!.addInput(videoDeviceInput)
                self.videoDeviceInput = videoDeviceInput
                
            }else{
                self.session!.addInput(self.videoDeviceInput)
            }
            
            self.session!.commitConfiguration()
            
            
            
            DispatchQueue.main.async(execute: {
                //self.recordButton.enabled = true
                self.snapButton.isEnabled = true
                // self.cameraButton.enabled = true
            })
            
        })
        
        
        
        
    }
    
    @IBAction func focusAndExposeTap(_ gestureRecognizer: UIGestureRecognizer) {
        
        //print("focusAndExposeTap")
        let devicePoint: CGPoint = (self.previewView.layer as! AVCaptureVideoPreviewLayer).captureDevicePointOfInterest(for: gestureRecognizer.location(in: gestureRecognizer.view))
        
        //print(devicePoint)
        
        self.focusWithMode(AVCaptureFocusMode.autoFocus, exposureMode: AVCaptureExposureMode.autoExpose, point: devicePoint, monitorSubjectAreaChange: true)
        
    }
}

