//
//  DetailsViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 21/06/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

protocol CustomDetailsCellDelegate {
    func deleteScannedRecord(_ view: UITapGestureRecognizer)
}
class CustomCashDetailsCell : UITableViewCell{

    @IBOutlet var cashkanBillLabel  : UILabel!
    @IBOutlet var currenciesButton  : UIButton!
    @IBOutlet var deleteButton      : UIButton!
    @IBOutlet var cashkanValueButton: UIButton!
    @IBOutlet var dividerView: UIView!
    @IBOutlet var deleteBtnView: UIView!
    @IBOutlet var currencySymbolLabel: UIButton!
    
    var delegate : CustomDetailsCellDelegate?

    
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseCurrencyDropDown
        ]
    }()
    
    // create Dropdowns
    
    let chooseCurrencyDropDown = DropDown()
    var den : [String] = []
    
    override func awakeFromNib() {
        
        self.contentView.layoutIfNeeded()
        self.customizeDropDown(self)
        setupDropDowns()
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        self.currenciesButton.layoutIfNeeded()
        self.cashkanValueButton.layoutIfNeeded()
        
        self.deleteBtnView.isUserInteractionEnabled = true
        let deleteTap = UITapGestureRecognizer(target:self, action:#selector(self.deleteButtonClicked(_:)))
        self.deleteBtnView.addGestureRecognizer(deleteTap)
        self.dividerView.backgroundColor = lightGray_color
        
    }
    
    @IBAction func currenciesButtonClicked(_ sender: UIButton) {
        
        chooseCurrencyDropDown.show()
        
    }
    
    
    func customizeDropDown(_ sender: AnyObject) {
        let appearance = DropDown.appearance()
        
        appearance.cellHeight = 40
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = lightGray_color
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 7
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        
        dropDowns.forEach {
            /*** FOR CUSTOM CELLS ***/
            $0.cellNib = UINib(nibName: "MyCell", bundle: nil)
            
            $0.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                guard cell is MyCell else { return }
                
            }
        }
    }
    
    func setupDropDowns() {
        
        chooseCurrencyDropDown.anchorView = currencySymbolLabel //cashkanValueButton
        chooseCurrencyDropDown.width = cashkanValueButton.frame.width + currenciesButton.frame.width + currencySymbolLabel.frame.width
        
        //print(cashkanValueButton.frame.width)
        
        // Will set a custom with instead of anchor view width
        //		dropDown.width = 100
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        
        let localeDenominationCount = HELPER.localeInfo.count
        
        for i in 0 ..< localeDenominationCount  {
            
            let denominationStr =  (((HELPER.localeInfo[i] as? NSDictionary )!["denomination"] as? NSNumber)!).stringValue
            
            den.insert(denominationStr, at: i)
        }
        
        chooseCurrencyDropDown.dataSource = den
        chooseCurrencyDropDown.bottomOffset = CGPoint(x: 0, y: 30) //cashkanValueButton.bounds.height)
        
        
    }

    @IBAction func deleteButtonClicked(_ sender: UITapGestureRecognizer) {
        
        delegate?.deleteScannedRecord(sender)
    }
    
    
}


class DetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, CustomDetailsCellDelegate {
    
    @IBOutlet var outerView         : UIView!
    @IBOutlet weak var tableView    : UITableView!
    @IBOutlet weak var footerView   : UIView!
    @IBOutlet var detailButton      : UIButton!
    @IBOutlet var noScannedCurruncyLabel: UILabel!

    
    var cashkanBills    : [(NSString,NSString)]! = []
    var cashview        :  CashViewController!
    var isCashkanEdited : Bool = false
    var finalChangedValue : String = ""
    var changeIndexPath : Int = 0
    let cellIdentifier = "CellIdentifier"
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()

    //#MARK : Default methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        HELPER.setFontFamily( self.view, andSubViews: true)

        self.noScannedCurruncyLabel.isHidden = true
        self.outerView.layer.borderWidth = 1
        self.outerView.layer.borderColor = lightGray_color.cgColor
        self.tableView.rowHeight = 40
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        HELPER.isCashDetailsOpen = true
        HELPER.setFontFamily( self.view, andSubViews: true)
        let blueColor = UIColor(red: 64/255, green: 89/255, blue: 128/255, alpha: 1)
        if(cashkanBills.count == 0)
        {
            self.tableView.isHidden = true
            self.noScannedCurruncyLabel.isHidden = false
            
        }
        else{
            
            self.tableView.isHidden = false
            self.noScannedCurruncyLabel.isHidden = true
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // Reset Details button image depending on scanned value
    func resetDetailsImage(){
        
        if(self.cashkanBills.count == 0){
            self.detailButton.imageView?.image = UIImage(named:"scan_cash_icon01_disabled.png")
            self.detailButton.isUserInteractionEnabled = false
        }
        else{
            self.detailButton.imageView?.image = UIImage(named:"ScanCash_icon01.png")
            self.detailButton.isUserInteractionEnabled = true
            
        }
        
    }

    //#MARK: Table view delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cashkanBills.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CustomCashDetailsCell
        
        let (bill,denom) = cashkanBills[(indexPath as NSIndexPath).row]
        cell.cashkanValueButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left 
        cell.deleteButton.tag = (indexPath as NSIndexPath).row
        cell.currenciesButton.tag = (indexPath as NSIndexPath).row
        cell.cashkanBillLabel.text = cashkanBill + (bill as String)
        cell.cashkanValueButton.setTitle(denom as String, for: UIControlState())
        cell.currencySymbolLabel.setTitle(HELPER.currencySym, for: UIControlState())
        cell.delegate = self
        
        // Action triggered on selection

        cell.chooseCurrencyDropDown.selectionAction = { [unowned self] (index, item) in
            cell.cashkanValueButton.setTitle(item, for: UIControlState())
            if(denom as String != item){
                self.activityIndicator.showActivityIndicatory(self.view)

               self.isCashkanEdited = true
                self.finalChangedValue = item
                self.changeIndexPath = (indexPath as NSIndexPath).row
                
                if self.isCashkanEdited{
                    
                    var pos : Int = 0
                    let localeDenominationCount = HELPER.localeInfo.count
                    let (bill,denom) = self.cashview.billArray[self.changeIndexPath]
                   // print(self.cashkanBills)
                    
                    for i in 0 ..< localeDenominationCount  {
                        
                        let denominationStr =  (((HELPER.localeInfo[i] as? NSDictionary )!["denomination"] as? NSNumber)!).stringValue
                        if (denominationStr) == denom as String{
                           // print("denom to match with : \(denom)")
                            pos = i
                            self.cashview.billCount[i] = (self.cashview.billCount[i].doubleValue - 1.0 )as NSNumber
                            self.cashview.totalScannedValue = (self.cashview.totalScannedValue - (Int( denominationStr)!)) as Int
                            
                        }
                        if( denominationStr == self.finalChangedValue){

                            self.cashview.billCount[i] = (self.cashview.billCount[i].doubleValue + 1.0) as NSNumber
                            self.cashview.totalScannedValue = self.cashview.totalScannedValue + Int(denominationStr)!
                        }
                    }
                    self.isCashkanEdited = false
                    let cashkanBillStr = self.cashview.finalCurrencyArray[self.changeIndexPath].3
                    let localeId = ((HELPER.localeInfo[pos] as? NSDictionary )!["localeId"] as? NSNumber)!
                    let currencyId = ((HELPER.localeInfo[pos] as? NSDictionary )!["currencyId"] as? NSNumber)!
                    self.cashview.finalCurrencyArray[self.changeIndexPath] = (localeId,currencyId,self.finalChangedValue as NSString,cashkanBillStr)
                    self.cashview.billArray[self.changeIndexPath] = (cashkanBillStr , self.finalChangedValue as NSString)
                    self.cashkanBills = self.cashview.billArray
                    self.tableView.reloadData()
                }
                
                self.cashview.refreshView()
                self.activityIndicator.hideActivityIndicator(self.view)

            }
        }
        
        return cell
    }
    
    @IBAction func collapseButtonClicked(_ sender: AnyObject) {
        
        self.closeView()
    }
    
    func closeView(){
        cashview.secondViewContainer.alpha = 1
        cashview.tabBarController?.tabBar.alpha = 1
        if (cashview.firstViewContainer.isHidden == true){
            cashview.secondViewContainer.backgroundColor = UIColor.white
        }
        else{
            cashview.secondViewContainer.backgroundColor = backgroundGray_color
            
        }
        cashview.secondViewContainer.isUserInteractionEnabled = true
        cashview.tabBarController?.tabBar.isUserInteractionEnabled = true
        self.dismiss(animated: false, completion: nil)
    }

    func deleteScannedRecord(_ sender: UITapGestureRecognizer) {
        let tapLocation = sender.location(in: self.tableView)
        //using the tapLocation, we retrieve the corresponding indexPath
        let indexPath = self.tableView.indexPathForRow(at: tapLocation)
        
        let row = (indexPath! as NSIndexPath).row
        
        let denom = cashkanBills[row].1
        let localeDenominationCount = HELPER.localeInfo.count
        
        for i in 0 ..< localeDenominationCount  {
            
            let denominationStr =  (((HELPER.localeInfo[i] as? NSDictionary )!["denomination"] as? NSNumber)!).stringValue
            if (denominationStr) == denom as String{
                self.cashview.billCount[i] = self.cashview.billCount[i].doubleValue - 1.0 as NSNumber
                self.cashview.totalScannedValue = self.cashview.totalScannedValue - Int(denominationStr)!
                break
            }
        }
        self.cashview.billArray.remove(at: row)
        self.cashkanBills.remove(at: row)
        self.cashview.finalCurrencyArray.remove(at: row)
        self.cashview.billArrayIndex -= 1
        self.cashview.refreshView()
        self.tableView.reloadData()
        if(cashkanBills.count == 0)
        {
            self.resetDetailsImage()
            self.tableView.isHidden = true
            self.noScannedCurruncyLabel.isHidden = false
            self.closeView()
            
        }
        else{
            
            self.tableView.isHidden = false
            self.noScannedCurruncyLabel.isHidden = true
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        HELPER.isCashDetailsOpen = false
    }
}
