//
//  RegsiterViewController.swift
//  Cashkan_auotoSizing
//
//  Created by emtarang on 24/06/16.
//  Copyright © 2016 emtarang. All rights reserved.
//

import UIKit
import CryptoSwift
import CoreLocation
import Firebase

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


struct MoveKeyboard {
    static let KEYBOARD_ANIMATION_DURATION : CGFloat = 0.3
    static let MINIMUM_SCROLL_FRACTION : CGFloat = 0.2;
    static let MAXIMUM_SCROLL_FRACTION : CGFloat = 0.8;
    static let PORTRAIT_KEYBOARD_HEIGHT : CGFloat = 216;
    static let LANDSCAPE_KEYBOARD_HEIGHT : CGFloat = 162;
}

class RegsiterViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,CLLocationManagerDelegate , NetworkFailureDelegate{
   
    var imageProf: UIImage!
    var animateDistance = CGFloat()
    let locationManager = CLLocationManager()
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()
    var EncrypetdPassword: String = ""
    var Imagepicker:UIImagePickerController?=UIImagePickerController()
    var popover:UIPopoverController? = nil
    var genderFlag = 0
    var activeField: UITextField?
    var popDatePicker : PopDatePicker?
    var gender: String = ""
    var imageFileName: String = ""
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var InnerView: UIView!
    
    var Datepicker = UIDatePicker()
    
    @IBOutlet var firstNameText:    UILabel!
    @IBOutlet var lastNameText:     UILabel!
    @IBOutlet var emailText:        UILabel!
    @IBOutlet var passwordText:     UILabel!
    @IBOutlet var confirmPassText:  UILabel!
    @IBOutlet var registerBtn:      UIButton!
    @IBOutlet var firstName:        UITextField!
    @IBOutlet var lastName:         UITextField!
    @IBOutlet var email:            UITextField!
    @IBOutlet var password:         UITextField!
    @IBOutlet var confirmPass:      UITextField!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var firstNameView:    UIView!
    @IBOutlet var lastNameView:     UIView!
    @IBOutlet var emailView:        UIView!
    @IBOutlet var passwordView:     UIView!
    @IBOutlet var confirmView:      UIView!
    @IBOutlet var termsOfUseTextView: UITextView!
    @IBOutlet var termsOfUseButton: UIButton!
    @IBOutlet var passLenLabel :    UILabel!
    
    var latitude            : String = ""
    var longitude           : String = ""
    var termsOfUseSelected  : Bool = false

    //#MARK: Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HELPER.setFontFamily( self.view, andSubViews: true)

        self.view.layoutIfNeeded()

         //  configurePicker()
        
        firstName.delegate      = self
        lastName.delegate       = self
        email.delegate          = self
        password.delegate       = self
        confirmPass.delegate    = self
    
        // set text color
        let blueColor = UIColor(red: 64/255, green: 89/255, blue: 128/255, alpha: 1)
        
        firstNameText.textColor   = blueColor
        lastNameText.textColor    = blueColor
        emailText.textColor       = blueColor
        passwordText.textColor    = blueColor
        confirmPassText.textColor = blueColor
        
        // change register button
        registerBtn.layer.borderColor = blueColor.cgColor
        registerBtn.layer.borderWidth = CGFloat(customButtonBorderWidth)
        registerBtn.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        registerBtn.clipsToBounds = true
        
        termsOfUseButton.setImage(UIImage(named: "ch_untick.png"), for: UIControlState())
        termsOfUseButton.setImage(UIImage(named: "ch_tick.png"), for: UIControlState.selected)
        
        self.termsOfUseTextView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        // Make web links clickable
        termsOfUseTextView.isSelectable = true
        termsOfUseTextView.isEditable = false
        termsOfUseTextView.dataDetectorTypes = UIDataDetectorTypes.link
        termsOfUseTextView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action:#selector(self.callTermsHyperlink(_:)))
        termsOfUseTextView.addGestureRecognizer(tap)

        
        let button: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        button.setImage(UIImage(named: "Cashkans_Back arrow"), for: UIControlState())
        //add function for button
        button.addTarget(self, action: #selector(RegsiterViewController.back(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton;
        
        
        // Change the navigation bar background color to blue.
        navigationController!.navigationBar.barTintColor = blueColor
        
        // Change the color of the navigation bar button items to white.
        navigationController!.navigationBar.tintColor = UIColor.white
        
        registerBtn.setTitleColor(blueColor, for: UIControlState())
       
        self.profileImageView.layer.cornerRadius = profileImageView.frame.width / 2
        self.profileImageView.clipsToBounds      = true
        self.profileImageView.isUserInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegsiterViewController.imageTapped(_:)))
        self.profileImageView.addGestureRecognizer(tapRecognizer)

        
        self.passLenLabel.text = password_length_str
        self.passLenLabel.textColor = textGray_color
        
        self.view.addSubview(scrollView)
        self.scrollView.addSubview(self.InnerView)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        var tap2 : UITapGestureRecognizer = UITapGestureRecognizer(target: self,action: #selector(RegsiterViewController.dismissKeyboard))
        self.scrollView.addGestureRecognizer(tap2)
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        let tv : UITextView = object as! UITextView
        var topCorrect : CGFloat = (tv.bounds.size.height - (tv.contentSize.height * tv.zoomScale)) / 2.0
        topCorrect = topCorrect < 0.0 ? 0.0 : topCorrect
        tv.contentOffset = CGPoint(x: 0.0, y : topCorrect)
    }
    
    func callTermsHyperlink(_ sender:UITapGestureRecognizer){
        if let url = URL(string: Terms_of_Use) {
            if #available(iOS 10.0, *) {
                // for ios 10.0
                UIApplication.shared.open(url, options: [:])
            } else {
                // for ios 9.0 and below
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        HELPER.setFontFamily( self.view, andSubViews: true)
        RequestResponceHelper.delegate = self
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        let blueColor = UIColor(red: 64/255, green: 89/255, blue: 128/255, alpha: 1)
        
        self.firstNameView.backgroundColor  = lightGray_color
        self.lastNameView.backgroundColor   = lightGray_color
        self.emailView.backgroundColor      = lightGray_color
        self.passwordView.backgroundColor   = lightGray_color
        self.confirmView.backgroundColor    = lightGray_color
        registerBtn.setTitleColor(blueColor, for: UIControlState())
        registerBtn.titleLabel?.font = UIFont(name: customAppFont, size: buttonFontSize )
        self.profileImageView.layoutIfNeeded()
        let attributedString = NSMutableAttributedString(string: termsOfUse)
        attributedString.addAttribute(NSLinkAttributeName, value: termsOfUse, range: NSRange(location: 0, length: attributedString.length))
        
        self.termsOfUseTextView.attributedText = attributedString
        if (self.imageProf != nil){
            let resizedImage : UIImage = HELPER.resizeImage(self.imageProf,size: CGSize(width: 1080, height: 1920))
            self.profileImageView.image = resizedImage
            self.profileImageView.autoresizingMask = [.flexibleBottomMargin, .flexibleHeight, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin, .flexibleWidth]
            
            self.profileImageView.contentMode = UIViewContentMode.scaleAspectFit
        }
        
    }
    

    
    //#MARK: Location manager methods
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        // alert when location is disabled
        if(!CLLocationManager.locationServicesEnabled()){
            
            let alertController = UIAlertController(title: alert_cashkan, message: alert_gpsOff, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                // goto settings page
                UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
            }
            let cancelAction = UIAlertAction(title: alert_cancel, style: .default) { (action:UIAlertAction!) in
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }

    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            
            if (error != nil) {
                return
            }
            
            if placemarks!.count > 0 {
                
                self.latitude =  String(format:"%f", (self.locationManager.location?.coordinate.latitude)!)
                
                self.longitude = String(format:"%f", (self.locationManager.location?.coordinate.longitude)!)
                
            } else {
                //print("Problem with the data received from geocoder")
            }
        })
    }

    
     override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
    }
    
    func back(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
        let transition = CATransition()
        transition.duration = 0.2
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        view.window!.layer.add(transition, forKey: kCATransition);
        self.present(vc, animated: false, completion: nil)
    }
    
    
    @IBAction func termsOfUseButtonClick(_ sender: AnyObject) {
        
        if (termsOfUseButton.isSelected)
        {
            termsOfUseButton.isSelected = false
            termsOfUseSelected = false
        }
        else
        {
            termsOfUseButton.isSelected = true
            termsOfUseSelected = true
        }

    }
    
    //#MARK: image picker methods
    
    func imageTapped(_ sender: UITapGestureRecognizer) {
        self.openGallary()
        Imagepicker?.delegate = self
        
    }

    
    func openGallary()
    {
        Imagepicker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(Imagepicker!, animated: true, completion: nil)
        }
        else
        {
            popover=UIPopoverController(contentViewController: Imagepicker!)
            popover!.present(from: profileImageView.frame, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            picker.dismiss(animated: true, completion: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "cropper-vc") as! CropperViewController
            vc._image = (info[UIImagePickerControllerOriginalImage] as? UIImage)
            vc.isfromRegister = true
            vc.picker = picker
            vc.register = self
            self.present(vc, animated:true, completion: nil)
        } else{
            //print("Something went wrong")
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    @IBAction func onRegisterBtnClick(_ sender: AnyObject) {
        self.resign()
        self.registerBtn.isUserInteractionEnabled = false
        if(Reachability.isConnectedToNetwork() == false){
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                self.registerBtn.isUserInteractionEnabled = true
                self.navigationController!.pushViewController(secondViewController, animated: false)
                
            }
            
        }
        else{
            
        if (self.latitude == "" || self.longitude == ""){
            
            let alertController = UIAlertController(title: alert_cashkan, message: alert_gpsOff, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
            }
            let cancelAction = UIAlertAction(title: alert_cancel, style: .default) { (action:UIAlertAction!) in
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
            self.registerBtn.isUserInteractionEnabled = true
        }
        else if (self.firstName.text!.isEmpty){
            let alertController = UIAlertController(title: alert_cashkan, message: alert_firstName, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
            self.registerBtn.isUserInteractionEnabled = true
        }
        else  if (self.lastName.text!.isEmpty){
            let alertController = UIAlertController(title: alert_cashkan, message: alert_lastName, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
            self.registerBtn.isUserInteractionEnabled = true
        }
        else  if (self.email.text!.isEmpty){
            let alertController = UIAlertController(title: alert_cashkan, message: alert_enterEmail, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
            self.registerBtn.isUserInteractionEnabled = true
        }
        else if(!isValidEmail(self.email.text!)){
            let alertController = UIAlertController(title: alert_cashkan, message: alert_invalidEmail, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
            self.registerBtn.isUserInteractionEnabled = true
        }
        else if (self.password.text!.isEmpty){
            let alertController = UIAlertController(title: alert_cashkan, message: alert_password, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
            self.registerBtn.isUserInteractionEnabled = true
        }
        else if (!(self.password.text!.isEmpty) && ((self.password.text?.characters.count < 6) || ((self.password.text?.characters.count)! > 8)) ){
            let alertController = UIAlertController(title: alert_cashkan, message: alert_pass_len_validation, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
            self.registerBtn.isUserInteractionEnabled = true
        }
        else if (self.confirmPass.text!.isEmpty){
            let alertController = UIAlertController(title: alert_cashkan, message: alert_reEntarPassword, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
            self.registerBtn.isUserInteractionEnabled = true
        }
        else if(self.password.text != self.confirmPass.text)
        {
            let alertController = UIAlertController(title: alert_cashkan, message: alert_match_password, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
            self.registerBtn.isUserInteractionEnabled = true
        }
        else if !termsOfUseSelected{
            let alertController = UIAlertController(title: alert_termsOfUseTitle, message: alert_termsofUse, preferredStyle: .alert)
            
            let agreeAction = UIAlertAction(title: alert_agree, style: .cancel) { (action:UIAlertAction!) in
                    self.termsOfUseButton.isSelected = true
                    self.termsOfUseSelected = true
                    self.onRegisterBtnClick(self.registerBtn)
                    self.registerBtn.isUserInteractionEnabled = false

            }
            let cancelAction = UIAlertAction(title: alert_cancel, style: .default) { (action:UIAlertAction!) in
                self.registerBtn.isUserInteractionEnabled = true
            }

            alertController.addAction(agreeAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }
        else{
        //send register request
        
        let fName = firstName.text! as String
        let lName = lastName.text! as String
        let emailValue = email.text! as String
        let pass = password.text! as String
            
        // Password Encryption
            do {
                let aes = try AES(key: key, iv: iv) // aes128
                let ciphertext = try aes.encrypt(pass.utf8.map({$0}))
                EncrypetdPassword = ciphertext.toHexString()
                
            } catch {
                //print("Password Encryption failed")
            }
            
        
        let userRoleId = "1"
            
        var deviceId = ""
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            deviceId = refreshedToken
        }

        
        if(profileImageView.image == nil)
        {
            // send registration request without image
            let params = [
                "fname"        : fName,
                "lname"        : lName,
                "email"        : emailValue,
                "password"     : EncrypetdPassword,
                "user_role_id" : userRoleId,
                "lat"          : self.latitude,
                "lng"          : self.longitude,
                "imageName"    : "",
                "imageArray"   : ""
                ] as Dictionary

            self.activityIndicator.showActivityIndicatory(self.view)
            self.navigationController?.navigationBar.isUserInteractionEnabled = false
            sendRegisterRequest(params)

        }
        else
        {
            // send registration request with image

            // resize profile image
            let newSize = CGSize(width: 300,height: 300)
            let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
            
            UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
            self.profileImageView.image?.draw(in: rect)
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // rename profile image
            imageFileName = "CROP_"
            imageFileName += String(Date().timeIntervalSince1970)
            imageFileName += ".png"
            let imageName = imageFileName as String
            let imageByteArray : NSData = UIImagePNGRepresentation(newImage!) as! NSData
            
            let base64Encoded = imageByteArray.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            
            let paramsWithImage = [
                "fname"        : fName,
                "lname"        : lName,
                "email"        : emailValue,
                "password"     : EncrypetdPassword,
                "user_role_id" : userRoleId,
                "lat"          : self.latitude,
                "lng"          : self.longitude,
                "imageName"    : imageName,
                "imageArray"   : base64Encoded
                ] as Dictionary
            self.activityIndicator.showActivityIndicatory(self.view)
            self.navigationController?.navigationBar.isUserInteractionEnabled = false
            sendRegisterRequest(paramsWithImage) //uncomment this
        }
    }
       
    }
}
    
    func sendRegisterRequest( _ dictParams : Dictionary<String, String>){
    
        if Reachability.isConnectedToNetwork() == true {

        self.NewRegister(dictParams)
        {
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            
            if( success == true){
                
                if let response = object as? HTTPURLResponse{
                    
                    // You can print out response object
                   // print("response = \(response)")
                }
                
                do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        if let successMsg = myJSON![display_msg]{
                            let alertController = UIAlertController(title: alert_cashkan, message: successMsg as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
                                let transition = CATransition()
                                transition.duration = 0.2
                                transition.type = kCATransitionPush
                                transition.subtype = kCATransitionFromLeft
                                self.view.window!.layer.add(transition, forKey: kCATransition);
                                self.present(vc, animated: false, completion: nil)

                            }
                            self.activityIndicator.hideActivityIndicator(self.view)
                            self.navigationController?.navigationBar.isUserInteractionEnabled = true
                            self.registerBtn.isUserInteractionEnabled = true
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    
                    }
                    catch {
                        //print(error)
                    }
            }
                
            else{
                
                // If registration failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            let error = myJSON![display_msg]
                            let alertController = UIAlertController(title: register_failed, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                                
                            }
                            self.activityIndicator.hideActivityIndicator(self.view)
                            self.navigationController?.navigationBar.isUserInteractionEnabled = true
                            self.registerBtn.isUserInteractionEnabled = true
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                        else{
                            
                            self.activityIndicator.hideActivityIndicator(self.view)
                            self.navigationController?.navigationBar.isUserInteractionEnabled = true
                            self.registerBtn.isUserInteractionEnabled = true
                            let alertController = UIAlertController(title: register_failed, message: "Server error occured", preferredStyle: .alert)
                            
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        //print(error)
                    }
                    
                }
                else{
                    let alertController = UIAlertController(title: register_failed, message: error as? String, preferredStyle: .alert)
                    
                    let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                        //print("you have pressed the Cancel button");
                    }
                    self.activityIndicator.hideActivityIndicator(self.view)
                    self.navigationController?.navigationBar.isUserInteractionEnabled = true
                    self.registerBtn.isUserInteractionEnabled = true
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion:nil)
                }
            }
        }
        }

    }
    
    
    func generateBoundaryString() -> String{
        return "Boundary-\(UUID().uuidString)"
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = validEmailPattern
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //#MARK: text field delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let textFieldRect : CGRect = self.view.window!.convert(textField.bounds, from: textField)
        let viewRect : CGRect = self.view.window!.convert(self.view.bounds, from: self.view)
        
        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
        let numerator : CGFloat = midline - viewRect.origin.y - MoveKeyboard.MINIMUM_SCROLL_FRACTION * viewRect.size.height
        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
        
        var heightFraction : CGFloat = numerator / denominator
        
        if heightFraction  > 1.0 {
            heightFraction = 1.0
        }
        
        let orientation : UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
        if (orientation == UIInterfaceOrientation.portrait || orientation == UIInterfaceOrientation.portraitUpsideDown) {
            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
        } else {
            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
        }
        
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y -= animateDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(TimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        
        self.view.frame = viewFrame
        
        UIView.commitAnimations()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y += animateDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        
        UIView.setAnimationDuration(TimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        
        self.view.frame = viewFrame
        
        UIView.commitAnimations()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        if textField.returnKeyType != UIReturnKeyType.done{
            let nextTag: NSInteger = textField.tag+1;
            let nextResponder: UIResponder = (textField.superview?.viewWithTag(nextTag))!
            
            if (nextResponder as UIResponder?) != nil{
                nextResponder.becomeFirstResponder()
            }
            
        }
        else{
            textField.resignFirstResponder()
        }
        
        return false;
    }
    
    func resign() {
        
        firstName.resignFirstResponder()
        lastName.resignFirstResponder()
        email.resignFirstResponder()
        password.resignFirstResponder()
        confirmPass.resignFirstResponder()
    }

    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {

        return true
    }
    
    
    
    func phoneNumberValidation(_ value: String) -> Bool {
        let charcter  = CharacterSet(charactersIn: "0123456789").inverted
        var filtered: String!
        let inputString: NSArray = value.components(separatedBy: charcter)  as NSArray
        filtered = (inputString.componentsJoined(by: "") as NSString!) as String!
        return  value == filtered
    }

    
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                                                 replacementString string: String) -> Bool
    {
        
        var result = true
        var startString = ""
        
        if(textField == firstName){
            
        if string.characters.count > 0 {
                    let disallowedCharacterSet = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.").inverted
                    let replacementStringIsLegal = string.rangeOfCharacter(from: disallowedCharacterSet) == nil
                    result = replacementStringIsLegal
                }
            }
        if(textField == lastName){
            if string.characters.count > 0 {
                let disallowedCharacterSet = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.").inverted
                let replacementStringIsLegal = string.rangeOfCharacter(from: disallowedCharacterSet) == nil
                result = replacementStringIsLegal
            }
        }
        
        if(textField == password)
        {
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
        }

        if(textField == confirmPass)
        {
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
        }
        
        return result
    }
    
    
 func NewRegister(_ dictParams: Dictionary<String, String>, completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
    
       
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest("/users", params: dictParams as Dictionary<String, AnyObject>? )
        
        RequestResponceHelper.post(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? NSData{
                        completion(success, object ,dataObject as Data ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }
    
    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
            
            self.navigationController!.pushViewController(secondViewController, animated: false)
        }
        
    }

 
}
