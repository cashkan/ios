//
//  ScannedCashPopUpViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 21/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class CustomDuplicateSessionTableCell: UITableViewCell  {
    

    @IBOutlet var currencyNo: UILabel!
    @IBOutlet var amount: UILabel!

}

class ScannedCashPopUpViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {

    var cash :  ScannedCashViewController!
    var originalScannedAmount : NSNumber = 0.0
    var actualAmt: NSNumber = 0.0
    var rejectedDenominationValue : [(String,String)] = []
    var totalScannedCount : String = "0"
    
    //@IBOutlet var successfulDialogue: UILabel!
    @IBOutlet var crossSign: UIButton!
    @IBOutlet var dialogueView: UIView!
    
    
    @IBOutlet var duplicateSessionTable : UITableView!
    @IBOutlet var duplicateMessageLabel : UILabel!
    @IBOutlet var originalAmountLabel   : UILabel!
    @IBOutlet var actualAmountLabel     : UILabel!
    @IBOutlet var originalValue         : UILabel!
    @IBOutlet var actualValue           : UILabel!
    @IBOutlet var continueButton        : UIButton!
    
    @IBOutlet var duplicateCurrenciesView   : UIView!
    @IBOutlet var successDialogueView       : UIView!
    @IBOutlet var successMsg1               : UILabel!
    @IBOutlet var successMsg2               : UILabel!
    @IBOutlet var successMsg3               : UILabel!
    @IBOutlet var dontShowPopoupButton      : UIButton!
    
    @IBOutlet var dontShowPopupLabel        : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HELPER.setFontFamily( self.view, andSubViews: true)

        self.duplicateSessionTable.dataSource = self
        self.duplicateSessionTable.delegate = self
        self.duplicateSessionTable.rowHeight = 25
        
        self.successMsg1.text = sessionSuccessMsg1
        self.successMsg2.text = sessionSuccessMsg2
        self.successMsg3.text = sessionSuccessMsg3
        self.successMsg1.textColor = UIColor.black
        self.successMsg2.textColor = UIColor.black
        self.successMsg3.textColor = UIColor.black
        self.dontShowPopupLabel.text = doNotshow
        self.dontShowPopupLabel.textColor = UIColor.black
        self.dontShowPopoupButton.setImage(UIImage(named: "ch_untick.png"), for: .normal)
        self.dontShowPopoupButton.setImage(UIImage(named: "ch_tick.png"), for: UIControlState.selected)


        self.view.backgroundColor = UIColor.clear

        if( rejectedDenominationValue.count > 0){
            duplicateMessageLabel.isHidden  = false
            originalAmountLabel.isHidden    = false
            actualAmountLabel.isHidden      = false
            originalValue.isHidden          = false
            actualValue.isHidden            = false
            continueButton.isHidden         = false
            duplicateSessionTable.isHidden  = false
            
            if rejectedDenominationValue.count <= 7{
                self.duplicateSessionTable.isScrollEnabled = false

            }
            else{
                self.duplicateSessionTable.isScrollEnabled = true

            }
            
            successDialogueView.isHidden = true
            crossSign.isHidden = true
            
            self.setValues()
            
        }
        else{
            duplicateMessageLabel.isHidden = true
            originalAmountLabel.isHidden = true
            actualAmountLabel.isHidden = true
            originalValue.isHidden = true
            actualValue.isHidden = true
            continueButton.isHidden = true
            duplicateSessionTable.isHidden = true

            
            successDialogueView.isHidden = false
            crossSign.isHidden = false

        }
    }

    @IBAction func closeButtonClicked(_ sender: AnyObject) {
        
//        cash.view.alpha = 1
        cash.navigationController?.navigationBar.alpha = 1
        cash.tabBarController?.tabBar.alpha = 1
        cash.tabBarController?.tabBar.isUserInteractionEnabled = true
    
        cash.callReceiptController()
        
        self.dismiss(animated: false, completion:nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        HELPER.setFontFamily( self.view, andSubViews: true)

    }
    
    @IBAction func onDontShowButtonClick(_ sender: AnyObject) {
        
        
        if (dontShowPopoupButton.isSelected){
            dontShowPopoupButton.isSelected = false
            HELPER.showSuccessDialogue = true
        }
        else{
            dontShowPopoupButton.isSelected = true
            HELPER.showSuccessDialogue = false
        }
        
        
        let deadlineTime = DispatchTime.now() + .seconds(1)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            self.closeButtonClicked(sender)
        }
        

    }
    
    func setValues(){
    
        originalValue.text = HELPER.currencySym+" "+String(describing: originalScannedAmount)
        actualValue.text = HELPER.currencySym+" "+String(describing: actualAmt)
        
        var toReplace : String = "some"
        var finalString = duplicate_sessionDialogue.replacingOccurrences(of: toReplace, with: String(rejectedDenominationValue.count))
        toReplace = "total"
        finalString = finalString.replacingOccurrences(of: toReplace, with: totalScannedCount)
        
        duplicateMessageLabel.text = finalString
        self.duplicateSessionTable.reloadData()

    
    }

    @IBAction func continueBtnClicked(_ sender: AnyObject) {
        
        //cash.view.alpha = 1
        cash.navigationController?.navigationBar.alpha = 1
        cash.tabBarController?.tabBar.alpha = 1
        cash.tabBarController?.tabBar.isUserInteractionEnabled = true
        
        cash.callReceiptController()
        
        self.dismiss(animated: false, completion:nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return rejectedDenominationValue.count;
    }
    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        var hgt:CGFloat = 25
//        var rArray    : [(String,NSNumber)] = []
//        rArray = rejectedDenominationValue
//        let cnt:Int = rArray.count
//        if cnt > 0 {
//            hgt = hgt + CGFloat(cnt+2)*hgt
//        }else{
//            hgt = 55
//        }
//        
//        return hgt
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = String((indexPath as NSIndexPath).row)
        
        
        let cell:CustomDuplicateSessionTableCell = self.duplicateSessionTable.dequeueReusableCell(withIdentifier: "DuplicateValueCell") as! CustomDuplicateSessionTableCell
        
        let pos : Int  = (indexPath as NSIndexPath).row
        let (currency,denomination) = rejectedDenominationValue[pos]
        
        cell.currencyNo.text = currency
        cell.amount.text = "("+HELPER.currencySym+" "+denomination+")"
        
        HELPER.setFontFamily( cell.contentView, andSubViews: true)

        
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
