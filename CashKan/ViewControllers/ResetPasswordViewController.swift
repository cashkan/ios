//
//  ResetPasswordViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 13/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit
import CryptoSwift
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class ResetPasswordViewController: UIViewController,UITextFieldDelegate, NetworkFailureDelegate {

    @IBOutlet var confirmPasswordLabel: UILabel!
    @IBOutlet var newPasswordLabel: UILabel!
    
    @IBOutlet var newPasswordTextField: UITextField!
    @IBOutlet var confirmPasswordTextField: UITextField!
    
    @IBOutlet var resetPasswordButton: UIButton!
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()
    
    var EncrypetdPassword: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        HELPER.setFontFamily( self.view, andSubViews: true)

        self.navigationItem.title = resetPassword_title
        
        let btnColor  = UIColor(red: 65/255, green: 89/255, blue: 130/255, alpha: 1) //#415982
        let blueColor = UIColor(red: 73/255, green: 95/255, blue: 135/255, alpha: 1) //#495f87
        
        newPasswordLabel.textColor = blueColor
        confirmPasswordLabel.textColor = blueColor

        resetPasswordButton.layer.borderColor = btnColor.cgColor
        resetPasswordButton.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        resetPasswordButton.layer.borderWidth = CGFloat(customButtonBorderWidth)
        resetPasswordButton.setTitleColor( btnColor, for: UIControlState())
        
        newPasswordTextField.delegate = self;
        confirmPasswordTextField.delegate = self;
        
        self.navigationItem.hidesBackButton = true
        
        let button: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        button.setImage(UIImage(named: "Cashkans_Back arrow"), for: UIControlState())
        //add function for button
        button.addTarget(self, action: #selector(ResetPasswordViewController.back(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton;



        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = resetPassword_title
        HELPER.setFontFamily( self.view, andSubViews: true)
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        RequestResponceHelper.delegate = self
    }
    func back(_ sender: UIBarButtonItem) {

        // Go back to the previous ViewController
        //self.navigationController?.popToRootViewControllerAnimated(true)
        dismiss(animated: false, completion: nil)
        
        
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resetPasswordButtonClicked(_ sender: AnyObject) {
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //print("TextField should return method called")
        newPasswordTextField.resignFirstResponder();
        confirmPasswordTextField.resignFirstResponder();
        
        return true;
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
    }

    
     @IBAction func resetPasswordClicked(_ sender: UIButton) {
        
        if(Reachability.isConnectedToNetwork() == false){
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                
                self.navigationController!.pushViewController(secondViewController, animated: false)
                
            }
            
        }
        else{
        
        if (self.newPasswordTextField.text!.isEmpty){
            let alertController = UIAlertController(title: alert_cashkan, message: alert_password, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                //print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }
        else  if (self.confirmPasswordTextField.text!.isEmpty){
            let alertController = UIAlertController(title: alert_cashkan, message: alert_reEntarPassword, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                //print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }
        else if(self.newPasswordTextField.text != self.confirmPasswordTextField.text){
            let alertController = UIAlertController(title: alert_cashkan, message: alert_match_password, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                //print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
            
            
        }
        else if ((self.newPasswordTextField.text?.characters.count < 6) || (self.confirmPasswordTextField.text?.characters.count < 6)){
            let alertController = UIAlertController(title: alert_cashkan, message: alert_passwordValidation, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                //print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }

            
        else{
        
            // send reset password request
            self.activityIndicator.showActivityIndicatory(self.view)
            sendResetPasswordRequest()
        }
        }
     }
    
    
    func sendResetPasswordRequest(){
        var emailTxt = HELPER.email
        var passwordTxt = newPasswordTextField!.text! as String
        let user_id = HELPER.user_id.stringValue
        
        // Password Encryption
        do {
            let aes = try AES(key: key, iv: iv) // aes128
            let ciphertext = try aes.encrypt(passwordTxt.utf8.map({$0}))
            EncrypetdPassword = ciphertext.toHexString()
            
//            print(ciphertext.toHexString())
//            print(EncrypetdPassword)
            
        } catch {
            //print(error)
        }
        
        let params = ["userId" : user_id, "password" :  EncrypetdPassword]
        
        self.ResetPassword(params)
        {
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            //print("got back: \(message)")
            
            if( success == true){
                
                if let response = object as? HTTPURLResponse{
                    
                    // You can print out response object
                    //print("response = \(response)")
                    
                }
                
                
                do {
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary

                    
                    if let successMsg = myJSON![display_msg]{
                        //print(successMsg)
                        self.activityIndicator.hideActivityIndicator(self.view)
                        let alertController = UIAlertController(title: alert_cashkan, message: successMsg as? String, preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                            self.dismiss(animated: false, completion: nil)
                            
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
         
                }
                catch {
                    //print(error)
                }
               
            }
                
            else{
                
                // If registration failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            let error = myJSON![display_msg]
                            self.activityIndicator.hideActivityIndicator(self.view)

                            let alertController = UIAlertController(title: alert_cashkan, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        //print(error)
                    }
                    
                }
            }
            
            
        }
    }

    
    
    
    func ResetPassword(_ dictParams: Dictionary<String, String>, completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
        
        
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest(resetPasssword_url, params: dictParams as Dictionary<String, AnyObject>? )
        
        RequestResponceHelper.post(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? NSData{
                        //message = passedMessage
                        completion(success, object ,dataObject as Data ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }

    
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                                                 replacementString string: String) -> Bool
    {
        
        let result = true
        
        
        if(textField == newPasswordTextField)
        {
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
        }
        
        if(textField == confirmPasswordTextField)
        {
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
        }
        
        
        
        
        return result
    }

    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
            
            self.navigationController!.pushViewController(secondViewController, animated: false)
        }
        
    }

}
