//
//  CashViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 17/06/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit
import AVFoundation
import SwiftyJSON
extension UILabel {
    func boundingRectForCharacterRange(range: NSRange) -> CGRect? {
        
        guard let attributedText = attributedText else { return nil }
        
        let textStorage = NSTextStorage(attributedString: attributedText)
        let layoutManager = NSLayoutManager()
        
        textStorage.addLayoutManager(layoutManager)
        
        let textContainer = NSTextContainer(size: bounds.size)
        textContainer.lineFragmentPadding = 0.0
        
        layoutManager.addTextContainer(textContainer)
        
        var glyphRange = NSRange()
        
        // Convert the range for glyphs.
        layoutManager.characterRange(forGlyphRange: range, actualGlyphRange: &glyphRange)
        
        return layoutManager.boundingRect(forGlyphRange: glyphRange, in: textContainer)
    }
}

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


enum Status: Int {
    case preview, still, error
}


class CashViewController: UIViewController,UINavigationControllerDelegate ,UITextFieldDelegate,XMCCameraDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate, SlideMenuDelegate, UIWebViewDelegate{
    
    var API_KEY = "AIzaSyD_FdG0z0PI2xxP4u2MxagxsPSBisFZvm4"

    @IBOutlet weak var cameraPreview        : UIView!
    @IBOutlet weak var circleView           : UIView!
    @IBOutlet weak var shadeContainer       : UIView!
    @IBOutlet weak var firstViewContainer   : UIView!
    @IBOutlet weak var secondViewContainer  : UIView!
    @IBOutlet weak var firstView            : UIView!
    @IBOutlet var cameraStrip               : UIView!
    @IBOutlet weak var currencyImageView    : UIImageView!
    @IBOutlet weak var currencyField        : UITextField!
    @IBOutlet weak var tickBtnView          : UIButton!
    @IBOutlet weak var retakeButton         : UIButton!
    @IBOutlet weak var doneButtonView       : UIView!
    @IBOutlet var cancelButton              : UIButton!
    @IBOutlet weak var detailbutton         : UIButton!
    @IBOutlet weak var totalValue           : UILabel!
    @IBOutlet weak var totallabel           : UILabel!
    @IBOutlet var switchview                : UIView!
    @IBOutlet var iAmDoneLabel  : UILabel!
    @IBOutlet weak var slideLable1: UILabel!
    @IBOutlet weak var slideLabel2: UILabel!
    @IBOutlet var tipTitleLbl: UILabel!
    @IBOutlet var tipDescLbl: UILabel!
    @IBOutlet var tipNextBtn: UILabel!
    
    @IBOutlet weak var TipViewHwight: NSLayoutConstraint!
    var preview                 : AVCaptureVideoPreviewLayer?
    var camera                  : XMCCamera?
    var status                  : Status = .preview
    var mySwitch                : SevenSwitch!
    var bgTapRecognizer         :UITapGestureRecognizer!
    var drawerBtn               :UIButton!
    var isDrawerOpen            : DarwinBoolean = false
    var webView                 : UIWebView!
    var cashkanBills            : [NSString]! = []
    var denominatios            : [NSString]! = []
    var billArray               : [(NSString,NSString)]! = []
    var finalCurrencyArray      : [(NSNumber,NSNumber,NSString,NSString)]! = []
    var billCount               : [NSNumber]! = []
    var selectedDenomination    : String = "00"
    var selectedDenominationTag : Int = 0
    var billArrayIndex          : Int = 0
    var totalScannedValue       : Int = 0
    var tickBtnClicked          : DarwinBoolean = false
    var aDelegate               : UITabBarController!
    let imagePicker             : UIImagePickerController!       = UIImagePickerController()
    var activityIndicator       : CustomActivityIndicator = CustomActivityIndicator()
    var menuVC                  : MenuViewController!
    static var tipCounter   = 0
    var isDone              = false
    var animateDistance     = CGFloat()
    
    //MARK: custom method
    
    @IBAction func cancelButtonClicked(_ sender: AnyObject) {
        
        self.currencyField.text = ""
        
        self.cancelButton.isHidden = true
        //open camera
        
        if self.status == .preview {
            UIView.animate(withDuration: 0.225, animations: { () -> Void in
                self.cameraPreview.alpha = 0.0;
            })
            
            self.camera?.captureStillImage({ (image) -> Void in
                if image != nil {
                    self.currencyImageView.image = image;
                    
                    let binaryImageData = self.base64EncodeImage(image!)
                    self.view.isUserInteractionEnabled = false
                    self.tabBarController?.tabBar.isUserInteractionEnabled = false
                    self.navigationController?.navigationBar.isUserInteractionEnabled = false

                    self.activityIndicator.showActivityIndicatory(self.view)
                    self.createRequest(binaryImageData)
                    
                    UIView.animate(withDuration: 0.225, animations: { () -> Void in
                    })
                    self.status = .still
                } else {
                    self.status = .error
                }
                
            })
        } else if self.status == .still || self.status == .error {
            UIView.animate(withDuration: 0.225, animations: { () -> Void in
                self.cameraPreview.alpha = 1.0;
                }, completion: { (done) -> Void in
                    self.currencyImageView.image = nil;
                    self.status = .preview
            })
        }

    }

    func resetCameraScreen(){
        
        self.firstViewContainer.isHidden = true
        self.secondViewContainer.isHidden = false
        self.secondViewContainer.backgroundColor = UIColor.white
        self.doneButtonView.backgroundColor = UIColor.white
        self.mySwitch.setOn(false, animated: false)
    }
    
    //MARK: Initializing method 
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        //HELPER.setFontFamily( self.view, andSubViews: true)
        
        self.slideLable1.adjustsFontSizeToFitWidth = true
        if UIScreen.main.bounds.size.height < 568 {
            // less than IPhone 5
            self.slideLable1.font = self.slideLable1.font.withSize(17)
            self.tipTitleLbl.font = self.tipTitleLbl.font.withSize(14)
            self.tipDescLbl.font = self.tipDescLbl.font.withSize(13)
            self.TipViewHwight.constant = 110
        } else
        if UIScreen.main.bounds.size.height == 568 {
            // IPhone 5s
            self.slideLable1.font = self.slideLable1.font.withSize(21)
            self.tipTitleLbl.font = self.tipTitleLbl.font.withSize(15)
            self.tipDescLbl.font = self.tipDescLbl.font.withSize(15)
            self.TipViewHwight.constant = 150

        } else if UIScreen.main.bounds.size.width == 375 {
            // iPhone 6
            self.slideLable1.font = self.slideLable1.font.withSize(23)
            self.tipTitleLbl.font = self.tipTitleLbl.font.withSize(16)
            self.tipDescLbl.font = self.tipDescLbl.font.withSize(16)
            self.TipViewHwight.constant = 150

        }
        else{
            // greater than iphone 6
            self.slideLable1.font = self.slideLable1.font.withSize(23)
            self.tipTitleLbl.font = self.tipTitleLbl.font.withSize(16)
            self.tipDescLbl.font = self.tipDescLbl.font.withSize(16)
            self.TipViewHwight.constant = 150
        }
        
        let button1: UIButton = UIButton(type: UIButtonType.custom)
        button1.setImage(UIImage(named: "Help_icon"), for: UIControlState())
        button1.addTarget(self, action: #selector(self.helpTap), for: UIControlEvents.touchUpInside)
        button1.frame = CGRect(x: 0, y: 200, width: 16, height: 25)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.rightBarButtonItem = barButton1;
        
        self.tabBarController?.tabBar.barTintColor =  UIColor.white
        self.tabBarController?.tabBar.layer.borderColor = lightGray_color.cgColor
        self.tabBarController?.tabBar.layer.borderWidth = 0.5
        self.navigationController?.navigationBar.barTintColor = cashGreen_color
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        currencyField.delegate = self;

         //creat currency circles object & feed values from there for badge,cnt & denomination
        let wView = self.view.frame.size.width
        let cnt = HELPER.localeInfo.count
        var fSize = (Int(wView)/25)
        var spaceInBtn = Int((wView*3) / 100)
        if(cnt>4){
            spaceInBtn = Int((wView*2) / 100)
            fSize = (Int(wView)/34)
        }
        let availableWidth = CGFloat(wView)-(CGFloat((cnt)*spaceInBtn) + (self.detailbutton.frame.size.width+24))
        let placer = CGFloat(availableWidth)/CGFloat(cnt+1)
        let wValue = placer
        let hValue = placer
        var xValue = Int(placer/2)
        let yValue = Int((self.circleView.frame.height / 2 ) - (wValue/2))
    
        self.firstViewContainer.isHidden = false
        self.secondViewContainer.backgroundColor = UIColor.white
        self.doneButtonView.backgroundColor = backgroundGray_color
        let localeDenominationCount = HELPER.localeInfo.count
        
        let spacer = wValue+12
        for i in 0 ..< localeDenominationCount  {
            
            let denominationStr = HELPER.currencySym + "\n" + (((HELPER.localeInfo[i] as? NSDictionary )!["denomination"] as? NSNumber)!).stringValue
            
            let btn1 = custombutton(CGRect(x: CGFloat(xValue), y: CGFloat(yValue), width: CGFloat(wValue), height: CGFloat(hValue)), buttontag: i+1, badge: 0, denomination: denominationStr, fontsize: CGFloat(fSize))
            
            self.circleView.addSubview(btn1)
            self.billCount.insert(0, at: i)
            
            
            xValue = xValue+Int(spacer)
        }
                
        
        self.firstViewContainer.backgroundColor = backgroundGray_color
        self.firstView.backgroundColor = backgroundGray_color
        
        // switch start
        mySwitch = SevenSwitch(frame: CGRect(origin: self.switchview.frame.origin, size: self.switchview
            .frame.size))
        mySwitch.addTarget(self, action: #selector(CashViewController.captureButtonClicked as (CashViewController) -> () -> ()), for: UIControlEvents.valueChanged)

        mySwitch.offLabel.text = ""
        mySwitch.onLabel.text = ""
        mySwitch.activeColor =  cashGreen_color
        mySwitch.inactiveColor = cashGreen_color
        mySwitch.onTintColor =  cashGreen_color
        mySwitch.borderColor = UIColor.clear
        let newImage : UIImage = self.imageWithImage(UIImage(named: "ScanCash_icon03_Green arrow")!, newSize: self.mySwitch.thumbView.frame.size)
        mySwitch.thumbImage = newImage

        self.switchview.addSubview(mySwitch)
        self.firstView.addSubview(mySwitch)
        mySwitch.isHidden = true
        // switch end
        
        //Drawer button start
        drawerBtn = UIButton(type: UIButtonType.custom)
        drawerBtn.setImage(UIImage(named: "iosdrawericon"), for: UIControlState())
        drawerBtn.addTarget(self, action: #selector(self.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        drawerBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 25)
        drawerBtn.backgroundColor = UIColor.clear
        let barButton2 = UIBarButtonItem(customView: drawerBtn)
        self.navigationItem.leftBarButtonItem = barButton2;
        //drawer button end
        
        firstView.layer.borderColor = cashGreen_color.cgColor
        
        self.doneButtonView.isUserInteractionEnabled = true
        var doneButtonClick = UITapGestureRecognizer(target: self, action:#selector(self.iAmDoneButtonClicked(_:)))
        doneButtonView.addGestureRecognizer(doneButtonClick)
        
         //doneButtonClick = UITapGestureRecognizer(target: self, action:#selector(self.tipNextButtonClicked(_:)))
        
        slideLabel2.isUserInteractionEnabled = true
        let tapOnHelpTxt = UITapGestureRecognizer(target:self, action:#selector(self.TapOnHelpTxt(_:)))
        slideLabel2.addGestureRecognizer(tapOnHelpTxt)
        
        let longString = HELPER.currencySym + " " + "0"
        let longestWord = "0"
        
        let longestWordRange = (longString as NSString).range(of: longestWord)
        
        let attributedString = NSMutableAttributedString(string: longString, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 17) , NSForegroundColorAttributeName : cashGreen_color])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 25), NSForegroundColorAttributeName : cashGreen_color], range: longestWordRange)
        
        totalValue?.attributedText = attributedString
        
        self.resetDetailsImage();
        
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "popDrawerController"), object: nil)
        
        if(HELPER.isDrawerOpen == true && self.menuVC != nil){
            self.dismissViewBehindWebView()
        }
        
        self.tabBarController?.selectedIndex = 0

        HELPER.setFontFamily( self.view, andSubViews: true)
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        
        CashViewController.tipCounter = 0
        self.navigationItem.title = scanCash_title
        self.navigationController?.navigationBar.barTintColor = cashGreen_color
        self.cancelButton.isHidden = true
        self.currencyField.text = ""
        
        // Set tip view
        tipTitleLbl.text = tipTitleArray[0]
        tipTitleLbl.textColor = cashGreen_color
        tipDescLbl.text = tipDescArray[0]
        tipDescLbl.textColor = textGray_color
        tipNextBtn.text = tipNext
        tipNextBtn.textColor = cashGreen_color
        let attributedStringofTipBtn = NSMutableAttributedString(string: self.tipNextBtn.text!, attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 17)])
        self.tipNextBtn.attributedText = attributedStringofTipBtn
        self.tipNextBtn.isUserInteractionEnabled = true
        var nextBtnClick = UITapGestureRecognizer(target: self, action:#selector(self.tipNextButtonClicked(_:)))
        tipNextBtn.addGestureRecognizer(nextBtnClick)
        
        
        // Set header label text
        slideLable1.text = slide_click_1
        slideLable1.lineBreakMode = NSLineBreakMode.byWordWrapping;
        slideLable1.numberOfLines = 0
        slideLable1.textColor = textGray_color//ghhghfgfg
        slideLabel2.lineBreakMode = NSLineBreakMode.byWordWrapping;
        slideLabel2.numberOfLines = 0
        slideLabel2.textColor = textGray_color//ghhghfgfg
        
        var longestWordRange = (slide_click_2 as NSString).range(of: "?")
        
        var attributedString = NSMutableAttributedString(string: slide_click_2, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 17), NSForegroundColorAttributeName : textGray_color])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.systemFont(ofSize: 17), NSForegroundColorAttributeName : journeyBlue_color], range: longestWordRange)
        
        
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSNumber(value: 1), range: longestWordRange)
                attributedString.addAttribute(NSUnderlineColorAttributeName, value: journeyBlue_color, range: longestWordRange)
        
        let labelAttachment = NSTextAttachment()
        slideLabel2.attributedText = attributedString
        
        firstView.backgroundColor = lightGray_color
        firstView.layer.borderWidth = 1
        firstView.layer.borderColor = border_color
        firstView.layer.cornerRadius = 8
        
        doneButtonView.layer.borderWidth = CGFloat(customButtonBorderWidth)
        doneButtonView.layer.borderColor = cashGreen_color.cgColor
        doneButtonView.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        iAmDoneLabel.textColor = cashGreen_color
        
        let longString = HELPER.currencySym + " " + "0"
        let longestWord = "0"
        
        longestWordRange = (longString as NSString).range(of: longestWord)
        
        attributedString = NSMutableAttributedString(string: longString, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 17) , NSForegroundColorAttributeName : cashGreen_color])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 25), NSForegroundColorAttributeName : cashGreen_color], range: longestWordRange)
                
        totalValue?.attributedText = attributedString
        
        currencyField.layer.cornerRadius = currencyField.frame.height / 2
                
        tickBtnView.layer.cornerRadius = tickBtnView.layer.frame.width / 2
        tickBtnView.clipsToBounds = true
      
        if !HELPER.isCashAlive { // if from any other view controller
            self.resetAll()
        }
        else{
            // if from scannedcash view controller
            
            if(self.mySwitch.isOn()){
                self.firstViewContainer.isHidden = true;
                self.secondViewContainer.isHidden = false;
                self.secondViewContainer.backgroundColor = UIColor.white
                self.doneButtonView.backgroundColor = UIColor.white
            }
            else{
                self.firstViewContainer.isHidden = false;
                self.doneButtonView.backgroundColor = backgroundGray_color
                self.secondViewContainer.backgroundColor = backgroundGray_color
                mySwitch.setOn(false, animated: false)
            }
            let textColor =  cashGreen_color
            let longString = HELPER.currencySym+" "+String(totalScannedValue)
            
            let longestWord = String(totalScannedValue)
            
            let longestWordRange = (longString as NSString).range(of: longestWord)
            let attributedString = NSMutableAttributedString(string: longString, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 17) , NSForegroundColorAttributeName : textColor])
            
            attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 25), NSForegroundColorAttributeName : textColor], range: longestWordRange)
            
            totalValue?.attributedText = attributedString
            HELPER.isCashAlive = true
            
        }
        self.resetDetailsImage();
        HELPER.isCashAlive = false
    }
    
    // Reset Details button image depending on scanned value 
    func resetDetailsImage(){
    
        if(self.totalScannedValue == 0){
            self.detailbutton.imageView?.image = UIImage(named:"scan_cash_icon01_disabled.png")
            self.detailbutton.isUserInteractionEnabled = false
        }
        else{
            self.detailbutton.imageView?.image = UIImage(named:"ScanCash_icon01.png")
            self.detailbutton.isUserInteractionEnabled = true

        }

    }
    
    func resetAll(){
        
        self.firstViewContainer.isHidden = false;
        self.secondViewContainer.backgroundColor = backgroundGray_color
        self.doneButtonView.backgroundColor = backgroundGray_color
        mySwitch.setOn(false, animated: false)
        
        HELPER.isCashAlive = false
        cashkanBills   = []
        denominatios   = []
        billArray      = []
        finalCurrencyArray = []
        selectedDenomination  = "00"
        billArrayIndex  = 0
        totalScannedValue  = 0
        
        self.totalScannedValue = 0
        self.resetDetailsImage();
        
        for case let btn as UIButton in self.circleView.subviews {
            if(btn.tag != 0){
                if billCount[btn.tag - 1] != 0 || selectedDenominationTag == btn.tag{
                    btn.isSelected = false
                    btn.badge.isHidden = true
                    btn.badgeValue = " "
                    btn.backgroundColor = UIColor.white
                    btn.layer.borderColor = lightGray_color.cgColor
                }
                for case let dummyView as UIView in btn.subviews{
                    if dummyView.tag == btn.tag{
                        dummyView.isHidden = false
                        dummyView.layer.borderWidth = 2.0
                        dummyView.layer.borderColor =  lightGray_color.cgColor
                    }
                }// inner for loop
            }// if
        }//outer for
        selectedDenominationTag  = 0
        billCount  = [0,0,0,0]

    }
    override func viewWillDisappear(_ animated: Bool) {
        if isDone {
            isDone = false
        }else{
            
            //self.navigationController?.popViewController(animated: true)
        }
        
        if HELPER.isDrawerOpen.boolValue && self.menuVC != nil{
            dismissContainer()
        }
        
    }

    //#MARK: method to resize image
    func imageWithImage( _ image: UIImage, newSize  :CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    
    //#MARK: function to detect tap on "?" in slideLabel2
    func TapOnHelpTxt(_ recognizer : UITapGestureRecognizer){
    
        let touchPoint = recognizer.location(in: self.slideLabel2)
        let longestWordRange = (slide_click_2 as NSString).range(of: "sh (?)")
        let validFrame : CGRect = slideLabel2.boundingRectForCharacterRange(range: longestWordRange)!
        
        if(validFrame.contains(touchPoint)){
        
          self.helpTap()
        }
        
      
    }
    //MARK: left drawer method start
    
    func dismissViewBehindWebView(){
        self.view.removeGestureRecognizer(bgTapRecognizer)
        //        self.slideMenuItemSelectedAtIndex(-1);
        let viewMenuBack : UIView = view.subviews.last!
        //        self.aDelegate.tabBar.hidden = false
        if viewMenuBack.tag == 123 {
            //reset drawer bbutton tag
            HELPER.isDrawerOpen = false
            self.drawerBtn.tag = 0
            self.firstViewContainer.alpha = 1.0
            self.secondViewContainer.alpha = 1.0
            
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * (UIScreen.main.bounds.size.width-drawerOffset)
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            
            
        }
        let subViews = self.view.subviews
        for subview in subViews{
            if subview.tag == 111 {
                subview.removeFromSuperview()
            }
        }
        self.tabBarController?.tabBar.isUserInteractionEnabled = true

        return

    }
    
    func dismissContainer(){
        
        self.view.removeGestureRecognizer(bgTapRecognizer)
        let viewMenuBack : UIView = view.subviews.last!
        if viewMenuBack.tag == 123 {
            //reset drawer bbutton tag
            HELPER.isDrawerOpen = false
            self.drawerBtn.tag = 0
            self.firstViewContainer.alpha = 1.0
            self.secondViewContainer.alpha = 1.0

            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * (UIScreen.main.bounds.size.width-drawerOffset)
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
            })
        }
        let subViews = self.view.subviews
        for subview in subViews{
            if subview.tag == 111 {
                subview.removeFromSuperview()
            }
        }
        self.tabBarController?.tabBar.isUserInteractionEnabled = true

        return
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        if index > 2  {
            self.navigationItem.title = Drawer_titles[Int(index)]
        }
    }
    func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            //drawer close
            HELPER.isDrawerOpen = false
            sender.tag = 0
            
            let viewMenuBack : UIView = view.subviews.last!
            
            if viewMenuBack.tag == 123 {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * (UIScreen.main.bounds.size.width-drawerOffset)
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    //viewMenuBack.removeFromSuperview()
            })
            }
            let subViews = self.view.subviews
            for subview in subViews{
                if subview.tag == 111 {
                    subview.removeFromSuperview()
                }
            }
            self.tabBarController?.tabBar.isUserInteractionEnabled = true

            return
        }
        
        //drawer open disabled the tabbar items
//        self.aDelegate.tabBar.hidden = true
        HELPER.isDrawerOpen = true
        sender.isEnabled = false
        sender.tag = 10
        //self.firstViewContainer.alpha = 0.7
        //self.secondViewContainer.alpha = 0.7
        
        if(menuVC == nil){
            menuVC  = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        }
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.view.bringSubview(toFront: menuVC.view)
        self.view.isUserInteractionEnabled = true
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        let dummyDisabledView: UIView = UIView.init(frame:self.view.frame)
        dummyDisabledView.tag = 111
        dummyDisabledView.alpha = 0.5
        dummyDisabledView.backgroundColor = darkGray_color
        
        self.view.insertSubview(dummyDisabledView, belowSubview: menuVC.view)
        
         bgTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(bgTapped(_:)))
        
        self.view.addGestureRecognizer(bgTapRecognizer)
        
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 64, width: UIScreen.main.bounds.size.width-drawerOffset-40, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.menuVC.view.frame=CGRect(x: 0, y: 64, width: UIScreen.main.bounds.size.width-drawerOffset-40, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
            }, completion:nil)
        self.tabBarController?.tabBar.isUserInteractionEnabled = false

    }
    
    
    func bgTapped(_ sender: UITapGestureRecognizer) {
        dismissContainer()
        self.tabBarController?.tabBar.isUserInteractionEnabled = true

    }
    //MARK: left drawer method end
    
    //MARK: custom camera click methods

    func captureButtonClicked(){
        self.firstViewContainer.isHidden = true;
        self.secondViewContainer.isHidden = false;
        self.secondViewContainer.backgroundColor = UIColor.white
        self.doneButtonView.backgroundColor = UIColor.white

        let textColor =  cashGreen_color
        let longString = HELPER.currencySym+" "+String(totalScannedValue)
        
        let longestWord = String(totalScannedValue)
        
        let longestWordRange = (longString as NSString).range(of: longestWord)
        let attributedString = NSMutableAttributedString(string: longString, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 17) , NSForegroundColorAttributeName : textColor])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 25), NSForegroundColorAttributeName : textColor], range: longestWordRange)
        
        totalValue?.attributedText = attributedString
        HELPER.isCashAlive = true
        
        self.resetDetailsImage();

        // Uncomment following
        self.initializeCamera()
    }
    
    func initializeCamera() {
        
        let mediaType:String =  AVMediaTypeVideo;
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized {
            // Already Authorized
            self.camera = XMCCamera(sender: self)
            self.establishVideoPreviewArea()
        } else {
        AVCaptureDevice.requestAccess(forMediaType: mediaType, completionHandler: { (granted: Bool) in
            
            if granted{
                self.camera = XMCCamera(sender: self)
                self.establishVideoPreviewArea()

            }else{
                
                let alertController = UIAlertController (title: "CashKan", message: "Go to Settings to enable camera access?", preferredStyle: .alert)
                
                let settingsAction = UIAlertAction(title: alert_ok, style: .default) { (_) -> Void in
                    
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    
                    if settingsUrl != nil {
                        
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }
                }
                let cancelAction = UIAlertAction(title: alert_cancel, style: .default) { (_) -> Void in
                    self.secondViewContainer.backgroundColor = backgroundGray_color
                    self.firstViewContainer.isHidden = false
                    self.doneButtonView.backgroundColor = backgroundGray_color
                    self.mySwitch.setOn(false, animated: false)
                }
                
                alertController.addAction(cancelAction)
                alertController.addAction(settingsAction)
                self.present(alertController, animated: true, completion: nil)
            }
        })
        }

    }
   
    func establishVideoPreviewArea() {
        self.cameraPreview.layoutIfNeeded()
        self.preview = AVCaptureVideoPreviewLayer(session: self.camera?.session)
        self.preview?.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.preview?.frame = self.cameraPreview.frame
        self.cameraPreview.layer.addSublayer(self.preview!)
    }
    
    func cameraSessionConfigurationDidComplete() {
        self.camera?.startCamera()
    }
    
    func cameraSessionDidBegin() {
        UIView.animate(withDuration: 0.225, animations: { () -> Void in
            self.cameraPreview.alpha = 1.0
        })
    }
    
    func cameraSessionDidStop() {
        UIView.animate(withDuration: 0.225, animations: { () -> Void in
            self.cameraPreview.alpha = 0.0
        })
    }
    
    // Tapped on help icon from navigation bar
    func helpTap()
    {
        
        if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "WebHelp") as? HelpWebViewController {
            if(!self.firstViewContainer.isHidden){
                resultController.urlToVisit = scanLanding_helpUrl // without camera view
            }
            else{
                resultController.urlToVisit = scan_helpurl //camera view
            }
            HELPER.isCashAlive = true
            self.navigationController?.pushViewController(resultController, animated: true)
        }

    }

    
    // custom button to show currency denomination with badges
    
    func custombutton(_ viewframe: CGRect, buttontag: Int, badge: Int, denomination: String, fontsize:CGFloat)->UIButton{
        let btn = UIButton(type: .custom)
        btn.frame = viewframe
        btn.layer.cornerRadius = 0.5 * btn.bounds.size.width 
        btn.layer.borderWidth = 2
        btn.layer.borderColor = lightGray_color.cgColor
        btn.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: fontsize)
        btn.titleLabel?.textAlignment = NSTextAlignment.center
        btn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
        btn.badgeValue = String(badge)
        btn.layoutIfNeeded()
        
        let rect = CGRect(x: 0, y: 0, width: viewframe.size.width, height: viewframe.size.height)
        
        //dummy view for handling badges
        let dummyView = UIView(frame: rect)
        dummyView.layer.cornerRadius = 0.5 * dummyView.bounds.size.width
        dummyView.layer.borderWidth = CGFloat(customButtonBorderWidth)
        dummyView.isUserInteractionEnabled = false
        dummyView.layer.borderColor = lightGray_color.cgColor
        dummyView.isHidden = true
        dummyView.tag = buttontag
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(CashViewController.buttonClicked(_:)))
        tap.delegate = self
        dummyView.addGestureRecognizer(tap)
        
        btn.insertSubview(dummyView, belowSubview: btn.badge)
        let darkOrange = darkOrange_color 
        btn.badgeBGColor = darkOrange
        btn.badge.isHidden = true
        btn.setTitle(denomination, for: UIControlState())
        btn.setTitleColor(UIColor.white, for: UIControlState.selected)
        btn.setTitleColor(cashGreen_color, for: UIControlState())
        btn.backgroundColor = UIColor.white
        btn.addTarget(self, action:#selector(CashViewController.buttonClicked(_:)), for: .touchUpInside)
        btn.tag = buttontag
        return btn
    }

    @IBAction func detailsButtonClicked(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TopOverVc") as! DetailsViewController
        vc.view.backgroundColor = UIColor.clear
        vc.cashview = self
        vc.cashkanBills = billArray
        self.secondViewContainer.backgroundColor = darkGray_color
        self.secondViewContainer.alpha = 0.4
        tabBarController?.tabBar.alpha = 0.4
        self.secondViewContainer.isUserInteractionEnabled = false
       tabBarController?.tabBar.isUserInteractionEnabled = false
        self.modalPresentationStyle =  UIModalPresentationStyle.currentContext
        self.present(vc, animated:false, completion: nil)
        
        
    }
    
    //denomination button click event
    func buttonClicked(_ sender:UIButton)
    {
        for case let btn as UIButton in self.circleView.subviews {
            if btn.tag == sender.tag && btn.isSelected != true && btn.tag != 0{
                btn.isSelected = true
                btn.backgroundColor = cashGreen_color
                btn.layer.borderColor = cashGreen_color.cgColor
                btn.layer.borderWidth = 0.0
                selectedDenominationTag = btn.tag
                selectedDenomination = (((HELPER.localeInfo[btn.tag - 1] as? NSDictionary )!["denomination"] as? NSNumber)!).stringValue
            }else if sender.tag != btn.tag && btn.tag != 0{
                btn.isSelected = false
                btn.backgroundColor = UIColor.white
                btn.layer.borderColor = lightGray_color.cgColor
            }
            
            for case let dummyView as UIView in btn.subviews{
                if dummyView.tag == sender.tag{
                    dummyView.isHidden = false
                    dummyView.layer.borderWidth = CGFloat(customButtonBorderWidth)
                    dummyView.layer.borderColor = cashGreen_color.cgColor
                }
                else if btn.tag != 0{
                    dummyView.layer.borderColor = lightGray_color.cgColor
                }
                
            }

        }
        if (self.firstViewContainer.isHidden == false && HELPER.isCashDetailsOpen == false){
            self.denominationBtnClick();
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // I am done changed to NEXT btn
    func iAmDoneButtonClicked(_ sender: UITapGestureRecognizer) {
        
        isDone = true
        if( totalScannedValue != 0){

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "ScannedCashController") as! ScannedCashViewController
        secondViewController.totalScannedValue =  HELPER.currencySym + String(self.totalScannedValue)
       
        secondViewController.cashViewController  = self
        secondViewController.cashkanBills = billArray
        secondViewController.finalCurrencyArray = self.finalCurrencyArray
        self.navigationController?.pushViewController(secondViewController, animated: true)
        }
        
        else{
            let alertController = UIAlertController(title: alert_spendAmount, message: alert_pleaseAddCurrencyValue , preferredStyle: .alert)
            let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion:nil)
        }
    }
    
    //  #MARK: Calling google api methods

    @IBAction func retakeButtonClicked(_ sender: UIButton) {
         //open camera & after finish camera action again need to move next screen

        if(selectedDenomination != "00"){

            if self.status == .preview {
                self.view.isUserInteractionEnabled = false
                tabBarController?.tabBar.isUserInteractionEnabled = false
                self.navigationController?.navigationBar.isUserInteractionEnabled = false
                self.activityIndicator.showActivityIndicatory(self.view)
                self.cancelButton.isHidden = false
                self.cameraPreview.alpha = 0.0;
                
                self.camera?.captureStillImage({ (image) -> Void in
                    if image != nil {
                        let newImage = HELPER.resizeImage(image!, size: CGSize(width: self.view.frame.width, height: self.view.frame.height))
                        self.currencyImageView.image = newImage
                        self.sendRequest()

                        self.status = .still
                    } else {
                        //    self.cameraStatus.text = "Uh oh! Something went wrong. Try it again."
                        self.status = .error
                    }
                    
                })
            } else if self.status == .still || self.status == .error {
                UIView.animate(withDuration: 0.225, animations: { () -> Void in
                    self.cameraPreview.alpha = 1.0;
                    }, completion: { (done) -> Void in
                        self.currencyImageView.image = nil;
                        self.status = .preview
                })
            }
          
        }
        else{
                let alertController = UIAlertController(title: alert_cashkan, message: alert_pleaseSelectDenomination, preferredStyle: .alert)
                let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion:nil)
        }
    }
    
    
    func sendRequest(){
        let croppedImage : UIImage = self.cropToBounds(image: self.currencyImageView.image!, viewStrip: self.cameraStrip)
        let binaryImageData = self.base64EncodeImage(croppedImage)
        self.createRequest(binaryImageData)
    }
    
    func createRequest(_ imageData: String) {
        // Create our request URL
        let request = NSMutableURLRequest(
            url: URL(string: "https://vision.googleapis.com/v1/images:annotate?key=\(API_KEY)")!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(
            Bundle.main.bundleIdentifier ?? "",
            forHTTPHeaderField: "X-Ios-Bundle-Identifier")
        
        // Build our API request
        let jsonRequest: [String: AnyObject] = ([
            "requests": [
                "image": [
                    "content": imageData
                ],
                "features": [
                    [
                        "type": "TEXT_DETECTION",
                        "maxResults": 10
                    ]
                ]
            ]
        ] as NSDictionary) as! [String : AnyObject]
        
        // Serialize the JSON
        request.httpBody = try! JSONSerialization.data(withJSONObject: jsonRequest, options: [])
        
        // Run the request on a background thread
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
            self.runRequestOnBackgroundThread(request)
        });
        
    }
    
    func runRequestOnBackgroundThread(_ request: NSMutableURLRequest) {
        
        let session = URLSession.shared
        
        // run the request
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            self.analyzeResults(data!)
        })
        task.resume()
    }
    
    func analyzeResults(_ dataToParse: Data) {
        
        // Update UI on the main thread
        DispatchQueue.main.async(execute: {
            
            
            // Use SwiftyJSON to parse results
            let json = JSON(data: dataToParse)
            let errorObj: JSON = json["error"]
            
            // Check for errors
            if (errorObj.dictionaryValue != [:]) {
                
                self.view.isUserInteractionEnabled = true
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                self.navigationController?.navigationBar.isUserInteractionEnabled = true
                self.activityIndicator.hideActivityIndicator(self.view)

            } else {
                // Parse the response
                let responses: JSON = json["responses"][0]
                
                // Get label annotations
                let labelAnnotations: JSON = responses["textAnnotations"]
                let numLabels: Int = labelAnnotations.count
                var labels: Array<String> = []
                if numLabels > 0 {
                    var labelResultsText:String = ""
                    var finalResult:String = ""
                    for index in 0..<numLabels {
                        let label = labelAnnotations[index]["description"].stringValue
                        labels.append(label)
                    }
                    for label in labels {
                        // if it's not the last item add a comma
                        if labels[labels.count - 1] != label {
                            labelResultsText += "\(label), "
                        } else {
                            labelResultsText += "\(label)"
                        }
                    }
                    
  /////////////////************Getting Scanned Currency Number****************//////////////////////
                    
            let resultArray = labelResultsText.characters.split{$0 == "\n"}.map(String.init)

                    finalResult = resultArray[0]
                    
                    
            finalResult = (self.removeSpecialCharsFromString(finalResult)).uppercased()
            finalResult = finalResult.trimmingCharacters(in: CharacterSet.whitespaces)
            finalResult =  finalResult.replacingOccurrences(of: " ", with: "")
                    
                    // Get only first 12 characters
                    
                    if finalResult.characters.count > 12{
                        
                        let str = finalResult[finalResult.startIndex..<finalResult.index(finalResult.startIndex, offsetBy: 12)]
                        finalResult = str
                    }
                    
                    self.view.isUserInteractionEnabled = true
                    self.tabBarController?.tabBar.isUserInteractionEnabled = true
                    self.cancelButton.isHidden = false
                    self.navigationController?.navigationBar.isUserInteractionEnabled = true
                    self.activityIndicator.hideActivityIndicator(self.view)
                    self.currencyField.text = finalResult
                } else {
                    self.view.isUserInteractionEnabled = true
                    self.tabBarController?.tabBar.isUserInteractionEnabled = true
                    self.cancelButton.isHidden = false
                    self.navigationController?.navigationBar.isUserInteractionEnabled = true
                    self.activityIndicator.hideActivityIndicator(self.view)

                    self.currencyField.text = "NOTHING"

                }
            }
        })
        
    }
    

    func removeSpecialCharsFromString(_ text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
    
    @IBAction func tickButtonClicked(_ sender: UIButton) {

        self.cancelButton.isHidden = true

            //toggle image view between tick & untick
        if(!(currencyField.text?.isEmpty)! && 0 != selectedDenominationTag){
            var flag : Bool = true
            for c in 0 ..< billArray.count{
                
            let (bill,denom) = billArray[c]
                if (currencyField.text! == bill as String){
                    
                    flag = false
                    let alertController = UIAlertController(title: alert_cashkan, message: alert_duplicateCurrencyValue, preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                    }
                    alertController.addAction(okAction)
                    currencyField.text = ""
                    self.present(alertController, animated: true, completion:nil)
                }
                
            
            }
          
            if( true == flag){
            
                self.tickBtnView.isSelected = true
                tickBtnClicked = true

                for case let btn as UIButton in self.circleView.subviews {
                    if btn.tag == selectedDenominationTag && btn.isSelected == true && btn.tag != 0{
             
                        let pos = (btn.tag - 1) as Int
                        btn.badge.isHidden = false
                        let a : Int = self.billCount[btn.tag - 1].intValue
                        let b = 1 
                        billCount[btn.tag - 1]  = (a +  b) as NSNumber
                        let badgeValue : NSNumber = billCount[btn.tag - 1]
                        btn.badgeValue =  badgeValue.stringValue
                        billArray.insert((currencyField.text! as NSString ,selectedDenomination as NSString), at: billArrayIndex)
                        
                        
                        let localeId = ((HELPER.localeInfo[pos] as? NSDictionary )!["localeId"] as? NSNumber)!
                        let currencyId = ((HELPER.localeInfo[pos] as? NSDictionary )!["currencyId"] as? NSNumber)!
                        self.finalCurrencyArray.insert((localeId,currencyId,selectedDenomination as NSString,currencyField.text! as NSString), at: billArrayIndex)
                        
                        billArrayIndex += 1
                        totalScannedValue += Int(selectedDenomination)!

                        let textColor =  cashGreen_color
                        let longString = HELPER.currencySym+" "+String(totalScannedValue)

                        let longestWord = String(totalScannedValue)
                        
                        let longestWordRange = (longString as NSString).range(of: longestWord)
                        
                        let attributedString = NSMutableAttributedString(string: longString, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 17) , NSForegroundColorAttributeName : textColor])
                        
                        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 25), NSForegroundColorAttributeName : textColor], range: longestWordRange)
                        
                        totalValue?.attributedText = attributedString
                        HELPER.isCashAlive = true

                        self.resetDetailsImage();

                        currencyField.text = ""
                        
                        break
                    
                    }
                }
                
                
            }
            }
        else if(0 == selectedDenominationTag){
            
            let alertController = UIAlertController(title: alert_cashkan, message: alert_pleaseSelectDenomination, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion:nil)
            
        }

        else if((currencyField.text?.isEmpty)!){
        
            let alertController = UIAlertController(title: alert_cashkan, message: alert_pleaseAddCurrencyValue, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion:nil)


        }
        
        self.cancelButton.isHidden = true
        //open camera
        
        UIView.animate(withDuration: 0.225, animations: { () -> Void in
            self.cameraPreview.alpha = 1.0;
            }, completion: { (done) -> Void in
                self.currencyImageView.image = nil;
                self.status = .preview
        })
        
    }
    
    // #MARK: text field delegate methods
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let textFieldRect : CGRect = self.view.window!.convert(textField.bounds, from: textField)
        let viewRect : CGRect = self.view.window!.convert(self.view.bounds, from: self.view)

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y += animateDistance
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        if textField.returnKeyType != UIReturnKeyType.done{
            let nextTag: NSInteger = textField.tag+1;
            let nextResponder: UIResponder = (textField.superview?.viewWithTag(nextTag))!
            
            if let res = nextResponder as UIResponder?{
                nextResponder.becomeFirstResponder()
            }
        }
        else{
            textField.resignFirstResponder()
        }
        
        return false;
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        var result = true
        
        let maxLength = 12
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if(newString.length <= maxLength){
            
        }
        else{
            return false
            
        }
        if string.characters.count > 0 {
            let disallowedCharacterSet = CharacterSet(charactersIn: currency_charSet).inverted
            let replacementStringIsLegal = string.rangeOfCharacter(from: disallowedCharacterSet) == nil
            result = replacementStringIsLegal
        }
        return result
        
    }

    func refreshView(){
    
        for case let btn as UIButton in self.circleView.subviews {
            if btn.tag != 0{
                let cnt = billCount[btn.tag - 1]
                if ( cnt.intValue <= 0){
                    billCount[btn.tag - 1] = 0
                    btn.badge.isHidden = true
                }
                else{
                    self.buttonClicked(btn)
                    btn.badge.isHidden = false
                    let badgeValue : NSNumber = billCount[btn.tag - 1]
                    btn.badgeValue =  badgeValue.stringValue
                    
                }
            }
        }
        
        let textColor =  cashGreen_color
        let longString = HELPER.currencySym+" "+String(totalScannedValue)
        
        let longestWord = String(totalScannedValue)
        
        let longestWordRange = (longString as NSString).range(of: longestWord)
        
        let attributedString = NSMutableAttributedString(string: longString, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 17) , NSForegroundColorAttributeName : textColor])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 25), NSForegroundColorAttributeName : textColor], range: longestWordRange)
        
        totalValue?.attributedText = attributedString
        HELPER.isCashAlive = true

        self.resetDetailsImage();

        currencyField.text = ""

    }
    
     func tipNextButtonClicked(_ sender: AnyObject) {
        
        CashViewController.tipCounter = CashViewController.tipCounter + 1
        
        if CashViewController.tipCounter == tipDescArray.count {
            
            CashViewController.tipCounter = 0
        }
        self.tipDescLbl.text = tipDescArray[CashViewController.tipCounter]
        self.tipTitleLbl.text = tipTitleArray[CashViewController.tipCounter]
        
        HELPER.updateWithSpacing(lineSpacing: 2, textView: self.tipDescLbl)
    }
    
    func denominationBtnClick(){
                self.tickBtnView.isSelected = true
                tickBtnClicked = true
                
        
                for case let btn as UIButton in self.circleView.subviews {
                    if btn.tag == selectedDenominationTag && btn.isSelected == true {
                        
                        let pos = (btn.tag - 1) as Int
                        btn.badge.isHidden = false
                        let a = self.billCount[btn.tag - 1] as Int
                        let b = 1 as Int
                        billCount[btn.tag - 1]  = (a +  b) as NSNumber
                        let badgeValue : NSNumber = billCount[btn.tag - 1]
                        btn.badgeValue =  badgeValue.stringValue
                        billArray.insert((currencyField.text! as NSString ,selectedDenomination as NSString), at: billArrayIndex)
                        
                        
                        let localeId = ((HELPER.localeInfo[pos] as? NSDictionary )!["localeId"] as? NSNumber)!
                        let currencyId = ((HELPER.localeInfo[pos] as? NSDictionary )!["currencyId"] as? NSNumber)!
                        self.finalCurrencyArray.insert((localeId,currencyId,selectedDenomination as NSString,currencyField.text! as NSString), at: billArrayIndex)
                        
                        billArrayIndex += 1
                        totalScannedValue += Int(selectedDenomination)!
                        
                        let textColor =  cashGreen_color
                        let longString = HELPER.currencySym+" "+String(totalScannedValue)
                        
                        let longestWord = String(totalScannedValue)
                        let longestWordRange = (longString as NSString).range(of: longestWord)
                        
                        let attributedString = NSMutableAttributedString(string: longString, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 17) , NSForegroundColorAttributeName : textColor])
                        
                        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 25), NSForegroundColorAttributeName : textColor], range: longestWordRange)
                        
                        totalValue?.attributedText = attributedString
                        HELPER.isCashAlive = true

                        self.resetDetailsImage();

                        currencyField.text = ""
                        
                        break
                        
                    }
                }
        
    }
    
    @IBAction func closeSecondConatinerBtnClcik(_ sender: Any) {
        
        let alertController = UIAlertController(title: alert_cashkan, message: alert_NoScanCash, preferredStyle: .alert)
        let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            self.secondViewContainer.backgroundColor = backgroundGray_color
            self.firstViewContainer.isHidden = false
            self.doneButtonView.backgroundColor = backgroundGray_color
            self.mySwitch.setOn(false, animated: false)
        }
        let cancelAction = UIAlertAction(title: alert_cancel, style: .default) { (action:UIAlertAction!) in
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
        
    }
    
    
    // #MARK: Image copping method
    
    func resizeImage(_ imageSize: CGSize, image: UIImage) -> Data {
        UIGraphicsBeginImageContext(imageSize)
        image.draw(in: CGRect(x: 0, y: 0, width: imageSize.width, height: imageSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        let resizedImage = UIImagePNGRepresentation(newImage!)
        UIGraphicsEndImageContext()
        return resizedImage!
    }
    
    func base64EncodeImage(_ image: UIImage) -> String {
        var imagedata = UIImagePNGRepresentation(image)
        
        // Resize the image if it exceeds the 2MB API limit
        if (imagedata?.count > 2097152) {
            let oldSize: CGSize = image.size
            let newSize: CGSize = CGSize(width: 800, height: oldSize.height / oldSize.width * 800)
            imagedata = resizeImage(newSize, image: image)
        }
        
        return imagedata!.base64EncodedString(options: .endLineWithCarriageReturn)
    }
    
    
    func cropToBounds(image: UIImage, viewStrip: UIView) -> UIImage {

        let posX: CGFloat = viewStrip.frame.origin.x
        var posY: CGFloat = viewStrip.frame.origin.y +  self.secondViewContainer.frame.origin.y
        let cgwidth: CGFloat = viewStrip.frame.width
        let cgheight: CGFloat = viewStrip.frame.height

        if(self.view.frame.height < 600){
            posY -= 17
        }

        let rect1: CGRect = CGRect(origin: CGPoint(x:posX, y:posY), size: CGSize(width: cgwidth,height: cgheight))
        // Create bitmap image from context using the rect
        let imageRef: CGImage = image.cgImage!.cropping(to: rect1)!
        //print(imageRef)
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
}
