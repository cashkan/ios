//
//  SplashScreen.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 17/08/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit
import Firebase

class SplashScreen: UIViewController, NetworkFailureDelegate {
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        //HELPER.setFontFamily( self.view, andSubViews: true)
//        var namesOfMenu : NSMutableArray = []
//        let DataFordashbord = ["My Coverage":"1","Policy Features":"2","Track Claims":"3","Network Hospitals":"4","Claims Procedures":"5","Utilities":"6","Intimate Claim":"7","Contact Details":"8"]
        //
        //
//        let DictinaryClaims : [String:String] = ["GMC":"1","GPA":"0","GTL":"1"]
//        for (key, value) in DictinaryClaims {
//            print("\(key) -> \(value)")
//        }
//        
//        for name in DictinaryClaims.values
//        {
//            print(name)
//        }
//        
//        let colors = Array(DictinaryClaims.values)
//        print(colors)
//        
//        let i = 0;
//        //for i in 0..<DictinaryClaims.count {
//            DictinaryClaims.forEach { (data) in
//            let (k,v) = DictinaryClaims[0]
//            if(data.value == "1")
//            {
//                print(data.key)
//                
//                namesOfMenu.add(data.key)
//                
//            }else{
//                print("hi")
//            }
//            
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        RequestResponceHelper.delegate = self

        super.viewDidAppear(animated)
        let defaults = UserDefaults.standard
        let dict = defaults.object(forKey: "userinfo") as? [String: String] ?? [String: String]()
        let isRemember = dict["isremember"] != nil
        let username = dict["user"]
        let password = dict["password"]
                
        if isRemember {
            // now val is not nil and the Optional has been unwrapped, so use it
            //self.activityIndicator.showActivityIndicatory(self.view)
            self.sendLoginRequest(username!,password: password!)
        }
        else{
            let defaults = UserDefaults.standard
            if var counter = Int(defaults.string(forKey: "showInstructionsCnt")!){
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if(counter < 3){
                // show instructions for first 3 times only
                let vc = storyboard.instantiateViewController(withIdentifier: "GetStartedViewControllerId") as! GetStartedViewController
                vc.splashScreen = self
                counter = counter+1
                defaults.set(counter, forKey: "showInstructionsCnt")
                defaults.synchronize()
                self.present(vc, animated: false, completion: nil)
            }
            else{
                // show login
                let vc = storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
                self.present(vc, animated: false, completion: nil)
            }
        }
        }

    }
    
    func sendLoginRequest(_ username:String,password:String) -> Void{
        
        var deviceId = ""
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            deviceId = refreshedToken
            //print("InstanceID login token: \(refreshedToken)")
            
        }
        
        var resultCompletion: (_ success: Bool, _ message: String?) -> ()
        
        self.Newlogin(username, password: password, deviceId: deviceId){
            (success :  Bool, object : AnyObject?,  dataObject: Data?, message: String?) in
            
            //print("got back: \(message)")
            
            if( success == true){
                
                if let response = object as? HTTPURLResponse{
                    
                    // You can print out response object
                    //print("response = \(response)")
                    
                    // Print out response body
                    let responseString = NSString(data: dataObject!, encoding: String.Encoding.utf8.rawValue)
                    if let auth = response.allHeaderFields["Authorization"] as? String {
                        
                        var myStringArr = auth.components(separatedBy: " ")
                        
                        HELPER.authrizationHeader = myStringArr[1]
                        
                        //print(HELPER.authrizationHeader)
                    }
                    
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        if let successMsg = myJSON![display_msg]{
                            //print(successMsg)
                        }
                        
                        if let parseJSON = myJSON![data] as! [String:Any]?{
                            // Now we can access value of First Name by its key
                            
                            HELPER.fname                = (parseJSON["fname"] as? String!)!
                            HELPER.lname                = (parseJSON["lname"] as? String)!
                            HELPER.user_id              = (parseJSON["user_id"] as! NSNumber)
                            //HELPER.sex                  = (parseJSON["sex"] as? String)!
                            //HELPER.dob                  = (parseJSON["dob"] as? String)!
                            HELPER.email                = (parseJSON["email"] as? String)!
                            //HELPER.mobile               = (parseJSON["mobile"] as? String)!
                            HELPER.lat                  = (parseJSON["lat"] as? String)!
                            HELPER.lng                  = (parseJSON["lng"] as? String)!
                            HELPER.image_url            = (parseJSON["image_url"] as? String)!
                            HELPER.regionShortCode      = (parseJSON["regionShortCode"] as? String)!
                            HELPER.is_active            = (parseJSON["is_active"] as? NSNumber)!
                            HELPER.total_cashkans       = (parseJSON["total_cashkans"] as? NSNumber)!
                            HELPER.version_obsolete     = (parseJSON["version_obsolete"] as? NSNumber)!
                            HELPER.localeInfo           = (parseJSON["localeInfo"] as? NSArray)!
                            HELPER.currencySymbol = (((parseJSON["localeInfo"] as? NSArray)![0] as? NSDictionary )!["symbol"] as? String)!
                            
                            HELPER.localeId       = (((parseJSON["localeInfo"] as? NSArray)![0] as? NSDictionary )!["localeId"] as? NSNumber)!
                            HELPER.distanceIn = (((parseJSON["localeInfo"] as? NSArray)![0] as? NSDictionary )!["tdt_uom"] as? String)! 


                                HELPER.currencySym = HELPER.currencySymbol
                            //}

                            
               }
                        
                    }
                    catch {
                        //print(error)
                    }
                }
                //self.activityIndicator.hideActivityIndicator(self.view)
               
                if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "TabBar") as? UITabBarController {
                 //   resultController.selectedIndex = 2

                    self.present(resultController, animated: true, completion: nil)
                }
                
                
            }
                
            else{
                
                // If Login failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        
                        if  (myJSON![error_msg]! as AnyObject).length != 0{
                            let error = myJSON![error_msg]
                            let alertController = UIAlertController(title: login_failed, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                          //  self.passwordTextfield.text = ""
                        }
                            
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            let error = myJSON![display_msg]
                            let alertController = UIAlertController(title: login_failed, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                           // self.passwordTextfield.text = ""
                        }
                        
                        //self.activityIndicator.hideActivityIndicator(self.view)
                    }
                    catch {
                        //print(error)
                    }
                    
                }
            }
            
            
        }
    }
    
    
    func Newlogin(_ email: String, password: String, deviceId : String, completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data?, _ message: String?) -> ()) {
        
        var loginObject : NSDictionary
        if( deviceId == ""){
            loginObject = ["email": email, "password": password]
        }
        else{
            loginObject = ["email": email, "password": password,"deviceId" : deviceId]
        }
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest(log_url, params: loginObject as? Dictionary<String, AnyObject> )
        
        RequestResponceHelper.post(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? NSData{
                        //message = passedMessage
                        completion(success, object ,dataObject as Data ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }
    



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func networkFailed(){
        let error = "Network connection failed..."
        let alertController = UIAlertController(title: login_failed, message: error as? String, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            //print("you have pressed the Cancel button");
            exit(0)
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
        
    }
}
