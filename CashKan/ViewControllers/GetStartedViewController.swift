//
//  GetStartedViewController.swift
//  CashKan
//
//  Created by emtarang on 19/09/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class GetStartedViewController: UIViewController, UIScrollViewDelegate {
    
    
    @IBOutlet var scrollView    : UIScrollView!
    @IBOutlet var pageControl   : UIPageControl!
    @IBOutlet var nextButton    : UIButton!
    @IBOutlet var skipButton    : UIButton!
    var imgOne: UIImageView!
    var imgTwo: UIImageView!
    var imgTh: UIImageView!
    var imgFour: UIImageView!
    var imgfive: UIImageView!
    
    var splashScreen : SplashScreen!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HELPER.setFontFamily( self.view, andSubViews: true)

        // Do any additional setup after loading the view.
        self.scrollView.frame = CGRect(x: 0,y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.scrollView.addSubview(nextButton)
        self.scrollView.addSubview(skipButton)
        
        
        //3
        imgOne = UIImageView(frame: CGRect(x: 0, y: 0,width: self.scrollView.frame.width, height: self.scrollView.frame.height))
        imgOne.image = UIImage(named: "how-it-works-screen-1")
        imgOne.center = self.view.frame.center
        imgOne.sendSubview(toBack: self.scrollView)
        self.scrollView.addSubview(imgOne)
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
        scrollView.isPagingEnabled = true
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width * CGFloat(5), height: self.scrollView.frame.height)
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        let scrollViewHeight:CGFloat = self.scrollView.frame.height
        
        imgTwo = UIImageView(frame: CGRect(x: scrollViewWidth * CGFloat(1), y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgTwo.image = UIImage(named: "how-it-works-screen-2")
        self.scrollView.addSubview(imgTwo)
        
        imgTh = UIImageView(frame: CGRect(x: scrollViewWidth * CGFloat(2), y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgTh.image = UIImage(named: "how-it-works-screen-3")
        self.scrollView.addSubview(imgTh)
        
        imgFour = UIImageView(frame: CGRect(x: scrollViewWidth * CGFloat(3), y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgFour.image = UIImage(named: "how-it-works-screen-4")
        self.scrollView.addSubview(imgFour)
        
        imgfive = UIImageView(frame: CGRect(x: scrollViewWidth * CGFloat(4), y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgfive.image = UIImage(named: "how-it-works-screen-5")
        self.scrollView.addSubview(imgfive)
        
        self.scrollView.addSubview(nextButton)
        self.scrollView.addSubview(skipButton)
        self.scrollView.addSubview(pageControl)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func nextButtonClick(_ sender: AnyObject) {
        
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        let scrollViewHeight:CGFloat = self.scrollView.frame.height
        
        scrollView.scrollRectToVisible(CGRect(x: scrollViewWidth * CGFloat(self.pageControl.currentPage+1), y:0,width:scrollViewWidth, height:scrollViewHeight), animated: true)
        
        if( self.nextButton.titleLabel?.text == "GET STARTED"){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let defaults = UserDefaults.standard
            if let counter = Int(defaults.string(forKey: "showInstructionsCnt")!){
                if(counter == 1){
                    let vc = storyboard.instantiateViewController(withIdentifier: "RegisterNavigationController")
                    
                    self.present(vc, animated: false, completion: nil)                }
                else if(counter < 4){
                // goto login
            let vc = storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
            self.present(vc, animated: false, completion: nil)
                }
            }
        }
        
    }
    
    
    @IBAction func skipButtonClick(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let defaults = UserDefaults.standard
        if let counter = Int(defaults.string(forKey: "showInstructionsCnt")!){
            if(counter == 1){
                //let vc = storyboard.instantiateViewControllerWithIdentifier("RegisterId") as! ViewController
                let vc = storyboard.instantiateViewController(withIdentifier: "RegisterNavigationController") 
                
                self.present(vc, animated: false, completion: nil)                }
            else if(counter < 4){
                // goto login
                let vc = storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
                self.present(vc, animated: false, completion: nil)
            }
        }

        
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        
        if(self.pageControl.currentPage == 4){
            self.nextButton.setTitle("GET STARTED", for: UIControlState())
        }
        else{
            self.nextButton.setTitle("NEXT", for: UIControlState())
        }
    }
    
}
