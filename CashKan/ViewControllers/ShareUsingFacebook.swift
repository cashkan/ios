//
//  ShareUsingFacebook.swift
//  CashKan
//
//  Created by emtarang on 26/09/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit

class ShareUsingFacebook :  UIViewController {
    
    let content : FBSDKShareLinkContent = FBSDKShareLinkContent()
    //let url : String = goldMedal
    
    var tevScore                    : Int!
    var appNameStr                  : String = ""
    var economy                     : String = ""
    var url                         : String = ""
    var openGraphtitle              : String = ""
    var graphScore                  : String = ""
    var graphBottomDesc             : String = ""

    var FBLaunchCount               : Int = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        HELPER.setFontFamily( self.view, andSubViews: true)
        self.view.backgroundColor = UIColor.white
        self.view.isOpaque = false

        self.appNameStr = "<b>" + appName + "</b>";
        
        // Calculate economy
        if( HELPER.currencySym == IndianCurrency){
            economy = IndianEconomy
        }
        else if( HELPER.currencySym == "$"){
            economy = USEconomy
        }
        else{
            economy = cannadianEconomy
        }
        
        /* Calculating the TEV Score */
        if (tevScore <= 10) {
            url = goldMedal
            openGraphtitle  = "Gold Medal"
            graphScore      = "Score: "+String(tevScore)+" out of 100"+"\n"+"(Economic contribution that is better than "+String(tevScore)+"% of Cashkan users)";
        } else if (tevScore > 10 && tevScore <= 50) {
            url            = silverMedal;
            openGraphtitle = "Silver Medal";
            graphScore     = "Score: "+String(tevScore)+" out of 100"+"\n"+"(Economic contribution that is better than "+String(tevScore)+"% of Cashkan users)";
        } else {
            url            = bronzeMedal;
            openGraphtitle = "Bronze Medal";
            graphScore     = "Score: "+String(tevScore)+" out of 100"+"\n"+"(Economic contribution that is better than "+String(tevScore)+"% of Cashkan users)";
        }
        
        graphBottomDesc = "www.cashkan.com";// + "\n" + "Cash can earn you rewards!";
        
        // call Facebooklogin method
        self.getFacebookUserInfo()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if( self.FBLaunchCount != 0){
            self.FBLaunchCount = 0
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func getFacebookUserInfo() {
        if(FBSDKAccessToken.current() != nil)
        {
            // user already logged in
            //print(FBSDKAccessToken.current().permissions)
            let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "id, name"])
            graphRequest?.start(completionHandler: { (connection, result, error) -> Void in
                
               // let FBid = result.value(forKey: "id") as? String
                self.postMsg()
                
            })
        } else {
            // if user not logged in
            
            let loginManager = FBSDKLoginManager()
            loginManager.loginBehavior=FBSDKLoginBehavior.native //fbnativelogin
            loginManager.logIn(withPublishPermissions: ["publish_actions"], from: self, handler: { (loginResult, error) in
                //print(loginResult)
                if error != nil {
                    //print(error)
                    //print(FBSDKAccessToken.current())
                } else if (loginResult?.isCancelled)! {
                    //print("Cancelled")
                    self.dismiss(animated: false, completion: nil)
                } else {
                    self.postMsg()
                    
                    // To get read permissions //
                    
//                    print("LoggedIn")
//                    loginManager.logIn(withPublishPermissions: ["publish_actions"], from: self, handler: { (loginResult, error) in
//                        if error != nil {
//                            print(error)
//                            print(FBSDKAccessToken.current())
//                        } else if (loginResult?.isCancelled)! {
//                            print("Cancelled")
//                            self.dismiss(animated: false, completion: nil)
//                        } else {
//                            self.postMsg()
//                        }
//                    })
                }

            })
            
            
        }

    }
    
    
    func postMsg() {
        
        if FBSDKAccessToken.current().hasGranted("publish_actions") {
            
            
            // Create the object
            let properties : NSDictionary = ["og:type"        : "dev_ckan:economy",
                                             "og:title"       : self.openGraphtitle,
                                             "og:description" : self.graphScore,
                                             "og:image"       : self.url,
                                             "dev_ckan:economy_type" : self.economy]
            
            let object : FBSDKShareOpenGraphObject = FBSDKShareOpenGraphObject(properties: properties as! [AnyHashable: Any])

            
            // Create the action
            let action : FBSDKShareOpenGraphAction = FBSDKShareOpenGraphAction.init()
            action.actionType = "dev_ckan:track_contribution"
            action.setObject(object, forKey: "dev_ckan:economy")
            
            // Create the content
            let content :  FBSDKShareOpenGraphContent = FBSDKShareOpenGraphContent.init()
            content.action = action
            content.previewPropertyName = "dev_ckan:economy"
            
            //FBSDKShareDialog.showFromViewController(self, withContent: content, delegate: nil)
            
            let shareDialog = FBSDKShareDialog.init()
            
            shareDialog.fromViewController = self
            shareDialog.shareContent = content
            if (!shareDialog.canShow()) {

            }
            if shareDialog.canShow(){
                shareDialog.show()
                self.FBLaunchCount += 1
            }
            
        } else {
            //print("require publish_actions permissions")
            self.dismiss(animated: false, completion: nil)

        }
    }
    
}
