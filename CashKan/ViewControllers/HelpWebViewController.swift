//
//  WebViewController.swift
//  CashKan
//
//  Created by emtarang on 26/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class HelpWebViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet var webView: UIWebView!
    
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()
    var urlToVisit:String!
    var isAddedAsChildVC : Bool = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        webView.delegate = self
        
        let button: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        button.setImage(UIImage(named: "Cashkans_Back arrow"), for: UIControlState())
        //add function for button
        button.addTarget(self, action: #selector(self.back(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton;
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        HELPER.setFontFamily( self.view, andSubViews: true)
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        NotificationCenter.default.addObserver(self, selector: #selector(self.closeWebView), name: NSNotification.Name(rawValue: "popHelpWebView"), object: nil)

       // self.navigationController?.navigationBar.barTintColor = UIColor(red: 42 / 255, green: 150 / 255,blue: 99 / 255, alpha: 1.0)
        
        self.navigationItem.title = "Help" //webViewUrls_title[HELPER.selectedWebViewIndex]
        
        if(Reachability.isConnectedToNetwork() == false){
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                
                self.navigationController!.pushViewController(secondViewController, animated: false)
                
            }
            
        }
        else{
            //UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let url = URL(string: urlToVisit);
            let requestObj = URLRequest(url: url!);
            webView.loadRequest(requestObj);
            
            //UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func back( _ sender: UIBarButtonItem) {
            if webView.canGoBack{
                webView.goBack()
            }
            else{
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                self.navigationController?.popViewController(animated: true)
            }
        
    }
    func closeWebView(){
        if(HELPER.isConnectedToNetwork){
            if(self.isAddedAsChildVC == false){
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    //#MARK: Web vie delegate methods
    
    public func webViewDidStartLoad(_ webView: UIWebView){
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        self.activityIndicator.showActivityIndicatory(self.webView)
        //self.navigationController?.navigationBar.isUserInteractionEnabled = false
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView){
        self.activityIndicator.hideActivityIndicator(self.webView)
        //self.navigationController?.navigationBar.isUserInteractionEnabled = true
        self.tabBarController?.tabBar.isUserInteractionEnabled = true

    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        //print(error.localizedDescription)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        if(HELPER.isConnectedToNetwork){
            if(self.isAddedAsChildVC == false){
                self.navigationController?.popViewController(animated: true)
            }
            else{
                self.isAddedAsChildVC = false
            }
        }
        
    }
    
    //    @IBAction func back(_ sender: AnyObject) {
    //
    //        if webView.canGoBack{
    //            webView.goBack()
    //        }
    //        else{
    //            if let resultController = storyboard!.instantiateViewController(withIdentifier: "login") as? UIViewController {
    //                present(resultController, animated: true, completion: nil)
    //            }
    //
    //        }
    //    }
    //
    //
    //
    //    @IBAction func close(_ sender: AnyObject) {
    //
    //        webView.stopLoading()
    //
    //    }
    //
    //    @IBAction func refresh(_ sender: AnyObject) {
    //        webView.reload()
    //    }
    //
    //
    //    @IBAction func forward(_ sender: AnyObject) {
    //
    //        if webView.canGoForward{
    //            webView.goForward()
    //        }
    //
    //
    //    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
