//
//  AppDelegate.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 14/06/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit
import CoreLocation
import HockeySDK
import Firebase
import FirebaseInstanceID
import FirebaseAnalytics
import UserNotifications
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import GoogleMaps
import GooglePlaces

var isLogin = String()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate, UNUserNotificationCenterDelegate, FIRMessagingDelegate {
    
    var window: UIWindow?
    let navigationController = UINavigationController()
    let defaults = UserDefaults.standard
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Facebook initialisation
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //Google map initialisation
        GMSServices.provideAPIKey("AIzaSyBI-bG_ZjNCWNHS2Q7pleRKTXGbtuiiirw")
        GMSPlacesClient.provideAPIKey("AIzaSyBI-bG_ZjNCWNHS2Q7pleRKTXGbtuiiirw")
        
        // frebase code
        FIRApp.configure()
        
        registerForRemoteNotification()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(tokenRefreshNotification(_:)),
                                               name: NSNotification.Name.firInstanceIDTokenRefresh,
                                               object: nil)
        
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            //print("InstanceID token: \(refreshedToken)")
        }
        
        // autologin code
        
        HELPER.notificationTag = ""
        
        if HELPER.currencySymbol == "Rs"{
            HELPER.currencySym = IndianCurrency
        }
        else{
            HELPER.currencySym = "$"
        }
        if let isAppAlreadyLaunchedOnce = defaults.string(forKey: "isAppAlreadyLaunchedOnce"){
            // app launched once
        }
        else{
            
            // app launching first time
            defaults.set(true, forKey: "isAppAlreadyLaunchedOnce") // is app already launched
            defaults.set(0 , forKey: "showInstructionsCnt")         // instructions count
            defaults.set(true, forKey: "showNotifications")         // show notifications
            defaults.synchronize()
        }
        
        self.setUpSplashScreen()
        
        return true
    }
    
    func registerForRemoteNotification() {
            
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
    }
    
    func setUpSplashScreen() -> Void{
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SplashScreen") as! SplashScreen
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
        
        
    }
    
    func setUpFirstScreen() -> Void{
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
        
    }
    
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            //print("InstanceID refreshed token: \(refreshedToken)")
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    //Called when a notification is delivered to a foreground app.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let notify = defaults.bool(forKey: "showNotifications")
        
        if(notify){
            
            //print("User Info = ",notification.request.content.userInfo)
            completionHandler([.alert, .badge, .sound])
            let userInfo = notification.request.content.userInfo
        }
        
    }
    
    
    //Called to let your app know which action was selected by the user for a given notification.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let notify = defaults.bool(forKey: "showNotifications")
        if(notify){
            //print("User Info = ",response.notification.request.content.userInfo)
            let userInfo = response.notification.request.content.userInfo
             HELPER.notificationTag = userInfo["TAG"] as! String
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ShowVCOnPushNotification"), object: userInfo)
            completionHandler()
        }
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // let tokenChars = UnsafeRawPointer(deviceToken.bytes)  //UnsafePointer<CChar>(deviceToken.bytes)
        //var tokenString = ""
        
//        for i in 0..<deviceToken.length {
//            tokenString += String(format: "%02.2hhx", arguments: [tokenChars.advanced(by: i) as! CVarArg])
//        }
        
        FIRInstanceID.instanceID().setAPNSToken(deviceToken as Data, type: FIRInstanceIDAPNSTokenType.sandbox)

        
        //FIRInstanceID.instanceID().setAPNSToken(deviceToken as Data, type: FIRInstanceIDAPNSTokenType.unknown)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        //print("Message ID: \(userInfo["gcm.message_id"]!)")
        
        // Print full message.
        //print( (userInfo))
        HELPER.notificationTag = userInfo["TAG"] as! String

        let notify = defaults.bool(forKey: "showNotifications")
        
        if(notify){
            let dict = defaults.object(forKey: "userinfo") as? [String: String] ?? [String: String]()
            let isRemember = dict["isremember"] != nil
            
            if ( application.applicationState == UIApplicationState.inactive || application.applicationState == UIApplicationState.background ){
                UIApplication.shared.applicationIconBadgeNumber = 0
                //opened from a push notification when the app was on background
                
                let Tag = userInfo["TAG"] as! String
                
                if (isRemember) {
                    if (  application.applicationState == UIApplicationState.background ){
                    //self.setUpSplashScreen()
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    if let resultController = storyboard.instantiateViewController(withIdentifier: "TabBar") as? UITabBarController {
                        //  resultController.selectedIndex = 2
                        self.window?.rootViewController = resultController
                        self.window?.makeKeyAndVisible()
                        HELPER.notificationTag = Tag
                        
                    }
                    }
                } else {
                    HELPER.notificationTag = Tag
                    self.setUpFirstScreen()
                }

            }
            
            if (application.applicationState == UIApplicationState.active ) {
                if !(ProcessInfo().isOperatingSystemAtLeast(OperatingSystemVersion(majorVersion: 10, minorVersion: 0, patchVersion: 0))) {
                // send local notification only when version is lower than 10
                //println("iOS >= 9.0.0")
                
                    //UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                    let localNotification : UILocalNotification = UILocalNotification.init()
                    localNotification.fireDate = Date(timeIntervalSinceNow: 5)
                    let alert = (userInfo["aps"] as! NSDictionary)["alert"] as! NSDictionary
                    let title = alert["title"] as! String
                    let body = alert["body"] as! String
                
                    localNotification.alertBody = body
                    localNotification.alertTitle = title
                    localNotification.alertAction = userInfo["TAG"] as! String
                    localNotification.timeZone = TimeZone.current
                    localNotification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
                    HELPER.notificationTag = userInfo["TAG"] as! String

                    UIApplication.shared.scheduleLocalNotification(localNotification)
                }
            }
            
        }
        
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        let notify = defaults.bool(forKey: "showNotifications")
        
        if(notify){
            let localNotification : UILocalNotification = UILocalNotification.init()
            localNotification.applicationIconBadgeNumber = 0;
            if ( application.applicationState == UIApplicationState.inactive || application.applicationState == UIApplicationState.background  ){
                
                //opened from a push notification when the app was on background
                
                let dict = defaults.object(forKey: "userinfo") as? [String: String] ?? [String: String]()
                let isRemember = dict["isremember"] != nil
                
                //opened from a push notification when the app was on background
                
                let Tag = notification.alertAction
                
                if (isRemember) {
                    
                    //self.setUpSplashScreen()
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    if let resultController = storyboard.instantiateViewController(withIdentifier: "TabBar") as? UITabBarController {
                        //  resultController.selectedIndex = 2
                        self.window?.rootViewController = resultController
                        self.window?.makeKeyAndVisible()
                        HELPER.notificationTag = Tag!
                        
                    }
                } else {
                    HELPER.notificationTag = Tag!
                    self.setUpFirstScreen()
                }
                
            }
        }
        
        if (application.applicationState == UIApplicationState.active ) {
            
            
            //            let alertController = UIAlertController(title: notification.alertTitle, message: notification.alertBody, preferredStyle: .Alert)
            //
            //            let okAction = UIAlertAction(title: alert_ok, style: .Cancel) { (action:UIAlertAction!) in
            //                //print("you have pressed the Cancel button");
            //                let defaults = NSUserDefaults.standardUserDefaults()
            //                let dict = defaults.objectForKey("userinfo") as? [String: String] ?? [String: String]()
            //                let isRemember = dict["isremember"] != nil
            //
            //                //opened from a push notification when the app was on background
            //
            //                let Tag =  notification.alertAction
            //
            //                if (isRemember) {
            //
            //                    //self.setUpSplashScreen()
            //                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //                    if let resultController = storyboard.instantiateViewControllerWithIdentifier("TabBar") as? UITabBarController {
            //                        //  resultController.selectedIndex = 2
            //                        self.window?.rootViewController = resultController
            //                        self.window?.makeKeyAndVisible()
            //                        HELPER.notificationTag = Tag!
            //
            //                    }
            //                } else {
            //                    HELPER.notificationTag = Tag!
            //                    //self.setUpFirstScreen()
            //                }
            //            }
            //            let cancelAction = UIAlertAction(title: alert_cancel, style: .Default) { (action:UIAlertAction!) in
            //                //print("you have pressed the Cancel button");
            //            }
            //            alertController.addAction(cancelAction)
            //            alertController.addAction(okAction)
            //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //
            //            let resultController = storyboard.instantiateViewControllerWithIdentifier("TabBar") as? UITabBarController
            //            self.window?.rootViewController = resultController
            //            self.window?.makeKeyAndVisible()
            //            self.window?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
            //
            
        }
        
        
    }
    func connectToFcm() {
        FIRMessaging.messaging().connect { (error) in
            if (error != nil) {
                //print("Unable to connect with FCM. \(error)")
            } else {
                //print("Connected to FCM.")
            }
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        FBSDKAppEvents.activateApp()

        FIRMessaging.messaging().connect { error in
            //print(error)
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        let keychainItemWrapper = KeychainItemWrapper(identifier:"YourAppLogin", accessGroup: nil)
        keychainItemWrapper.resetKeychain()
        
    }
    
    public func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage){
        
    }
    
       
//    func application(application: UIApplication, openURL url: URL, sourceApplication: String?, annotation: Any) -> Bool
//    {
//        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
//        
//    }
    
    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool
    {
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
    }

    
    
}

