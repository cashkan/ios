//
//  RewardsTableViewCell.swift
//  CashKan
//
//  Created by emtarang on 21/10/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit
protocol rewardsCustomCellDelegate {
    func redeemBtnTapped(_ sender: UIButton)
}

class RewardsTableViewCell : UITableViewCell {

    @IBOutlet var outerView: UIView!
    @IBOutlet var dividerView: UIView!
    @IBOutlet var descriptionView: UIView!
    @IBOutlet var arrowBtn: UIButton!
    @IBOutlet var rewardsImageButton: UIButton!
    @IBOutlet var pointsRequiredLbl: UILabel!
    @IBOutlet var descrLbl: UILabel!
    @IBOutlet var rewardsProviderLbl: UILabel!
    @IBOutlet var pointsValueLbl: UILabel!
    
    @IBOutlet var redeemBtn: UIButton!
    
    var delegate : rewardsCustomCellDelegate?
    
    override func awakeFromNib() {
        
        self.outerView.layer.borderColor = rewardsPink_color.cgColor
        self.outerView.layer.borderWidth = 1
        self.outerView.layer.cornerRadius = 10
        self.outerView.layer.masksToBounds = true
        
        self.dividerView.backgroundColor = rewardsPink_color
        self.redeemBtn.setTitleColor(rewardsPink_color, for: .normal)
        self.redeemBtn.layer.borderColor = rewardsPink_color.cgColor
        self.redeemBtn.layer.borderWidth = 3
        self.redeemBtn.layer.cornerRadius = 10
        self.redeemBtn.layer.masksToBounds = true

        self.descrLbl.textColor = textGray_color
        self.rewardsProviderLbl.textColor = textGray_color
        self.pointsValueLbl.textColor = textGray_color
        self.pointsRequiredLbl.textColor = textGray_color
        self.rewardsProviderLbl.textColor = rewardsPink_color
        
    }
    
    @IBAction func redeemBtnClicked(_ sender: AnyObject) {
        
        delegate?.redeemBtnTapped(sender as! UIButton)
    }
}
