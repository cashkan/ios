//
//  ReceiptController.swift
//  cashkan3
//
//  Created by emtarang on 14/06/16.
//  Copyright © 2016 emtarang. All rights reserved.
//

import UIKit


class ReceiptController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate, NetworkFailureDelegate {
    
    var animateDistance = CGFloat()
    
    var imageProf: UIImage!
    
    @IBOutlet var profileImageContainerView: UIView!
    @IBOutlet var imageLabel:    UILabel!
    @IBOutlet var captureImgBtn: UIButton!
    @IBOutlet var imgView:       UIImageView!
    @IBOutlet var uploadBtn:     UIButton!
    @IBOutlet var txtVendorName: UILabel!
    @IBOutlet var txtAmount:     UILabel!
    @IBOutlet var txtSpentOn:    UILabel!
    @IBOutlet var txtNotes:      UILabel!
    
    @IBOutlet var vendorNameTextfield   : UITextField!
    @IBOutlet var notesTextfield        : UITextField!
    @IBOutlet var spentOnTextfield      : UITextField!
    @IBOutlet var amountTextfield       : UITextField!
    
    @IBOutlet var imageViewContainer    : UIView!
    @IBOutlet var amountView            : UIView!
    @IBOutlet var spetOnView            : UIView!
    @IBOutlet var notesView             : UIView!
    @IBOutlet var vendorNameView        : UIView!
    
    @IBOutlet var scrollView: UIScrollView!
    var barButton           : UIBarButtonItem!
    //var isUploadClicked = false
    
    var picker:UIImagePickerController?=UIImagePickerController()
    var popover:UIPopoverController?=nil
    var activityIndicator   : CustomActivityIndicator = CustomActivityIndicator()
    var receiptController2  : ReceiptController2!
    var spentOn         : String!
    var imageFileName   : String = ""
    var sessionId       : NSNumber!
    var newImage : UIImage!
    var toHideNotificationButon: Bool = true
    
    var resultController : HelpWebViewController!
    var isAddedAsChildViewController : Bool = false
    
    func imageTapped(_ sender: UITapGestureRecognizer) {
        HELPER.isUploadClicked = true
        self.imageProf = nil
        self.imgView.image = nil
        self.openCamera()
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        HELPER.setFontFamily( self.view, andSubViews: true)
        
        // Do any additional setup after loading the view.
        
        
        self.uploadBtn.titleLabel?.font = UIFont(name: customAppFont, size:  buttonFontSize)
        
        
        let  button = UIButton(type: UIButtonType.custom) as UIButton
        
        //UIButton.buttonWithType(UIButtonType.Custom) as? UIButton
        button.setImage(UIImage(named: "Help_icon"), for: UIControlState())
        button.addTarget(self, action:#selector(ReceiptController.helpTap), for: UIControlEvents.touchDragInside)
        button.frame=CGRect(x: 0, y: 0, width: 16,height: 25)
        barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
        self.uploadBtn.layer.borderWidth = CGFloat(customButtonBorderWidth)
        self.uploadBtn.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        self.uploadBtn.layer.masksToBounds = true
        
        self.navigationItem.hidesBackButton = true
        
        let buttonBack: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        buttonBack.setImage(UIImage(named: "Cashkans_Back arrow"), for: UIControlState())
        //add function for button
        buttonBack.addTarget(self, action: #selector(ReceiptController.back(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        buttonBack.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        var barButton2 = UIBarButtonItem(customView: buttonBack)
        self.navigationItem.leftBarButtonItem = barButton2;
        
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        
        self.imgView.layer.borderWidth = CGFloat(customButtonBorderWidth)
        self.imgView.isUserInteractionEnabled = true
        self.imgView.backgroundColor = UIColor.white
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(ReceiptController.imageTapped(_:)))
        
        //Add the recognizer to your view.
        self.imgView.addGestureRecognizer(tapRecognizer)
        
        self.perform(#selector(ReceiptController.setStyleToCircularImageView(_:)), with: imgView, afterDelay: 0)
        
        setViewColor()
        
        vendorNameTextfield.delegate = self
        amountTextfield.delegate     = self
        spentOnTextfield.delegate    = self
        notesTextfield.delegate      = self
        spentOnTextfield.text = spentOn
        
        var tap2 : UITapGestureRecognizer = UITapGestureRecognizer(target: self,action: #selector(self.dismissKeyboard))
        self.scrollView.addGestureRecognizer(tap2)
        
        
        
    }
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RequestResponceHelper.delegate = self
        if self.receiptController2.notificationButton.isHidden ==  false{
            self.receiptController2.notificationButton.isHidden = true
            self.receiptController2.notificationButton.isUserInteractionEnabled = false
            toHideNotificationButon = false
        }
        else{
            toHideNotificationButon = true
        }
        
        
        HELPER.setFontFamily( self.view, andSubViews: true)
        self.navigationItem.title = uploadReceipt_title
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        
        // Change the navigation bar background color to blue.
        navigationController!.navigationBar.barTintColor = orangeReceipt_color
        
        setViewColor()
        
        if (self.imageProf != nil  ){
            
            let resizedImage : UIImage = HELPER.resizeImage(self.imageProf,size: CGSize(width: 1080, height: 1920))
            imgView.image               = resizedImage
            
            captureImgBtn.layer.isHidden  = true
            imageLabel.layer.isHidden     = true
            imgView.layer.borderWidth   = 0.0
        }
        else if(self.imgView.image == nil){
            captureImgBtn.layer.isHidden  = false
            imageLabel.layer.isHidden     = false
            imgView.layer.borderWidth   = 2.0
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if HELPER.isConnectedToNetwork == false{
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if HELPER.isUploadClicked {
            HELPER.isUploadClicked = false
        }else if HELPER.isConnectedToNetwork{
            
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func back(_ sender: UIBarButtonItem) {
        self.navigationItem.title = uploadReceipt_title
        if(isAddedAsChildViewController){
            if((self.resultController) != nil){
                resultController.willMove(toParentViewController: nil)
                resultController.view.removeFromSuperview()
                resultController.removeFromParentViewController()
                isAddedAsChildViewController = false
                self.navigationItem.rightBarButtonItem = barButton
            }
        }
        else{
            
            if self.toHideNotificationButon == false{
                self.receiptController2.notificationButton.isHidden = false
                self.receiptController2.notificationButton.isUserInteractionEnabled = true
                
            }
            // Go back to the previous ViewController
            self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    
    @IBAction func onCameraImageClicked(_ sender: AnyObject) {
        HELPER.isUploadClicked = true
        self.openCamera()
    }
    
    
    @IBAction func onUploadClick(_ sender: AnyObject) {
        
        uploadBtn.isUserInteractionEnabled = false
        vendorNameTextfield.resignFirstResponder()
        amountTextfield.resignFirstResponder()
        spentOnTextfield.resignFirstResponder()
        notesTextfield.resignFirstResponder()
        
        
        if(Reachability.isConnectedToNetwork() == false){
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                
                HELPER.isConnectedToNetwork = false
                
                self.navigationController!.pushViewController(secondViewController, animated: false)
                
            }
            
        }
        else{
            
            if((vendorNameTextfield.text?.isEmpty)!)
            {
                let alertController = UIAlertController(title: alert_uploadtitle, message:alert_validVendorName , preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                }
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion:nil)
                uploadBtn.isUserInteractionEnabled = true
                
            }
            else if((amountTextfield.text?.isEmpty)!){
                let alertController = UIAlertController(title: alert_uploadtitle, message: alert_validAmount, preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                }
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion:nil)
                uploadBtn.isUserInteractionEnabled = true
                
            }
            else if((spentOnTextfield.text?.isEmpty)!){
                
                let alertController = UIAlertController(title: alert_uploadtitle, message: alert_spentOn, preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                }
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion:nil)
                uploadBtn.isUserInteractionEnabled = true
                
                
            }
            else{
                var showerror : Bool = false
                
                if((amountTextfield.text!) != "")
                {
                    let cafe = amountTextfield.text
                    
                    if ((Double(amountTextfield.text!)! > (1000000.00))  || (cafe![cafe!.startIndex] == "0" )){
                        showerror = true
                    }
                    var decimals = 0
                    let fullString = amountTextfield.text
                    if(( fullString?.range(of: ".")) != nil){
                        let StringArray = fullString!.characters.split{$0 == "."}.map(String.init)
                        
                        if(StringArray.count < 2){
                            showerror = true
                        }
                        else{
                            decimals = StringArray[1].characters.count
                            if( decimals > 2){
                                showerror = true
                            }
                        }
                    }
                }
                
                if (showerror){
                    
                    
                    let alertController = UIAlertController(title: alert_uploadtitle, message: alert_amountDigitsValidation, preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                    }
                    alertController.addAction(okAction)
                    
                    self.present(alertController, animated: true, completion:nil)
                    uploadBtn.isUserInteractionEnabled = true
                }
                    
                    
                else{
                    
                    let amt = amountTextfield.text! as String
                    let spentOn =  spentOnTextfield.text! as String
                    let vendorName = vendorNameTextfield.text! as String
                    let notes  = notesTextfield.text! as String
                    var dict : [[String:AnyObject]]! = []
                    
                    if imageProf == nil{
                        
                        dict.insert(["amt":amt as AnyObject,"spentOn" :spentOn as AnyObject,"vendorName" : vendorName as AnyObject, "notes" : notes as AnyObject, "imageName" : "" as AnyObject, "imageArray" : "" as AnyObject],at: 0)
                        
                        let alertController = UIAlertController(title: alert_uploadtitle, message: alert_beforeUploadWithoutImage , preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(title: alert_ok, style: .default) { (action:UIAlertAction!) in
                            self.activityIndicator.showActivityIndicatory(self.view)
                            
                            let params : [String : AnyObject] = (["ckSnId" : self.sessionId,
                                                                  "rcpts" : dict] as NSDictionary) as! [String : AnyObject]
                            
                            self.sendUploadReceiptRequest(params)
                            
                        }
                        
                        let cancelAction = UIAlertAction(title: alert_cancel, style: .cancel) { (action:UIAlertAction!) in
                            self.uploadBtn.isUserInteractionEnabled = true
                        }
                        alertController.addAction(cancelAction)
                        alertController.addAction(okAction)
                        
                        self.present(alertController, animated: true, completion:nil)
                        
                    }
                    else{
                        // rename image
                        imageFileName = "RECEIPT_"
                        imageFileName += String(Date().timeIntervalSince1970)
                        imageFileName += ".jpeg"
                        let imageName = imageFileName as String
                        
                        self.activityIndicator.showActivityIndicatory(self.view)
                        
                        // resize image
                        let newSize = CGSize(width: 300,height: 300)
                        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
                        
                        // Actually do the resizing to the rect using the ImageContext stuff
                        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
                        (self.imgView.image!).draw(in: rect)
                        newImage = UIGraphicsGetImageFromCurrentImageContext()
                        UIGraphicsEndImageContext()
                        
                        let imgData : NSData = UIImagePNGRepresentation(self.newImage) as! NSData
                        
                        //var imageSize: Int = imgData.length
                        //print("size of image in MB: %f ", Double(Double(imageSize) / 1024.0)/1024.0)
                        
                        let base64Encoded = imgData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                        
                        dict.insert(["amt":amt as AnyObject,"spentOn" :spentOn as AnyObject,"vendorName" : vendorName as AnyObject, "notes" : notes as AnyObject, "imageName" : imageName as AnyObject, "imageArray" : base64Encoded as AnyObject],at: 0)
                        let params : [String : AnyObject] = (["ckSnId" : self.sessionId,
                                                              "rcpts" : dict] as NSDictionary) as! [String : AnyObject]
                        
                        self.sendUploadReceiptRequest(params)
                        
                    }
                }
                
            }
        }
        
        
    }
    
    func setViewColor(){
        
        uploadBtn.layer.borderColor     = lightOrange_color.cgColor
        uploadBtn.titleLabel?.textColor = lightOrange_color
        txtVendorName.textColor         = lightOrange_color
        txtAmount.textColor             = lightOrange_color
        txtSpentOn.textColor            = lightOrange_color
        txtNotes.textColor              = lightOrange_color
        uploadBtn.layer.borderColor     = lightOrange_color.cgColor
        imgView.layer.borderColor       = lightOrange_color.cgColor
        imageLabel.textColor            = lightOrange_color
        uploadBtn.setTitleColor(darkOrange_color, for: UIControlState())
        
    }
    
    func setStyleToCircularImageView(_ img : UIImageView ) {
        
        img.layer.cornerRadius = img.frame.size.width / 2;
        img.clipsToBounds = true
        
    }
    
    
    func openCamera()
    {
        
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        
        let cropper = storyboard1.instantiateViewController(withIdentifier: "cropper-vc") as! CropperViewController
        cropper.receipt = self
        
        
        let secondViewController = storyboard!.instantiateViewController(withIdentifier: "CaptureReceiptViewControllerId") as! CaptureReceiptViewController
        secondViewController.view.backgroundColor = UIColor.clear
        secondViewController.receipt = self
        self.modalPresentationStyle = UIModalPresentationStyle.currentContext
        
        
        self.present(
            secondViewController,
            animated: false,
            completion: nil)
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        picker .dismiss(animated: true, completion: nil)
        captureImgBtn.layer.isHidden  = true
        imageLabel.layer.isHidden     = true
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        // picker cancel
    }
    
    
    func helpTap()
    {
        
        vendorNameTextfield.resignFirstResponder()
        amountTextfield.resignFirstResponder()
        spentOnTextfield.resignFirstResponder()
        notesTextfield.resignFirstResponder()
        
        self.resultController = self.storyboard!.instantiateViewController(withIdentifier: "WebHelp") as? HelpWebViewController
        self.isAddedAsChildViewController = true
        self.navigationItem.title = "Help"
        self.resultController.isAddedAsChildVC = true
        self.resultController.urlToVisit = uploadReceipt_helpUrl
        self.addChildViewController(resultController)
        self.view.addSubview(resultController.view)
        resultController.didMove(toParentViewController: self)
        self.navigationItem.rightBarButtonItem = nil
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let textFieldRect : CGRect = self.view.window!.convert(textField.bounds, from: textField)
        let viewRect : CGRect = self.view.window!.convert(self.view.bounds, from: self.view)
        
        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
        let numerator : CGFloat = midline - viewRect.origin.y - MoveKeyboard.MINIMUM_SCROLL_FRACTION * viewRect.size.height
        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
        
        var heightFraction : CGFloat = numerator / denominator
        
        if heightFraction  > 1.0 {
            heightFraction = 1.0
        }
        
        let orientation : UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
        if (orientation == UIInterfaceOrientation.portrait || orientation == UIInterfaceOrientation.portraitUpsideDown) {
            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
        } else {
            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
        }
        
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y -= animateDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(TimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        
        self.view.frame = viewFrame
        
        UIView.commitAnimations()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y += animateDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        
        UIView.setAnimationDuration(TimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        
        self.view.frame = viewFrame
        
        UIView.commitAnimations()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        if textField.returnKeyType != UIReturnKeyType.done{
            let nextTag: NSInteger = textField.tag+1;
            let nextResponder: UIResponder = (textField.superview?.viewWithTag(nextTag))!
            
            if (nextResponder as UIResponder?) != nil{
                nextResponder.becomeFirstResponder()
            }
            
        }
        else{
            textField.resignFirstResponder()
        }
        
        return false;
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        var result = true
        if(textField == vendorNameTextfield){
            
            if string.characters.count > 0 {
                let disallowedCharacterSet = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"+" "+"!@#$%^&*()_+{}[]|<>,.~`/:;?-=\\¥'£•¢\'").inverted
                let replacementStringIsLegal = string.rangeOfCharacter(from: disallowedCharacterSet) == nil
                result = replacementStringIsLegal
            }
        }
        
        if(textField == amountTextfield){
            
            
            let maxLength = 9
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            if(newString.length <= maxLength){
                
            }
            else{
                return false
                
            }
            if string.characters.count > 0 {
                let disallowedCharacterSet = CharacterSet(charactersIn: "0123456789.").inverted
                let replacementStringIsLegal = string.rangeOfCharacter(from: disallowedCharacterSet) == nil
                result = replacementStringIsLegal
            }
        }
        if(textField == spentOnTextfield){
            
            
            let maxLength = 25
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            if(newString.length <= maxLength){
                
            }
            else{
                return false
                
            }
        }
        
        
        return result
        
    }
    
    func sendUploadReceiptRequest( _ dictParams : Dictionary<String, AnyObject>){
        
        
        self.uploadReceipt(dictParams)
        {
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            
            if( success == true){
                
                if (object as? HTTPURLResponse) != nil{
                    
                    // You can print out response object
                    
                }
                
                do {
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                    self.activityIndicator.hideActivityIndicator(self.view)
                    self.uploadBtn.isUserInteractionEnabled = true
                    
                    if let successMsg = myJSON![display_msg]{
                        self.receiptController2.activityIndicator.showActivityIndicatory(self.receiptController2.view)
                        self.receiptController2.objSEFilterControl.setSelectedIndex(3, animated: true)
                        HELPER.searchDateFilterIndex = 3
                        self.receiptController2.getReceiptsObjects(HELPER.searchDateFilterIndex)
                        HELPER.isJourneyRefreshed = false
                        if self.toHideNotificationButon == false{
                            self.receiptController2.notificationButton.isHidden = false
                        }
                        self.navigationController?.popViewController(animated: true)
                        //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        
                    }
                    
                    
                }
                catch {
                    //print(error)
                    self.catchAnyException()
                }
            }
                
            else{
                
                // If uploading failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        self.activityIndicator.hideActivityIndicator(self.view)
                        self.uploadBtn.isUserInteractionEnabled = true
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            let error = myJSON![display_msg]
                            let alertController = UIAlertController(title: alert_uploadReceipt, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }
                            
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        //print(error)
                        self.catchAnyException()
                    }
                    
                }
            }
            
            
        }
    }
    
    func uploadReceipt(_ dictParams: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
        let id =  HELPER.user_id.stringValue
        // HELPER.isUploadClicked = true
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest("/users/"+id+uploadReceipt_url, params: dictParams )
        
        RequestResponceHelper.post(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? NSData{
                        //message = passedMessage
                        completion(success, object ,dataObject as Data ,message)
                    }
                    else{
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }
    
    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
            
            self.navigationController!.pushViewController(secondViewController, animated: false)
        }
        
    }
    
    func catchAnyException(){
        self.activityIndicator.hideActivityIndicator(self.view)
        self.uploadBtn.isEnabled = true
        let error = "Server Error : Uploading receipt failed..."
        let alertController = UIAlertController(title: alert_uploadReceipt, message: error as? String, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            //print("you have pressed the Cancel button");
        }
        
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
}
