//
//  ScannedCashViewController.swift
//  CashKan
//
//  Created by emtarang on 13/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit
import CoreLocation


class CustomScanCashCell : UITableViewCell{


    @IBOutlet var imgView: UIImageView!
    @IBOutlet var lbl: UILabel!
}

extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

class ScannedCashViewController: UIViewController,UITableViewDataSource, UITableViewDelegate ,UITextFieldDelegate, CLLocationManagerDelegate, NetworkFailureDelegate{
    
    var cashViewController : CashViewController! // = CashViewController!
    @IBOutlet var maxCharLabel: UILabel!
    let locationManager     = CLLocationManager()
    var activityIndicator   : CustomActivityIndicator = CustomActivityIndicator()
    var cashkanBills        : [(NSString,NSString)]! = []
    var finalCurrencyArray  : [(NSNumber,NSNumber,NSString,NSString)]!   = []
    var totalScannedValue   : String!
    var totalScannedCount   : String! = "0"
    var latitude            : String! = ""
    var longitude           : String! = ""

    @IBOutlet var scanLabel:                UILabel!
    @IBOutlet var searchTxt:                UITextField!    // search text field
    @IBOutlet var searchResultTableView:    UITableView!
    @IBOutlet var sureBtn:                  UIButton!       // submit buton
    @IBOutlet var notSureBtn:               UIButton!
    @IBOutlet var searchBarView:            UIView!         // container view for table
    @IBOutlet var searchView:               UIView!         // container view for search tet field
    
    
    @IBOutlet var bottomButtonsView: UIView!
    
    var spendObjects        : [(NSNumber,String,String)]! = []
    var filteredArray       : [(NSNumber,String,String)]! = []
    var objectImages        : [Data]! = []
    var selectedObjectIndex : Int = 1000
    var names               : [String]! = []
    var filtered            : [String] = []
    var searchActive        : Bool = false
    var searchOnTop         : Bool = false
    var shouldOpenReceipt   : Bool! = false
    var animateDistance     = CGFloat()
    var cash                :  CashViewController!
    var heightFraction      : CGFloat = 0.0
    
    var isAddedAsChildViewController : Bool = false
    var resultController : HelpWebViewController!
    var barButtonHelp    : UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HELPER.setFontFamily( self.view, andSubViews: true)

        searchTxt.autocapitalizationType = UITextAutocapitalizationType.sentences        

        self.navigationItem.title = scanCash_title
        
        let cashcolor = UIColor(red: 111/255, green: 202/255, blue: 147/255, alpha: 1)
        
        self.navigationItem.hidesBackButton = true
        
        let buttonBack: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        buttonBack.setImage(UIImage(named: "Cashkans_Back arrow"), for: UIControlState())
        //add function for button
        buttonBack.addTarget(self, action: #selector(ScannedCashViewController.back(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        buttonBack.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        
        var barButton = UIBarButtonItem(customView: buttonBack)
        self.navigationItem.leftBarButtonItem = barButton;

        let longString = spendString
        let stringToReplace = "value"
        
        let longestWord = String(totalScannedValue)
        
        let long_spendString = longString.replacingOccurrences(of: stringToReplace, with: String(totalScannedValue))
        //print(long_spendString)
        
        let longestWordRange = (long_spendString as NSString).range(of: longestWord!)
        
        let attributedString = NSMutableAttributedString(string: long_spendString, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 20), NSForegroundColorAttributeName : UIColor.black])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 25), NSForegroundColorAttributeName : UIColor.black], range: longestWordRange)
        
        scanLabel.attributedText = attributedString
        
        searchTxt.delegate = self
        
        //searchResultTableView.register(UITableViewCell.self, forCellReuseIdentifier: "searchCell")
        searchResultTableView.delegate = self
        searchResultTableView.dataSource = self
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        //locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
  
        searchTxt.addTarget(self, action: #selector(ScannedCashViewController.searchTextFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        // set buttton attributes
        
        let buttonColor = UIColor(red: 42/255, green: 150/255, blue: 99/255, alpha: 1)
        
        sureBtn.layer.borderColor = buttonColor.cgColor
        sureBtn.layer.borderWidth = CGFloat(customButtonBorderWidth)
        sureBtn.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        sureBtn.clipsToBounds = true
        
        notSureBtn.layer.borderColor = buttonColor.cgColor
        notSureBtn.layer.borderWidth = CGFloat(customButtonBorderWidth)
        notSureBtn.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        notSureBtn.clipsToBounds = true
        
        sureBtn.setTitleColor(buttonColor, for: UIControlState())
        notSureBtn.setTitleColor(buttonColor, for: UIControlState())
        
        // set search result table view attributes
        
        searchResultTableView.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        searchResultTableView.layer.masksToBounds = true
        searchResultTableView.clipsToBounds = true
        searchBarView.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        searchBarView.layer.shadowColor  = darkGray_color.cgColor
        searchBarView.layer.shadowOpacity  = 1
        searchBarView.layer.shadowRadius = 3.0
        searchBarView.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        searchBarView.isHidden = true
        searchView.layer.backgroundColor = cashcolor.cgColor
        searchView.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        searchBarView.isHidden = true
        searchResultTableView.bounces = false
        maxCharLabel.textColor = textGray_color


        self.navigationController?.navigationBar.barTintColor = UIColor(red: 42 / 255,
            green: 150 / 255,
            blue: 99 / 255,
            alpha: 1.0)
        
        
        let button: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        button.setImage(UIImage(named: "Help_icon"), for: UIControlState())
        //add function for button
        button.addTarget(self, action: #selector(self.helpTap), for: UIControlEvents.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 16, height: 25)
        
        barButtonHelp = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButtonHelp;
        
        self.searchResultTableView.rowHeight = 52
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(_:)))
        
        //Add the recognizer to your view.
        self.view.addGestureRecognizer(tapRecognizer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        searchResultTableView.isHidden = true
        maxCharLabel.isHidden = false
        searchBarView.isHidden = true
    }
    
    func back(_ sender: UIBarButtonItem) {
        self.navigationItem.title = scanCash_title
        if(isAddedAsChildViewController){
            if((self.resultController) != nil){
                resultController.willMove(toParentViewController: nil)
                resultController.view.removeFromSuperview()
                resultController.removeFromParentViewController()
                isAddedAsChildViewController = false
                self.navigationItem.rightBarButtonItem = barButtonHelp
            }
        }
        else{
        // Go back to the previous ViewController
            HELPER.isCashAlive = true
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = scanCash_title
        HELPER.setFontFamily( self.view, andSubViews: true)
        RequestResponceHelper.delegate = self
        self.notSureBtn.titleLabel?.font = UIFont(name: customAppFont, size:  buttonFontSize)
        self.sureBtn.titleLabel?.font = UIFont(name: customAppFont, size: buttonFontSize )
        self.searchResultTableView.isUserInteractionEnabled = true
        self.searchBarView.isUserInteractionEnabled = true
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        
        let longString = spendString
        let stringToReplace = "value"
        
        let longestWord = String(totalScannedValue)
        
        let long_spendString = longString.replacingOccurrences(of: stringToReplace, with: String(totalScannedValue))
        //print(long_spendString)
        
        let longestWordRange = (long_spendString as NSString).range(of: longestWord!)
        
        let attributedString = NSMutableAttributedString(string: long_spendString, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 20), NSForegroundColorAttributeName : UIColor.black])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 25), NSForegroundColorAttributeName : UIColor.black], range: longestWordRange)
        
        scanLabel.attributedText = attributedString
        
        self.view.isUserInteractionEnabled = true
        self.view.backgroundColor = UIColor.white
        
        if (HELPER.backToRoot) {
            HELPER.backToRoot = false
            self.navigationController?.popViewController(animated: false)
        }
        else{
            if shouldOpenReceipt == true{
                openReceiptController()
            }
            else{
            if(Reachability.isConnectedToNetwork() == false){
                
                DispatchQueue.main.async {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                    
                    HELPER.isConnectedToNetwork = false
                    
                    self.navigationController!.pushViewController(secondViewController, animated: false)
                    
                }
                
            }
            else{
                

            activityIndicator.showActivityIndicatory(self.view)
            getObjectives()
            }
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        if (HELPER.isConnectedToNetwork){
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func helpTap()
    {
        self.searchTxt.resignFirstResponder()
         resultController = self.storyboard!.instantiateViewController(withIdentifier: "WebHelp") as? HelpWebViewController
            isAddedAsChildViewController = true
            self.resultController.isAddedAsChildVC = true
            self.navigationItem.title = "Help"
            self.resultController.urlToVisit = scanObj_helpUrl
            self.addChildViewController(resultController)
            self.view.addSubview(resultController.view)
            resultController.didMove(toParentViewController: self)        
            self.navigationItem.rightBarButtonItem = nil
    }

    func viewTapped(_ recognizer : UITapGestureRecognizer){
        searchResultTableView.isHidden = true
        maxCharLabel.isHidden = false
        searchBarView.isHidden = true
    }
    
    func callReceiptController(){

        self.tabBarController?.selectedIndex = 1
        self.tabBarController?.tabBar.tintColor =  UIColor(red: 191/255, green: 81/255, blue: 64/255, alpha: 1)
        HELPER.isCashAlive = false

        self.navigationController?.popViewController(animated: false)

    }
    
    func searchTextFieldDidChange(_ textField: UITextField) {
        
        let searchText: String  = textField.text!
        
        if searchText.characters.count == 0{
            searchActive = false;
            searchResultTableView.isHidden = true
            maxCharLabel.isHidden = false

            searchBarView.isHidden = true
        } else {
            filtered = names.filter({ (text) -> Bool in
                let tmp: NSString = text as NSString
                let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
            if(filtered.count == 0){
                searchActive = false;
            } else {
                searchActive = true;
                //let previousFilteredArrayCount = filteredArray.count
                //let previousHeight = searchView.frame.height + self.searchBarView.frame.height + 10
                changeFilteredArray()
                self.searchResultTableView.reloadData()
                self.UITableView_Auto_Height()
                
//                heightFraction = searchView.frame.height + self.searchBarView.frame.height + 10
//                
//                
//                if (!searchOnTop) {
//                    
//                    if (filteredArray.count > 1){
//
//                        UIView.animate(withDuration: 0.0, animations:{self.searchBarView.frame = CGRect(x: self.searchBarView.frame.origin.x,y: self.searchBarView.frame.origin.y - self.heightFraction ,width: self.searchBarView.frame.width,height: self.searchBarView.frame.height);})
//                    }
//                    
//                
//                } else {
//                    
//                    if(previousFilteredArrayCount > 1){
//                    UIView.animate(withDuration: 0.0, animations:{self.searchBarView.frame = CGRect(x: self.searchBarView.frame.origin.x,y:  self.searchBarView.frame.origin.y + previousHeight,width: self.searchBarView.frame.width,height: self.searchBarView.frame.height);})
//                    
//                    searchOnTop = false;
//                    }
//                    
//                    if (filteredArray.count > 1){
//                        UIView.animate(withDuration: 0.0, animations:{self.searchBarView.frame = CGRect(x: self.searchBarView.frame.origin.x,y: self.searchBarView.frame.origin.y - self.heightFraction ,width: self.searchBarView.frame.width,height: self.searchBarView.frame.height);})
//                        searchOnTop = true;
//                    }
//                }
            }
        }
    
    }
    
    func changeFilteredArray(){
        for fCnt in 0  ..< filtered.count {
            
    for count in 0 ..< spendObjects.count{
    if filtered[fCnt] == spendObjects[count].2 {
    
    let (id,img,title) = spendObjects[count]
    filteredArray.insert((id,img,title), at: fCnt)
    
    break
            }
    }
    }
}
    func UITableView_Auto_Height()
    {
        //print(searchResultTableView.contentSize.height)

        let keyboardY = self.view.frame.height-MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT
        let heightTobe = keyboardY - self.searchBarView.frame.origin.y
        
            //let heightTobe = self.bottomButtonsView.frame.origin.y  - self.searchBarView.frame.origin.y - 30 //10
            var frame: CGRect = self.searchResultTableView.frame;
            if self.searchResultTableView.contentSize.height > heightTobe{
                frame.size.height = heightTobe
            }
            else{
                frame.size.height = self.searchResultTableView.contentSize.height
            }
            self.searchResultTableView.frame = frame;
            let heightConstraint = NSLayoutConstraint (item: searchResultTableView,attribute: NSLayoutAttribute.height,relatedBy: NSLayoutRelation.equal,toItem: nil,attribute: NSLayoutAttribute.notAnAttribute ,multiplier: 1,constant: frame.size.height)
            NSLayoutConstraint.activate([heightConstraint])

            self.view.addConstraint(heightConstraint)
            
            
        //}
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return filtered.count
        }
        
        return spendObjects.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath)
        let reuseIdentifier = "searchCell"
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! CustomScanCashCell
        cell.backgroundColor = lightGray_color
        
        
        if(searchActive){
            if filteredArray.count > 0{
            self.searchResultTableView.isHidden = false
            maxCharLabel.isHidden = true

            self.searchBarView.isHidden = false

                let count = (indexPath as NSIndexPath).row
                    let (id,cirimage,title) = filteredArray[count]
                
                        let longString = title
                        let longestWord = searchTxt.text
                        
                        let longestWordRange = (longString as NSString).range(of: longestWord!)
                        
                        let attributedString = NSMutableAttributedString(string: longString, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 15), NSForegroundColorAttributeName : UIColor.black])
                        
                        attributedString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 15), NSForegroundColorAttributeName : UIColor.black], range: longestWordRange)
                        
                        cell.lbl.attributedText = attributedString
                
                loadImageFromUrl(cirimage,view: (cell.imgView))
                let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.CellTapped(_:)))
                cell.addGestureRecognizer(tapRecognizer)

            }
            
        } else {
            self.searchResultTableView.isHidden = true
            self.searchBarView.isHidden = true
            maxCharLabel.isHidden = false

            
        }
        
        return cell
    }
    
    func CellTapped(_ recognizer  : UITapGestureRecognizer){
        
        let tapLocation = recognizer.location(in: self.searchResultTableView)
        let indexPath : IndexPath = self.searchResultTableView.indexPathForRow(at: tapLocation)!
        
        searchTxt.text = filteredArray[indexPath.row].2
        selectedObjectIndex = indexPath.row
        self.searchResultTableView.isHidden = true
        self.searchBarView.isHidden = true
        self.maxCharLabel.isHidden = false
    }
    
    @IBAction func notSureBtnClick(_ sender: AnyObject) {
        HELPER.isReceiptRefreshed = false
        HELPER.isJourneyRefreshed = false
        
        // reset search filter
        HELPER.search_vendorName = ""
        HELPER.search_min_amount = "0"
        HELPER.search_max_amount = "0"
        HELPER.search_spentOn = ""
        HELPER.search_fromDate = ""
        HELPER.search_toDate = ""
        HELPER.isSearchActive = false

        sendSessionObject(sender as! UIButton)
    }
    
    @IBAction func submitBtnClick(_ sender: AnyObject) {
        
        if (searchTxt.text!.isEmpty)  {
            let alertController = UIAlertController(title: alert_cashkan, message: "Please select a spend objective", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion:nil)
        }
        else{
            HELPER.isReceiptRefreshed = false
            HELPER.isJourneyRefreshed = false
            // reset search filter
            HELPER.search_vendorName = ""
            HELPER.search_min_amount = "0"
            HELPER.search_max_amount = "0"
            HELPER.search_spentOn = ""
            HELPER.search_fromDate = ""
            HELPER.search_toDate = ""
            HELPER.isSearchActive = false
            sendSessionObject(sender as! UIButton)
            //self.showPopOver(0)
        }
    }
    
    func showPopOver(_ buttonStatus: Int16){
        
        if( HELPER.showSuccessDialogue ){

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ScanCashPopUp") as! ScannedCashPopUpViewController
            //vc.view.backgroundColor = UIColor.clearColor()
            //view.alpha = 0.5
            navigationController?.navigationBar.alpha = 0.5
            tabBarController?.tabBar.alpha = 0.5
            tabBarController?.tabBar.isUserInteractionEnabled = false
            vc.cash = self
            vc.rejectedDenominationValue = []
        
            self.modalPresentationStyle =  UIModalPresentationStyle.overCurrentContext
            self.present(vc, animated:false, completion: nil)
        }
        else{
            self.callReceiptController()
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        var nameOfString = textField.text
        nameOfString?.capitalizeFirstLetter()
        
        let textFieldRect : CGRect = self.view.window!.convert(textField.bounds, from: textField)
        let viewRect : CGRect = self.view.window!.convert(self.view.bounds, from: self.view)
        
//        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
//        let numerator : CGFloat = midline - viewRect.origin.y - MoveKeyboard.MINIMUM_SCROLL_FRACTION * viewRect.size.height
//        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
        
//        var heightFraction : CGFloat = numerator / denominator
//        
//        if heightFraction  > 1.0 {
//            heightFraction = 1.0
//        }
//        
//        let orientation : UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
//        if (orientation == UIInterfaceOrientation.portrait || orientation == UIInterfaceOrientation.portraitUpsideDown) {
//            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
//        } else {
//            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
//        }
//        
//        var viewFrame : CGRect = self.view.frame
//        viewFrame.origin.y -= animateDistance
//        
//        UIView.beginAnimations(nil, context: nil)
//        UIView.setAnimationBeginsFromCurrentState(true)
//        UIView.setAnimationDuration(TimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        
       // self.view.frame = viewFrame
        
        //UIView.commitAnimations()
        
 
        
   }
    func textFieldDidEndEditing(_ textField: UITextField) {
        var viewFrame : CGRect = self.view.frame
        //viewFrame.origin.y += animateDistance
        
        //UIView.beginAnimations(nil, context: nil)
        //UIView.setAnimationBeginsFromCurrentState(true)
        
        //UIView.setAnimationDuration(TimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        
        //self.view.frame = viewFrame
        
        //UIView.commitAnimations()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        if textField.returnKeyType != UIReturnKeyType.done{
            let nextTag: NSInteger = textField.tag+1;
            let nextResponder: UIResponder = (textField.superview?.viewWithTag(nextTag))!
            
            if let res = nextResponder as UIResponder?{
                nextResponder.becomeFirstResponder()
            }
            
        }
        else{
            textField.resignFirstResponder()
            searchResultTableView.isHidden = true
            maxCharLabel.isHidden = false
            searchBarView.isHidden = true
            
            
//            if( !searchResultTableView.isHidden ){
//                if (searchOnTop){
//                    let xPosition = searchBarView.frame.origin.x
//                    let yPosition = searchBarView.frame.origin.y
//                    let height = searchBarView.frame.size.height
//                    let width = searchBarView.frame.size.width
//                    
//                    UIView.animate(withDuration: 0.5, animations:{self.searchBarView.frame = CGRect(x: xPosition,y: yPosition + self.heightFraction,width: width,height: height);})
//                    searchOnTop = false;
//                }
//            }

        }
        
        return false;
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
    }
    
    func openReceiptController(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "ReceiptController2Id") as! ReceiptController2
        HELPER.isCashAlive = false
        self.present(secondViewController, animated: false, completion: nil)

    
    }
    
    func showDuplicateValuePopOver(_ originalScannedAmount : NSNumber,actualAmt : NSNumber ,rejectedDenominationValue: [(String,String)]){
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ScanCashPopUp") as! ScannedCashPopUpViewController
        vc.cash = self
        vc.originalScannedAmount = originalScannedAmount
        vc.actualAmt = actualAmt
        vc.rejectedDenominationValue = rejectedDenominationValue
        vc.totalScannedCount = self.totalScannedCount
        
        //vc.view.backgroundColor = UIColor.clearColor()
        //view.alpha = 0.5
        navigationController?.navigationBar.alpha = 0.5
        tabBarController?.tabBar.alpha = 0.5
        tabBarController?.tabBar.isUserInteractionEnabled = false
        
        self.modalPresentationStyle =  UIModalPresentationStyle.currentContext
        self.present(vc, animated:false, completion: nil)

    }
    
    
    
    func getObjectives(){
     
        self.GetObjectives(){
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            
            if( success == true){
                
                if let response = object as? HTTPURLResponse{
                    
                    // You can print out response object
                    //print("response = \(response)")
                    
                }
                
                
                do {
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                    
                    if let parseJSON = myJSON![data]{
                        //print(parseJSON)
                       if (parseJSON as AnyObject).count > 0{
                            for var i in 0 ..< (parseJSON as AnyObject).count  {
                                let obj = (myJSON!["data"] as! NSArray)[i] as! NSDictionary
                                
                                let image = obj["img_url"] as! String
                                let descr = obj["title"] as! String
                                let id = obj["id"] as! NSNumber
                                
                                self.names.insert(descr, at: i)
                                self.spendObjects.insert((id,image,descr), at: i)
                                                            }
                        
                            self.activityIndicator.hideActivityIndicator(self.view)
                            self.searchResultTableView.reloadData()
                            self.UITableView_Auto_Height()
                            
                        }
                            
                        else{
                            self.activityIndicator.hideActivityIndicator(self.view)
                        
                        }
                    }
                }
                catch {
                    //print(error)
                }
                
            }
                
            else{
                
                // If request failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            let error = myJSON![display_msg]
                            let alertController = UIAlertController(title: alert_cashkan, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        //print(error)
                    }
                    
                }
            }
        }
    }
    

    
    func loadImageFromUrl(_ url: String, view: UIImageView){
    
        let url = URL(string: url)!
    
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
    
                // execute in UI thread
                DispatchQueue.main.async(execute: { () -> Void in
                    view.image = UIImage(data: data)
                })
            }
    }) 
    
    // Run task
    task.resume()
}
    
    func GetObjectives( _ completion: @escaping ( _ success: Bool,  _ object: AnyObject?,   _ dataObject: Data? ,  _ message: String?) -> ()) {
        
        let id =  HELPER.user_id.stringValue
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest("/users/"+id+get_spendObjective_url, params: nil )
        RequestResponceHelper.get(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.sync(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? Data{
                        //message = passedMessage
                        completion(success, object ,dataObject ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //print("Error while updating location " + error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            
            if (error != nil) {
                return
            }
            
            if placemarks!.count > 0 {
                
                self.latitude =  String(format:"%f", (self.locationManager.location?.coordinate.latitude)!)
                
                self.longitude = String(format:"%f", (self.locationManager.location?.coordinate.longitude)!)
                
                
            } else {
                //print("Problem with the data received from geocoder")
            }
        })
    }

    func sendSessionObject( _ sender : UIButton){
        if(Reachability.isConnectedToNetwork() == false){
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                
                HELPER.isConnectedToNetwork = false
                
                self.navigationController!.pushViewController(secondViewController, animated: false)
                
            }
            
        }
        else{
            
        var spendObjNameStr : String = ""
        if sender.tag == 1 {
           spendObjNameStr =  "Not Specified"
        }
        if sender.tag == 2 {
            spendObjNameStr = searchTxt.text! //filteredArray[selectedObjectIndex].2
        }
        
        let nameStrr = "mySession"
        var dict : [[String:AnyObject]]! = []
        
        for i in 0 ..< finalCurrencyArray.count {
            
            let (l,c,d,b) = finalCurrencyArray[i]
            var denFlag : NSString  = "false"
            if( b == ""){
                denFlag = "true"
            }
            
            dict.insert(
                ["localeId" : l ,
                    "currencyId" : c,
                    "denomination" : d ,
                    "currencyNo" : b,
                    "denominationFlag" : denFlag],  at: i)
            
        }

        let paramsObj : [String : AnyObject] = ([
            "name" : nameStrr ,
            "spendObjName" : spendObjNameStr,
            "lat" : self.latitude,
            "lng" : self.longitude,
            "cashkan" : dict  ] as NSDictionary) as! [String : AnyObject]

        self.totalScannedCount = String(dict.count)
        self.activityIndicator.showActivityIndicatory(self.view)
        

        self.addSession(paramsObj ){
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            
            if( success == true){
                
                if let response = object as? HTTPURLResponse{
                    
                    // You can print out response object
                    //print("response = \(response)")
                    
                }
                
                
                do {
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                    //print(myJSON)
                    
                    if let parseJSON = myJSON![data] as! [String:Any]?{
                        
                        let dataArray = parseJSON as! NSDictionary
                        if (parseJSON as AnyObject).count > 0{
                            
                            if let duplicateCurrencies = dataArray["currencies"] {
                                
                                let originalScannedAmmount = parseJSON["origAmt"] as! NSNumber
                                let duplicateScannedAmmount = parseJSON["dupAmt"] as! NSNumber
                                let actualAmt = originalScannedAmmount.doubleValue - duplicateScannedAmmount.doubleValue
                                
                                var rejectedDenominationValue : [(String, String)] = []
                                for i in 0 ..< (duplicateCurrencies as AnyObject).count{
                                
                                    let dummyArray = (duplicateCurrencies as! NSArray)[i] as! NSDictionary
                                    let number = dummyArray["currencyNo"] as! String
                                    let value = dummyArray["denomination"] as! NSNumber
                                    
                                    rejectedDenominationValue.insert((number,String(describing: value)), at: i)
                                }
                                
                                self.showDuplicateValuePopOver( originalScannedAmmount,actualAmt: NSNumber(value: Double(actualAmt)),rejectedDenominationValue: rejectedDenominationValue)
                            }
                            else{
                                self.showPopOver(0)

                            }
                            self.activityIndicator.hideActivityIndicator(self.view)
                            
                        }
                            
                        else{
                            self.showPopOver(0)
                            self.activityIndicator.hideActivityIndicator(self.view)
                            
                        }
                        
                    }
                }
                catch {
                    //print(error)
                }
                
            }
                
            else{
                
                // If request failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        
                        if (myJSON![display_msg]! as AnyObject).length != 0{
                            let error = myJSON![display_msg]
                            let alertController = UIAlertController(title: alert_cashkan, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        //print(error)
                    }
                    
                }
            }
            }
        }
    }


    func addSession( _ dictParams: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
        
        let id =  HELPER.user_id.stringValue
        HELPER.addUuid = true
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest("/users/"+id+cashkan_session_url, params: dictParams )
        RequestResponceHelper.post(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.sync(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? NSData{
                        //message = passedMessage
                        completion(success, object ,dataObject as Data ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }
    
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                                                 replacementString string: String) -> Bool
    {
        
        let result = true
        if(textField == searchTxt)
        {
            let maxLength = 25
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        
        return result
    }


    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        //maxCharLabel.isHidden = true
        
        return true
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if(textField == searchTxt){
            
           
            if(searchTxt.text == "")
            {
                //maxCharLabel.isHidden = false
            }
            else{
                //maxCharLabel.isHidden = true
            }
            
        }
        return true
    }
    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
            
            self.navigationController!.pushViewController(secondViewController, animated: false)
        }
        
    }

    
    
}
