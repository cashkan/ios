//
//  CashkansViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 15/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit
import Social
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import AMPopTip

class CashkansViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, SlideMenuDelegate,UIWebViewDelegate, NetworkFailureDelegate, FBSDKSharingDelegate  {
    
   // @IBOutlet var totalTEVLabel        : UILabel!
    
    @IBOutlet var totalTEVLabel: UILabel!
    
    @IBOutlet var savingsScoreButton    : UIButton!
    @IBOutlet var progressBarViewOne    : UIView!
    @IBOutlet var firstProgressView     : UIProgressView!
    @IBOutlet var secondProgressView    : UIProgressView!
    @IBOutlet var spendObjectiveLabel   : UILabel!
    @IBOutlet var overSpentLabel        : UILabel!
    @IBOutlet var actualSpentLabel      : UILabel!
    @IBOutlet var savingsLabel          : UILabel!
    @IBOutlet var inActiveLabel         : UILabel!
    @IBOutlet var activeLabel           : UILabel!
    @IBOutlet var centerActualSpentValueLabel   : UILabel!
    @IBOutlet var centerActualSpentLabel        : UILabel!
    @IBOutlet var centerObjectiveLabel          : UILabel!
    @IBOutlet var savingsView       : UIView!
    @IBOutlet var spendObjectiveView: UIView!
    @IBOutlet var overSpentView     : UIView!
    @IBOutlet var actualSpentView   : UIView!
    @IBOutlet var centerObjectiveView   : UIView!
    @IBOutlet var cashkanCollectionView : UICollectionView!
    @IBOutlet var centerActualSpentView : UIView!
    @IBOutlet var fshareButton          : UIButton!
    @IBOutlet var centerObjectiveValueLabel : UILabel!
    @IBOutlet var savingSummaryDividerview  : UIView!
    @IBOutlet var cashkanJourneyDividerView : UIView!
    @IBOutlet var scrollView                : UIScrollView!
    @IBOutlet var carsCollectionView_height : NSLayoutConstraint!

    @IBOutlet var refreshView       : UIView!
    @IBOutlet var ecsScoreView      : UIView!
    
    var denomination    :  [NSNumber]! = []
    var inActiveCashkans : [NSNumber]! = []
    var totalCashkans   :  [NSNumber]! = []
    var activeCashkans  :  [NSNumber]! = []
    var gameEnabled     :  [NSNumber]! = []
    var ecsScore : NSNumber = 0
    var totalTevScore : NSNumber = 0
    var drawerBtn: UIButton!
    weak var webView: UIWebView!
    var aDelegate: UITabBarController!
    var isCashkanCellTapped = false
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()
    var medalImage : UIImage!
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    
    @IBOutlet var spendObjValueLabel    : UILabel!
    @IBOutlet var overSpentValueLabel   : UILabel!
    @IBOutlet var savingsValueLabel     : UILabel!
    @IBOutlet var actualSpentValueLabel : UILabel!
    
    var popTip = AMPopTip()
    var menuVC : MenuViewController!
    var bgTapRecognizer:UITapGestureRecognizer!
    
    // facebook sharing variables
    var appNameStr                  : String = ""
    var economy                     : String = ""
    var url                         : String = ""
    var openGraphtitle              : String = ""
    var graphScore                  : String = ""

    // #MARK: Initializing
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.layoutIfNeeded()
        HELPER.setFontFamily(self.view , andSubViews : true)
        HELPER.setFontFamily(progressBarViewOne , andSubViews : true)
        HELPER.setFontFamily(cashkanCollectionView, andSubViews: true)
        self.popTip = AMPopTip()
        self.popTip.font = UIFont(name: customAppFont, size: 12)!
        self.popTip.edgeMargin = 5
        self.popTip.offset = 2
        self.popTip.edgeInsets = UIEdgeInsetsMake(0, 5, 0, 5)
        self.popTip.isHidden = true
        self.fshareButton.isHidden = true
        self.carsCollectionView_height.constant = CGFloat((HELPER.localeInfo.count*86)/2)
        self.tabBarController?.tabBar.barTintColor =  UIColor.white
        self.tabBarController?.tabBar.layer.borderColor = lightGray_color.cgColor
        self.tabBarController?.tabBar.layer.borderWidth = 0.5
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.activeLabel.text = journey_active
        self.inActiveLabel.text = journey_inactive
        self.activeLabel.textColor = savingsGreen_color
        self.inActiveLabel.textColor = overSpentRed_color
        
        self.navigationController?.navigationBar.barTintColor = journeyBlue_color
        self.savingSummaryDividerview.backgroundColor = journeyBlue_color
        self.cashkanJourneyDividerView.backgroundColor = journeyBlue_color
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true

        self.navigationController?.navigationBar.titleTextAttributes =     [NSForegroundColorAttributeName: UIColor.white]

        
        let firstProgressViewColor = UIColor(red: 246 / 255,green: 162 / 255,blue: 3 / 255,alpha: 1.0)
        firstProgressView.progressTintColor = firstProgressViewColor
        spendObjectiveView.backgroundColor = firstProgressViewColor
        spendObjectiveLabel.textColor =  firstProgressViewColor
        centerObjectiveLabel.textColor =  firstProgressViewColor
        centerObjectiveView.backgroundColor = firstProgressViewColor
        

        savingsView.backgroundColor = savingsGreen_color
        savingsLabel.textColor =  savingsGreen_color
        secondProgressView.trackTintColor = savingsGreen_color
        
        
        firstProgressView.trackTintColor = overSpentRed_color
        overSpentView.backgroundColor = overSpentRed_color
        overSpentLabel.textColor =   overSpentRed_color
        firstProgressView.trackTintColor =  overSpentRed_color

        secondProgressView.progressTintColor = actualSpentGray_color
        actualSpentLabel.textColor =  actualSpentGray_color
        actualSpentView.backgroundColor = actualSpentGray_color
        centerActualSpentLabel.textColor = actualSpentGray_color
        centerActualSpentView.backgroundColor = actualSpentGray_color
        
        
        savingsScoreButton.isUserInteractionEnabled = false
        savingsScoreButton.layoutIfNeeded()
        savingsScoreButton.layer.cornerRadius = savingsScoreButton.frame.width / 2
        
        savingsScoreButton.backgroundColor = journeyBlue_color
        
        totalTEVLabel.textColor = journeyBlue_color

        centerObjectiveView.isHidden = true
        centerObjectiveLabel.isHidden = true
        
        // help icon
        let button1: UIButton = UIButton(type: UIButtonType.custom)
        button1.setImage(UIImage(named: "Help_icon"), for: UIControlState())
        button1.addTarget(self, action: #selector(CashkansViewController.helpTap), for: UIControlEvents.touchUpInside)
        button1.frame = CGRect(x: 0, y: 200, width: 16, height: 25)
        
        let barButton1 = UIBarButtonItem(customView: button1)
        
        // Pull to refresh button
        let button2: UIButton = UIButton(type: UIButtonType.custom)
        button2.setImage(UIImage(named: "ic_action_refresh"), for: UIControlState())
        button2.addTarget(self, action: #selector(CashkansViewController.refreshView(_:)), for: UIControlEvents.touchUpInside)
        button2.frame = CGRect(x: 0, y: 200, width: 30, height: 30)
        
        let barButton2 = UIBarButtonItem(customView: button2)
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 15.0
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = 0 //-7.0
        
        self.navigationItem.rightBarButtonItems = [negativeSpace, barButton1, fixedSpace, barButton2]

        
        //Drawer button start
        self.drawerBtn = UIButton(type: UIButtonType.custom)
        drawerBtn.setImage(UIImage(named: "iosdrawericon"), for: UIControlState())
        drawerBtn.addTarget(self, action: #selector(self.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        drawerBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 25)
        drawerBtn.backgroundColor = UIColor.clear
        let barButton3 = UIBarButtonItem(customView: drawerBtn)
        self.navigationItem.leftBarButtonItem = barButton3;
        //Drawer button end
        
        self.refreshView.isUserInteractionEnabled = true
        let refreshingTap = UISwipeGestureRecognizer(target:self, action:#selector(self.refreshView(_:)))
        refreshingTap.direction = UISwipeGestureRecognizerDirection.down
        
        self.refreshView.addGestureRecognizer(refreshingTap)
        
        self.view.isUserInteractionEnabled = true
        let tapAnywhere = UITapGestureRecognizer(target:self, action:#selector(self.tapOnSuperview(_:)))
        self.view.addGestureRecognizer(tapAnywhere)
        
    }
    
    func tapOnSuperview(_ sender: UITapGestureRecognizer){
        self.popTip.hide() //isHidden = true
        self.popTip.isHidden = true
    }

    
    func refreshView(_ recognizer : AnyObject){
        if (HELPER.isDrawerOpen == false){
            
            if(Reachability.isConnectedToNetwork() == false){
                HELPER.isJourneyRefreshed = false
                DispatchQueue.main.async {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                    
                    self.navigationController!.pushViewController(secondViewController, animated: false)
                }
            }
            else{
                
                //self.view.userInteractionEnabled = true
                self.activityIndicator.showActivityIndicatory(self.view)
                self.activityIndicator.customlabel.isHidden = false
                self.activityIndicator.customlabel.text = journeyRefreshString
                getCashkanSummary()
                self.view.isUserInteractionEnabled = true
            }
        }
    }
    
    func helpTap()
    {
        if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "WebHelp") as? HelpWebViewController {
            //resultController.isFromLogin = false
            resultController.urlToVisit = journey_helpurl
            self.navigationController?.pushViewController(resultController, animated: true)
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RequestResponceHelper.delegate = self

        NotificationCenter.default.post(name: Notification.Name(rawValue: "popDrawerController"), object: nil)

        if(HELPER.isDrawerOpen == true && self.menuVC != nil){
            self.dismissViewBehindWebView()
        }
        self.tabBarController?.selectedIndex = 2
        
        self.navigationItem.title = journey_title
        HELPER.setFontFamily( self.view, andSubViews: true)
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]

        self.navigationController?.navigationBar.barTintColor = journeyBlue_color
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if(!HELPER.isJourneyRefreshed){
            if(Reachability.isConnectedToNetwork() == false){
                
                DispatchQueue.main.async {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                    
                    self.navigationController!.pushViewController(secondViewController, animated: false)
                    
                }
                
            }
            else{
                //self.view.userInteractionEnabled = false
                
                self.activityIndicator.showActivityIndicatory(self.view)
                getCashkanSummary()
                HELPER.isJourneyRefreshed = true
            }
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isCashkanCellTapped {
            isCashkanCellTapped = false
        }else{
            //self.navigationController?.popViewController(animated: true)
        }
        
        if HELPER.isDrawerOpen.boolValue && self.menuVC != nil{
            dismissViewBehindWebView()
        }
    }
    
    //  #MARK: Get cashkan summary from server


    func getCashkanSummary(){
        
        
        self.GetCashkans(){
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            
            if( success == true){// if 1
                self.denomination = []
                self.activeCashkans = []
                self.inActiveCashkans = []
                self.activeCashkans = []
                
                if let response = object as? HTTPURLResponse{//if 2
                    
                    // You can print out response object
                   // print("response = \(response)")
                    
                }// end if 2
                do {
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                    
                    if let parseJSON =  myJSON!["data"] as! [String:Any]?{ // if 3
                        print(parseJSON)
                        
                        if (parseJSON as AnyObject).count > 0{ // if 4
                            
                            self.ecsScore  =  parseJSON["ecsScore"] as! NSNumber
                            
                            let actSpend =  NSNumber(value:(parseJSON["actualSpend"] as! NSString).doubleValue)
                            
                            let spndObjective = NSNumber(value:(parseJSON["spendObj"] as! NSString).doubleValue)
                            
                            self.totalTevScore = NSNumber(value:(parseJSON["totalTev"] as! NSString).doubleValue)
                           
                            self.updateProgressBar(spndObjective: spndObjective,actSpend: actSpend)
                            
                            if self.ecsScore.doubleValue > 30{ // if 5
                                self.fshareButton.isHidden = false
                                self.fshareButton.isUserInteractionEnabled = true
                            }// end if 5
                            else{
                                self.fshareButton.isHidden = true
                                self.fshareButton.isUserInteractionEnabled = false
                            }
                            self.savingsScoreButton.setTitle(String(describing: self.ecsScore), for: UIControlState())
                            
                            //self.totalTevButton.setTitle(" "+String(describing: self.totalTevScore), for: UIControlState())
                            
                            let totalTevText = "Total Economic Value : "+String(describing: self.totalTevScore)

                            let longestWordRange = (totalTevText as NSString).range(of: String(describing: self.totalTevScore))
                            
                            let attributedString = NSMutableAttributedString(string: totalTevText, attributes: [NSForegroundColorAttributeName : UIColor.black])
                            
                            attributedString.setAttributes([ NSForegroundColorAttributeName : journeyBlue_color], range: longestWordRange)
                            
                            self.totalTEVLabel.attributedText = attributedString
                            

                            if let cashkanJSON : NSArray =  parseJSON["cashkans"] as! NSArray{ // if 6
                            
                            for var i in 0 ..< cashkanJSON.count  {
                                let obj = (parseJSON["cashkans"] as! NSArray)[i] as! NSDictionary
                                
                                let denomination = obj["denomination"] as! NSNumber
                                let inActiveCashkans = obj["inActiveCashkans"] as! NSNumber
                                let totalCashkans = obj["totalCashkans"] as! NSNumber
                                let activeCashkans  = (totalCashkans.intValue - inActiveCashkans.intValue) as NSNumber
                                
                                self.denomination.insert(denomination, at: i)
                                self.inActiveCashkans.insert(inActiveCashkans, at : i)
                                self.totalCashkans.insert(totalCashkans, at : i)
                                self.activeCashkans.insert(activeCashkans, at : i)
                                self.gameEnabled.insert(obj["gameEnabled"] as! NSNumber, at: i)
                                
                            }
                            }// end if 6
                            self.cashkanCollectionView.reloadData()
                            if((self.cashkanCollectionView.frame.y + self.cashkanCollectionView.frame.height) > self.view.frame.height - 100){ // if 7
                                self.scrollView.isScrollEnabled = true
                            }
                            else{
                                self.scrollView.isScrollEnabled = false
                            }// end if 7
                            self.activityIndicator.hideActivityIndicator(self.view)

                        }
                            
                        else{
                            
                            self.activityIndicator.hideActivityIndicator(self.view)

                        }

                    }
                    
                }
                catch {
                    //print(error)
                }
                
            }
                
            else{
                
                // If registration failed
                
                if( dataObject != nil){
                    
                    do {
                        self.activityIndicator.hideActivityIndicator(self.view)
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            let error = myJSON![display_msg]
                            let alertController = UIAlertController(title: alert_cashkan, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        //print(error)
                    }
                    
                }
            }
            
            
        }
        
        
    }

    func GetCashkans( _ completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
        
        let id =  HELPER.user_id.stringValue
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest("/users/"+id+cashkan_summary, params: nil )
        
        RequestResponceHelper.get(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.sync(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? Data{
                        completion(success, object ,dataObject ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }


    // #MARK: Collection view methods

    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return denomination.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CarCollectionViewCell
        HELPER.setFontFamily( cell.contentView, andSubViews: true)
        
        cell.activeLabel.isHidden = true
        cell.inactiveLabel.isHidden = true
        cell.carButton.setImage(UIImage(named: "faded_car_image"), for: UIControlState())
        
        cell.activeLabel.text = activeCashkans[(indexPath as NSIndexPath).row].stringValue
        cell.inactiveLabel.text = inActiveCashkans[(indexPath as NSIndexPath).row].stringValue
        cell.currencyLabel.text = HELPER.currencySym+denomination[(indexPath as NSIndexPath).row].stringValue
       
        if( denomination.count > 0 && 0 == totalCashkans[(indexPath as NSIndexPath).row]){

            cell.carButton.setImage(UIImage(named: "faded_car_image"), for: UIControlState())
            cell.activeLabel.isHidden = false
            cell.inactiveLabel.isHidden = false
            cell.currencyLabel.text = HELPER.currencySym+denomination[(indexPath as NSIndexPath).row].stringValue
            cell.currencyLabel.textColor = UIColor.gray
            cell.activeLabel.textColor = savingsGreen_color
            cell.inactiveLabel.textColor = overSpentRed_color
            
            cell.isUserInteractionEnabled = false
        
        }
        else if totalCashkans.count > 0 {
            
            cell.activeLabel.isHidden = false
            cell.inactiveLabel.isHidden = false
            cell.currencyLabel.textColor = journeyBlue_color
            
        //cell.rankLabel.text = rank[(indexPath as NSIndexPath).row].stringValue
 
        
            if(self.gameEnabled[(indexPath as NSIndexPath).row] == 0){
                cell.carButton.setImage(UIImage(named: "faded_car_image"), for: UIControlState())
                cell.activeLabel.textColor = savingsGreen_color
                cell.inactiveLabel.textColor = overSpentRed_color
                cell.isUserInteractionEnabled = false
                cell.currencyLabel.textColor = UIColor.gray

            }
            else{
                cell.carButton.setImage(UIImage(named: "Cashkans_Car"), for: UIControlState())
                cell.activeLabel.textColor = UIColor.white
                cell.inactiveLabel.textColor = UIColor.white
                cell.isUserInteractionEnabled = true
                cell.currencyLabel.textColor = journeyBlue_color
                let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(CashkansViewController.cashkanCellTapped(_:)))
                
                cell.carButton.addGestureRecognizer(tapRecognizer)

            }

        cell.backgroundColor = UIColor.white
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
//        print("You selected cell #\(indexPath.item)!")
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func cashkanCellTapped(_ sender: UITapGestureRecognizer) {
    
        isCashkanCellTapped = true
        //using sender, we can get the point in respect to the table view
        let tapLocation = sender.location(in: self.cashkanCollectionView)
        //using the tapLocation, we retrieve the corresponding indexPath
        let indexPath = self.cashkanCollectionView.indexPathForItem(at: tapLocation)
/*
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "detailCashkanSummary") as! CashkanDetailsViewController
        secondViewController.denomination_detail = denomination[(indexPath! as NSIndexPath).row].stringValue
        secondViewController.active_detail = activeCashkans[(indexPath! as NSIndexPath).row].stringValue
        secondViewController.inactive_detail = inActiveCashkans[(indexPath! as NSIndexPath).row].stringValue
        //secondViewController.rank_detail = rank[(indexPath! as NSIndexPath).row].stringValue
        
        // Take user to SecondViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
 
 */
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "JourneyMapViewController") as! JourneyMapViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        secondViewController.denomination_detail = denomination[(indexPath! as NSIndexPath).row].stringValue
        secondViewController.active_detail = activeCashkans[(indexPath! as NSIndexPath).row].stringValue
        secondViewController.inactive_detail = inActiveCashkans[(indexPath! as NSIndexPath).row].stringValue

        
    }
    
    //MARK: left drawer method start
    
    func dismissViewBehindWebView(){
        self.view.removeGestureRecognizer(bgTapRecognizer)
        
        //        self.slideMenuItemSelectedAtIndex(-1);
        let viewMenuBack : UIView = view.subviews.last!
        
        if viewMenuBack.tag == 123 {
            self.drawerBtn.tag = 0
            HELPER.isDrawerOpen = false
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * (UIScreen.main.bounds.size.width-drawerOffset)
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
               
        }
        
        let subViews = self.view.subviews
        for subview in subViews{
            if subview.tag == 111 {
                subview.removeFromSuperview()
            }
        }
        self.tabBarController?.tabBar.isUserInteractionEnabled = true

        return
    
    }
    func dismissContainer(){
        
        self.view.removeGestureRecognizer(bgTapRecognizer)

//        self.slideMenuItemSelectedAtIndex(-1);
        let viewMenuBack : UIView = view.subviews.last!
        
        if viewMenuBack.tag == 123 {
            self.drawerBtn.tag = 0
            HELPER.isDrawerOpen = false
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * (UIScreen.main.bounds.size.width-drawerOffset)
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                   // viewMenuBack.removeFromSuperview()
            })
        }
        
        let subViews = self.view.subviews
        for subview in subViews{
            if subview.tag == 111 {
                subview.removeFromSuperview()
            }
        }
        self.tabBarController?.tabBar.isUserInteractionEnabled = true

        return
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        if index > 2 {
            self.navigationItem.title = Drawer_titles[Int(index)]
        }
    }
    
    func onSlideMenuButtonPressed(_ sender : UIButton){
        
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
//            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            HELPER.isDrawerOpen = false
            let viewMenuBack : UIView = view.subviews.last!
            if viewMenuBack.tag == 123 {
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    var frameMenu : CGRect = viewMenuBack.frame
                    frameMenu.origin.x = -1 * (UIScreen.main.bounds.size.width-drawerOffset)
                    viewMenuBack.frame = frameMenu
                    viewMenuBack.layoutIfNeeded()
                    viewMenuBack.backgroundColor = UIColor.clear
                    }, completion: { (finished) -> Void in
//                        viewMenuBack.removeFromSuperview()
                })
            }
            
            let subViews = self.view.subviews
            for subview in subViews{
                if subview.tag == 111 {
                    subview.removeFromSuperview()
                }
            }
            
            self.tabBarController?.tabBar.isUserInteractionEnabled = true

            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        HELPER.isDrawerOpen = true
        if(menuVC == nil){

            menuVC  = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        }
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.view.bringSubview(toFront: menuVC.view)
        
        self.addChildViewController(menuVC)
        let dummyDisabledView: UIView = UIView.init(frame: self.view.frame)
        dummyDisabledView.tag = 111
        dummyDisabledView.alpha = 0.5
        dummyDisabledView.backgroundColor = darkGray_color

        self.view.insertSubview(dummyDisabledView, belowSubview: menuVC.view)
        
        menuVC.view.layoutIfNeeded()
        bgTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(bgTapped(_:)))
        
        self.view.addGestureRecognizer(bgTapRecognizer)
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-drawerOffset-40, height: UIScreen.main.bounds.size.height);
    
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.menuVC.view.frame=CGRect(x: 0, y: 64, width: UIScreen.main.bounds.size.width-drawerOffset-40, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
            }, completion:nil)
        
        self.tabBarController?.tabBar.isUserInteractionEnabled = false

    }
    
    //MARK: left drawer method end
    func bgTapped(_ sender: UITapGestureRecognizer) {
        dismissContainer()
        self.tabBarController?.tabBar.isUserInteractionEnabled = true

    }
    
    func updateProgressBar(spndObjective : NSNumber, actSpend : NSNumber){
    
        
        let total: Double
        let saving: Double
        let overSpent: Double
        var Total : Double
        var spentResult : Double = 0.0
        var actResult : Double = 0.0
        
        self.denomination = []
        self.activeCashkans = []
        self.inActiveCashkans = []
        self.activeCashkans = []
        
        
        if  floor(spndObjective.doubleValue) == spndObjective.doubleValue{
            
            self.spendObjValueLabel.text = HELPER.currencySym + String(describing: Int(spndObjective.doubleValue))
            self.centerObjectiveValueLabel.text = HELPER.currencySym + String(describing: Int(spndObjective.doubleValue))
            
        }
        else{
            self.spendObjValueLabel.text = HELPER.currencySym + String(spndObjective.doubleValue)
            self.centerObjectiveValueLabel.text = HELPER.currencySym + String(spndObjective.doubleValue)
            
        }
        
        if  floor(actSpend.doubleValue) == actSpend.doubleValue{
            self.actualSpentValueLabel.text = HELPER.currencySym + String(describing: Int(actSpend.doubleValue))
            self.centerActualSpentValueLabel.text = HELPER.currencySym + String(describing: Int(actSpend.doubleValue))
            
        }
        else{
            self.actualSpentValueLabel.text = HELPER.currencySym + String(actSpend.doubleValue)
            self.centerActualSpentValueLabel.text = HELPER.currencySym + String(actSpend.doubleValue)
            
        }
        
        
        if actSpend.doubleValue > spndObjective.doubleValue{
            
            //Over Spent Occurs
            
            total = actSpend.doubleValue - spndObjective.doubleValue
            
            overSpent = total
            
            if  floor(total) == total{
                self.overSpentValueLabel.text = HELPER.currencySym + String(Int(total))
            }
            else{
                self.overSpentValueLabel.text = HELPER.currencySym + String(format:"%.2f",CGFloat(total))
                
            }
            
            Total = spndObjective.doubleValue + overSpent
            
            spentResult = (Double)(spndObjective.doubleValue/Total)
            
            var percentvalue : Double = (spentResult * 100)
            //print(percentvalue)
            if( percentvalue < 10 && percentvalue > 0){
                percentvalue = 10 // 10 percent
            }
            else{
                Total = spndObjective.doubleValue + overSpent
                spentResult = (Double)(overSpent/Total)
                 let percentvalue2  = (spentResult * 100)
                if( percentvalue2 < 10 && percentvalue2 > 0){
                    percentvalue = 100 - 10 // 10 percent
                }
                
            }
            self.firstProgressView.progress = Float(percentvalue / 100)
            self.secondProgressView.progress = Float(actSpend)
            
            self.spendObjectiveLabel.isHidden = false
            self.spendObjectiveView.isHidden = false
            self.spendObjValueLabel.isHidden = false
            
            self.overSpentView.isHidden = false
            self.overSpentLabel.isHidden = false
            self.overSpentValueLabel.isHidden = false
            
            self.savingsLabel.isHidden = true
            self.savingsView.isHidden = true
            self.savingsValueLabel.isHidden = true
            
            self.actualSpentView.isHidden = true
            self.actualSpentLabel.isHidden = true
            self.actualSpentValueLabel.isHidden = true
            
            self.centerObjectiveValueLabel.isHidden = true
            self.centerObjectiveLabel.isHidden = true
            self.centerObjectiveView.isHidden = true
            
            self.centerActualSpentLabel.isHidden = false
            self.centerActualSpentValueLabel.isHidden = false
            self.centerActualSpentView.isHidden = false
            self.centerActualSpentView.backgroundColor = actualSpentGray_color
            self.centerActualSpentLabel.text = "Actual Spend"
            self.centerActualSpentLabel.textColor = actualSpentGray_color
            
            if  floor(actSpend.doubleValue) == actSpend.doubleValue{
                self.centerActualSpentValueLabel.text = HELPER.currencySym+String(describing: Int(actSpend))
            }
            else{
                self.centerActualSpentValueLabel.text = HELPER.currencySym+String(describing: actSpend)
                
            }
            
        }
        else if actSpend.doubleValue < spndObjective.doubleValue{
            
            
            if actSpend.doubleValue == 0{
                saving = spndObjective.doubleValue
                
                self.firstProgressView.progress =  Float(100)
                self.secondProgressView.progress = Float(0)
                
                self.overSpentLabel.isHidden = true
                self.overSpentView.isHidden = true
                self.overSpentValueLabel.isHidden = true
                
                self.spendObjectiveLabel.isHidden = true
                self.spendObjectiveView.isHidden = true
                self.spendObjValueLabel.isHidden = true
                
                self.savingsLabel.isHidden = true
                self.savingsView.isHidden = true
                self.savingsValueLabel.isHidden = true
                
                self.actualSpentView.isHidden = true
                self.actualSpentLabel.isHidden = true
                self.actualSpentValueLabel.isHidden = true
                
                self.centerActualSpentLabel.isHidden = false
                self.centerActualSpentValueLabel.isHidden = false
                self.centerActualSpentView.isHidden = false
                self.centerActualSpentView.backgroundColor = savingsGreen_color
                self.centerActualSpentLabel.text = "Savings"
                self.centerActualSpentLabel.textColor = savingsGreen_color
                //self.centerActualSpentValueLabel.text = HELPER.currencySym+String(describing: saving)
                if floor(saving) == saving {
                    self.centerActualSpentValueLabel.text = HELPER.currencySym+String(describing: Int(saving))
                }
                else{
                    self.centerActualSpentValueLabel.text = HELPER.currencySym+String(describing: saving)
                    
                }
                
                self.centerObjectiveLabel.isHidden = false
                self.centerObjectiveView.isHidden = false
                self.centerObjectiveValueLabel.isHidden = false
                
            }

            else{
            // Saving Occurs
            
            total = spndObjective.doubleValue -  actSpend.doubleValue
            saving = total
            
            if  (floor(total) == total){
                self.savingsValueLabel.text = HELPER.currencySym + String(Int(total))
            }
            else{
                self.savingsValueLabel.text = HELPER.currencySym + String(format:"%.2f",CGFloat(total))
                
            }
            
            Total = actSpend.doubleValue + saving
            actResult = (Double)(actSpend.doubleValue/Total)
            
            var percentvalue: Double = (actResult * 100)
            
            if( percentvalue < 10 && percentvalue > 0){
                percentvalue = 10// 10 percent
            }
            else{
                Total = actSpend.doubleValue + saving
                actResult = (Double)(saving/Total)
                let percentvalue2  = (actResult * 100)
                if( percentvalue2 < 10 && percentvalue2 > 0){
                    percentvalue = 100 - 10 // 10 percent
                }
                
            }
            self.firstProgressView.progress = Float(spndObjective)
            self.secondProgressView.progress = Float(percentvalue / 100)
            
            self.overSpentLabel.isHidden = true
            self.overSpentView.isHidden = true
            self.overSpentValueLabel.isHidden = true
            
            self.spendObjectiveLabel.isHidden = true
            self.spendObjectiveView.isHidden = true
            self.spendObjValueLabel.isHidden = true
                
            self.centerActualSpentValueLabel.isHidden = true
            self.centerActualSpentLabel.isHidden = true
            self.centerActualSpentView.isHidden = true
                
            self.actualSpentView.isHidden = false
            self.actualSpentLabel.isHidden = false
            self.actualSpentValueLabel.isHidden = false
                
            self.savingsView.isHidden = false
            self.savingsLabel.isHidden = false
            self.savingsValueLabel.isHidden = false
                
            self.centerObjectiveLabel.isHidden = false
            self.centerObjectiveView.isHidden = false
            self.centerObjectiveValueLabel.isHidden = false
            }
        }
        else if actSpend.doubleValue == spndObjective.doubleValue{
            
            self.firstProgressView.progress =  Float(100)
            self.secondProgressView.progress = Float(100)
            
            self.overSpentLabel.isHidden = true
            self.overSpentView.isHidden = true
            self.overSpentValueLabel.isHidden = true
            
            self.spendObjectiveLabel.isHidden = true
            self.spendObjectiveView.isHidden = true
            self.spendObjValueLabel.isHidden = true
            
            self.savingsLabel.isHidden = true
            self.savingsView.isHidden = true
            self.savingsValueLabel.isHidden = true
            
            self.actualSpentView.isHidden = true
            self.actualSpentLabel.isHidden = true
            self.actualSpentValueLabel.isHidden = true
            
            self.centerActualSpentLabel.isHidden = false
            self.centerActualSpentValueLabel.isHidden = false
            self.centerActualSpentView.isHidden = false
            self.centerActualSpentView.backgroundColor = actualSpentGray_color
            self.centerActualSpentLabel.text = "Actual Spend"
            self.centerActualSpentLabel.textColor = actualSpentGray_color
            //self.centerActualSpentValueLabel.text = HELPER.currencySym+String(describing: actSpend)
            if  floor(actSpend.doubleValue) == actSpend.doubleValue{
                self.centerActualSpentValueLabel.text = HELPER.currencySym+String(describing: Int(actSpend))
            }
            else{
                self.centerActualSpentValueLabel.text = HELPER.currencySym+String(describing: actSpend)
                
            }
            self.centerObjectiveLabel.isHidden = false
            self.centerObjectiveView.isHidden = false
            self.centerObjectiveValueLabel.isHidden = false
        }
    }

    
    //MARK: Facebook login method starts
    
    
    @IBAction func fbShareBtnClick(_ sender: UIButton) {
        
        if(self.popTip.isHidden){
        
        var tevScore                    : Int!
        
        // calculate TEV Score
        appNameStr = "<b>" + appName + "</b>";
        
            // Calculate economy
            if( HELPER.currencySym == IndianCurrency){
                economy = IndianEconomy
            }
            else if( HELPER.currencySym == "$"){
                if(HELPER.regionShortCode == "us"){
                    economy = USEconomy
                }
                    
                else{
                    economy = cannadianEconomy
                }
            }
        
        /* Calculating the TEV Score */
        if (self.ecsScore.doubleValue >= 90) {
            //url = goldMedal
            if (HELPER.regionShortCode == "in") {
                url  = goldMedal_in;
            } else  if (HELPER.regionShortCode == "us") {
                url  = goldMedal_us;
            } else {
                url  = goldMedal_ca;
            }
            openGraphtitle  = "Gold Medal"
            graphScore      = "Score: "+String(describing: self.ecsScore)+" out of 100"+"\n"+"(Contribution to "+economy+" that is better than "+String(describing: self.ecsScore)+"% of Cashkan users)";
        }
        else if (self.ecsScore.doubleValue >= 60 && self.ecsScore.doubleValue < 90) {
            //url            = silverMedal;
            if (HELPER.regionShortCode == "in") {
                url  = silverMedal_in;
            } else  if (HELPER.regionShortCode == "us") {
                url  = silverMedal_us;
            } else {
                url  = silverMedal_ca;
            }

            openGraphtitle = "Silver Medal";
            graphScore     = "Score: "+String(describing: self.ecsScore)+" out of 100"+"\n"+"(Contribution to "+economy+" that is better than "+String(describing: self.ecsScore)+"% of Cashkan users)";
        } else if (self.ecsScore.doubleValue >= 30 && self.ecsScore.doubleValue < 60) {
            //url            = bronzeMedal;
            if (HELPER.regionShortCode == "in") {
                url  = bronzeMedal_in;
            } else  if (HELPER.regionShortCode == "us") {
                url  = bronzeMedal_us;
            } else {
                url  = bronzeMedal_ca;
            }

            openGraphtitle = "Bronze Medal";
            graphScore     = "Score: "+String(describing: self.ecsScore)+" out of 100"+"\n"+"(Contribution to "+economy+" that is better than "+String(describing: self.ecsScore)+"% of Cashkan users)";
        }
        // Build customview
        let width = self.scrollView.frame.width * 0.9
        let customView = UIView(frame: CGRect(x: 0, y: 0, width:  width, height: 120))
        customView.layer.cornerRadius = 20
        customView.layer.masksToBounds = true
        
        //Build imageView
        let imageView : UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 80, height: 85))
        imageView.frame = CGRect(x:0, y: 0, width: 80, height: 80)
        self.loadImageFromUrl(url, view: imageView)
        customView.addSubview(imageView)
        
        // Build Title label
        let label = UILabel(frame: CGRect(x: 85, y: 0, width: width - 85, height:15))
        label.numberOfLines = 0
        label.text = openGraphtitle
        label.textAlignment = .left
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 15)
        customView.addSubview(label)
        
        //Build score label
        let label2 = UILabel(frame: CGRect(x: 85, y: 15, width: width - 85, height:60))
        label2.numberOfLines = 0
        label2.text = graphScore
        label2.textAlignment = .left
        label2.textColor = UIColor.white
        label2.font = UIFont(name : customAppFont, size : 12)

        customView.addSubview(label2)
        
        let buttonOriginY = label2.frame.origin.y + label2.frame.height + 5
        
        let remindbutton=UIButton(frame: CGRect(x: 0, y: buttonOriginY, width: customView.frame.width * 0.4, height: 30))
        remindbutton.backgroundColor = facebookBlue_color
        remindbutton.setTitle("REMIND ME LATER", for: UIControlState.normal)
        remindbutton.addTarget(self, action:#selector(self.remind), for: .touchUpInside)
        customView.addSubview(remindbutton)
        remindbutton.titleLabel?.font = UIFont(name: customAppFont, size: 13)
        
        let shareonfb=UIButton(frame: CGRect(x: customView.frame.width * 0.4, y: buttonOriginY, width: customView.frame.width * 0.6, height: 30))
        shareonfb.backgroundColor =  facebookBlue_color
        shareonfb.setTitle("SHARE ON FACEBOOK", for: UIControlState.normal)
        shareonfb.addTarget(self, action:#selector(self.sharetofacebook), for: .touchUpInside)
        shareonfb.titleLabel?.font = UIFont(name: customAppFont, size: 13)
        customView.addSubview(shareonfb)
            
        HELPER.setFontFamily(self.popTip, andSubViews: true)
        self.popTip.popoverColor = facebookBlue_color
        let x_point = self.ecsScoreView.frame.origin.x + sender.frame.origin.x
        let y_point = self.ecsScoreView.frame.y + sender.frame.y
        self.popTip.showCustomView(customView, direction: .down, in: self.scrollView, fromFrame: CGRect(origin : CGPoint(x : x_point, y : y_point + 10), size : sender.frame.size ))
        self.popTip.arrowSize = CGSize(width : 25, height : 10)
        self.popTip.actionAnimation = AMPopTipActionAnimation.float
        self.popTip.isHidden = false
        }
        else{
            self.popTip.hide()
            self.popTip.isHidden = true
        }
        
    }
    
    func loadImageFromUrl(_ url: String, view: UIImageView){
        
        // Create Url from string
        let url = URL(string: url)!
        
        // Download task:
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.sync(execute: { () -> Void in
                    view.image = UIImage(data: data)
                    self.medalImage = UIImage(data: data)
                    //self.activityIndicator.stopAnimating()
                })
            }
        })
        // Run task
        task.resume()
    }
    
    func remind(){
        
        //print("remind tappeed")
        self.popTip.hide()
        self.popTip.isHidden = true
    }
    
    
    func sharetofacebook(){
        
        if( self.ecsScore.doubleValue != 0 ){
            self.popTip.hide()
            self.popTip.isHidden = true
            self.getFacebookUserInfo()
        }
        else{
            let alertController = UIAlertController(title: alert_cashkan, message: "You need to create some sessions before sharing!!" as? String, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }
    
    }
    func getFacebookUserInfo() {
       
        if(FBSDKAccessToken.current() != nil)
        {
            //print(FBSDKAccessToken.current())
            // user already logged in
            
                self.postMsg()
            
                
           // })
        } else {
            // if user not logged in

            let loginManager = FBSDKLoginManager()
            //loginManager.loginBehavior = FBSDKLoginBehavior.native
            loginManager.logIn(withPublishPermissions: ["publish_actions"], from: self, handler: { (loginResult, error) in
                if error != nil {
                    //print(FBSDKAccessToken.current())
                } else if (loginResult?.isCancelled)! {
                    //self.dismiss(animated: false, completion: nil)
                } else {
                    self.postMsg()
                    
                }
                
            })
        }
        
       }
    
    
    func postMsg1() {
        // original postMSG method
        
        //if FBSDKAccessToken.current().hasGranted("publish_actions") {
            
        //   showFacebookShareDialog(mode: FBSDKShareDialogMode.feedBrowser)

        let shareToFb : SLComposeViewController = SLComposeViewController(forServiceType:SLServiceTypeFacebook)
        showFacebookShareDialog(mode: FBSDKShareDialogMode.feedBrowser)
        
        self.present(shareToFb, animated: true, completion: nil)
        
        //} else {
            //print("require publish_actions permissions")
            //self.dismiss(animated: false, completion: nil)

        //}
    }
    
    func postMsg() {
        
        let shareToFb : SLComposeViewController = SLComposeViewController(forServiceType:SLServiceTypeFacebook)
        
        // Create an object
        let properties = [
            "og:type": "article",
            "og:title": openGraphtitle,
            "og:description": graphScore,
            "og:image" : url,
            "og:url" : "http://cashkan.com",
            "og:ttl" : "5",
            "fb:explicitly_shared": "true"
        ]
        
        let object : FBSDKShareOpenGraphObject = FBSDKShareOpenGraphObject.init(properties: properties)
        
        // Create an action
        let action : FBSDKShareOpenGraphAction = FBSDKShareOpenGraphAction()
        action.actionType = "news.publishes"
        action.setObject(object, forKey: "article")

        // Create the content
        let content : FBSDKShareOpenGraphContent = FBSDKShareOpenGraphContent()
        content.action = action
        content.previewPropertyName = "article"
        
        try FBSDKShareDialog.show(from: self, with: content, delegate: nil)
        
    }
    
    private func showFacebookShareDialog(mode:FBSDKShareDialogMode){
        
        var content : FBSDKShareLinkContent = FBSDKShareLinkContent()
        content.contentURL = URL(string: "http://cashkan.com")
        
        let shareDialog = FBSDKShareDialog.init()
        shareDialog.fromViewController = self
        shareDialog.delegate = nil
        shareDialog.shareContent = content
        shareDialog.mode = mode
        if(shareDialog.canShow()){
            shareDialog.show()
        }
        
    }
    // #MARK: FB share delegate methods
    public func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!){
        
        //TODO: Take action after sharing has completed
        //NSLog("Photo shared!")
    }
    
 
    public func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!){
        // TODO: Take action if sharing failed
        if let err = error{
            //NSLog("Photo share failed! - \(err)")
            
        }
    }
    
    public func sharerDidCancel(_ sharer: FBSDKSharing!){
        //TODO: Take action if user cancelled sharing of photos
        //NSLog("Photo share cancelled!")
    }

    //MARK: Network failure delegate method
    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
            
            self.navigationController!.pushViewController(secondViewController, animated: false)
        }
        
    }
  

}
