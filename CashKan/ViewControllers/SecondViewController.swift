//
//  SecondViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 23/06/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var currentImage: UIImageView!
    @IBOutlet weak var retakeImageview: UIImageView!
    @IBOutlet weak var totalValueLabel: UILabel!
    @IBOutlet weak var iAmDoneButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        label.text = "Pick a currency denomination,slide and click to Cashkan"
        label.lineBreakMode = NSLineBreakMode.byWordWrapping;
        label.numberOfLines = 0
        label.textColor = darkGray_color
    

        retakeImageview.layer.cornerRadius = retakeImageview.frame.size.height / 2;
        retakeImageview.clipsToBounds = true
        retakeImageview.layer.borderWidth = CGFloat(customButtonBorderWidth)

        iAmDoneButton.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        iAmDoneButton.layer.borderWidth = CGFloat(customButtonBorderWidth)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func iAmDoneButtonClicked(_ sender: UIButton) {
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
