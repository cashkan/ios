//
//  ViewReceiptsController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 22/08/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class ViewReceiptsController: UIViewController ,UIScrollViewDelegate, NetworkFailureDelegate{
    
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()
   var sessionNumber: String!
    
    
 var imgOne: UIImageView!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var detailView: UIView!
    @IBOutlet var pageControl: UIPageControl!
    
    var VendorName  : [String]!   = []
    var createDt    : [String]!   = []
    var amount      : [String]!   = []
    var notes       : [String]!   = []
    var spentON     : [String]!   = []
    var imageUrl    : [String]!   = []

    @IBOutlet var vendorNameLabel: UILabel!
    @IBOutlet var notesLabel     : UILabel!
    @IBOutlet var spentOnLabel   : UILabel!
    @IBOutlet var amountLabel    : UILabel!
    @IBOutlet var dateLabel      : UILabel!
    
    
     var myActivityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HELPER.setFontFamily( self.view, andSubViews: true)

        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]

        self.navigationItem.hidesBackButton = true
        
        let button: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        button.setImage(UIImage(named: "Cashkans_Back arrow"), for: UIControlState())
        //add function for button
        button.addTarget(self, action: #selector(ViewReceiptsController.back(_:) as (ViewReceiptsController) -> (UIBarButtonItem) -> ()), for: UIControlEvents.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton;
        
    myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
       
        // Do any additional setup after loading the view.
        //1
        self.scrollView.frame = CGRect(x: 0,y: 0, width: self.view.frame.width, height: self.view.frame.height)
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        let scrollViewHeight:CGFloat = self.scrollView.frame.height
        self.detailView.isHidden = true
        
       imgOne = UIImageView(frame: CGRect(x: 0, y: 0,width: self.scrollView.frame.width, height: self.scrollView.frame.height ))

        myActivityIndicator.center = imgOne.center
        myActivityIndicator.hidesWhenStopped = true

        imgOne.addSubview(myActivityIndicator)
        self.scrollView.addSubview(imgOne)
        self.scrollView.addSubview(detailView)
        
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
        
  
        let gesture = UITapGestureRecognizer(target: self, action: #selector(ViewReceiptsController.toggle(_:)))
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(gesture)

    }
    
    func back(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
   self.navigationController?.popViewController(animated: true)
    
    }
    
    
    func toggle(_ sender: AnyObject) {
     self.navigationController?.setNavigationBarHidden(navigationController?.isNavigationBarHidden == false, animated: true)

        if(detailView.isHidden == false){
            detailView.isHidden = true

        }
        else{
            detailView.isHidden = false
        }
    }
    
    override var prefersStatusBarHidden : Bool {
       // detailView.hidden = false
        return self.navigationController?.isNavigationBarHidden == true
    }
    
    override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        return UIStatusBarAnimation.slide
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        RequestResponceHelper.delegate = self
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        if(Reachability.isConnectedToNetwork() == false){
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                
                self.navigationController!.pushViewController(secondViewController, animated: false)
                
            }
            
        }
        else{
            self.activityIndicator.showActivityIndicatory(self.view)
            self.getAllReceipts()
        }
        
        }
    
    func getAllReceipts() -> Void{
        
        self.GetAllReceipts(){
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            //print("got back: \(message)")
            
            var imageUrl : String

            
            if( success == true){
                
                if let response = object as? HTTPURLResponse{
                    
                    // You can print out response object
                    //print("response = \(response)")
                    
                }
                
                
                do {
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                    //print(myJSON)
                    
                    if let parseJSON = myJSON![data]{
                        
                        self.pageControl.numberOfPages = (parseJSON as AnyObject).count
                        
                        if (parseJSON as AnyObject).count > 0{
                            for  i in 0 ..< (parseJSON as AnyObject).count  {
                                
                                let obj = (myJSON!["data"] as! NSArray)[i] as! NSDictionary
                                
                                let name = obj["VendorName"] as! String
                                let spenton = obj["spentON"] as! String
                                //self.totalReceiptsValue = NSNumber(double: Double(parseJSON["actualSpend"] as! String)!)
                                let amount = obj["amount"] as! String
                                let notes = obj["notes"] as! String
                                var date = (obj["createDt"] as! String)
                               
                                
                                if let imageUrls : String = obj["imageUrl"] as? String{
                                    imageUrl = imageUrls
                                }
                                else{
                                    imageUrl = ""
                                }
                                
                                self.VendorName.insert(name, at: i)
                                self.amount.insert(amount, at : i)
                                self.createDt.insert(date, at : i)
                                self.notes.insert(notes, at : i)
                                self.spentON.insert(spenton, at : i)
                                self.imageUrl.insert(imageUrl, at: i)
                                
                            }
                            self.updateScrollView()

                            self.detailView.isHidden = false
                            self.activityIndicator.hideActivityIndicator(self.view)
                            
                        }
                            
                        else{
                            self.activityIndicator.hideActivityIndicator(self.view)
                        }
                        
                    }
                    
                    
                }
                catch {
                    //print(error)
                }
                
            }
                
            else{
                
                // If registration failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            let error = myJSON![display_msg]
                            let alertController = UIAlertController(title: alert_cashkan, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        //print(error)
                    }
                    
                }
            }
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UIScrollViewDelegate

    func updateScrollView(){
     self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width * CGFloat(self.VendorName.count), height: self.scrollView.frame.height)
    
    let scrollViewWidth:CGFloat = self.scrollView.frame.width
    let scrollViewHeight:CGFloat = self.scrollView.frame.height
        
        scrollView.showsVerticalScrollIndicator = false
        //print(self.scrollView.frame.height)
        var imgOne : UIImageView!
        for cnt in 0 ..< self.imageUrl.count{
        
            imgOne = UIImageView(frame: CGRect(x: scrollViewWidth * CGFloat(cnt), y: 0,width:scrollViewWidth, height:scrollViewHeight))
            if(imageUrl[cnt] != ""){
                self.myActivityIndicator.startAnimating()
                self.loadImageFromUrl(imageUrl[cnt], view: imgOne)
                self.scrollView.addSubview(imgOne)
                self.scrollView.sendSubview(toBack: imgOne)
            }
            else{
                let x = (scrollViewWidth * CGFloat(cnt)) + (scrollViewHeight/6)
                let y = scrollViewHeight/2 - (scrollViewHeight/6)
                let dummyImage = UIImageView(frame: CGRect(x:  x , y: y, width:scrollViewHeight/4, height:scrollViewHeight/4))
                dummyImage.image = UIImage(named : "no_receipt")
                self.scrollView.addSubview(dummyImage)
                self.scrollView.sendSubview(toBack: dummyImage)
            }
        }
        if self.VendorName.count>0 {
            self.vendorNameLabel.text =  self.VendorName[0]
        }
        
        if  self.amount.count>0{
            self.amountLabel.text =  HELPER.currencySym + self.amount[0]
        }
        
        if  self.spentON.count>0{
            self.spentOnLabel.text =  self.spentON[0]
        }
        
        if  self.createDt.count>0{
            self.dateLabel.text =  self.getFormatteddate(self.createDt[0])
        }
        if  self.notes.count>0{
            self.notesLabel.text =  self.notes[0]
        }
        self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width * CGFloat(VendorName.count), height:self.scrollView.frame.height)
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
        
    }
    
    func resizeImage( _ image: UIImage, newSize  :CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    

    func getFormatteddate(_ creationDate : String)-> String{
        // dd mm yy -> dd MMM yyyy
        let fullString = creationDate
        let StringArray = fullString.characters.split{$0 == "-"}.map(String.init)
        var monthStr = "Jan"
        for i in 0 ..< monthsArray.count{
            
            if(String(monthsArray[i].0) == StringArray[1]){
                monthStr = monthsArray[i].1
                break
            }
        }
        
        let y = StringArray[2]
        let d = StringArray[0]
        let dateStr = d+" "+monthStr+" "+y
        return dateStr
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
           // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        
        for var i in 0 ..< VendorName.count  {
             if Int(currentPage) == i{
                
                vendorNameLabel.text = VendorName[i]
                amountLabel.text = HELPER.currencySym + amount[i]
                spentOnLabel.text = spentON[i]
                dateLabel.text = getFormatteddate(createDt[i])
                notesLabel.text = notes[i]
            }
            
        }
    }
    
   func loadImageFromUrl(_ url: String, view: UIImageView){
        
        // Create Url from string
        let url = URL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.sync(execute: { () -> Void in
                    view.image = UIImage(data: data)
                    self.myActivityIndicator.stopAnimating()
                })
            }
        }) 
        // Run task
        task.resume()
    }

    
    func GetAllReceipts( _ completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
        
        // Modify the url if search is active
        var newUrl : String = ""

        if(HELPER.isSearchActive){
        
            
            if(HELPER.search_vendorName != ""){
                if(newUrl == ""){
                    newUrl = newUrl+"vendorName="+HELPER.search_vendorName
                }
                else{
                    newUrl = newUrl+"&vendorName="+HELPER.search_vendorName
                }
            }
            if( HELPER.search_min_amount != "0" ){
                if(newUrl == ""){
                    newUrl = newUrl+"amountMin="+HELPER.search_min_amount
                }
                else{
                    newUrl = newUrl+"&amountMin="+HELPER.search_min_amount
                }
            }
            if( HELPER.search_max_amount != "0" ){
                if(newUrl == ""){
                    newUrl = newUrl+"amountMax="+HELPER.search_max_amount
                }
                else{
                    newUrl = newUrl+"&amountMax="+HELPER.search_max_amount
                }
            }
            
            if(HELPER.search_spentOn != ""){
                if(newUrl == ""){
                    newUrl = newUrl+"spentOn="+HELPER.search_spentOn
                }
                else{
                    newUrl = newUrl+"&spentOn="+HELPER.search_spentOn
                }
                
            }
            if(HELPER.search_fromDate != ""){
                if(newUrl == ""){
                    newUrl = newUrl+"startDt="+getFormatteddateForRequest(HELPER.search_fromDate)
                }
                else{
                    newUrl = newUrl+"&startDt="+getFormatteddateForRequest(HELPER.search_fromDate)
                }
                
            }
            if(HELPER.search_toDate != ""){
                if(newUrl == ""){
                    newUrl = newUrl+"endDt="+getFormatteddateForRequest(HELPER.search_toDate)
                }
                else{
                    newUrl = newUrl+"&endDt="+getFormatteddateForRequest(HELPER.search_toDate)
                }
                
            }
            
            newUrl = "?"+newUrl
        }
        
        
        let id =  HELPER.user_id.stringValue
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest("/users/"+id+view_receipts_url+sessionNumber+newUrl, params: nil )
        RequestResponceHelper.get(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.sync(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? Data{
                        //message = passedMessage
                        completion(success, object ,dataObject ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }

    func getFormatteddateForRequest(_ date : String)-> String{
        //dd mmm yy -> yyyy-mm-dd
        let fullString = date
        let StringArray = fullString.characters.split{$0 == "-"}.map(String.init)
        var monthNumber = "01"
        for i in 0 ..< monthsArray.count{
            
            if(monthsArray[i].1 == StringArray[1]){
                monthNumber = monthsArray[i].0
                break
            }
        }
        
        let y = StringArray[2]
        let d = StringArray[0]
        let dateStr = y+"-"+monthNumber+"-"+d
        return dateStr
        
    }
    
    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        let error = "Network connection failed..."
        let alertController = UIAlertController(title: login_failed, message: error as? String, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            //print("you have pressed the Cancel button");
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    

}
