//
//  SearchViewController.swift
//  cashkan3
//
//  Created by emtarang on 22/06/16.
//  Copyright © 2016 emtarang. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


extension Date {
    func isGreaterThanDate(_ dateToCompare: Date) -> Bool {
        var isGreater = false
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        return isGreater
    }
    
    func isLessThanDate(_ dateToCompare: Date) -> Bool {
        var isLess = false
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        return isLess
    }
    
    func equalToDate(_ dateToCompare: Date) -> Bool {
        var isEqualTo = false

        if self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        return isEqualTo
    }

}
class SearchViewController: UIViewController,UITextFieldDelegate, NetworkFailureDelegate {

    var animateDistance = CGFloat()

    var receiptController2: ReceiptController2!
    
    var popDatePicker : PopDatePicker?
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()

    
    @IBOutlet var resetBtn:      UIButton!
    @IBOutlet var searchBtn:     UIButton!
    @IBOutlet var txtVendorName: UILabel!
    @IBOutlet var txtAmount:     UILabel!
    @IBOutlet var txtSpentOn:    UILabel!
    @IBOutlet var txtUploadDate: UILabel!
    @IBOutlet var txtTo:         UILabel!
    @IBOutlet var fromDate:      UITextField!
    @IBOutlet var toDate:        UITextField!
    @IBOutlet var vendorNameTextField: UITextField!
    
    @IBOutlet var spentOnTextField: UITextField!
    @IBOutlet var minAmountTextField: UITextField!
    @IBOutlet var maxAmountTextField: UITextField!
    
    @IBOutlet var vendorNameView: UIView!
    @IBOutlet var maxAmtView: UIView!
    @IBOutlet var minAmtView: UIView!
    @IBOutlet var spentOnView: UIView!
    @IBOutlet var dateFromView: UIView!
    @IBOutlet var dateToView: UIView!
    
    
    var items: [(String,String,String,NSNumber,NSNumber,NSNumber)] = []
    
    var uploadedReceiptsArray : [[(String,NSNumber)]]  = []
    
    var totalReceiptsValue : NSNumber = 0.0
    var totalValue : NSNumber = 0.0
    var overSpent  : NSNumber = 0.0
    var underSpent  : NSNumber = 0.0
    var difference : NSNumber = 0.0
    var totalSessions : NSNumber = 0.0
    var totalUnuploadedReceipts : Int = 0
    var searchspentValue : NSNumber = 0.0
    
    var fromDateSetPriviously : Bool = false
    var toDateSetPriviously : Bool = false

    
    var actualSpend     : [NSNumber]! = []
    var creationDate    : [String]! = []
    var receiptDate     : [String]! = []
    var spendObjTitle   : [String]! = []
    var totalvalue      : [NSNumber]! = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        HELPER.setFontFamily( self.view, andSubViews: true)
        self.navigationController?.title = "Spending Object"
        
        setViewColor()
        
        self.searchBtn.layer.borderWidth =  CGFloat(customButtonBorderWidth)
        self.searchBtn.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        
        self.searchBtn.layer.masksToBounds = true
        
        self.resetBtn.layer.borderWidth = CGFloat(customButtonBorderWidth)
        self.resetBtn.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        
        self.resetBtn.layer.masksToBounds = true
        
        self.view.isOpaque = false
        
        vendorNameTextField.delegate = self
        minAmountTextField.delegate = self
        maxAmountTextField.delegate = self
        spentOnTextField.delegate = self
        toDate.delegate = self
        fromDate.delegate = self

        
    }

    
    func setViewColor(){
        
        fromDate.tintColor      = lightOrange_color
        searchBtn.layer.borderColor   = lightOrange_color.cgColor
        searchBtn.titleLabel?.textColor = lightOrange_color
        resetBtn.layer.borderColor   = lightOrange_color.cgColor
        resetBtn.titleLabel?.textColor = lightOrange_color
        txtVendorName.textColor = orangeReceipt_color
        txtAmount.textColor     = orangeReceipt_color
        txtSpentOn.textColor    = orangeReceipt_color
        txtTo.textColor         = orangeReceipt_color
        txtUploadDate.textColor = orangeReceipt_color

        searchBtn.setTitleColor(lightOrange_color, for: UIControlState())
        resetBtn.setTitleColor(lightOrange_color, for: UIControlState())
        
        self.vendorNameView.backgroundColor = lightGray_color
        self.spentOnView.backgroundColor = lightGray_color
        self.minAmtView.backgroundColor = lightGray_color
        self.maxAmtView.backgroundColor = lightGray_color
        self.dateToView.backgroundColor = lightGray_color
        self.dateFromView.backgroundColor = lightGray_color
        self.dateFromView.isHidden = true
        self.dateToView.isHidden = true
        fromDate.layoutIfNeeded()
        fromDate.layer.borderWidth = 1
        fromDate.layer.borderColor = lightGray_color.cgColor
        fromDate.layer.cornerRadius = fromDate.frame.height / 2
        toDate.layoutIfNeeded()
        toDate.layer.borderWidth = 1
        toDate.layer.borderColor = lightGray_color.cgColor
        toDate.layer.cornerRadius = toDate.frame.height / 2
        
        searchBtn.titleLabel?.font = UIFont(name: customAppFont, size: buttonFontSize )
        resetBtn.titleLabel?.font = UIFont(name: customAppFont, size: buttonFontSize )

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationItem.title =
        self.view.layoutIfNeeded()
        HELPER.setFontFamily( self.view, andSubViews: true)
        RequestResponceHelper.delegate = self
        self.minAmountTextField.text = ""
        self.maxAmountTextField.text = ""
        self.vendorNameTextField.text = ""
        self.spentOnTextField.text = ""
        self.fromDate.text = ""
        self.toDate.text = ""
        self.fromDateSetPriviously = false
        self.toDateSetPriviously = false

        if(Double(HELPER.search_min_amount) > 0){
            self.minAmountTextField.text = HELPER.search_min_amount
        }
        if(Double(HELPER.search_max_amount) > 0){
            self.maxAmountTextField.text = HELPER.search_max_amount
        }
        if(!HELPER.search_vendorName.isEmpty){
            self.vendorNameTextField.text = HELPER.search_vendorName
        }
        if(!HELPER.search_spentOn.isEmpty){
            self.spentOnTextField.text = HELPER.search_spentOn
        }
        if(!HELPER.search_fromDate.isEmpty){
            self.fromDate .text = HELPER.search_fromDate
            self.fromDateSetPriviously = true
        }
        if(!HELPER.search_toDate.isEmpty){
            self.toDate.text = HELPER.search_toDate
            self.toDateSetPriviously = true
        }

    }
    

    @IBAction func onSearchBtnClick(_ sender: AnyObject) {
        
        var allFieldsValid: Bool = true
        
        if((vendorNameTextField.text?.isEmpty)!  && (minAmountTextField.text?.isEmpty)!  && (maxAmountTextField.text?.isEmpty)! && (spentOnTextField.text?.isEmpty)! && (fromDate.text?.isEmpty)! && (toDate.text?.isEmpty)!){
            allFieldsValid = false
            let alertController = UIAlertController(title: alert_cashkan, message: alert_pleaseProvideSearch, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:nil)

        }
        else{
         if(!(minAmountTextField.text == "")){
            let cafe = minAmountTextField.text
            var showMinerror : Bool  = false
            var decimals = 0
            
            if((Double(minAmountTextField.text!)! > (1000000.00)) || (cafe![cafe!.startIndex] <= "0" )){
                allFieldsValid = false
                showMinerror = true
            }
            
            else{
                let fullString = minAmountTextField.text
                    if(( fullString?.range(of: ".")) != nil){
                        let StringArray = fullString!.characters.split{$0 == "."}.map(String.init)
                
                        if(StringArray.count < 2){
                            showMinerror = true
                        }
                        else{
                            decimals = StringArray[1].characters.count
                            if( decimals > 2){
                                showMinerror = true
                            }
                        }
                    }
                }
            
            if( showMinerror){
                allFieldsValid = false
                
                let alertController = UIAlertController(title: alert_receiptSearch, message: alert_amountDigitsValidation, preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                }
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion:nil)
               
            }
        }
            
            
            if(!(maxAmountTextField.text == "")){
                let amt = maxAmountTextField.text
                var showMinerror : Bool  = false
                var decimals = 0
                
                if((Double(maxAmountTextField.text!)! > (1000000.00)) || (amt![amt!.startIndex] <= "0" )){
                    allFieldsValid = false
                    showMinerror = true
                }
                    
                else{
                    let fullString = maxAmountTextField.text
                    if(( fullString?.range(of: ".")) != nil){
                        let StringArray = fullString!.characters.split{$0 == "."}.map(String.init)
                        
                        if(StringArray.count < 2){
                            showMinerror = true
                        }
                        else{
                            decimals = StringArray[1].characters.count
                            if( decimals > 2){
                                showMinerror = true
                            }
                        }
                    }
                }
                
                if( showMinerror){
                    allFieldsValid = false
                    
                    let alertController = UIAlertController(title: alert_receiptSearch, message: alert_amountDigitsValidation, preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                    }
                    alertController.addAction(okAction)
                    
                    self.present(alertController, animated: true, completion:nil)
                    
                }
            }

        if( maxAmountTextField.text! != "" && minAmountTextField.text! != ""){
            if(Double(maxAmountTextField.text!) < Double(minAmountTextField.text!)){
                allFieldsValid = false
                let alertController = UIAlertController(title: alert_receiptSearch, message: alert_amountRangeValidation, preferredStyle: .alert)
            
                    let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                }
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion:nil)

            }
        }
            
         if(!(toDate.text?.isEmpty)! && !(fromDate.text?.isEmpty)!){
           
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            formatter.timeStyle = .none
            
            let str = " 19:29:50 +0000"
            
            let toDateStr = toDate.text! + str
            let fromDateStr = fromDate.text! + str
           
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm:ss Z"
            let todate = dateFormatter.date(from: toDateStr)
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd-MMM-yyyy HH:mm:ss Z"
            let fromdate = dateFormatter1.date(from: fromDateStr)
            
            if(fromdate!.isGreaterThanDate(todate!)) {
                allFieldsValid = false

                let alertController = UIAlertController(title: alert_receiptSearch, message: alert_dateCompare, preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                }
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion:nil)
            }
        }
                
                
         if allFieldsValid{
            
            if(Reachability.isConnectedToNetwork() == false){
                
                let alertController = UIAlertController(title: "No Network", message: "You are not connected to internet", preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                }
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion:nil)
                
            }
            else{
                
        HELPER.isSearchActive = true
        //self.receiptController2.objSEFilterControl.handler.isHidden = true

            self.activityIndicator.showActivityIndicatory(self.view)
        self.getReceiptsObjects()
            }
        }
        }

    }
   
    @IBAction func onTransperentButtonClick(_ sender: UIButton) {
        
        self.dismiss(animated: false, completion: nil)
        self.receiptController2.view.alpha = 1
        self.receiptController2.navigationController?.navigationBar.alpha = 1
        self.receiptController2.tabBarController?.tabBar.alpha = 1
        
        self.receiptController2.tabBarController?.tabBar.isUserInteractionEnabled = true
    }
    
    
    @IBAction func onResetButtonClicked(_ sender: AnyObject) {
        HELPER.search_vendorName = ""
        HELPER.search_min_amount = "0"
        HELPER.search_max_amount = "0"
        HELPER.search_spentOn = ""
        HELPER.search_fromDate = ""
        HELPER.search_toDate = ""
        HELPER.isSearchActive = false
        
        self.receiptController2.items = []
        self.receiptController2.uploadedReceiptsArray = []
        self.receiptController2.totalValue = 0.0
        self.receiptController2.totalValue = 0.0
        self.receiptController2.totalReceiptsValue = 0.0
        self.receiptController2.overSpent = 0.0
        self.receiptController2.underSpent = 0.0
        self.receiptController2.view.alpha = 1
        self.receiptController2.navigationItem.title = receipts_title
        self.receiptController2.tabBarController?.tabBar.alpha = 1
        self.receiptController2.objSEFilterControl.handler.isHidden = false
        self.receiptController2.objSEFilterControl.resetSelectedTitleColor(self.receiptController2.objSEFilterControl.selectedIndex)
        self.receiptController2.tabBarController?.tabBar.isUserInteractionEnabled = true
        self.receiptController2.searchButton.setImage(UIImage(named : "Receipts_icon02_Search.png"), for: UIControlState())
        self.receiptController2.getReceiptsObjects(HELPER.searchDateFilterIndex)
        self.dismiss(animated: false, completion: nil)
    }
    
    //#MARK: Text field delegate methods
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        
//         let textFieldRect : CGRect = self.view.window!.convert(textField.bounds, from: textField)
//        let viewRect : CGRect = self.view.window!.convert(self.view.bounds, from: self.view)
//        
//        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
//        let numerator : CGFloat = midline - viewRect.origin.y - MoveKeyboard.MINIMUM_SCROLL_FRACTION * viewRect.size.height
//        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
//        
//        var heightFraction : CGFloat = numerator / denominator
//        
//        if heightFraction  > 1.0 {
//            heightFraction = 1.0
//        }
//        
//        let orientation : UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
//        if (orientation == UIInterfaceOrientation.portrait || orientation == UIInterfaceOrientation.portraitUpsideDown) {
//            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
//        } else {
//            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
//        }
//        
//        var viewFrame : CGRect = self.view.frame
//        viewFrame.origin.y -= animateDistance
//        
//        UIView.beginAnimations(nil, context: nil)
//        UIView.setAnimationBeginsFromCurrentState(true)
//        UIView.setAnimationDuration(TimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
//        
//        self.view.frame = viewFrame
//        
//        UIView.commitAnimations()
//    }
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        var viewFrame : CGRect = self.view.frame
//        viewFrame.origin.y += animateDistance
//        
//        UIView.beginAnimations(nil, context: nil)
//        UIView.setAnimationBeginsFromCurrentState(true)
//        
//        UIView.setAnimationDuration(TimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
//        
//        self.view.frame = viewFrame
//        
//        UIView.commitAnimations()
//        
//    }
//    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        
        var result = true
    if(textField == vendorNameTextField){
            
            if string.characters.count > 0 {
                let disallowedCharacterSet = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"+" "+"!@#$%^&*()_+{}[]|<>,.~`/:;?-=\\¥£•¢\"\'").inverted
                let replacementStringIsLegal = string.rangeOfCharacter(from: disallowedCharacterSet) == nil
                result = replacementStringIsLegal
            }
        }
        if(textField == minAmountTextField)
        {
        let maxLength = 9
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if(newString.length <= maxLength){
            
        }
        else{
            return false
            
        }
        if string.characters.count > 0 {
            let disallowedCharacterSet = CharacterSet(charactersIn: "0123456789.").inverted
            let replacementStringIsLegal = string.rangeOfCharacter(from: disallowedCharacterSet) == nil
            result = replacementStringIsLegal
        }
        }
        
        if(textField == maxAmountTextField)
        {
            let maxLength = 9
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            if(newString.length <= maxLength){
                
            }
            else{
                return false
                
            }
            if string.characters.count > 0 {
                let disallowedCharacterSet = CharacterSet(charactersIn: "0123456789.").inverted
                let replacementStringIsLegal = string.rangeOfCharacter(from: disallowedCharacterSet) == nil
                result = replacementStringIsLegal
            }
        }

        
        return result
        
    }
    
    

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        if textField.returnKeyType != UIReturnKeyType.done{
            let nextTag: NSInteger = textField.tag+1;
            let nextResponder: UIResponder = (textField.superview?.viewWithTag(nextTag))!
            
            if (nextResponder as UIResponder?) != nil{
                nextResponder.becomeFirstResponder()
            }
            
        }
        else{
            textField.resignFirstResponder()
        }
        
        
        
        return false;
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
    }
    
    
    func resign(_ tag : Int) {
        if tag == fromDate.tag{
            fromDate.resignFirstResponder()
        }
        if tag == toDate.tag{
            toDate.resignFirstResponder()
        }
        
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        
        if (textField === fromDate) {
            
            spentOnTextField.resignFirstResponder()
            minAmountTextField.resignFirstResponder()
            vendorNameTextField.resignFirstResponder()
            
            if !fromDateSetPriviously{
                
                self.fromDateSetPriviously = true
                self.popDatePicker = PopDatePicker(forTextField: fromDate)

                resign(fromDate.tag)
                let formatter = DateFormatter()
            
                formatter.dateStyle = .medium
                formatter.timeStyle = .none
                let initDate : Date? = formatter.date(from: fromDate.text!)
                
                let dataChangedCallback : PopDatePicker.PopDatePickerCallback = { (newDate : Date, forTextField : UITextField) -> () in
                
                    // here we don't use self (no retain cycle)
                    forTextField.text = (newDate.ToDateMediumString() ?? "?") as String
                
                }
            
                popDatePicker!.pick(self, initDate: initDate, dataChanged: dataChangedCallback)
            }
            else{
                fromDateSetPriviously = false
            }
            return false
        }
       else if (textField === toDate) {
            
            spentOnTextField.resignFirstResponder()
            minAmountTextField.resignFirstResponder()
            vendorNameTextField.resignFirstResponder()
            
            if !toDateSetPriviously{
                self.toDateSetPriviously = true
                self.popDatePicker = PopDatePicker(forTextField: toDate)
                resign(toDate.tag)
                let formatter = DateFormatter()
                formatter.dateStyle = .medium
                formatter.timeStyle = .none
                let initDate : Date? = formatter.date(from: toDate.text!)
            
                let dataChangedCallback : PopDatePicker.PopDatePickerCallback = { (newDate : Date, forTextField1 : UITextField) -> () in
                
                    // here we don't use self (no retain cycle)
                    forTextField1.text = (newDate.ToDateMediumString() ?? "?") as String
                
                }
            
                popDatePicker!.pick(self, initDate: initDate, dataChanged: dataChangedCallback)
            }
            else{
                toDateSetPriviously = false
            }
            return false
        }


        else {
          
            return true
        }
    }
    
    //#MARK: get search receipts result from server
    func getFormatteddate(_ date : String)-> String{
        let fullString = date
        let StringArray = fullString.characters.split{$0 == "-"}.map(String.init)
        var monthNumber = "01"
        for i in 0 ..< monthsArray.count{
            
            if(monthsArray[i].1 == StringArray[1]){
                monthNumber = monthsArray[i].0
                break
            }
        }
        
        let y = StringArray[2]
        let d = StringArray[0]
        let dateStr = y+"-"+monthNumber+"-"+d
        return dateStr
        
    }
   
    func getReceiptsObjects(){
        var url : String = ""
        var urlString : String = search_url
        
        
        if(vendorNameTextField.text?.characters.count > 0){
            
            var trimmedString = vendorNameTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)
            trimmedString = trimmedString.replacingOccurrences(of: " ", with: "%20")
            
//            var customAllowedSet =  NSCharacterSet(charactersIn:"!@#$%^&*()_+{}[]|<>,.~`/:;?-=\\¥£•¢\"\'"+" ").inverted
//            var escapedString = trimmedString.addingPercentEncoding(withAllowedCharacters: customAllowedSet)
            
            url = url+"vendorName="+trimmedString //escapedString!
            
            
            //self.receiptController2.objSEFilterControl.handler.isHidden = false
            HELPER.search_vendorName = vendorNameTextField.text!
            //print(url)

        }
        else{
            HELPER.search_vendorName = ""
        }
        
        if( !(minAmountTextField.text?.isEmpty)! ){
            let trimmedString = String(minAmountTextField.text!.characters.filter { !" \n\t\r".characters.contains($0) })

            if(url != ""){
                url = url+"&amountMin="+trimmedString
            }
            else{
                url = url+"amountMin="+trimmedString
            }
            //self.receiptController2.objSEFilterControl.handler.isHidden = false
            HELPER.search_min_amount = minAmountTextField.text!
            
        }
        else{
            HELPER.search_min_amount = "0"
        }
        if( !(maxAmountTextField.text?.isEmpty)! ){
            let trimmedString = String(maxAmountTextField.text!.characters.filter { !" \n\t\r".characters.contains($0) })
            
            if(url != ""){
                url = url+"&amountMax="+trimmedString
            }
            else{
                url = url+"amountMax="+trimmedString
            }
            //self.receiptController2.objSEFilterControl.handler.isHidden = false
            HELPER.search_max_amount = maxAmountTextField.text!
            //print(HELPER.search_max_amount)
        }
        else{
            HELPER.search_max_amount = "0"
        }
        if(!(spentOnTextField.text?.isEmpty)!){
            
            var trimmedString = spentOnTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)
            trimmedString = trimmedString.replacingOccurrences(of: " ", with: "%20")
            
            if(url != ""){
            
                url = url+"&spentOn="+trimmedString
            }
            else{
                url = url+"spentOn="+trimmedString
            }
            //self.receiptController2.objSEFilterControl.handler.isHidden = false
            HELPER.search_spentOn = spentOnTextField.text!
            
        }
        else{
            HELPER.search_spentOn = ""
        }
        
        if(!(fromDate.text?.isEmpty)!){
            let trimmedString = String(fromDate.text!.characters.filter { !" \n\t\r".characters.contains($0) })
            let formattedDate = getFormatteddate(trimmedString)
            if(url != ""){
            
                url = url+"&startDt="+formattedDate
            }
            else{
                url = url+"startDt="+formattedDate

            }
            self.receiptController2.objSEFilterControl.hideSelectedTitleColor(self.receiptController2.objSEFilterControl.selectedIndex)
            //self.receiptController2.objSEFilterControl.handler.isHidden = true
            HELPER.search_fromDate = fromDate.text!
            
        }
        else{
            HELPER.search_fromDate = ""
            self.receiptController2.objSEFilterControl.resetSelectedTitleColor(self.receiptController2.objSEFilterControl.selectedIndex)

        }
        if(!(toDate.text?.isEmpty)!){
            let trimmedString = String(toDate.text!.characters.filter { !" \n\t\r".characters.contains($0) })
            let formattedDate = getFormatteddate(trimmedString)

            if(url != ""){
                url = url+"&endDt="+formattedDate
            }
            else{
                url = url+"endDt="+formattedDate

            }
            //self.receiptController2.objSEFilterControl.handler.isHidden = true
            //self.receiptController2.objSEFilterControl.hideSelectedTitleColor(self.receiptController2.objSEFilterControl.selectedIndex)
            HELPER.search_toDate = toDate.text!
            
        }
        else{
            HELPER.search_toDate = ""
            if( fromDate.text?.isEmpty)!{
            //self.receiptController2.objSEFilterControl.resetSelectedTitleColor(self.receiptController2.objSEFilterControl.selectedIndex)
            }

        }

//        if(fromDate.text?.isEmpty == true) && (toDate.text?.isEmpty == true){
//        
//            switch(HELPER.searchDateFilterIndex){
//                
//            case 0 : urlString = search_url
//                break
//                
//            case 1: urlString += "threeMonths=1&"
//                break
//                
//            case 2: urlString += "lastMonth=1&"
//                break
//                
//            default : urlString += "thisMonth=1&"
//                
//            }
//        }
    //url = urlString+url
        
        url = search_url+url
        
        
        self.GetSearchResult(url){
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            //print("got back: \(message)")
            
            if( success == true){
                
                self.totalReceiptsValue = 0.0
                self.totalValue = 0.0
                self.overSpent = 0.0
                self.underSpent = 0.0
                self.totalSessions = 0.0
                self.totalUnuploadedReceipts = 0
                self.difference = 0.0

                
                if (object as? HTTPURLResponse) != nil{
                    
                    // You can print out response object
                   // print("response = \(response)")
                    
                }
                
                do {
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                    if let parseJSON = myJSON![data]  as! [String:Any]?{
                        
                        if (parseJSON as AnyObject).count > 0{
                            
                            if (parseJSON["sessions"] as! NSArray).count > 0{
                                
                                self.difference = NSNumber(value: Double(parseJSON["difference"] as! String)! as Double)
                                self.totalValue = NSNumber(value: Double(parseJSON["totalValue"] as! String)! as Double)
                                self.totalReceiptsValue = NSNumber(value: Double(parseJSON["actualSpend"] as! String)! as Double)
                                
                            }
                            
                            for i in 0 ..<
                                (parseJSON["sessions"]as! NSArray).count  {
                                    let obj = (parseJSON["sessions"] as! NSArray)[i] as! NSDictionary
                                
                                let amount = obj["totalValue"] as! NSNumber
                                let objTitle = obj["spendObjTitle"] as! String
                                let crDate = obj["createDt"] as! String
                                let id = obj["id"] as! NSNumber
                                var rcptDate : String = crDate
                                    
                                    if let dt : String = obj["lastReceiptDate"] as! String{
                                        rcptDate = dt
                                    }
                                
                                
                                    let actualSpend = (obj["actualSpend"] as! NSString)
                                    let myNumber = NSNumber(value: Double(actualSpend as String)! as Double)
                                    self.items.insert((objTitle,crDate,rcptDate,amount,myNumber,id), at: i)
                                    if myNumber == 0{
                                        self.totalUnuploadedReceipts += 1
                                    }
                                
                                var spentOnArray : [(String,NSNumber)] = []
                                for  cnt in 0 ..< (obj["spentOn"]! as AnyObject).count  {
                                    
                                    let spentOn = (obj["spentOn"] as! NSArray)[cnt] as! NSDictionary
                                    let descr = spentOn["category"] as! String
                                    let amt = spentOn["amount"] as! String
                                    let myAmt = NSNumber(value: Double(amt)! as Double)
                                    spentOnArray.insert((descr,myAmt), at: cnt)
                                    
                                }
                                    
                                    self.uploadedReceiptsArray.insert(spentOnArray, at: i)
                            }
                            self.totalSessions = ((parseJSON["sessions"]as! NSArray).count) as NSNumber
                            if (self.totalValue.doubleValue < self.totalReceiptsValue.doubleValue){
                                self.overSpent = self.totalReceiptsValue.doubleValue - self.totalValue.doubleValue as NSNumber
                                
                                self.underSpent = 0.0
                                
                            }
                            else{
                                self.underSpent = self.totalValue.doubleValue - self.totalReceiptsValue.doubleValue as NSNumber
                                self.overSpent = 0.0
                                
                            }
                            self.activityIndicator.hideActivityIndicator(self.view)
                            self.receiptController2.totalUnuploadedReceipts = self.totalUnuploadedReceipts
                            self.updateReceiptController()
                            self.dismiss(animated: false, completion: nil)
                        }
                            
                        else{
                        }
                        
                    }
                    
                }
                catch {
                    //print(error)
                }
                
            }
                
            else{
                
                // If request failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            self.activityIndicator.hideActivityIndicator(self.view)
                            self.receiptController2.totalUnuploadedReceipts = 0
                            self.updateReceiptController()
                            self.dismiss(animated: false, completion: nil)

                        }
                    }
                    catch {
                        //print(error)
                    }
                    
                }
                
                
            }
            
            
        }
        
    }

    
    

    func GetSearchResult( _ urlStr : String, completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
        
        let id =  HELPER.user_id.stringValue
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest("/users/"+id+urlStr, params: nil )
        RequestResponceHelper.get(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.sync(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? Data{
                        //message = passedMessage
                        completion(success, object ,dataObject ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }
    
    func updateReceiptController(){
        //print(self.uploadedReceiptsArray)
        self.receiptController2.objSEFilterControl.handler.isHidden = true
        self.receiptController2.objSEFilterControl.hideSelectedTitleColor(self.receiptController2.objSEFilterControl.selectedIndex)
        self.searchspentValue = self.totalReceiptsValue
        self.receiptController2.searchspentValue = self.searchspentValue
        self.receiptController2.items = self.items
        self.receiptController2.uploadedReceiptsArray = self.uploadedReceiptsArray
        self.receiptController2.totalValue = self.totalValue
        self.receiptController2.overSpent = self.overSpent
        self.receiptController2.underSpent = self.underSpent
        self.receiptController2.totalReceiptsValue = self.totalReceiptsValue
        self.receiptController2.totalSessions = self.totalSessions
        if self.items.count > 1{
            headerLabelString = string_searchHeaderLabel_plural
        }
        else{
            headerLabelString = string_searchHeaderLabel
            
        }
        self.receiptController2.difference = self.difference
        self.receiptController2.loadSearchTableView()
        self.receiptController2.searchButton.setImage(UIImage(named : "search_selected.jpg"), for: UIControlState())
        self.receiptController2.view.alpha = 1
        self.receiptController2.navigationItem.title = receipts_title
        self.receiptController2.tabBarController?.tabBar.alpha = 1
        self.receiptController2.tabBarController?.tabBar.isUserInteractionEnabled = true
    }
    
    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        let error = "Network connection failed..."
        let alertController = UIAlertController(title: login_failed, message: error as? String, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            //print("you have pressed the Cancel button");
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
}
