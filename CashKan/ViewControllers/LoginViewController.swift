//
//  LoginViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 12/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit
import Foundation
import Firebase

extension String {
    
    func hmac( _ key: String) -> String {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = Int(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = Int(CC_SHA256_DIGEST_LENGTH) //algorithm.digestLength
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        //let result = UnsafeMutablePointer<CUnsignedChar>(allocatingCapacity: digestLen)
        let keyStr = key.cString(using: String.Encoding.utf8)
        let keyLen = Int(key.lengthOfBytes(using: String.Encoding.utf8))
        
        CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA256), keyStr!, keyLen, str!, strLen, result)
        
        let digest = stringFromResult(result, length: digestLen)
        
        result.deallocate(capacity: digestLen)
        
        return digest
    }
    
    fileprivate func stringFromResult(_ result: UnsafeMutablePointer<CUnsignedChar>, length: Int) -> String {
        let hash = NSMutableString()
        for i in 0..<length {
            hash.appendFormat("%02x", result[i])
        }
        return String(hash)
    }
    
}

class LoginViewController: UIViewController,UITextFieldDelegate, NetworkFailureDelegate {

    var animateDistance = CGFloat()
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()

    var password: String = ""
    var isLogin : String = ""
    var isRewardsShown : Bool = false
    var isJourneyShown : Bool = false
    var isReceiptsShown : Bool = false
    var isCashShown : Bool = false


    @IBOutlet var passwordTextfield: UITextField!
    @IBOutlet var emailTextfield: UITextField!
    @IBOutlet var rememberMeButton: UIButton!
    @IBOutlet var label: UILabel!
    
    @IBOutlet var forgotPasswordButton: UIButton!
    @IBOutlet var signUpButton: UIButton!
 
    @IBOutlet var upCardImgView: UIImageView!
    @IBOutlet var cashImageView: UIImageView!
    @IBOutlet var receiptsImageView: UIImageView!
    @IBOutlet var journeyImageView: UIImageView!
    @IBOutlet var rewardsImageView: UIImageView!
    
    @IBOutlet var howItWorksButton: UIButton!
    @IBOutlet var privacyButton: UIButton!
    @IBOutlet var termsOfUseButton: UIButton!
    @IBOutlet var contactUs: UIButton!
    @IBOutlet var whatIscashkanButton: UIButton!
    
    // Constraint outlets for imageviews
    
    
    @IBOutlet var rewardsIvTopConstraint: NSLayoutConstraint!
    @IBOutlet var journeyIvTopConstraint: NSLayoutConstraint!
    @IBOutlet var receiptsIvTopConstraint: NSLayoutConstraint!
    @IBOutlet var cashIvTopConstraint: NSLayoutConstraint!
    @IBAction func rememberMeButtonClicked(_ sender: UIButton) {
        
        
        if (rememberMeButton.isSelected)
        {
            rememberMeButton.isSelected = false
                isLogin = "false"
        }
        else
        {
            rememberMeButton.isSelected = true
           isLogin = "true"
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HELPER.setFontFamily( self.view, andSubViews: true)

        isLogin = "false"
        
        self.emailTextfield.backgroundColor = UIColor(red: 139/255, green: 106/255, blue:81/255, alpha: 1.0)
        
        self.passwordTextfield.backgroundColor = UIColor(red: 139/255, green: 106/255, blue:81/255, alpha: 1.0)
        
        let placeholder = NSAttributedString(string: login_email, attributes: [NSForegroundColorAttributeName : UIColor.white])
        emailTextfield.attributedPlaceholder = placeholder;

        
        let placeholder1 = NSAttributedString(string: login_password, attributes: [NSForegroundColorAttributeName : UIColor.white])
        passwordTextfield.attributedPlaceholder = placeholder1;
        
        
        rememberMeButton.setImage(UIImage(named: "01_Remember Me.png"), for: UIControlState())
        rememberMeButton.setImage(UIImage(named: "01_Remember Me_Selected.png"), for: UIControlState.selected)
        
        emailTextfield.delegate = self;
        passwordTextfield.delegate = self;
        
       label.textColor = UIColor(red: 110/255, green: 108/255, blue:108/255, alpha: 1.0)
        upCardImgView.isUserInteractionEnabled = false
        
        self.rewardsImageView.isUserInteractionEnabled = true
        var tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.rewardsImgTapped(_:)))
        self.rewardsImageView.addGestureRecognizer(tapRecognizer)
        
        self.journeyImageView.isUserInteractionEnabled = true
         tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.journeyImgTapped(_:)))
        self.journeyImageView.addGestureRecognizer(tapRecognizer)

        
        self.receiptsImageView.isUserInteractionEnabled = true
         tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.receiptsImgTapped(_:)))
        self.receiptsImageView.addGestureRecognizer(tapRecognizer)

        self.cashImageView.isUserInteractionEnabled = true
         tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.cashImgTapped(_:)))
        self.cashImageView.addGestureRecognizer(tapRecognizer)

        
        self.view.isUserInteractionEnabled = true
        tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(_:)))
        self.view.addGestureRecognizer(tapRecognizer)
}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        HELPER.setFontFamily( self.view, andSubViews: true)
        RequestResponceHelper.delegate = self
        self.label.textColor = textGray_color
        howItWorksButton.setTitleColor(textGray_color, for: UIControlState())
        whatIscashkanButton.setTitleColor(textGray_color, for: UIControlState())
        contactUs.setTitleColor(textGray_color, for: UIControlState())
        termsOfUseButton.setTitleColor(textGray_color, for: UIControlState())
        privacyButton.setTitleColor(textGray_color, for: UIControlState())
        
        var underlinedText = NSAttributedString(string: "Sign Up", attributes: [NSForegroundColorAttributeName : UIColor.white, NSUnderlineStyleAttributeName : 1])
        
        self.signUpButton.setAttributedTitle(underlinedText, for: .normal)
        
        underlinedText = NSAttributedString(string: "Forgot Password", attributes: [NSForegroundColorAttributeName : UIColor.white, NSUnderlineStyleAttributeName : 1])
        
        self.forgotPasswordButton.setAttributedTitle(underlinedText, for: .normal)
        
        if self.view.frame.height < 667{
            
            self.rewardsIvTopConstraint.constant = 27
            self.journeyIvTopConstraint.constant = 27
            self.receiptsIvTopConstraint.constant
             = 27
            self.cashIvTopConstraint.constant
             = 27
        }

    }
    
    func rewardsImgTapped(_ recognizer : UITapGestureRecognizer){
        
        let xPosition = rewardsImageView.frame.origin.x
        let yPosition = rewardsImageView.frame.origin.y
        let height = rewardsImageView.frame.size.height
        let width = rewardsImageView.frame.size.width
        
        if (!isRewardsShown) {
            self.closeImageView()

           UIView.animate(withDuration: 0.5, animations:{self.rewardsImageView.frame = CGRect(x: xPosition,y: yPosition - (height * 0.5),width: width,height: height);})
            isRewardsShown = true;
            
        } else {
            UIView.animate(withDuration: 0.5, animations:{self.rewardsImageView.frame = CGRect(x: xPosition,y: yPosition + (height * 0.5),width: width,height: height);})
            isRewardsShown = false;
        }

    }
    
    func journeyImgTapped(_ recognizer : UITapGestureRecognizer){
        
        let xPosition = journeyImageView.frame.origin.x
        let yPosition = journeyImageView.frame.origin.y
        let height = journeyImageView.frame.size.height
        let width = journeyImageView.frame.size.width
        
        if (!isJourneyShown) {
            self.closeImageView()

            UIView.animate(withDuration: 0.5, animations:{self.journeyImageView.frame = CGRect(x: xPosition,y: yPosition - (height * 0.5),width: width,height: height);})
            isJourneyShown = true;
            
        } else {
            UIView.animate(withDuration: 0.5, animations:{self.journeyImageView.frame = CGRect(x: xPosition,y: yPosition + (height * 0.5),width: width,height: height);})
            isJourneyShown = false;
        }
        
    }

    func receiptsImgTapped(_ recognizer : UITapGestureRecognizer){
        
        let xPosition = receiptsImageView.frame.origin.x
        let yPosition = receiptsImageView.frame.origin.y
        let height = receiptsImageView.frame.size.height
        let width = receiptsImageView.frame.size.width
        
        if (!isReceiptsShown) {
            self.closeImageView()

            UIView.animate(withDuration: 0.5, animations:{self.receiptsImageView.frame = CGRect(x: xPosition,y: yPosition - (height * 0.5),width: width,height: height);})
            isReceiptsShown = true;
            
        } else {
            UIView.animate(withDuration: 0.5, animations:{self.receiptsImageView.frame = CGRect(x: xPosition,y: yPosition + (height * 0.5),width: width,height: height);})
            isReceiptsShown = false;
        }
        
    }

    func cashImgTapped(_ recognizer : UITapGestureRecognizer){
        
        let xPosition = cashImageView.frame.origin.x
        let yPosition = cashImageView.frame.origin.y
        let height : CGFloat = cashImageView.frame.size.height
        let width = cashImageView.frame.size.width
        //height = (cashImageView.image?.size.height)!
        //print(cashImageView.image?.size.height)
        
        if (!isCashShown) {
            self.closeImageView()

            UIView.animate(withDuration: 0.5, animations:{self.cashImageView.frame = CGRect(x: xPosition,y: yPosition - (height * 0.45),width: width,height: height);})
            isCashShown = true;
            
        } else {
            UIView.animate(withDuration: 0.5, animations:{self.cashImageView.frame = CGRect(x: xPosition,y: yPosition + (height * 0.45),width: width,height: height);})
            isCashShown = false;
        }
        
    }
    
    func closeImageView(){
    
        if(isRewardsShown){
            UIView.animate(withDuration: 0.5, animations:{self.rewardsImageView.frame = CGRect(x: self.rewardsImageView.frame.origin.x,y: self.rewardsImageView.frame.origin.y + (self.rewardsImageView.frame.height * 0.5),width: self.rewardsImageView.frame.width,height: self.rewardsImageView.frame.height);})
            isRewardsShown = false;
            
        }
        if(isJourneyShown){
            UIView.animate(withDuration: 0.5, animations:{self.journeyImageView.frame = CGRect(x: self.journeyImageView.frame.origin.x,y: self.journeyImageView.frame.origin.y + (self.journeyImageView.frame.height * 0.5),width: self.journeyImageView.frame.width,height: self.journeyImageView.frame.height);})
            isJourneyShown = false;
            
        }
        if(isReceiptsShown){
            UIView.animate(withDuration: 0.5, animations:{self.receiptsImageView.frame = CGRect(x: self.receiptsImageView.frame.origin.x,y: self.receiptsImageView.frame.origin.y + (self.receiptsImageView.frame.height * 0.5),width: self.receiptsImageView.frame.width,height: self.receiptsImageView.frame.height);})
            isReceiptsShown = false;
            
        }
        if(isCashShown){
            UIView.animate(withDuration: 0.5, animations:{self.cashImageView.frame = CGRect(x: self.cashImageView.frame.origin.x,y: self.cashImageView.frame.origin.y + (self.cashImageView.frame.height * 0.45),width: self.cashImageView.frame.width,height: self.cashImageView.frame.height);})
            isCashShown = false;
            
        }

    }
    
    
    func viewTapped(_ recognizer : UITapGestureRecognizer){
        self.closeImageView()
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
       /* let textFieldRect : CGRect = self.view.window!.convert(textField.bounds, from: textField)
        let viewRect : CGRect = self.view.window!.convert(self.view.bounds, from: self.view)
        
        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
        let numerator : CGFloat = midline - viewRect.origin.y - 0.4 * viewRect.size.height
        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
        
        var heightFraction : CGFloat = numerator / denominator
        
        if heightFraction  > 1.0 {
            heightFraction = 1.0
        }
        
        let orientation : UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
        if (orientation == UIInterfaceOrientation.portrait || orientation == UIInterfaceOrientation.portraitUpsideDown) {
            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
            self.closeImageView()
        } else {
            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
        }
        
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y -= animateDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(TimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        
        self.view.frame = viewFrame
        
        UIView.commitAnimations()*/
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        /*var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y += animateDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        
        UIView.setAnimationDuration(TimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        
        self.view.frame = viewFrame
        
        UIView.commitAnimations()*/
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        if textField.returnKeyType != UIReturnKeyType.done{
            let nextTag: NSInteger = textField.tag+1;
            let nextResponder: UIResponder = (textField.superview?.viewWithTag(nextTag))!
            
            if (nextResponder as UIResponder?) != nil{
                nextResponder.becomeFirstResponder()
            }
            
        }
        else{
            textField.resignFirstResponder()
        }
        
        return false;
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
    }
    
    

    
    @IBAction func loginButtonClicked(_ sender: UIButton) {
        
   
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)

           if (self.emailTextfield.text!.isEmpty)
           {
            let alertController = UIAlertController(title: alert_cashkan, message: alert_enterEmail, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                //print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion:nil)
        }
        else if(!isValidEmail(self.emailTextfield.text!.trimmingCharacters(in: CharacterSet.whitespaces)))
           {
            let alertController = UIAlertController(title: alert_cashkan, message: alert_invalidEmail, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                //print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }
        else if(self.passwordTextfield.text!.isEmpty)
           {
            let alertController = UIAlertController(title: alert_cashkan, message: alert_enterPassword, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                //print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            
        self.present(alertController, animated: true, completion:nil)
        }
        else if !validatePassword(self.passwordTextfield.text!){
            
                let alertController = UIAlertController(title: alert_cashkan, message: alert_invalidPass, preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                    //print("you have pressed the Cancel button");
                }
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion:nil)
                          }
        else
           {
            self.activityIndicator.showActivityIndicatory(self.view)
            self.sendLoginRequest()
        }
        
    }
    

    func sendLoginRequest() -> Void{
        
    emailTextfield.resignFirstResponder()
    passwordTextfield.resignFirstResponder()
        
        
    if Reachability.isConnectedToNetwork() == true {

        var deviceId = ""
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            deviceId = refreshedToken
            //print("InstanceID login token: \(refreshedToken)")

        }
        
        let username = emailTextfield.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let password = passwordTextfield.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        
        let resultPassword = password.hmac(username)
        
        
        self.Newlogin(username, password: resultPassword, deviceId: deviceId){
            (success :  Bool, object : AnyObject?,  dataObject: Data?, message: String?) in
            
            //print("got back: \(message)")
            
            if( success == true){
                
                if let response = object as? HTTPURLResponse{
                    
                    // You can print out response object
                    //print("response = \(response)")
                    
                    // Print out response body
                    let responseString = NSString(data: dataObject!, encoding: String.Encoding.utf8.rawValue)
                    //print("responseString = \(responseString)")
                                if let auth = response.allHeaderFields["Authorization"] as? String {
                        
                                            var myStringArr = auth.components(separatedBy: " ")
                                    //auth.componentsSeparatedByString(" ")
                        
                                            HELPER.authrizationHeader = myStringArr[1]
                        
                                            //print(HELPER.authrizationHeader)
                                }
                    
    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        if let successMsg = myJSON![display_msg]{
                            //print(successMsg)
                        }

                                if let parseJSON = myJSON!["data"] as! [String:Any]?{
                       // Now we can access value of First Name by its key
                                    //print(parseJSON);
                                    HELPER.isLogin = self.isLogin
                                    HELPER.fname                = (parseJSON["fname"] as? String!)!
                                    HELPER.lname                = (parseJSON["lname"] as? String)!
                                    HELPER.user_id              = (parseJSON["user_id"] as! NSNumber)
//                                    HELPER.sex                  = (parseJSON["sex"] as? String)!
                                    //HELPER.dob                  = (parseJSON["dob"] as? String)!
                                    HELPER.email                = (parseJSON["email"] as? String)!
                                    //HELPER.mobile               = (parseJSON["mobile"] as? String)!
                                    HELPER.lat                  = (parseJSON["lat"] as? String)!
                                    HELPER.lng                  = (parseJSON["lng"] as? String)!
                                    HELPER.image_url            = (parseJSON["image_url"] as? String)!
                                    HELPER.regionShortCode      = (parseJSON["regionShortCode"] as? String)!
                                    HELPER.is_active            = (parseJSON["is_active"] as? NSNumber)!
                                    HELPER.total_cashkans       = (parseJSON["total_cashkans"] as? NSNumber)!
                                    HELPER.version_obsolete     = (parseJSON["version_obsolete"] as? NSNumber)!
                                    HELPER.localeInfo           = (parseJSON["localeInfo"] as? NSArray)!
                                    HELPER.currencySymbol = (((parseJSON["localeInfo"] as? NSArray)![0] as? NSDictionary )!["symbol"] as? String)!
                                    
                                    HELPER.localeId       = (((parseJSON["localeInfo"] as? NSArray)![0] as? NSDictionary )!["localeId"] as? NSNumber)!
                                    
                                    HELPER.distanceIn = (((parseJSON["localeInfo"] as? NSArray)![0] as? NSDictionary )!["tdt_uom"] as? String)! 

                                        HELPER.currencySym = HELPER.currencySymbol

                                    if  self.isLogin == "true" {
                                        let defaults = UserDefaults.standard
                                        let dict:NSMutableDictionary = ["user": self.emailTextfield.text!, "password": resultPassword, "isremember": "true", "firstname": HELPER.fname, "lastname": HELPER.lname]
                                        defaults.set(dict, forKey: "userinfo")
                                        defaults.synchronize()
                                    }
                                    
                        }
                        
                    }
                    catch {
                        //print(error)
                    }
                }
                self.activityIndicator.hideActivityIndicator(self.view)

                if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "TabBar") as? UITabBarController {
                  //  resultController.selectedIndex = 2
                    self.present(resultController, animated: true, completion: nil)
                }
                
                
            }
            
            else{
            
                // If Login failed
                
                if( dataObject != nil){
                
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        
                        if (myJSON![error_msg]! as! String).characters.count != 0{
                            let error = myJSON![error_msg]
                            let alertController = UIAlertController(title: login_failed, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            self.passwordTextfield.text = ""
                        }
                        
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            let error = myJSON![display_msg]
                            let alertController = UIAlertController(title: login_failed, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            self.passwordTextfield.text = ""
                        }
                        
                        self.activityIndicator.hideActivityIndicator(self.view)
                        }
                        catch {
                            //print(error)
                        }
                }
            }
        }
    }
        else{
            
            self.activityIndicator.hideActivityIndicator(self.view)
            
            let alertController = UIAlertController(title: network_title, message: network_message, preferredStyle: .alert)
            
            
            let OKAction = UIAlertAction(title: "OK", style: .default) {
                
                (action:UIAlertAction!) in
           
                let settingsUrl = URL(string: "prefs:root=Settings&path=Wifi") //UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url)
                    
                }
             
            }
                
            
            let cancelAction = UIAlertAction(title: "CANCEL", style: .cancel) { (action:UIAlertAction!) in

            }
            
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
            
        }
    }
    
   
    func isValidEmail(_ testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func validatePassword(_ passwordStr : String) -> Bool{
        let length_pass = passwordStr.characters.count
        if( length_pass > 5){
            return true
        }
        return false
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if (segue.identifier == "test1"){
            HELPER.selectedWebViewIndex = 0
        }
        if (segue.identifier == "test2"){
            HELPER.selectedWebViewIndex = 1
            
        }
        if (segue.identifier == "test3"){
            HELPER.selectedWebViewIndex = 2
        }
        if (segue.identifier == "test4"){
            HELPER.selectedWebViewIndex = 3
        }
        if (segue.identifier == "test5"){
            HELPER.selectedWebViewIndex = 4            
        }
        
    }

 
    
    func Newlogin(_ email: String, password: String, deviceId: String,completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data?, _ message: String?) -> ()) {
        
        var loginObject: NSDictionary
        
        if( deviceId == ""){
            loginObject = ["email": email, "password": password]
        }
        else{
             loginObject = ["email": email, "password": password,"deviceId" : deviceId]
        }
        
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest(log_url, params: loginObject as? Dictionary<String, AnyObject> )
        
        RequestResponceHelper.post(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? NSData{
                        //message = passedMessage
                         completion(success, object ,dataObject as Data ,message)
                    }
                    else{
                    
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }
    
    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        let error = "Network connection failed..."
        let alertController = UIAlertController(title: login_failed, message: error as? String, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            //print("you have pressed the Cancel button");
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
    }


}
