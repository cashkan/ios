//
//  MapInfoWindow.swift
//  CashKan
//
//  Created by Shyamal on 10/06/17.
//  Copyright © 2017 Emtarang TechLabs. All rights reserved.
//

import UIKit


class MapInfoWindow: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var HeaderLbl: UILabel!
    @IBOutlet weak var subHeaderLbl: UILabel!
    
    
    override func awakeFromNib() {
        self.frame = CGRect(x: 0, y: 0, width: 150, height: 40)
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "MapInfoWindowView", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }

}
