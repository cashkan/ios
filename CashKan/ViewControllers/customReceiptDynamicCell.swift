//
//  customReceiptDynamicCell.swift
//  CashKan
//
//  Created by emtarang on 15/11/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import Foundation

class customReceiptDynamicCell : UITableViewCell{
    
    
    @IBOutlet var price: UILabel!
    @IBOutlet var obj: UILabel!

    @IBOutlet var priceLblWidthConstant: NSLayoutConstraint!
}
