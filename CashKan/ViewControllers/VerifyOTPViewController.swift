//
//  VerifyOTPViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 13/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class VerifyOTPViewController: UIViewController,UITextFieldDelegate, NetworkFailureDelegate {

    @IBOutlet var label: UILabel!
    @IBOutlet var otpTextField: UITextField!
    
    @IBOutlet var verifyButton: UIButton!
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()

    override func viewDidLoad() {
        super.viewDidLoad()
        HELPER.setFontFamily( self.view, andSubViews: true)

        self.navigationItem.title = otp_title // "Verify OTP"
        
        let btnColor  = UIColor(red: 65/255, green: 89/255, blue: 130/255, alpha: 1) //#415982
        let blueColor = UIColor(red: 73/255, green: 95/255, blue: 135/255, alpha: 1) //#495f87
        label.textColor = blueColor
        
        verifyButton.layer.borderColor = btnColor.cgColor
        verifyButton.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        verifyButton.layer.borderWidth = CGFloat(customButtonBorderWidth)
        verifyButton.setTitleColor( btnColor, for: UIControlState())
        
        otpTextField.delegate = self;

        
        //self.navigationItem.hidesBackButton = true
        
        let button: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        button.setImage(UIImage(named: "Cashkans_Back arrow"), for: UIControlState())
        //add function for button
        button.addTarget(self, action: #selector(VerifyOTPViewController.back(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton;
    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = otp_title
        HELPER.setFontFamily( self.view, andSubViews: true)
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        RequestResponceHelper.delegate = self
    }
    func back(_ sender: UIBarButtonItem) {

        // Go back to the previous ViewController
        self.navigationController?.popViewController(animated: true)
    
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func verifyButtonClicked(_ sender: AnyObject) {
        
        if(Reachability.isConnectedToNetwork() == false){
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                
                self.navigationController!.pushViewController(secondViewController, animated: false)
                
            }
            
        }
        else{
        
        if (self.otpTextField.text!.isEmpty){
            let alertController = UIAlertController(title: alert_cashkan, message: alert_otp, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                //print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }
        
        else{
            // call sendForgotPasswordRequest
            self.activityIndicator.showActivityIndicatory(self.view)
            sendVerifyOtpRequest()
            
        }
        }

    }
    
    func sendVerifyOtpRequest(){
        let otvcTxt = otpTextField.text! as String
        let otvcId = HELPER.otvcId.stringValue
        
        let params = ["otvcId" : otvcId,
                      "otvc" : otvcTxt]

        
        self.verifyOtp(params)
        {
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            //print("got back: \(message)")
            
            if( success == true){
                
                if let response = object as? HTTPURLResponse{
                    
                    // You can print out response object
                    //print("response = \(response)")
                    
                }
                
                
                do {
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                    //print(myJSON)
                    if let parseJSON = myJSON![data] as! [String: Any]?{
                        
                        HELPER.user_id               = (parseJSON["user_id"] as! NSNumber)
                        //print(HELPER.user_id)
                    }
                    
                    if let successMsg = myJSON![display_msg]{
                        //print(successMsg)
                        self.activityIndicator.hideActivityIndicator(self.view)
                        let alertController = UIAlertController(title: alert_cashkan, message: successMsg as? String, preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                            if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "ResetPassword") as? UIViewController {
                                //self.presentViewController(resultController, animated: true, completion: nil)
                                self.navigationController!.pushViewController(resultController, animated: true)
                            }
                            
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion:nil)
                        
                    }
                    
                    
                    
                }
                catch {
                    //print(error)
                }
                
                
                
            }
                
            else{
                
                // If registration failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            let error = myJSON![display_msg]
                            self.activityIndicator.hideActivityIndicator(self.view)
                            let alertController = UIAlertController(title: alert_cashkan, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        //print(error)
                    }
                    
                }
            }
            
            
        }
    }

    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //print("TextField should return method called")
        otpTextField.resignFirstResponder();
        
        return true;
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
    }


    func verifyOtp(_ dictParams: Dictionary<String, String>, completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
        
        
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest(confirm_otp_url, params: dictParams as Dictionary<String, AnyObject>? )
        
        RequestResponceHelper.post(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? NSData{
                        //message = passedMessage
                        completion(success, object ,dataObject as Data ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }
    
    
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                                                 replacementString string: String) -> Bool
    {
        
        let result = true
        
        
        if(textField == otpTextField)
        {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            if(newString.length <= maxLength){
                
            }
            else{
                return false
                
            }
            
            return result
            
        }
        
        
        return result
    
    }
    
    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
            
            self.navigationController!.pushViewController(secondViewController, animated: false)
        }
        
    }


}
