//
//  ReceiptController2.swift
//  cashkan3
//
//  Created by emtarang on 15/06/16.
//  Copyright © 2016 emtarang. All rights reserved.
//

import UIKit
import AMPopTip

/*...to compare dates...*/
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ReceiptController2: UIViewController,XMSegmentedControlDelegate,UIViewControllerTransitioningDelegate,UIPopoverPresentationControllerDelegate, UITableViewDataSource, UITableViewDelegate,CustomCellDelegate, SlideMenuDelegate,UIWebViewDelegate, NetworkFailureDelegate {
    
    var aDelegate: UITabBarController!
    var objSEFilterControl: SEFilterControl!
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()
   
    var bgTapRecognizer:UITapGestureRecognizer!

    //#MARK: UIControls
    @IBOutlet var cashTableView     : UITableView!
    @IBOutlet var searchButton      : UIButton!
    @IBOutlet var totalView         : UIView!
    @IBOutlet var receiptsView      : UIView!
    @IBOutlet var spentView         : UIView!
    @IBOutlet var searchResultView  : UIView!
    @IBOutlet var noReceiptsLabel   : UILabel!
    @IBOutlet var totalValueLabel   : UILabel!
    @IBOutlet var receiptsValueLabel: UILabel!
    @IBOutlet var overSpentValueLabel: UILabel!
    @IBOutlet var spentLabel        : UILabel!
    @IBOutlet var searchSpentText   : UILabel!
    @IBOutlet var searchSpentValue  : UILabel!
    @IBOutlet var monthsSliderView  : UIView!
    @IBOutlet var totalDividerView  : UIView!
    @IBOutlet var dividerView       : UIView!
    @IBOutlet var bottomObjectiveLabel: UILabel!
    @IBOutlet var bottomSpentLabel    : UILabel!
    @IBOutlet var totalSpendObjectiveLabel: UILabel!
    @IBOutlet var refreshView: UIView!
    @IBOutlet var searchBtnView: UIView!
    
    // Table Header View 
    
    @IBOutlet var tableHeaderView: UIView!
    @IBOutlet var objectiveView: UIView!
    @IBOutlet var receiptDateView: UIView!
    @IBOutlet var actualSpentView: UIView!
    @IBOutlet var scanDateView: UIView!
    @IBOutlet var amountView: UIView!
    @IBOutlet var objectiveArrowBtn: UIButton!
    @IBOutlet var objectiveLbl: UILabel!
    @IBOutlet var amountLbl: UILabel!
    @IBOutlet var actualSpentLbl: UILabel!
    @IBOutlet var receiptDateLbl: UILabel!
    @IBOutlet var scanDateLbl: UILabel!
    @IBOutlet var amountArrowBtn: UIButton!
    @IBOutlet var scanDateBtn: UIButton!
    @IBOutlet var actualSpendArrowBtn: UIButton!
    @IBOutlet var receiptDateArrowBtn: UIButton!
    
    @IBOutlet var objectiveArrow_width: NSLayoutConstraint!
    @IBOutlet var scanDateArrow_width: NSLayoutConstraint!
    @IBOutlet var actualSpentArrow_width: NSLayoutConstraint!
    @IBOutlet var receiptDateArrow_width: NSLayoutConstraint!
    @IBOutlet var amountArrow_width: NSLayoutConstraint!
    @IBOutlet var spentView_heigth: NSLayoutConstraint!
    @IBOutlet var dividerView_top: NSLayoutConstraint!

    var popover:UIPopoverController?=nil
    
    var items: [(String,String,String,NSNumber,NSNumber,NSNumber)] = []
    
    var uploadedReceiptsArray : [[(String,NSNumber)]]  = []
    
    var totalReceiptsValue : NSNumber = 0.0
    var totalValue : NSNumber = 0.0
    var overSpent  : NSNumber = 0.0
    var underSpent : NSNumber = 0.0
    var difference : NSNumber = 0.0
    var totalSessions : NSNumber = 0.0
    var totalUnuploadedReceipts : Int = 0
    var searchspentValue : NSNumber = 0.0
    var originalSpentViewHeight : CGFloat = 100.0

    var isAscendingSorted_objective     : Bool = false
    var isAscendingSorted_amount        : Bool = false
    var isAscendingSorted_actualSpend   : Bool = false
    var isAcsendingSorted_scanDate      : Bool = false
    var isAscendingSorted_receiptDate   : Bool = false
    var tableHeaderViewIsOnTop          : Bool = false

    var widthConstant   : NSNumber  = 20
    var actualSpend     : [NSNumber]! = []
    var creationDate    : [String]! = []
    var receiptDate     : [String]! = []
    var spendObjTitle   : [String]! = []
    var totalvalue      : [NSNumber]! = []
    var barButton2      : UIBarButtonItem!
    var barButton1      : UIBarButtonItem!
    var isToolTipVisible : Bool = false
    var offset          : CGFloat = 0.0
    var menuVC          : MenuViewController!
    var fixedSpace      : UIBarButtonItem!
    var negativeSpace   : UIBarButtonItem!
    var barButtonRefresh : UIBarButtonItem!

    var drawerBtn:UIButton!
    weak var webView: UIWebView!
    let notificationButton  = UIButton(type: UIButtonType.custom)

    let listOfNames = [slider_yearToDate, slider_last3months, slider_lastMonth,slider_thisMonth]
    var popTip : AMPopTip!


    //MARK: Initializing
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        self.navigationItem.hidesBackButton = true
        self.tabBarController?.tabBar.barTintColor =  UIColor.white
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.tabBarController?.tabBar.layer.borderColor = lightGray_color.cgColor
        self.tabBarController?.tabBar.layer.borderWidth = 1
        cashTableView.dataSource = self
        cashTableView.rowHeight = 44
        
        self.spentView.isHidden = false
        self.searchResultView.isHidden = true
        
        // action bar buttons
        let button1: UIButton = UIButton(type: UIButtonType.custom)
        button1.setImage(UIImage(named: "Help_icon"), for: UIControlState())
        button1.addTarget(self, action: #selector(ReceiptController2.helpTap), for: UIControlEvents.touchUpInside)
        button1.frame = CGRect(x: 0, y: 200, width: 16, height: 25)
         barButton1 = UIBarButtonItem(customView: button1)
        
        notificationButton.setImage(UIImage(named: "ic_notifications_white"), for: UIControlState())
        notificationButton.addTarget(self, action: #selector(ReceiptController2.notificationTapped), for: UIControlEvents.touchUpInside)
        notificationButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
         barButton2 = UIBarButtonItem(customView: notificationButton)
        self.notificationButton.isHidden = false
        
        // Pull to refresh button
        let button2: UIButton = UIButton(type: UIButtonType.custom)
        button2.setImage(UIImage(named: "ic_action_refresh"), for: UIControlState())
        button2.addTarget(self, action: #selector(self.refreshView(_:)), for: UIControlEvents.touchUpInside)
        button2.frame = CGRect(x: 0, y: 200, width: 30, height: 30)
        
        barButtonRefresh = UIBarButtonItem(customView: button2)
        
        fixedSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 15.0
        
        negativeSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = 0 //-7.0
        
        // Add the rightBarButtonItems on the navigation bar
        self.navigationItem.rightBarButtonItems = [negativeSpace, barButton1, fixedSpace, barButtonRefresh]
        
        let dummyView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 40))
        dummyView.isUserInteractionEnabled = false
        dummyView.isHidden = false
        
        notificationButton.badgeValue = "0"
        notificationButton.badgeOriginX = notificationButton.frame.width-notificationButton.frame.width/2
        notificationButton.badgeOriginY = -7
        notificationButton.insertSubview(dummyView, belowSubview: notificationButton.badge)
        let darkGreen =  cashGreen_color
        notificationButton.badgeBGColor = darkGreen
        notificationButton.badge.isHidden = true
        
        navigationController!.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white]
        
        self.popTip = AMPopTip()
        self.popTip.font = UIFont(name: customAppFont, size: 13)!
        self.popTip.edgeMargin = 5
        self.popTip.offset = 2
        self.popTip.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10)
        self.popTip.popoverColor = orangeReceipt_color
        
        //Drawer button start
        self.drawerBtn = UIButton(type: UIButtonType.custom)
        //set image for button
        drawerBtn.setImage(UIImage(named: "iosdrawericon"), for: UIControlState())
        //add function for button
        drawerBtn.addTarget(self, action: #selector(self.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        drawerBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 25)
        drawerBtn.backgroundColor = UIColor.clear
        let barButton3 = UIBarButtonItem(customView: drawerBtn)
        self.navigationItem.leftBarButtonItem = barButton3;
        //Drawer button end
        
        // change the text color
       
        setViewColor()
        
        // custom slider
        
        objSEFilterControl = SEFilterControl(frame: CGRect(x: 0,y: 0, width: self.monthsSliderView.frame.width,height: self.monthsSliderView.frame.height), titles:listOfNames)

        self.monthsSliderView.addSubview(objSEFilterControl)
        objSEFilterControl.setSelectedIndex(3, animated: false)
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(ReceiptController2.detectTap(_:)))
        objSEFilterControl.addGestureRecognizer(tap)

//        let pan = UIPanGestureRecognizer(target: self, action:#selector(ReceiptController2.detectPan(_:)))
//        objSEFilterControl.addGestureRecognizer(pan)
        
//        self.activityIndicator.showActivityIndicatory(self.view)
        HELPER.searchDateFilterIndex = 3
//        self.getReceiptsObjects(HELPER.searchDateFilterIndex)
        
        self.objectiveView.isUserInteractionEnabled = true
        var sortingReceiptsTap = UITapGestureRecognizer(target: self, action:#selector(ReceiptController2.sortingObjective(_:)))
        objectiveView.addGestureRecognizer(sortingReceiptsTap)
        
        self.amountView.isUserInteractionEnabled = true
        sortingReceiptsTap = UITapGestureRecognizer(target: self, action:#selector(ReceiptController2.sortingAmount(_:)))
        amountView.addGestureRecognizer(sortingReceiptsTap)
        
        self.actualSpentView.isUserInteractionEnabled = true
        sortingReceiptsTap = UITapGestureRecognizer(target: self, action:#selector(ReceiptController2.sortingActualSpend(_:)))
        actualSpentView.addGestureRecognizer(sortingReceiptsTap)
        
        self.scanDateView.isUserInteractionEnabled = true
        sortingReceiptsTap = UITapGestureRecognizer(target: self, action:#selector(ReceiptController2.sortingScanDate(_:)))
        scanDateView.addGestureRecognizer(sortingReceiptsTap)

        self.receiptDateView.isUserInteractionEnabled = true
        sortingReceiptsTap = UITapGestureRecognizer(target: self, action:#selector(ReceiptController2.sortingReceiptDate(_:)))
        receiptDateView.addGestureRecognizer(sortingReceiptsTap)

        
        self.refreshView.isUserInteractionEnabled = true
        let refreshingTap = UISwipeGestureRecognizer(target:self, action:#selector(ReceiptController2.refreshView(_:)))
        refreshingTap.direction = UISwipeGestureRecognizerDirection.down
        self.refreshView.addGestureRecognizer(refreshingTap)
        
        self.view.isUserInteractionEnabled = true
        let tapAnywhere = UITapGestureRecognizer(target:self, action:#selector(self.tapOnSuperview(_:)))
        self.view.addGestureRecognizer(tapAnywhere)
        
        self.searchBtnView.isUserInteractionEnabled = true
        let searchTap = UITapGestureRecognizer(target:self, action:#selector(self.searchBtnClick(_:)))
        self.searchBtnView.addGestureRecognizer(searchTap)
        
        self.tableHeaderView.backgroundColor     = lightGray_color
        
        self.objectiveArrow_width.constant = 0
        self.amountArrow_width.constant = 0
        self.receiptDateArrow_width.constant = 0
        self.actualSpentArrow_width.constant = 0
        self.scanDateArrow_width.constant = CGFloat(widthConstant)
        
    }
    
    func tapOnSuperview(_ sender: UITapGestureRecognizer){
        if(isToolTipVisible){
            isToolTipVisible = false
            popTip.hide()
        }
    }
    
    func refreshView(_ recognizer : AnyObject){
        if (HELPER.isDrawerOpen == false){
        
        //}&& recognizer.direction == .down){
            if(Reachability.isConnectedToNetwork() == false){
                
                DispatchQueue.main.async {
                    HELPER.isReceiptRefreshed = false
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                    
                    self.navigationController!.pushViewController(secondViewController, animated: false)
                    
                }
                
            }
            else{
                self.activityIndicator.showActivityIndicatory(self.view)
                self.activityIndicator.customlabel.isHidden = false
                self.activityIndicator.customlabel.text = receiptsRefreshString
                getReceiptsObjects(HELPER.searchDateFilterIndex )
            }

        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        NotificationCenter.default.post(name: Notification.Name(rawValue: "popDrawerController"), object: nil)

        if(HELPER.isDrawerOpen == true && self.menuVC != nil){
            self.dismissViewBehindWebView()
        }
        RequestResponceHelper.delegate = self

        self.tabBarController?.tabBar.tintColor =  UIColor(red: 191/255, green: 81/255, blue: 64/255, alpha: 1)
        
        self.navigationItem.title           = receipts_title
        HELPER.setFontFamily( self.view, andSubViews: true)
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        navigationController!.navigationBar.barTintColor = UIColor(red: 239/255, green: 126/255, blue: 95/255, alpha: 1) //lightOrange

        
        //setViewColor()
        self.view.alpha = 1.0
        
        self.actualSpendArrowBtn.setImage(UIImage(named: "ic_receipt_asc.png"), for: .selected)
        self.actualSpendArrowBtn.setImage(UIImage(named: "ic_receipt_desc.png"), for: .normal)
        self.objectiveArrowBtn.setImage(UIImage(named: "ic_receipt_asc.png"), for: .selected)
        self.objectiveArrowBtn.setImage(UIImage(named: "ic_receipt_desc.png"), for: .normal)
        self.scanDateBtn.setImage(UIImage(named: "ic_receipt_asc.png"), for: .selected)
        self.scanDateBtn.setImage(UIImage(named: "ic_receipt_desc.png"), for: .normal)
        self.amountArrowBtn.setImage(UIImage(named: "ic_receipt_asc.png"), for: .selected)
        self.amountArrowBtn.setImage(UIImage(named: "ic_receipt_desc.png"), for: .normal)
        self.receiptDateArrowBtn.setImage(UIImage(named: "ic_receipt_asc.png"), for: .selected)
        self.receiptDateArrowBtn.setImage(UIImage(named: "ic_receipt_desc.png"), for: .normal)
        
            }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(!HELPER.isReceiptRefreshed){
            if(Reachability.isConnectedToNetwork() == false){
                
                DispatchQueue.main.async {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                    
                    self.navigationController!.pushViewController(secondViewController, animated: false)
                    
                }
                
            }
            else{
                self.searchButton.setImage(UIImage(named : "Receipts_icon02_Search.png"), for: UIControlState())
                self.activityIndicator.showActivityIndicatory(self.view)
                HELPER.searchDateFilterIndex = 3
                self.objSEFilterControl.setSelectedIndex(3, animated: false)
                getReceiptsObjects(HELPER.searchDateFilterIndex )
                HELPER.isReceiptRefreshed = true
                self.tabBarController?.selectedIndex = 1
                
            }
        }
        

    }
    override func viewWillDisappear(_ animated: Bool) {
        
        if HELPER.isDrawerOpen.boolValue && self.menuVC != nil{
            dismissContainer()
        }
    }
    
    func notificationTapped(_ sender : UIButton){
        if items.count > 0{
  
               if(!isToolTipVisible){
                isToolTipVisible = true
                // Build customview
                let width = self.view.frame.width * 0.8
                let customView = UIView(frame: CGRect(x: 0, y: 0, width:  width, height: 110))
                customView.layer.cornerRadius = 20
                customView.layer.masksToBounds = true

                let label = UILabel(frame: CGRect(x: 10, y: 10, width: customView.frame.width - 20, height: 100 ))
                label.numberOfLines = 0
                label.lineBreakMode = .byWordWrapping
                var toReplace = "few"
                var replacedWith =  String(describing: self.totalUnuploadedReceipts)
                var newString = notificationToolTip.replacingOccurrences(of: toReplace, with:replacedWith)
                
                if(self.totalUnuploadedReceipts <= 1){
                 toReplace = "objectives"
                 replacedWith =  "objective"
                 newString = newString.replacingOccurrences(of: toReplace, with:replacedWith)
                }
                label.text = newString
                label.textAlignment = .left
                label.textColor = UIColor.white
                label.font = UIFont(name : customAppFont, size : 13)
                customView.addSubview(label)
                HELPER.updateWithSpacing(lineSpacing: 2, textView: label)
                self.popTip.showCustomView(customView, direction: .down, in: self.view, fromFrame: CGRect(x : sender.frame.x, y: sender.frame.y + sender.frame.height+10, width: sender.frame.width, height : sender.frame.height))
                self.popTip.arrowSize = CGSize(width : 25, height : 10)
                
                label.isUserInteractionEnabled = true
                let tapOntip = UITapGestureRecognizer(target:self, action:#selector(self.TipViewDidDismiss(_:)))
                label.addGestureRecognizer(tapOntip)
                
            self.popTip.actionAnimation = AMPopTipActionAnimation.float
            }
            else{
                isToolTipVisible = false
                popTip.hide()

            }

            
        }
    }

    
    func setViewColor(){
                        
        let darkGreen =  cashGreen_color
        
        totalView.backgroundColor       = lightOrange_color
        receiptsView.backgroundColor    = lightOrange_color
        spentView.backgroundColor       = darkGreen //darkOrange
        spentLabel.text                 = underSpentStr
        noReceiptsLabel.textColor       = lightOrange_color
        dividerView.backgroundColor     = darkOrange_color
        totalDividerView.backgroundColor = darkOrange_color
        navigationController!.navigationBar.barTintColor = UIColor(red: 239/255, green: 126/255, blue: 95/255, alpha: 1) //lightOrange
        totalSpendObjectiveLabel.textColor = darkOrange_color
        bottomObjectiveLabel.text = objective
        bottomSpentLabel.text = spent
        searchSpentText.text = spent
        searchSpentText.textColor = UIColor.white
        searchResultView.backgroundColor = darkOrange_color
        //spentView.backgroundColor = darkOrange_color

        objectiveView.backgroundColor = darkGray_color
        amountView.backgroundColor = darkGray_color
        actualSpentView.backgroundColor = darkGray_color
        receiptDateView.backgroundColor = darkGray_color
        scanDateView.backgroundColor = darkGray_color

        self.objectiveLbl.textColor       = textGray_color
        self.amountLbl.textColor          = textGray_color
        self.actualSpentLbl.textColor     = textGray_color
        self.receiptDateLbl.textColor     = textGray_color
        self.scanDateLbl.textColor        = textGray_color
        
    }

    //#MARK: notification tip tapped method

    func TipViewDidDismiss(_ recognizer : UITapGestureRecognizer) {
        notificationButton.isUserInteractionEnabled = false
        isToolTipVisible = false
        notificationButton.badge.isHidden = true
        
        // sorting based on actual spend
        let newOrder = items.enumerated().sorted(by: {($0.1.4).doubleValue < ($1.1.4).doubleValue}).map({$0.0})
        items = newOrder.map({items[$0]})
        uploadedReceiptsArray  = newOrder.map({uploadedReceiptsArray[$0]})
        self.resetOtherSortingViews()
        self.actualSpentArrow_width.constant = CGFloat(widthConstant)
        self.actualSpentView.backgroundColor = lightGray_color
        self.actualSpendArrowBtn.isSelected = true
        self.isAscendingSorted_actualSpend = false
        self.cashTableView.reloadData()
        self.cashTableView.setContentOffset(CGPoint.zero, animated: true)
        popTip.hide()
    }
    
    // #MARK: Tap detection on SEFilterControl
    func detectTap(_ recognizer : UITapGestureRecognizer) {
        
        if(popTip != nil){
            isToolTipVisible = false
            popTip.hide()
        }
        
        if(Reachability.isConnectedToNetwork() == false){
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                
                self.navigationController!.pushViewController(secondViewController, animated: false)
                
            }
            
        }
        else{
            objSEFilterControl.tapGestureDetected(recognizer)
            let selected = objSEFilterControl.selectedIndex
            self.objSEFilterControl.handler.isHidden = false
            self.objSEFilterControl.resetSelectedTitleColor(self.objSEFilterControl.selectedIndex)
            
            if !HELPER.isSearchActive{
                self.searchButton.setImage(UIImage(named : "Receipts_icon02_Search.png"), for: UIControlState())
            }
            
            HELPER.searchDateFilterIndex = selected
            self.activityIndicator.showActivityIndicatory(self.view)
            getReceiptsObjects(selected)
            
        }

        
    }
    
    func detectPan(_ recognizer : UIPanGestureRecognizer) {
        
        if(popTip != nil){
            isToolTipVisible = false
            popTip.hide()
        }
        if(Reachability.isConnectedToNetwork() == false){
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                
                self.navigationController!.pushViewController(secondViewController, animated: false)
                
            }
            
        }
        else{
            //objSEFilterControl.panGestureDetected(recognizer)
            let selected = objSEFilterControl.selectedIndex
            self.objSEFilterControl.handler.isHidden = false
            self.objSEFilterControl.resetSelectedTitleColor(self.objSEFilterControl.selectedIndex)
            
            
            if !HELPER.isSearchActive{
                self.searchButton.setImage(UIImage(named : "Receipts_icon02_Search.png"), for: UIControlState())
            }
            
            HELPER.searchDateFilterIndex = selected
            
            self.activityIndicator.showActivityIndicatory(self.view)
            getReceiptsObjects(selected)
            
        }

       
    }
    
    // #MARK: Sorting table view
    
    func resetOtherSortingViews(){
        self.objectiveArrow_width.constant = 0
        self.scanDateArrow_width.constant = 0
        self.amountArrow_width.constant = 0
        self.receiptDateArrow_width.constant = 0
        self.actualSpentArrow_width.constant = 0
        
        objectiveView.backgroundColor = darkGray_color
        scanDateView.backgroundColor = darkGray_color
        amountView.backgroundColor = darkGray_color
        actualSpentView.backgroundColor = darkGray_color
        receiptDateView.backgroundColor = darkGray_color
     }
    
    // sorting based on objective
    func sortingObjective(_ recognizer : UITapGestureRecognizer){
        if items.count > 0{
            self.resetOtherSortingViews()
            self.objectiveArrow_width.constant = CGFloat(widthConstant)
            self.objectiveView.backgroundColor = lightGray_color
            if isAscendingSorted_objective {
                self.objectiveArrowBtn.isSelected = true
                let newOrder = items.enumerated().sorted(by: {$0.1.0<$1.1.0}).map({$0.0})
                items = newOrder.map({items[$0]})
                uploadedReceiptsArray  = newOrder.map({uploadedReceiptsArray[$0]})
                isAscendingSorted_objective = false
            }
            else{
                self.objectiveArrowBtn.isSelected = false
                let newOrder = items.enumerated().sorted(by: {$0.1.0>$1.1.0}).map({$0.0})
                items = newOrder.map({items[$0]})
                uploadedReceiptsArray  = newOrder.map({uploadedReceiptsArray[$0]})
                isAscendingSorted_objective = true
            }
            self.cashTableView.reloadData()
            self.cashTableView.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    
    // sorting based on actual spend
    func sortingActualSpend(_ recognizer : UITapGestureRecognizer){
        if items.count > 0{
            self.resetOtherSortingViews()
            self.actualSpentArrow_width.constant = CGFloat(widthConstant)
            self.actualSpentView.backgroundColor = lightGray_color

            if isAscendingSorted_actualSpend {
                self.actualSpendArrowBtn.isSelected = true
                let newOrder = items.enumerated().sorted(by: {($0.1.4).doubleValue < ($1.1.4).doubleValue}).map({$0.0})
                items = newOrder.map({items[$0]})
                uploadedReceiptsArray  = newOrder.map({uploadedReceiptsArray[$0]})
                isAscendingSorted_actualSpend = false
            }
            else{
                self.actualSpendArrowBtn.isSelected = false
                let newOrder = items.enumerated().sorted(by: {($0.1.4).doubleValue > ($1.1.4).doubleValue}).map({$0.0})
                items = newOrder.map({items[$0]})
                uploadedReceiptsArray  = newOrder.map({uploadedReceiptsArray[$0]})
                isAscendingSorted_actualSpend = true
            }
            self.cashTableView.reloadData()
            self.cashTableView.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    
    // sorting based on amount
    func sortingAmount(_ recognizer : UITapGestureRecognizer){
        if items.count > 0{
            self.resetOtherSortingViews()
            self.amountArrow_width.constant = CGFloat(widthConstant)
            self.amountView.backgroundColor = lightGray_color

            if isAscendingSorted_amount {
                self.amountArrowBtn.isSelected = true
                let newOrder = items.enumerated().sorted(by: {($0.1.3).doubleValue < ($1.1.3).doubleValue}).map({$0.0})
                items = newOrder.map({items[$0]})
                uploadedReceiptsArray  = newOrder.map({uploadedReceiptsArray[$0]})
                isAscendingSorted_amount = false
            }
            else{
                self.amountArrowBtn.isSelected = false
                let newOrder = items.enumerated().sorted(by: {($0.1.3).doubleValue > ($1.1.3).doubleValue}).map({$0.0})
                items = newOrder.map({items[$0]})
                uploadedReceiptsArray  = newOrder.map({uploadedReceiptsArray[$0]})
                isAscendingSorted_amount = true
            }
            self.cashTableView.reloadData()
            self.cashTableView.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    
    // sorting based on scan date
    func sortingScanDate(_ recognizer : UITapGestureRecognizer){
        
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"

        if items.count > 0{
            self.resetOtherSortingViews()
            self.scanDateArrow_width.constant = CGFloat(widthConstant)
            self.scanDateView.backgroundColor = lightGray_color
            if isAcsendingSorted_scanDate {
                self.scanDateBtn.isSelected = true
                let newOrder = items.enumerated().sorted(by: {df.date(from:$0.1.1) < df.date(from:$1.1.1)}).map({$0.0})
                items = newOrder.map({items[$0]})
                uploadedReceiptsArray  = newOrder.map({uploadedReceiptsArray[$0]})
                isAcsendingSorted_scanDate = false
            }
            else{
                self.scanDateBtn.isSelected = false
                let newOrder = items.enumerated().sorted(by: {df.date(from:$0.1.1) > df.date(from:$1.1.1)}).map({$0.0})
                items = newOrder.map({items[$0]})
                uploadedReceiptsArray  = newOrder.map({uploadedReceiptsArray[$0]})
                isAcsendingSorted_scanDate = true
            }
            self.cashTableView.reloadData()
            self.cashTableView.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    
    // sorting based on receipt date
    func sortingReceiptDate(_ recognizer : UITapGestureRecognizer){
        
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"

        if items.count > 0{
            self.resetOtherSortingViews()
            self.receiptDateArrow_width.constant = CGFloat(widthConstant)
            self.receiptDateView.backgroundColor = lightGray_color
            
            if isAscendingSorted_receiptDate {
                self.receiptDateArrowBtn.isSelected = true
                
                let newOrder = items.enumerated().sorted(by: {df.date(from:$0.1.2) < df.date(from:$1.1.2)}).map({$0.0})
                items = newOrder.map({items[$0]})
                uploadedReceiptsArray  = newOrder.map({uploadedReceiptsArray[$0]})
                isAscendingSorted_receiptDate = false
            }
            else{
                self.receiptDateArrowBtn.isSelected = false
                
                let newOrder = items.enumerated().sorted(by: {df.date(from:$0.1.2) > df.date(from:$1.1.2)}).map({$0.0})

                items = newOrder.map({items[$0]})
                uploadedReceiptsArray  = newOrder.map({uploadedReceiptsArray[$0]})
                isAscendingSorted_receiptDate = true
            }
            self.cashTableView.reloadData()
            self.cashTableView.setContentOffset(CGPoint.zero, animated: true)
        }
    }


    
    //MARK: Tableview method

    func heightForView(text:String, index : Int) -> CGFloat{
        
        let width = self.widthForView(index: index)
        let font = UIFont(name : customAppFont, size : 11)
        let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width : width, height : 9999))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func widthForView(index:Int) -> CGFloat{
        
        var maxWidth : CGFloat = 0
         var nArray    : [(String,NSNumber)] = uploadedReceiptsArray[index]
        for k in 0 ..< uploadedReceiptsArray[index].count{
            
            var text : NSString = "" // String(describing: self.receiptsArray[k].1)  as NSString
            
            if(String(describing: nArray[k].1).contains(".")){
                text = String( format: "%.2f", CGFloat(nArray[k].1)) as NSString
            }
            else{
                text =  String(describing: nArray[k].1) as NSString
                
            }
            
            let mWidth : CGFloat = text.size(attributes: [NSFontAttributeName: UIFont(name : customAppFont, size : 11)]).width
            if(mWidth > maxWidth){
                maxWidth = mWidth
            }
        }
        maxWidth = maxWidth + 5
        let newWidth = (self.cashTableView.frame.width * 0.42) - (maxWidth + 10 + 10) - 5
        //cell.contentView.frame.width -  cell.obj.frame.origin.x
        return newWidth
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var hgt : CGFloat = 45
        
        var rArray    : [(String,NSNumber)] = uploadedReceiptsArray[(indexPath as NSIndexPath).row]
        if rArray.count > 0{
            for c in 0..<rArray.count{
                var name = rArray[c].0
                hgt += self.heightForView(text: name, index : (indexPath as NSIndexPath).row)
                hgt += 10
            }
        }
        else{
        
            hgt = 65
        }
        return hgt
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = String(describing: indexPath.row)
        let cellIdentifier = "customCell"+index
        
        var cell:CustomCashTableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CustomCashTableViewCell
        if (cell == nil)
        {
            let nib: NSArray = Bundle.main.loadNibNamed("CustomCashTableCell", owner: self, options: nil) as! NSArray
            
            cell = nib[0] as? CustomCashTableViewCell
        } 
        
        cell!.delegate = self
        cell!.receiptsArray = []
        
        
        if (self.uploadedReceiptsArray.count>0){
            cell!.receiptsArray = uploadedReceiptsArray[(indexPath as NSIndexPath).row]

            cell!.addCustomlabels()
        }

        let pos : Int  = (indexPath as NSIndexPath).row
        if(items.count  > 0){
                let (title,date,rcptDate,cashPrice,receiptPrice,id) = items[(indexPath as NSIndexPath).row]
        
            cell!.loadItem(title,creationDate:date,receiptDate : rcptDate,amount:cashPrice,actualSpend:receiptPrice,pos: pos)
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
    }
    
    func cellImgTapped(_ cell: UIImageView) {
        
        if(popTip != nil){
            isToolTipVisible = false
            popTip.hide()
        }
        self.notificationButton.isUserInteractionEnabled = false
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "ReceiptControllerId") as! ReceiptController
        
        secondViewController.receiptController2 = self
        let title = items[cell.tag].0
        let id = items[cell.tag].5
        secondViewController.spentOn = title
        secondViewController.sessionId = id
        
        self.navigationController!.pushViewController(secondViewController, animated: false)

    }

    //#MARK: view uploaded receipt tapped
    func receiptViewTapped(_ view: UIView) {
        if(popTip != nil){
            isToolTipVisible = false
            popTip.hide()
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "ViewReceiptsController") as! ViewReceiptsController
        
        let id = items[view.tag].5
        
        secondViewController.sessionNumber =  id.stringValue
        
        
        self.navigationController?.pushViewController(secondViewController, animated: false)
        
    }
    
    func loadSearchTableView(){
        
        if(!HELPER.isSearchActive){
            
            self.searchResultView.isHidden = true
            self.spentView.isHidden = false
            
            self.spentView_heigth.constant = self.originalSpentViewHeight
        
            var spentStr : String = ""
            if(self.difference.doubleValue < 0){
                self.difference = (fabs(self.difference.doubleValue)) as NSNumber
            }
            if (self.totalValue.doubleValue < self.totalReceiptsValue.doubleValue){
                self.overSpent = self.difference //self.totalReceiptsValue.doubleValue - self.totalValue.doubleValue
                self.underSpent = 0.0
                if(String(describing: overSpent).contains(".")){
                    spentStr = String( format: "%.2f", CGFloat(overSpent))
                }
                else{
                    spentStr = String(describing: overSpent)
                }
            
                self.spentView.backgroundColor = UIColor(red: 191/255, green: 81/255, blue: 64/255, alpha: 1)
                self.spentLabel.text = overSpentStr
            
            }
            else{
                self.underSpent  = self.difference //self.totalValue.doubleValue - self.totalReceiptsValue.doubleValue
                self.overSpent = 0.0
                if(String(describing: underSpent).contains(".")){
                    spentStr = String( format: "%.2f", CGFloat(underSpent))
                }
                else{
                    spentStr = String(describing: underSpent)
                }
                self.spentView.backgroundColor = cashGreen_color
                self.spentLabel.text = underSpentStr
            }
        
            var receiptsvaluestr : String = ""
            if(String(describing: self.totalReceiptsValue).contains(".")){
                receiptsvaluestr = String( format: "%.2f", CGFloat(self.totalReceiptsValue))
        
            }
            else{
                receiptsvaluestr = String(describing: self.totalReceiptsValue)
            }
            var totalvaluestr : String = ""
            if(String(describing: self.totalValue).contains(".")){
                totalvaluestr = String( format: "%.2f", CGFloat(self.totalValue))
            
            }
            else{
                totalvaluestr = String(describing: self.totalValue)
            }


            self.overSpentValueLabel.text = HELPER.currencySym + spentStr //getAttributedText(spentStr)
            self.totalValueLabel.text = HELPER.currencySym+totalvaluestr
            self.receiptsValueLabel.text = HELPER.currencySym+receiptsvaluestr
        
            if (self.totalValue.doubleValue > 0.0){
                self.cashTableView.reloadData()
                self.cashTableView.isHidden = false
                self.noReceiptsLabel.isHidden = true
                self.tableHeaderView.isHidden = false
            }
            else{
                self.cashTableView.isHidden = true
                self.noReceiptsLabel.isHidden = false
                self.tableHeaderView.isHidden = true
            
            }
        }
        else{
            // if HELPER.isSearchActive = true

            if (self.totalValue.doubleValue > 0.0){
                self.cashTableView.reloadData()
                self.spentView_heigth.constant = self.searchResultView.frame.height
                self.cashTableView.isHidden = false
                self.noReceiptsLabel.isHidden = true
                self.tableHeaderView.isHidden = false
            }
            else{
                self.cashTableView.isHidden = true
                self.noReceiptsLabel.isHidden = false
                self.tableHeaderView.isHidden = true
                
            }
            self.searchspentValue = self.totalReceiptsValue
            self.searchResultView.isHidden = false
            self.spentView.isHidden = true
            self.searchSpentValue.text = HELPER.currencySym+String(describing: searchspentValue)
        }
        if(self.cashTableView.isHidden == false){
            if(cashTableView.contentSize.height <= cashTableView.frame.size.height){
                cashTableView.isScrollEnabled = false
            }
            else{
                cashTableView.isScrollEnabled = true
            }
        }
        var toReplace = "some"
        var replacedWith =  String(describing: self.totalSessions)
        var newString = headerLabelString.replacingOccurrences(of: toReplace, with:replacedWith)
        if newString.contains("selectedMonth"){
            toReplace = "selectedMonth"
            replacedWith = self.listOfNames[Int(self.objSEFilterControl.selectedIndex)]
            newString = newString.replacingOccurrences(of: toReplace, with:replacedWith)
        }
        
        self.totalSpendObjectiveLabel.text = newString
        
        
        if totalUnuploadedReceipts > 0 {
            self.notificationButton.isHidden = false
            self.navigationItem.rightBarButtonItems = [negativeSpace, barButton1, fixedSpace, barButtonRefresh , fixedSpace, barButton2]

            self.notificationButton.isUserInteractionEnabled = true
            self.notificationButton.badge.isHidden = false
            self.notificationButton.badgeValue = String(describing: totalUnuploadedReceipts)
        }
        else{
            self.notificationButton.badge.isHidden = true
            self.navigationItem.rightBarButtonItems = [negativeSpace, barButton1, fixedSpace, barButtonRefresh]

            self.notificationButton.isHidden = true

        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        // scrollingMethod
        let yVelocity: CGFloat = scrollView.panGestureRecognizer.velocity(in: scrollView).y
        if yVelocity < 0 {
            //print("Up")
            
            UIView.animate(withDuration: 0.3, animations: {
                self.dividerView_top.constant = 0
                
            })
            self.view.layoutIfNeeded()
           
        }
        else if yVelocity > 0 {
            //print("Down")
            
            UIView.animate(withDuration: 0.3, animations: {
                self.tableHeaderViewIsOnTop = false
                self.dividerView_top.constant = 86.5
            })
            self.view.layoutIfNeeded()

        }
        else {
            //print("Can't determine direction as velocity is 0")
        }
    }
    func helpTap()
    {
       // print("Hi")
        if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "WebHelp") as? HelpWebViewController {
            //resultController.isFromLogin = false
            resultController.urlToVisit = receipts_helpUrl
            self.navigationController?.pushViewController(resultController, animated: true)
        }
       
    }

    // #MARK: search Button click method
    
    func searchBtnClick(_ sender: UITapGestureRecognizer) {
        
        self.view.alpha = 0.5
        self.navigationItem.title = searchReceipts_title
        self.tabBarController?.tabBar.alpha = 0.5
        tabBarController?.tabBar.isUserInteractionEnabled = false
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "SearchViewControllerId") as! SearchViewController
        
        secondViewController.receiptController2 = self
        secondViewController.view.backgroundColor = UIColor.clear
        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(
            secondViewController,
            animated: false,
            completion: nil)
    }
 
    func getAttributedText(_ spentStr : String) -> NSMutableAttributedString{
        
        let longString = HELPER.currencySym+spentStr
        let longestWordRange = (longString as NSString).range(of: spentStr)
        let attributedString = NSMutableAttributedString(string: longString, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 15), NSForegroundColorAttributeName : UIColor.white])
        
        attributedString.setAttributes([NSFontAttributeName : UIFont.systemFont(ofSize: 25), NSForegroundColorAttributeName : UIColor.white], range: longestWordRange)
        
        return attributedString
    }
    
    
    func xmSegmentedControl(_ xmSegmentedControl: XMSegmentedControl, selectedSegment: Int) {
    }
   
    // #MARK: Get receipts from server
    func changeAlpha() {
        self.view.alpha = 1.0
    }
    
    func getReceiptsObjects(_ index : UInt){
        
        var url : String = ""
        
        if !HELPER.isSearchActive{
        switch(index){
        
            case 0 : url = get_receipts_url_lastyearToDate
            break
            
            case 1: url = get_receipts_url_threeMonth
            break
            
            case 2: url = get_receipts_url_lastMonth
            break
            
            default : url = get_receipts_url_thisMonth
            
            }
        }
            
        else{
            
            var newUrl : String = ""
            
            if(HELPER.search_vendorName != ""){
                
                var trimmedString = HELPER.search_vendorName.trimmingCharacters(in: CharacterSet.whitespaces)
                trimmedString = trimmedString.replacingOccurrences(of: " ", with: "%20")
                if(newUrl == ""){
                    newUrl = newUrl+"vendorName="+trimmedString
                }
                else{
                    newUrl = newUrl+"&vendorName="+trimmedString
                }
            }
            if( HELPER.search_min_amount != "0" ){
                if(newUrl == ""){
                    newUrl = newUrl+"amountMin="+HELPER.search_min_amount
                }
                else{
                    newUrl = newUrl+"&amountMin="+HELPER.search_min_amount
                }
            }
            if( HELPER.search_max_amount != "0" ){
                if(newUrl == ""){
                    newUrl = newUrl+"amountMax="+HELPER.search_max_amount
                }
                else{
                    newUrl = newUrl+"&amountMax="+HELPER.search_max_amount
                }
            }
            
            if(HELPER.search_spentOn != ""){
                var trimmedString = HELPER.search_spentOn.trimmingCharacters(in: CharacterSet.whitespaces)
                trimmedString = trimmedString.replacingOccurrences(of: " ", with: "%20")
                if(newUrl == ""){
                    newUrl = newUrl+"spentOn="+trimmedString
                }
                else{
                    newUrl = newUrl+"&spentOn="+trimmedString
                }
                
            }
            switch(index){
                
            case 0 : url = search_url
                break
                
            case 1: url = search_url+"threeMonths=1&"
                break
                
            case 2: url = search_url+"lastMonth=1&"
                break
                
            default : url = search_url+"thisMonth=1&"
                
            }
            
            if (newUrl != ""){
                url += newUrl
            }

        }
        
        
            self.GetReceipts(url){
                (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
                
                if( success == true){
                    
                    self.totalReceiptsValue = 0.0
                    self.totalValue = 0.0
                    self.overSpent = 0.0
                    self.underSpent = 0.0
                    self.totalSessions = 0.0
                    self.totalUnuploadedReceipts = 0
                    self.difference = 0.0
                    self.items = []
                    self.uploadedReceiptsArray = []
                    self.actualSpend     = []
                    self.creationDate     = []
                    self.receiptDate      = []
                    self.spendObjTitle    = []
                    self.totalvalue      = []
                    self.cashTableView.reloadData()
                    if (object as? HTTPURLResponse) != nil{
                        
                        // You can print out response object
                        //print("response = \(response)")
                        
                    }
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        if let parseJSON = myJSON![data] as! [String:Any]?{
                            
                            if (parseJSON as NSDictionary).count > 0{
                                
                                if (parseJSON["sessions"] as! NSArray).count > 0{
                                
                                    self.difference = NSNumber(value: Double(parseJSON["difference"] as! String)! as Double)
                                    self.totalValue = NSNumber(value: Double(parseJSON["totalValue"] as! String)! as Double)
                                    self.totalReceiptsValue = NSNumber(value: Double(parseJSON["actualSpend"] as! String)! as Double)
                                   
                                }
                                    for var i in 0 ..<
                                    (parseJSON["sessions"]as! NSArray).count  {
                                        
                                    let obj = (parseJSON["sessions"] as! NSArray)[i] as! NSDictionary
                                    
                                    let amount = obj["totalValue"] as! NSNumber
                                    let objTitle = obj["spendObjTitle"] as! String
                                    let crDate = obj["createDt"] as! String
                                    var rcptDate : String = "01-01-1970" //crDate
                                    let id = obj["id"] as! NSNumber
                                        
                                    if let lastDt : String = obj["lastReceiptDate"] as? String{
                                            rcptDate = lastDt
                                        }
                                    let actualSpend = (obj["actualSpend"] as! NSString)
                                    let myNumber = NSNumber(value: Double(actualSpend as String)! as Double)
                                    self.items.insert((objTitle,crDate,rcptDate,amount,myNumber,id), at: i)
                                        if myNumber == 0{
                                            self.totalUnuploadedReceipts += 1
                                        }

                                    var spentOnArray : [(String,NSNumber)] = []
                                    for var cnt in 0 ..< (obj["spentOn"]! as AnyObject).count  {
                                        
                                        let spentOn = (obj["spentOn"] as! NSArray)[cnt] as! NSDictionary
                                        let descr = spentOn["category"] as! String
                                        let amt = spentOn["amount"] as! String
                                        let myAmt = NSNumber(value: Double(amt)! as Double)
                                        spentOnArray.insert((descr,myAmt), at: cnt)
                                        
                                    }

                                    self.uploadedReceiptsArray.insert(spentOnArray, at: i)
                                    
                                }
                                self.totalSessions = (parseJSON["sessions"] as! NSArray).count as NSNumber
                                self.activityIndicator.hideActivityIndicator(self.view)
                                self.cashTableView.isHidden = false
                                self.noReceiptsLabel.isHidden = true
                                self.tableHeaderView.isHidden = false
                                
                                if(self.items.count > 1){
                                    headerLabelString = string_headerLabel_plurals[Int(self.objSEFilterControl.selectedIndex)]
                                }
                                else{
                                    headerLabelString = string_headerLabel[Int(self.objSEFilterControl.selectedIndex)]

                                }
                                if self.items.count > 0{
                                    self.resetOtherSortingViews()
                                    self.scanDateArrow_width.constant = CGFloat(self.widthConstant)
                                    self.scanDateView.backgroundColor = lightGray_color
                                    self.scanDateBtn.isSelected = false
                                    let df = DateFormatter()
                                    df.dateFormat = "dd-MM-yyyy"
                                    let newOrder = self.items.enumerated().sorted(by: {df.date(from:$0.1.1) > df.date(from:$1.1.1)}).map({$0.0})
                                    self.items = newOrder.map({self.items[$0]})
                                    self.uploadedReceiptsArray  = newOrder.map({self.uploadedReceiptsArray[$0]})
                                    self.isAcsendingSorted_scanDate = true
                                    
                                    self.cashTableView.reloadData()
                                    self.cashTableView.setContentOffset(CGPoint.zero, animated: true)
                                }

                                self.loadSearchTableView()
                     
                            }
                                
                            else{
                                self.activityIndicator.hideActivityIndicator(self.view)
                                self.cashTableView.isHidden = true
                                self.noReceiptsLabel.isHidden = false
                                self.tableHeaderView.isHidden = true
                                self.overSpentValueLabel.text = HELPER.currencySym + "0" //self.getAttributedText("0")
                                self.totalValueLabel.text = HELPER.currencySym+"0"
                                self.receiptsValueLabel.text = HELPER.currencySym+"0"
                                headerLabelString = string_headerLabel[Int(self.objSEFilterControl.selectedIndex)]
                                
                            }
                            
                        }
                        else{
                            
                            self.activityIndicator.hideActivityIndicator(self.view)
                            
                        }
                    
                    }
                    catch {
                        //print(error)
                    }
                    
                }
                    
                else{
                    
                    // If result failed
                    
                    if( dataObject != nil){
                        
                        do {
                            let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                            
                            if (myJSON![display_msg]! as! String).characters.count != 0{
                                let error = myJSON![display_msg]
                                
                                self.activityIndicator.hideActivityIndicator(self.view)
                                
                                self.cashTableView.isHidden = true
                                self.noReceiptsLabel.isHidden = false
                                self.tableHeaderView.isHidden = true
                                self.overSpentValueLabel.text = HELPER.currencySym + "0"//self.getAttributedText("0")
                                self.totalValueLabel.text = HELPER.currencySym+"0"
                                self.receiptsValueLabel.text = HELPER.currencySym+"0"
                                
                            }
                        }
                        catch {
                            //print(error)
                        }
                        
                    }
                    else{
                        self.activityIndicator.hideActivityIndicator(self.view)
                    }
                }
            }
    
    }
    
    
    func GetReceipts( _ urlStr : String, completion:@escaping(_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
        
        let id =  HELPER.user_id.stringValue
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest("/users/"+id + urlStr, params: nil )
        RequestResponceHelper.get(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.sync(execute: { () -> Void in
                
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                }
                else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? Data{
                        //message = passedMessage
                        completion(success, object ,dataObject ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
            
        }
    }

    
    //MARK: left drawer method start
    func dismissViewBehindWebView(){
        self.view.removeGestureRecognizer(bgTapRecognizer)
        
        //        self.slideMenuItemSelectedAtIndex(-1);
        let viewMenuBack : UIView = view.subviews.last!
        
        if viewMenuBack.tag == 123 {
            HELPER.isDrawerOpen = false
            self.drawerBtn.tag = 0
            
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * (UIScreen.main.bounds.size.width-drawerOffset)
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            
        }
        let subViews = self.view.subviews
        for subview in subViews{
            if subview.tag == 111 {
                subview.removeFromSuperview()
            }
        }
        self.tabBarController?.tabBar.isUserInteractionEnabled = true

        return
        
    }

    func dismissContainer(){
        
        self.view.removeGestureRecognizer(bgTapRecognizer)

//        self.slideMenuItemSelectedAtIndex(-1);
        let viewMenuBack : UIView = view.subviews.last!
        
        if viewMenuBack.tag == 123 {
            HELPER.isDrawerOpen = false
            self.drawerBtn.tag = 0
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * (UIScreen.main.bounds.size.width-drawerOffset)
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    //viewMenuBack.removeFromSuperview()
            })
        }
        let subViews = self.view.subviews
        for subview in subViews{
            if subview.tag == 111 {
                subview.removeFromSuperview()
            }
        }
        self.tabBarController?.tabBar.isUserInteractionEnabled = true

        return
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        if index > 2 {
            self.navigationItem.title = Drawer_titles[Int(index)]
        }
    }
    
    func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
//            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            HELPER.isDrawerOpen = false
            
            let viewMenuBack : UIView = view.subviews.last!
            if viewMenuBack.tag == 123 {
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    var frameMenu : CGRect = viewMenuBack.frame
                    frameMenu.origin.x = -1 * (UIScreen.main.bounds.size.width-drawerOffset)
                    viewMenuBack.frame = frameMenu
                    viewMenuBack.layoutIfNeeded()
                    viewMenuBack.backgroundColor = UIColor.clear
                    }, completion: { (finished) -> Void in
                        //viewMenuBack.removeFromSuperview()
                })
            }
            let subViews = self.view.subviews
            for subview in subViews{
                if subview.tag == 111 {
                    subview.removeFromSuperview()
                }
            }
            
            self.tabBarController?.tabBar.isUserInteractionEnabled = true

            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        HELPER.isDrawerOpen = true
        
        if(menuVC == nil){
            menuVC = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        }
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.view.bringSubview(toFront: menuVC.view)
        
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        let dummyDisabledView: UIView = UIView.init(frame: self.view.frame)
        dummyDisabledView.tag = 111
        dummyDisabledView.alpha = 0.5
        dummyDisabledView.backgroundColor = darkGray_color
        
        self.view.insertSubview(dummyDisabledView, belowSubview: menuVC.view)
        
        bgTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(bgTapped(_:)))
        
        self.view.addGestureRecognizer(bgTapRecognizer)
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-drawerOffset-40, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.menuVC.view.frame=CGRect(x: 0, y: 64, width: UIScreen.main.bounds.size.width-drawerOffset-40, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
            }, completion:nil)
        
        self.tabBarController?.tabBar.isUserInteractionEnabled = false

    }
    
    //MARK: left drawer method end

    func bgTapped(_ sender: UITapGestureRecognizer) {
        dismissContainer()
        self.tabBarController?.tabBar.isUserInteractionEnabled = true

    }
    
    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
            
            self.navigationController!.pushViewController(secondViewController, animated: false)
        }
        
    }
}


