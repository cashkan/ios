//
//  JourneyMapViewController.swift
//  CashKan
//
//  Created by emtarang on 10/04/17.
//  Copyright © 2017 Emtarang TechLabs. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import AVFoundation

class JourneyMapViewController: UIViewController, NetworkFailureDelegate, GMSMapViewDelegate {
    var resultController            : HelpWebViewController!
    var activityIndicator           : CustomActivityIndicator = CustomActivityIndicator()
    var isAddedAsChildViewController: Bool = false
    var barButtonHelp               : UIBarButtonItem!
    let regionRadius                : CLLocationDistance = 1000
    var shootingStarAnimation       : ShootingStar! = ShootingStar()
    //let locationManager     = CLLocationManager()
    var markers             = [GMSMarker()]
    var startMarker = GMSMarker()
    var denomination_detail : String?
    var active_detail       : String?
    var inactive_detail     : String?
    var isInitialized       = false
    
    var cashkanId : [NSNumber]!     = []
    var currencyNo : [String]!      = []
    var denomination : [NSNumber]!  = []
    var distance : [NSNumber]!      = []
    var latitude : [String]         = []
    var longitude : [String]        = []
    var receiptId : [NSNumber]!     = []
    var sessionId : [NSNumber]!     = []
    var spentOn : [String]! = []
    var vendorOptions : [(String,String,String)]! = [] //     (Vendor,Abbs,"Pizza hut");
    
    var SP_CashkanId : NSNumber = 0
    var SP_currencyNo : String!
    var SP_denomination : NSNumber!
    var SP_tev : NSNumber!
    var SP_latitude : String!
    var SP_longitude : String!
    var SP_tdt : NSNumber!
    var SP_sessionId : NSNumber!
    
    var totalRewardPoints : NSNumber!
    var availablePoints : NSNumber!
    var distanceToTravel : NSNumber!
    var redeemedPoints : NSNumber!
    var rewardPointsToEarn : NSNumber!
    
    var selectedMarker : Int! = 0
    var player : AVAudioPlayer! = nil
    private var infoWindow = MapInfoWindow()
    fileprivate var locationMarker : GMSMarker? = GMSMarker()
    
    @IBOutlet var mapview: GMSMapView!
    @IBOutlet var btn1: UIButton!
    @IBOutlet var btn2: UIButton!
    @IBOutlet var btn3: UIButton!
    @IBOutlet var label1: UILabel!
    @IBOutlet var label2: UILabel!
    @IBOutlet var label3: UILabel!
    @IBOutlet var HeaderLabel: UILabel!
    @IBOutlet var guessOptLabel: UILabel!
    @IBOutlet var rewardPoints: SACountingLabel!
    @IBOutlet var topTileHeaderIV: UIImageView!
    @IBOutlet var rewardsBgIV: UIImageView!
    @IBOutlet var totalRewardsLbl: UILabel!
    @IBOutlet var blackStripIV: UIImageView!
    @IBOutlet var bottomBoxStripIV: UIImageView!
    @IBOutlet var mapViewBottomConstraint: NSLayoutConstraint!
    
//    @IBOutlet var bottomContainerView: UIView!
    
        override func viewDidLoad() {
        super.viewDidLoad()
            
            self.hideAll(true)

        RequestResponceHelper.delegate = self
        self.mapview.delegate = self
            
        // Do any additional setup after loading the view.
        self.navigationItem.hidesBackButton = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        let button: UIButton = UIButton(type: UIButtonType.custom)
        button.setImage(UIImage(named: "Cashkans_Back arrow"), for: UIControlState())
        button.addTarget(self, action: #selector(self.back(_:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton;
        
        let button1: UIButton = UIButton(type: UIButtonType.custom)
        button1.setImage(UIImage(named: "Help_icon"), for: UIControlState())
        button1.addTarget(self, action: #selector(self.helpTap), for: UIControlEvents.touchUpInside)
        button1.frame = CGRect(x: 0, y: 200, width: 16, height: 25)
        
        barButtonHelp = UIBarButtonItem(customView: button1)
        self.navigationItem.rightBarButtonItem = barButtonHelp;
            
        let myMutableString = NSMutableAttributedString(string: "0")
        myMutableString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 21.0), NSForegroundColorAttributeName : UIColor.white], range: NSRange(
                location: 0,
                length: myMutableString.length))
            
        self.rewardPoints.attributedText = myMutableString
        self.rewardPoints.font = UIFont(name: customAppFont, size: 21)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        HELPER.setFontFamily( self.view, andSubViews: true)
        HELPER.setFontFamily(self.infoWindow, andSubViews:true)
        RequestResponceHelper.delegate = self
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        self.navigationItem.title = journeyDetail_title
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.infoWindow.frame = CGRect(x: 0, y: 0, width: 150, height: 55)
        
        self.btn1.setBackgroundImage(UIImage(named:"bottom-box.png"), for: UIControlState.normal)
        self.btn2.setBackgroundImage(UIImage(named:"bottom-box.png"), for: UIControlState.normal)
        self.btn3.setBackgroundImage(UIImage(named:"bottom-box.png"), for: UIControlState.normal)
        
        self.label1.layer.cornerRadius = 5
        self.label1.layer.masksToBounds = true
        self.label2.layer.cornerRadius = 5
        self.label2.layer.masksToBounds = true
        self.label3.layer.cornerRadius = 5
        self.label3.layer.masksToBounds = true
        
//        if HELPER.backToRoot == true{
//            HELPER.backToRoot = false
//            self.navigationController?.popViewController(animated: false)
//        }
//        else{
            if(Reachability.isConnectedToNetwork() == false){
                
                self.networkFailed()
            }
            else{
                self.hideAll(true)
                activityIndicator.showActivityIndicatory(self.view)
                getCashkans()
            }
        
    }
    
    
    func back(_ sender: UIBarButtonItem) {
        self.navigationItem.title = journeyDetail_title
        if(isAddedAsChildViewController){
            if((self.resultController) != nil){
                resultController.willMove(toParentViewController: nil)
                resultController.view.removeFromSuperview()
                resultController.removeFromParentViewController()
                isAddedAsChildViewController = false
                self.navigationItem.rightBarButtonItem = barButtonHelp;
            }
        }
        else{
            
            // Go back to the previous ViewController
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func networkFailed(){
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
            HELPER.isConnectedToNetwork = false

            self.navigationController!.pushViewController(secondViewController, animated: false)
        }
        
    }
    
    
    // #MARK: get all cashkans
    
    func getCashkans(){
        self.GetAllCashkans(){
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            //print("got back: \(message)")
            
            if( success == true){ //if1
                
                self.btn1.isUserInteractionEnabled = true
                self.btn2.isUserInteractionEnabled = true
                self.btn3.isUserInteractionEnabled = true
                
                self.activityIndicator.hideActivityIndicator(self.view)
                if (object as? HTTPURLResponse) != nil{ //if2
                    
                    // You can print out response object
                    // print("response = \(response)")
                    
                }//endif2
                
                do {
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                    if let parseJSON = myJSON![data]  as! [String:Any]?{ //if3
                        
                        if (parseJSON as AnyObject).count > 0{ //if4
                            
                            let destinationPts = (parseJSON["destinationPoints"] as! NSArray)
                            for  i in 0 ..< (destinationPts as AnyObject).count  {
                                
                                let obj = (destinationPts )[i] as! NSDictionary
                                self.cashkanId.insert(obj["cashkanId"] as! NSNumber, at:i)
                                self.currencyNo.insert(obj["currencyNo"] as! String , at: i)
                                self.denomination.insert(obj["denomination"] as! NSNumber, at: i)
                                self.latitude.insert((obj["latitude"] as! NSString) as String, at: i)
                                
                                self.longitude.insert((obj["longitude"] as! NSString) as String, at: i)
                                
                                self.distance.insert(obj["distance"] as! NSNumber, at: i)
                                self.receiptId.insert(obj["receiptId"] as! NSNumber, at: i)
                                self.sessionId.insert(obj["sessionId"] as! NSNumber, at: i)
                                self.spentOn.insert(obj["spentOn"] as! String, at: i)
                                
                                let vendor  = (obj["vendorOptions"]) as! [String]
                                self.vendorOptions.insert((vendor[0],vendor[1],vendor[2]), at: i)
                                
                            }
                            // Get Starting point details 
                            let startPt = parseJSON["startingPoint"] as AnyObject
                                self.SP_CashkanId = startPt["cashkanId"] as! NSNumber
                                self.SP_currencyNo = startPt["currencyNo"] as! String
                                self.SP_denomination = startPt["denomination"] as! NSNumber
                                self.SP_latitude = startPt["latitude"]  as! String
                                self.SP_longitude = startPt["longitude"]  as! String
                                self.SP_tev = startPt["tev"] as! NSNumber
                                self.SP_tdt = startPt["tdt"] as! NSNumber
                                self.SP_sessionId = startPt["sessionId"] as! NSNumber
                            
                            
                            // Get other details
                            self.totalRewardPoints = parseJSON["totalRewardPoints"] as! NSNumber
                            self.redeemedPoints = parseJSON["redeemedPoints"] as! NSNumber
                            self.availablePoints = parseJSON["availablePoints"] as! NSNumber
                            self.distanceToTravel = parseJSON["distanceToTravel"] as! NSNumber
                            self.rewardPointsToEarn = parseJSON["rewardPointsToEarn"] as! NSNumber
                            
                            let myMutableString = NSMutableAttributedString(string: self.availablePoints.stringValue)
                            myMutableString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 21.0), NSForegroundColorAttributeName : UIColor.white], range: NSRange(
                                location: 0,
                                length: myMutableString.length))
                            
                            self.rewardPoints.attributedText = myMutableString
                            HELPER.setFontFamily( self.rewardPoints, andSubViews: false)

                            
                            self.loadInitialData()
                            
                            
                        }// if 4
                        else{
                        }
                        
                    }// if 3
                    else{
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        if (myJSON![error_msg]! as! String).characters.count != 0{
                            self.activityIndicator.hideActivityIndicator(self.view)
                            let alertController = UIAlertController(title: alert_cashkan, message: myJSON![error_msg]! as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_cancel, style: .cancel) { (action:UIAlertAction!) in
                                self.navigationController?.popViewController(animated: true)
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                    }
                    }
                    
                }
                catch {
                    //print(error)
                }
                
            }
                
            else{
                
                // If request failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        if (myJSON![error_msg]! as! String).characters.count != 0{
                            self.activityIndicator.hideActivityIndicator(self.view)
                            let alertController = UIAlertController(title: "No Destinations!!!", message: myJSON![error_msg]! as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                
                                HELPER.isJourneyRefreshed = false
                                self.navigationController?.popViewController(animated: true)
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        //print(error)
                    }
                    
                }
                
                
            }

    }
    }
    
    func GetAllCashkans( _ completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
        
        let id =  HELPER.user_id.stringValue
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest("/users/"+id+CashkansForMpView_url+"denomination="+self.denomination_detail!, params: nil )
        
        RequestResponceHelper.get(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.sync(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? Data{
                        //message = passedMessage
                        completion(success, object ,dataObject ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }
    

    // #MARK: MapView delegate methods

    func loadInitialData() {

        label1.attributedText = setShadowToLabelText(self.vendorOptions[0].0, textSize: 14)
        label2.attributedText = setShadowToLabelText(self.vendorOptions[0].1, textSize: 14)
        label3.attributedText = setShadowToLabelText(self.vendorOptions[0].2, textSize: 14)
        
        self.selectedMarker = 0
        // draw header label with image2String
        let fullString = NSMutableAttributedString(string: "Earn ")
        
        // create our NSTextAttachment
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = HELPER.changeImageSize(UIImage(named: "icon-rewards.png")!,newSize: CGSize(width: 20, height: 20))
        // wrap the attachment in its own attributed string so we can append it
        let image1String = NSAttributedString(attachment: image1Attachment)
        // add the NSTextAttachment wrapper to our full string, then add some more text.
        fullString.append(image1String)
        
        fullString.append(NSAttributedString(string: " "+self.rewardPointsToEarn.stringValue + " reward points when you travel "))
        let image2Attachment = NSTextAttachment()
        image2Attachment.image = HELPER.changeImageSize(UIImage(named: "map-mile.png")!,newSize: CGSize(width: 18, height: 20))
        let image2String = NSAttributedString(attachment: image2Attachment)
        fullString.append(image2String)
        
        var longestWord = self.rewardPointsToEarn.stringValue
        
        let longestWordRange = (fullString.string as NSString).range(of: longestWord)
        
        fullString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 13), NSForegroundColorAttributeName : orangeReceipt_color], range: longestWordRange)
        
        let attributedStr2 = NSMutableAttributedString(string: self.distanceToTravel.stringValue)
        let longestWord2 = self.distanceToTravel.stringValue
        
        let longestWordRange2 = (attributedStr2.string as NSString).range(of: longestWord2)
        
        attributedStr2.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: 13), NSForegroundColorAttributeName : savingsGreen_color], range: longestWordRange2)
        fullString.append(attributedStr2)
        fullString.append(NSAttributedString(string:" "+HELPER.distanceIn.lowercased()))
        
        self.HeaderLabel.attributedText = fullString
        
        // set bottom guess label text
        let longString = journey_mapGuessStr
        var stringToReplace = "dist"
        longestWord = String(format:"%.2f",CGFloat(self.distance[0]))
        var long_spendString = longString.replacingOccurrences(of: stringToReplace, with: String(describing: longestWord))
        stringToReplace = "param"
        longestWord = (String(HELPER.distanceIn)).lowercased()
         long_spendString = long_spendString.replacingOccurrences(of: stringToReplace, with: String(HELPER.distanceIn).lowercased())
        
        self.guessOptLabel.text = long_spendString
        let startMarkerImage : UIImage = HELPER.changeImageSize(UIImage(named: "map-pin.png")!,newSize: CGSize(width: 30, height: 40))
        let MarkerImage : UIImage = HELPER.changeImageSize(UIImage(named: "map-pin2.png")!,newSize: CGSize(width: 30, height: 40))
        
        // set starting point marker
        startMarker.position = CLLocationCoordinate2D(latitude: Double(self.SP_latitude)!, longitude: Double(self.SP_longitude)!)
        startMarker.icon = startMarkerImage
        startMarker.map = self.mapview
        startMarker.zIndex = 10000
        
        
        //set other markers
        //for i in 0..<self.cashkanId.count{
        let i = 0
        markers.append(GMSMarker())
        markers[i].position = CLLocationCoordinate2D(latitude: Double(self.latitude[i])!, longitude: Double(self.longitude[i])!)
        markers[i].icon = MarkerImage
        markers[i].map = self.mapview
        markers[i].iconView?.tag = i
        markers[i].zIndex = Int32(i)
        
        //}
        
        
        let mk1 = CLLocationCoordinate2D(latitude: Double(SP_latitude)!, longitude: Double(SP_longitude)!)
        let mk2 = markers[0].position
        let bounds = GMSCoordinateBounds(coordinate: mk1, coordinate: mk2)
        let camera = mapview.camera(for: bounds, insets: UIEdgeInsetsMake(100 , 100, 100, 100))!
        self.mapview.camera = camera
        self.infoWindow = loadNiB()
        
        let result = self.mapView(self.mapview, didTap: markers[0])
        
    }
    
    //#MARK: bottom btn click methods
    
    @IBAction func FirstButtonClicked(_ sender: UIButton) {
 
        let startPoint = ["cashkanId" : String(describing: SP_CashkanId)]
        let destinationPoint = [
            "cashkanId" : String(describing: cashkanId[self.selectedMarker as Int]),
            "receiptId" : String(describing: receiptId[self.selectedMarker as Int])]
        
        let params : [String : AnyObject] = (["startingPoint"       : startPoint,
                      "destinationPoint"    : destinationPoint,
                      "userAnswer"          : self.label1.text ?? "" ] as NSDictionary) as! [String : AnyObject]
        
        self.callGameApi(params ,sender: sender)
    }
    
    @IBAction func SecondButtonClicked(_ sender: Any) {
        let startPoint = ["cashkanId" : String(describing: SP_CashkanId)]
        let destinationPoint = [
            "cashkanId" : String(describing: cashkanId[self.selectedMarker as Int]),
            "receiptId" : String(describing: receiptId[self.selectedMarker as Int])]
        
        let params : [String : AnyObject] = (["startingPoint"       : startPoint,
                                              "destinationPoint"    : destinationPoint,
                                              "userAnswer"          : self.label2.text ?? "" ] as NSDictionary) as! [String : AnyObject]
        
        self.callGameApi(params ,sender: sender as! UIButton)
    }
    
    @IBAction func ThirdButtonClicked(_ sender: Any) {
        let startPoint = ["cashkanId" :  self.SP_CashkanId.stringValue]
        let destinationPoint = [
            "cashkanId" : String(describing: cashkanId[self.selectedMarker as Int]),
            "receiptId" : String(describing: receiptId[self.selectedMarker as Int])]
        
        let params : [String : AnyObject] = (["startingPoint"       : startPoint,
                                              "destinationPoint"    : destinationPoint,
                                              "userAnswer"          : self.label3.text ?? "" ] as NSDictionary) as! [String : AnyObject]
        
        self.callGameApi(params ,sender: sender as! UIButton)
    }
    
    //#MARK: sound play method
    func playSound(_ flag:Bool){
        var soundFile : String = ""
        if(flag){
            soundFile = "c_answer"
        }
        else{
            soundFile = "w_answer1"
        }
        guard let url = Bundle.main.url(forResource: soundFile, withExtension: "mp3") else {
            return
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player.play()
        } catch let error {
            
        }
        
    }
    func hideAll(_ flag : Bool){
        
        self.btn1.backgroundColor = nil
        self.btn2.backgroundColor = nil
        self.btn3.backgroundColor = nil

        self.label1.backgroundColor = nil
        self.label2.backgroundColor = nil
        self.label3.backgroundColor = nil

        self.btn1.isHidden = flag
        self.btn2.isHidden = flag
        self.btn3.isHidden = flag
        self.label1.isHidden = flag
        self.label2.isHidden = flag
        self.label3.isHidden = flag
        self.HeaderLabel.isHidden = flag
        self.guessOptLabel.isHidden = flag
        self.blackStripIV.isHidden = flag
        self.bottomBoxStripIV.isHidden = flag
        self.mapview.isHidden = flag
        //self.bottomContainerView.isHidden = flag
    }

    //#MARK: Call geme api
    func callGameApi( _ dictParams : Dictionary<String, AnyObject>, sender : UIButton){
        self.btn1.isUserInteractionEnabled = false
        self.btn2.isUserInteractionEnabled = false
        self.btn3.isUserInteractionEnabled = false

        if Reachability.isConnectedToNetwork() == true {
            self.activityIndicator.showActivityIndicatory(self.view)

            self.CallGame(dictParams )
            {
                (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
                self.activityIndicator.hideActivityIndicator(self.view)

                if( success == true){
                    
                    if (object as? HTTPURLResponse) != nil{
                        
                        // You can print out response object
                        // print("response = \(response)")
                    }
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        if let parseJSON = myJSON![data]  as! [String:Any]?{
                            if (parseJSON as AnyObject).count > 0{
                                
                                let new_availablePoints = parseJSON["availablePoints"] as! NSNumber
                                let gamewon = parseJSON["gameWon"] as! Bool
                                if(gamewon == true){
                                    
                                    self.playSound(true)
                                    
                                    if(sender.tag == 0){
                                     self.label1.backgroundColor = savingsGreen_color
                                    }
                                    if(sender.tag == 1){
                                        self.label2.backgroundColor = savingsGreen_color
                                    }
                                    if(sender.tag == 2){
                                        self.label3.backgroundColor = savingsGreen_color
                                    }
                                    
                                    sender.isHidden = true
                                    let index = self.selectedMarker as Int
                                    let coordinate  = CLLocationCoordinate2D(latitude: Double(self.latitude[index])!, longitude: Double(self.longitude[index])!)
                                    
                                    CATransaction.begin()
                                    CATransaction.setAnimationDuration(2.0)
                                    self.startMarker.position =  coordinate
                                    CATransaction.commit()
                                    
                                    var deadlineTime = DispatchTime.now() + .seconds(2)
                                    
                                    if(new_availablePoints.intValue != self.availablePoints.intValue){
                                        
                                        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                                            self.rewardPoints.countFrom( fromValue: self.totalRewardPoints.floatValue , to: self.totalRewardPoints.floatValue + (new_availablePoints.floatValue - self.totalRewardPoints.floatValue), withDuration: 1.5, andAnimationType: .EaseIn, andCountingType: .Int)
                                        }

                                        self.shootingStarAnimation.shootOffSpriteKitStar(from: self.view)
                                        
                                        HELPER.isRewardsRefreshed = false
                                            
                                        deadlineTime = DispatchTime.now() + .seconds(6)
                                    
                                    }
                                    
                                    else{
                                        deadlineTime = DispatchTime.now() + .seconds(3)
                                    }
                                    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                                        self.activityIndicator.showActivityIndicatory(self.view)
                                        self.infoWindow.removeFromSuperview()
                                        self.getCashkans()
                                        
                                    }
                                    
                                }
                                else{
                                    self.playSound(false)
                                    
                                    if(sender.tag == 0){
                                        self.label1.backgroundColor = overSpentRed_color
                                    }
                                    if(sender.tag == 1){
                                        self.label2.backgroundColor = overSpentRed_color
                                    }
                                    if(sender.tag == 2){
                                        self.label3.backgroundColor = overSpentRed_color
                                    }
                                    
                                    sender.isHidden = true

                                    let deadlineTime = DispatchTime.now() + .seconds(1)
                                    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                                        self.activityIndicator.showActivityIndicatory(self.view)
                                        self.infoWindow.removeFromSuperview()
                                        self.getCashkans()
                                    }
                                }
                            }
                            
                            
                        }
                        
                    }
                    catch {
                        //print(error)
                    }
                }
                    
                else{
                    
                    
                    if( dataObject != nil){
                        
                        do {
                            let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                            
                            if (myJSON![error_msg]! as! String).characters.count != 0{
                                let error = myJSON![error_msg]
                                let alertController = UIAlertController(title: alert_cashkan, message: error as? String, preferredStyle: .alert)
                                
                                let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                    //print("you have pressed the Cancel button");
//                                    self.activityIndicator.showActivityIndicatory(self.view)
//                                    self.infoWindow.removeFromSuperview()
//                                    self.getCashkans()
                                    self.btn1.isUserInteractionEnabled = true
                                    self.btn2.isUserInteractionEnabled = true
                                    self.btn3.isUserInteractionEnabled = true
                                    
                                }
                                self.navigationController?.navigationBar.isUserInteractionEnabled = true
                                alertController.addAction(cancelAction)
                                self.present(alertController, animated: true, completion:nil)
                                
                            }
                            else{
                                
                                self.navigationController?.navigationBar.isUserInteractionEnabled = true
                                let alertController = UIAlertController(title: alert_cashkan
                                    , message: "Server error occured", preferredStyle: .alert)
                                
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    self.btn1.isUserInteractionEnabled = true
                                    self.btn2.isUserInteractionEnabled = true
                                    self.btn3.isUserInteractionEnabled = true
                                    
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                                
                            }
                        }
                        catch {
                            //print(error)
                        }
                        
                    }
                    else{
                        let alertController = UIAlertController(title: "Failed!!!", message: error as? String, preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                        }
                        self.activityIndicator.hideActivityIndicator(self.view)
                        self.navigationController?.navigationBar.isUserInteractionEnabled = true
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
        }
        else{ //if no network error
            self.networkFailed()
        }
    
    }
    
func CallGame(_ dictParams: Dictionary<String, AnyObject>, completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
    
    //print(dictParams)
        let id =  HELPER.user_id.stringValue
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest("/users/"+id+"/journey/game", params: dictParams )
    
        RequestResponceHelper.post(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.sync(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? Data{
                        completion(success, object ,dataObject ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }

    
    // #MARK: map delegate methods
    
    // UpdteLocationCoordinate
    func updateLocationoordinates(coordinates:CLLocationCoordinate2D) {
        var destinationMarker : GMSMarker!
        if destinationMarker == nil
        {
            destinationMarker = GMSMarker()
            destinationMarker.position = coordinates
            let image = UIImage(named:"destinationmarker")
            destinationMarker.icon = image
            destinationMarker.map = self.mapview
            destinationMarker.appearAnimation = kGMSMarkerAnimationPop
        }
        else
        {
            CATransaction.begin()
            CATransaction.setAnimationDuration(1.0)
            destinationMarker.position =  coordinates
            CATransaction.commit()
        }
    }
        
    // MARK: Needed to create the custom info window
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (locationMarker != nil){
            guard let location = locationMarker?.position else {
                //print("locationMarker is nil")
                return
            }
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.center.x = infoWindow.center.x + 10
            infoWindow.center.y = infoWindow.center.y + sizeForOffset(view: infoWindow)
        }
    }
    
    // MARK: Needed to create the custom info window
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return infoWindow
    }
    
    // MARK: Needed to create the custom info window (this is optional)
    func sizeForOffset(view: UIView) -> CGFloat {
        return  20 //30
    }
    
    // MARK: Needed to create the custom info window (this is optional)
    func loadNiB() -> MapInfoWindow{
        let infoWindow = MapInfoWindow.instanceFromNib() as! MapInfoWindow
            
        return infoWindow
    }
    
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        infoWindow.removeFromSuperview()
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        locationMarker = marker
        infoWindow.removeFromSuperview()
        //infoWindow = loadNiB()
        guard let location = locationMarker?.position else {
            return false
        }
        infoWindow.center = mapView.projection.point(for: location)
        infoWindow.center.x = infoWindow.center.x + 10
        infoWindow.center.y = infoWindow.center.y + sizeForOffset(view: infoWindow)
        //- 36
        self.view.insertSubview(infoWindow, aboveSubview: self.mapview)
        
        let image3Attachment = NSTextAttachment()
        image3Attachment.image = HELPER.changeImageSize(UIImage(named: "mapInfoMarker.jpg")!,newSize: CGSize(width: 13, height: 12))
        let image3String = NSAttributedString(attachment: image3Attachment)
        let fullDistString = NSMutableAttributedString(string: "")
        fullDistString.append(image3String)
        
        if (marker.zIndex != 10000 ){
            self.selectedMarker = Int(marker.zIndex)
            
            
            fullDistString.append(NSAttributedString(string: String(format:"%.2f",CGFloat(self.distance[Int(marker.zIndex)])) + HELPER.distanceIn.lowercased()))
            
            infoWindow.HeaderLbl.text = self.spentOn[Int(marker.zIndex)]
            infoWindow.subHeaderLbl.attributedText = fullDistString
            
            label1.attributedText = setShadowToLabelText(self.vendorOptions[Int(marker.zIndex)].0, textSize: 16)
            label2.attributedText = setShadowToLabelText(self.vendorOptions[Int(marker.zIndex)].1, textSize: 16)
            label3.attributedText = setShadowToLabelText(self.vendorOptions[Int(marker.zIndex)].2, textSize: 16)
            
            self.hideAll(false)
//            
//            self.mapview.frame = CGRect(x:0, y: self.topTileHeaderIV.frame.size.height, width: self.view.frame.size.width, height: self.guessOptLabel.frame.origin.y)
//            
//            self.mapViewBottomConstraint.constant = 0;
            
            
        }
        else{
            infoWindow.HeaderLbl.text = "ID#: "+self.SP_CashkanId.stringValue
            infoWindow.subHeaderLbl.text = HELPER.currencySym + self.SP_tev.stringValue

            let flag = true
            self.btn1.isHidden = flag
            self.btn2.isHidden = flag
            self.btn3.isHidden = flag
            self.label1.isHidden = flag
            self.label2.isHidden = flag
            self.label3.isHidden = flag
            self.guessOptLabel.isHidden = flag
            self.blackStripIV.isHidden = flag
            self.bottomBoxStripIV.isHidden = flag
            //self.bottomContainerView.isHidden = flag
            
//            self.mapview.frame = CGRect(x:0, y: self.topTileHeaderIV.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height-self.topTileHeaderIV.frame.size.height)
//            self.mapViewBottomConstraint.constant = self.guessOptLabel.frame.size.height + bottomBoxStripIV.frame.size.height
            
         }
        
        
        return true
    }
    
    func setShadowToLabelText(_ text:String, textSize:Int) -> NSMutableAttributedString{
    
        //: Make the Drop Shadow
        let shadow = NSShadow()
        shadow.shadowOffset = CGSize(width: 1, height: 1)
        shadow.shadowBlurRadius = 1
        shadow.shadowColor = textGray_color
        
        //: Add a drop shadow to the text
        let myMutableString = NSMutableAttributedString(string: text)
        myMutableString.setAttributes([NSFontAttributeName : UIFont.boldSystemFont(ofSize: CGFloat(textSize)), NSForegroundColorAttributeName : UIColor.black], range: NSRange(
            location: 0,
            length: myMutableString.length))
        
        myMutableString.addAttribute(
            NSShadowAttributeName,
            value: shadow,
            range: NSRange(
                location: 0,
                length: myMutableString.length))
        
        return myMutableString
    }
    
    func helpTap()
    {
        resultController = self.storyboard!.instantiateViewController(withIdentifier: "WebHelp") as? HelpWebViewController
        isAddedAsChildViewController = true
        self.navigationItem.title = "Help"
        resultController.isAddedAsChildVC = true
        self.resultController.urlToVisit = journeyGame_helpUrl
        self.addChildViewController(resultController)
        self.view.addSubview(resultController.view)
        resultController.didMove(toParentViewController: self)
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(HELPER.isConnectedToNetwork){
            
            self.navigationController?.popViewController(animated: true)
        }
    }
   }
