//
//  CustomCashTableViewCell.swift
//  CashKan
//
//  Created by emtarang on 15/11/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import Foundation

protocol CustomCellDelegate {
    func cellImgTapped(_ cell: UIImageView)
    
    func receiptViewTapped(_ view : UIView)
    
}
class CustomCashTableViewCell: UITableViewCell , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var firstView                 : UIView!
    @IBOutlet var noUploadedReceiptLabel    : UILabel!
    @IBOutlet var secondView                : UIView!
    @IBOutlet var ItemName                  : UILabel!
    @IBOutlet var DateAndTime               : UILabel!
    @IBOutlet var CashPrice                 : UILabel!
    @IBOutlet var ReceiptPrice              : UILabel!
    @IBOutlet var cameraImage               : UIImageView!
    @IBOutlet var lastReceiptUploadedDate   : UILabel!
    @IBOutlet var showReceiptsImage         : UIImageView!
    
    @IBOutlet var dynamicTableView: UITableView!
    @IBOutlet var cameraView: UIView!
    @IBOutlet var dividerView: UIView!
    
    var receiptsArray    : [(String,NSNumber)] = []
    var width : CGFloat = 40
    var obj_width : CGFloat = 50

    var delegate         : CustomCellDelegate?
    
    let colorsArray     : [UIColor] = [watchMovie_color,cd_player_color,music_color,Beverages_color,food_color,groceries_color,clothes_color]
    var colorIndex = 0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func awakeFromNib() {
        HELPER.setFontFamily( self.contentView, andSubViews: true)
        self.contentView.layoutIfNeeded()
        self.dividerView.backgroundColor = lightGray_color
        self.dynamicTableView.isHidden = true
        self.dynamicTableView.delegate = self
        self.dynamicTableView.dataSource = self
        // Do any additional setup after loading the view.
        dynamicTableView.rowHeight = 20 //UITableViewAutomaticDimension
        //dynamicTableView.estimatedRowHeight = 170
        
        dynamicTableView.register(UINib(nibName: "ReceiptDynamicTableView", bundle: nil), forCellReuseIdentifier: "ReceiptCell")
        
        dynamicTableView.separatorStyle = .none
        

    }
    
    func heightForView(text:String, width:CGFloat) -> CGFloat{
        
        
        let font = UIFont(name : customAppFont, size : 11)
        let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width : width, height : 9999))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
       
        return label.frame.height
    }
    
    func widthForView(){
        
        var maxWidth : CGFloat = 0
        for k in 0 ..< self.receiptsArray.count{
            
            var text : NSString = "" // String(describing: self.receiptsArray[k].1)  as NSString
            
            if(String(describing: self.receiptsArray[k].1).contains(".")){
                text = String( format: "%.2f", CGFloat(self.receiptsArray[k].1)) as NSString
            }
            else{
                text =  String(describing: self.receiptsArray[k].1) as NSString

            }
            
            let mWidth : CGFloat = text.size(attributes: [NSFontAttributeName: UIFont(name : customAppFont, size : 11)]).width
            if(mWidth > maxWidth){
                maxWidth = mWidth
            }
        }
        
        self.width = maxWidth + 5
        obj_width = self.dynamicTableView.frame.width - (self.width + 10 + 10 + 5)
        //cell.contentView.frame.width -  cell.obj.frame.origin.x

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return receiptsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell : customReceiptDynamicCell = tableView.dequeueReusableCell(withIdentifier: "ReceiptCell", for: indexPath) as! customReceiptDynamicCell
        
        if(colorIndex  == 7){
            colorIndex = 0
        }
            
        HELPER.setFontFamily((cell.contentView), andSubViews: true)
        
        cell.obj.text = receiptsArray[indexPath.row].0
        cell.obj.textColor = colorsArray[colorIndex]
        cell.obj.lineBreakMode = .byWordWrapping
        cell.obj.numberOfLines = 0
        
        var text : NSString = String(describing: receiptsArray[indexPath.row].1) as NSString
        
        if(text.contains(".")){
            text = String( format: "%.2f", CGFloat(receiptsArray[indexPath.row].1)) as NSString
        }
        cell.price.text = text as String
        cell.price.textColor = colorsArray[colorIndex]
        cell.priceLblWidthConstant.constant = width
//        obj_width = cell.contentView.frame.width -  cell.obj.frame.origin.x
        colorIndex += 1
        
         return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var hgt = heightForView(text: receiptsArray[indexPath.row].0, width: obj_width)
        hgt = hgt+10
        return hgt //(hgt/1.6)
    }
    func addCustomlabels() {
        self.dynamicTableView.isHidden = false
        self.widthForView()
        self.dynamicTableView.reloadData()
        
        self.contentView.bringSubview(toFront: self.dynamicTableView)
        
        self.dynamicTableView.isScrollEnabled = false
    }
    
    func getFormatteddate(_ creationDate : String)-> String{
        // dd mm yy -> dd MMM yyyy
        let fullString = creationDate
        let StringArray = fullString.characters.split{$0 == "-"}.map(String.init)
        var monthStr = "Jan"
        for i in 0 ..< monthsArray.count{
            
            if(String(describing: monthsArray[i].0) == StringArray[1]){
                monthStr = monthsArray[i].1
                break
            }
        }
        
        let y = StringArray[2]
        let d = StringArray[0]
        let dateStr = d+" "+monthStr+" "+y
        return dateStr
        
    }
    
    func loadItem( _ itemName:String, creationDate : String, receiptDate : String,amount :NSNumber, actualSpend : NSNumber, pos : Int) {
        
        self.noUploadedReceiptLabel.textColor = orangeReceipt_color         
        
        ItemName.text = itemName
        DateAndTime.text = getFormatteddate(creationDate)
        DateAndTime.textColor = textGray_color
        
        var textCash : String = amount.stringValue
        
        if(textCash.contains(".")){
            textCash = String( format: "%.2f", CGFloat(amount) )
        }
        CashPrice.text = HELPER.currencySym+textCash

        
        if( actualSpend == 0){
            //&& receiptDate == ""){
            ReceiptPrice.isHidden = true
            lastReceiptUploadedDate.isHidden = true
            showReceiptsImage.isHidden = true
            noUploadedReceiptLabel.isHidden = false
            noUploadedReceiptLabel.textColor = orangeText_color
            self.noUploadedReceiptLabel.isUserInteractionEnabled = true
            let receiptTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(CustomCashTableViewCell.uploadReceiptTapped(_:)))
            //Add the recognizer to your view.
            self.noUploadedReceiptLabel.addGestureRecognizer(receiptTapRecognizer)
        }
        else{
            ReceiptPrice.isHidden = false
            lastReceiptUploadedDate.isHidden = false
            lastReceiptUploadedDate.textColor = textGray_color
            noUploadedReceiptLabel.isHidden = true
            showReceiptsImage.isHidden = false
            
            textCash  = actualSpend.stringValue
            
            if(textCash.contains(".")){
                textCash = String( format: "%.2f", CGFloat(actualSpend) )
            }
            ReceiptPrice.text = HELPER.currencySym+textCash
            lastReceiptUploadedDate.text = getFormatteddate(receiptDate)
            
            self.secondView.isUserInteractionEnabled = true
            let receipttapRecognizer = UITapGestureRecognizer(target: self, action: #selector(CustomCashTableViewCell.receiptViewTapped(_:)))
            //Add the recognizer to your view.
            self.secondView.addGestureRecognizer(receipttapRecognizer)
        }
        self.cameraImage.tag = pos
        self.secondView.tag = pos
        
        self.cameraImage.isUserInteractionEnabled = false
        self.cameraView.isUserInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(CustomCashTableViewCell.imageTapped(_:)))
        //Add the recognizer to your view.
        self.cameraView.addGestureRecognizer(tapRecognizer)
    }
    
    func imageTapped(_ sender: UITapGestureRecognizer) {
        delegate?.cellImgTapped(self.cameraImage)
        
    }
    
    func receiptViewTapped(_ sender: UITapGestureRecognizer) {
        delegate?.receiptViewTapped(self.secondView)
        
    }
    
    func uploadReceiptTapped(_ sender: UITapGestureRecognizer) {
        delegate?.cellImgTapped(self.cameraImage)
        
    }
    
    
}
