//
//  TabBarController.swift
//  CashKan
//
//  Created by emtarang on 12/08/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HELPER.setFontFamily( self.view, andSubViews: true)

        // Do any additional setup after loading the view.
        self.delegate = self
        self.tabBar.barTintColor = UIColor.white

        // set currency symbol
        if(HELPER.currencySymbol == IndianCurrency){
            tabBar.items![0].image = imageWithImage(UIImage(named: "rupee.png")!,newSize: CGSize(width: 32, height: 23))
        }
        else{
            tabBar.items![0].image = imageWithImage(UIImage(named: "Bottom-Navi-icon_01")!,newSize: CGSize(width: 32, height: 23))
        }
        
        tabBar.items![1].image = imageWithImage(UIImage(named: "Bottom-Navi-icon_02")!,newSize: CGSize(width: 32, height: 23))
        tabBar.items![2].image = imageWithImage(UIImage(named: "journeyImage")!,newSize: CGSize(width: 42, height: 23))
        tabBar.items![3].image = imageWithImage(UIImage(named: "Bottom-Navi-icon_04")!,newSize: CGSize(width: 32, height: 23))
        
        HELPER.isJourneyRefreshed = false
        HELPER.isRewardsRefreshed = false
        HELPER.isReceiptRefreshed = false
        
        if (HELPER.notificationTag == ""){
            if(HELPER.total_cashkans.doubleValue > 0){
                tabBar.tintColor =  UIColor(red: 51/255, green: 137/255, blue:255/255, alpha: 1.0)
                self.selectedIndex = 2
            }
            else{
                tabBar.tintColor =  cashGreen_color
                self.selectedIndex = 0
                
            }
        }
        else{

            HELPER.isJourneyRefreshed = false
            HELPER.isRewardsRefreshed = false
            HELPER.isReceiptRefreshed = false
            
            if(HELPER.notificationTag == cashkanSummary){
                tabBar.tintColor =  UIColor(red: 51/255, green: 137/255, blue:255/255, alpha: 1.0)
                self.selectedIndex = 2
            }
            else if(HELPER.notificationTag == uploadReceipt){
                tabBar.tintColor =  UIColor(red: 191/255, green: 81/255, blue: 64/255, alpha: 1)
                self.selectedIndex = 1
            }
            else if(HELPER.notificationTag == "rewards"){
                tabBar.tintColor =  UIColor(red: 187 / 255,green: 92 / 255,blue: 222 / 255,alpha: 1.0)
                self.selectedIndex = 3
            }
            else{
                tabBar.tintColor =  cashGreen_color
                self.selectedIndex = 0
            }
            
        }
        

        
        //tabbar button method
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.webviewpresent), name: NSNotification.Name(rawValue:"changeTabBarColor"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeViewcontrollerOnNotification(_:)), name: NSNotification.Name(rawValue: "ShowVCOnPushNotification"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func imageWithImage( _ image: UIImage, newSize  :CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }


    func didTapOpenButton(_ sender: UIBarButtonItem) {
        //print("inside didTapOpenButton")
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        

    // MARK: UITabBarDelegate Methods
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        if(HELPER.iswebviewPresent){
            return true
        }
        return viewController != tabBarController.selectedViewController;
        
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if tabBar.selectedItem == tabBar.items![0]{
            tabBar.tintColor =  cashGreen_color
        }
        else if tabBar.selectedItem == tabBar.items![1]{
            HELPER.isCashAlive = false
            tabBar.tintColor =  UIColor(red: 191/255, green: 81/255, blue: 64/255, alpha: 1)
        }
        else if tabBar.selectedItem == tabBar.items![2]{
            HELPER.isCashAlive = false
            tabBar.tintColor =  UIColor(red: 51/255, green: 137/255, blue:255/255, alpha: 1.0)
        }
        else if tabBar.selectedItem == tabBar.items![3]{
            HELPER.isCashAlive = false
            tabBar.tintColor =  UIColor(red: 187 / 255,green: 92 / 255,blue: 222 / 255,alpha: 1.0)
        }
        
        
    }
    
    
    // UITabBarControllerDelegate
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {

        if tabBarController.selectedIndex == 0{
            HELPER.randomUUID = HELPER.getRandonUUID()
            let navigationcontrollr=tabBarController.viewControllers?[0] as? UINavigationController
            navigationcontrollr?.topViewController?.viewWillAppear(true)
        }
        
        if tabBarController.selectedIndex == 1{
            let navigationcontrollr=tabBarController.viewControllers?[1] as? UINavigationController
            navigationcontrollr?.topViewController?.viewWillAppear(true)
        }
        if tabBarController.selectedIndex == 2{
            let navigationcontrollr=tabBarController.viewControllers?[2] as? UINavigationController
            navigationcontrollr?.topViewController?.viewWillAppear(true)

        }
        if tabBarController.selectedIndex == 3{
            let navigationcontrollr=tabBarController.viewControllers?[3] as? UINavigationController
            //navigationcontrollr?.topViewController?.viewWillAppear(true)
        }

    }
    
    func changeViewcontrollerOnNotification(_ notification : NSNotification){
        let dict = notification.object as! [String:AnyObject]
        let tag : String =  dict["TAG"] as! String
        HELPER.isJourneyRefreshed = false
        HELPER.isRewardsRefreshed = false
        HELPER.isReceiptRefreshed = false

        if(tag == cashkanSummary){
            tabBar.tintColor =  UIColor(red: 51/255, green: 137/255, blue:255/255, alpha: 1.0)
            self.selectedIndex = 2
        }
        else if(tag == uploadReceipt){
            tabBar.tintColor =  UIColor(red: 191/255, green: 81/255, blue: 64/255, alpha: 1)
            self.selectedIndex = 1
        }
        else if(tag == "rewards"){
            tabBar.tintColor =  UIColor(red: 187 / 255,green: 92 / 255,blue: 222 / 255,alpha: 1.0)
            self.selectedIndex = 3

        }
    }
    //tabbar button method
    func webviewpresent()  {
        tabBar.tintColor=UIColor.gray
        
    }
    //tabbar button method
    

} //end of class
