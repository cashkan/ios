//
//  CashkanDetailsViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 18/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class CashkanDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NetworkFailureDelegate {

    
    @IBOutlet var CKLabel: UILabel!
    var minimumRank: NSNumber = 0.0
    @IBOutlet var TTDLabel: UILabel!
    @IBOutlet var TEVLabel: UILabel!
    @IBOutlet var rewardsPtsLbl: UILabel!
    var denomination_detail : String?
    var active_detail : String?
    var inactive_detail : String?
    
 
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()

    
    var bill     :   [NSString]! = []
    var currency :   [NSString]! = []
    var tev      :   [NSNumber]! = []
    var tdt      :   [NSNumber]! = []
    var position :   [NSNumber]! = []
    var isActive :   [NSNumber]! = []
    var rewardPoints :   [NSNumber]! = []
    
    @IBOutlet var tableView    : UITableView!
    @IBOutlet var activeLabel  : UILabel!
    @IBOutlet var inactiveLabel: UILabel!
    @IBOutlet var currencyLabel: UILabel!
    @IBOutlet var activeStringLabel: UILabel!
    @IBOutlet var inactiveStringLabel: UILabel!
    var resultController : HelpWebViewController!
    var isAddedAsChildViewController : Bool = false
    var barButtonHelp       : UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        HELPER.setFontFamily( self.view, andSubViews: true)

        // Do any additional setup after loading the view.
        
        self.navigationItem.hidesBackButton = true
       self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true

        
        let button: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        button.setImage(UIImage(named: "Cashkans_Back arrow"), for: UIControlState())
        //add function for button
        button.addTarget(self, action: #selector(CashkanDetailsViewController.back(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
            
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton;
        
        
        let button1: UIButton = UIButton(type: UIButtonType.custom)
        button1.setImage(UIImage(named: "Help_icon"), for: UIControlState())
        button1.addTarget(self, action: #selector(CashkanDetailsViewController.helpTap), for: UIControlEvents.touchUpInside)
        button1.frame = CGRect(x: 0, y: 200, width: 16, height: 25)
        
        barButtonHelp = UIBarButtonItem(customView: button1)
        self.navigationItem.rightBarButtonItem = barButtonHelp;
        
        self.inactiveStringLabel.textColor = overSpentRed_color
        self.activeStringLabel.textColor = savingsGreen_color
        
        let nib = UINib(nibName: "CashkanDetailTableViewCell", bundle: nil)
        
        tableView.register(nib, forCellReuseIdentifier: "Cell")
        
        let headerBlueColor = journeyBlue_color
        
        self.CKLabel.textColor = headerBlueColor
        
        self.TTDLabel.textColor = headerBlueColor
        
        //self.myLabel.textColor  = headerBlueColor
        
        self.TEVLabel.textColor = headerBlueColor
        self.rewardsPtsLbl.textColor = headerBlueColor
        self.TEVLabel.text = "TEV ("+HELPER.currencySym+")";
        self.TTDLabel.text = "TDT ("+(HELPER.distanceIn).lowercased()+")";
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        HELPER.setFontFamily( self.view, andSubViews: true)
        
        RequestResponceHelper.delegate = self
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        self.navigationItem.title = journeyDetail_title


        if let den = denomination_detail{
            currencyLabel.text = HELPER.currencySym+den
        }
        if let act = active_detail{
            activeLabel.text = act
        }
        if let inact = inactive_detail{
            inactiveLabel.text = inact
        }

        currencyLabel.textColor = journeyBlue_color
        

    }
 
    override func viewDidAppear(_ animated: Bool) {
        
        if HELPER.backToRoot == true{
            HELPER.backToRoot = false
            self.navigationController?.popViewController(animated: false)
        }
        else{
            if(Reachability.isConnectedToNetwork() == false){
                
                DispatchQueue.main.async {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                    
                    HELPER.isConnectedToNetwork = false
                    
                    self.navigationController!.pushViewController(secondViewController, animated: false)
                    
                }
                
            }
            else{
                activityIndicator.showActivityIndicatory(self.view)
                getCashkanSummary()
            }

        }
    }
    func helpTap()
    {
        // print("Hi")
        resultController = self.storyboard!.instantiateViewController(withIdentifier: "WebHelp") as? HelpWebViewController
        isAddedAsChildViewController = true
        self.navigationItem.title = "Help"
        resultController.isAddedAsChildVC = true
        self.resultController.urlToVisit = scanList_helpUrl
        self.addChildViewController(resultController)
        self.view.addSubview(resultController.view)
        resultController.didMove(toParentViewController: self)
        self.navigationItem.rightBarButtonItem = nil
    }
    
    func back(_ sender: UIBarButtonItem) {
        self.navigationItem.title = journeyDetail_title
        if(isAddedAsChildViewController){
            if((self.resultController) != nil){
                resultController.willMove(toParentViewController: nil)
                resultController.view.removeFromSuperview()
                resultController.removeFromParentViewController()
                isAddedAsChildViewController = false
                self.navigationItem.rightBarButtonItem = barButtonHelp;
            }
        }
        else{

        // Go back to the previous ViewController
        self.navigationController?.popViewController(animated: true)
        }
    }
    
    func help(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
       return tev.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CashkanDetailTableViewCell
        HELPER.setFontFamily(cell.contentView, andSubViews: true)
        
        if( tev.count > 0){
        
            cell.billLabel.text = bill[(indexPath as NSIndexPath).row] as String
            let value  = tev[(indexPath as NSIndexPath).row].stringValue
            //cell.valueLabel.text = HELPER.currencySym+value
            cell.valueLabel.text = value

            let tdtStr : String
            
            //tdtStr = tdt[(indexPath as NSIndexPath).row].stringValue + " " + HELPER.distanceIn
            tdtStr = tdt[(indexPath as NSIndexPath).row].stringValue
            cell.travelledLabel.text = tdtStr
            cell.bulletLabel.text = cashkan_bullet
            cell.ptsLabel.text = rewardPoints[(indexPath as NSIndexPath).row].stringValue
            
            if 0 == isActive[(indexPath as NSIndexPath).row]{
                    cell.bulletLabel.textColor = overSpentRed_color
            }
            else {
                cell.bulletLabel.textColor = savingsGreen_color
            }
        }
        else{
                tableView.separatorColor = UIColor.white
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        }

            return cell
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(HELPER.isConnectedToNetwork){
        
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func getCashkanSummary(){
        
        //UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        self.GetCashkansSummary(denomination_detail!){
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            //print("got back: \(message)")
            
            if( success == true){
                
                if let response = object as? HTTPURLResponse{
                    
                    // You can print out response object
                   // print("response = \(response)")
                    
                }
                
                
                do {
                    var activeCnt : NSNumber = 0
                    var inActiveCnt : NSNumber = 0
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                    
                    if let parseJSON =  myJSON!["data"]{
                        
                        if (parseJSON as AnyObject).count > 0{
                            
                                for  i in 0 ..< (parseJSON as AnyObject).count  {
                                    
                                    let obj = (parseJSON as! NSArray)[i] as! NSDictionary
                                    
                                    let bill = obj["currencyNo"] as! NSString
                                    let currency = obj["curSymbol"] as! NSString
                                    let tev = obj["tev"] as! NSNumber
                                    var tdt :NSNumber = 0;
                                    if(HELPER.distanceIn == "km"){
                                        tdt = obj["tdt_in_kms"] as! NSNumber
                                    }
                                    else{
                                        tdt = obj["tdt_in_mi"] as! NSNumber
                                    }
                                    
                                    let position = obj["position"] as! NSNumber
                                    let isActive  = obj["isActive"] as! NSNumber
                                    let rewardPoints = obj["rewardPoints"] as! NSNumber
                                    if(isActive == 1){
                                        activeCnt = activeCnt.doubleValue + 1 as NSNumber
                                    }
                                    else{
                                        inActiveCnt = inActiveCnt.doubleValue + 1 as NSNumber
                                    }
//                                    if i == 0{
//                                        self.minimumRank = obj["position"] as! NSNumber
//                                    }
//                                    if(self.minimumRank.doubleValue > (obj["position"] as! NSNumber).doubleValue){
//
//                                        self.minimumRank = obj["position"] as! NSNumber
//                                    }
                                    self.bill.insert(bill, at: i)
                                    self.currency.insert(currency, at: i)
                                    self.tev.insert(tev, at : i)
                                    self.tdt.insert(tdt, at : i)
                                    self.position.insert(position, at : i)
                                    self.isActive.insert(isActive, at : i)
                                    self.rewardPoints.insert(rewardPoints, at : i)
                                }
                            
                            self.activeLabel.text = String(describing: activeCnt)
                            self.inactiveLabel.text = String(describing: inActiveCnt)
                            self.tableView.reloadData()
                            if self.tableView.contentSize.height > self.tableView.frame.height{
                                self.tableView.isScrollEnabled = true
                            }
                            else{
                                self.tableView.isScrollEnabled = false
                            }
                            self.activityIndicator.hideActivityIndicator(self.view)
                            //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            
                        }
                            
                        else{
                            
                            
                        }
                        
                    }
                    
                }
                catch {
                    //print(error)
                }
                
            }
                
            else{
                
                // If registration failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            let error = myJSON![display_msg]
                            let alertController = UIAlertController(title: alert_cashkan, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        print(error)
                    }
                    
                }
            }
            
            
        }
    }

    
    func GetCashkansSummary( _ denomination : String, completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
        
        let id =  HELPER.user_id.stringValue
        let summaryUrl : String = cashkan_journey_detail+"?denominations="+denomination+"&userId="+id
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest(summaryUrl, params: nil )
        
        RequestResponceHelper.get(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.sync(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? Data{
                        //message = passedMessage
                        completion(success, object ,dataObject ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }
    
    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
            
            self.navigationController!.pushViewController(secondViewController, animated: false)
        }
        
    }
    
    

 }
