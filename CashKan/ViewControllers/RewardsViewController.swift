//
//  RewardsViewController.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 13/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class RewardsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, SlideMenuDelegate,UIWebViewDelegate, rewardsCustomCellDelegate, NetworkFailureDelegate, UIDocumentInteractionControllerDelegate {
    
    @IBOutlet var NoRewardsLabel:  UILabel!
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var pointRedeemedButton: UIButton!
    @IBOutlet var pointsRedeemedLbl: UILabel!
    @IBOutlet var pointBalanceLbl: UILabel!
    @IBOutlet var pointsBalanceButton: UIButton!
    @IBOutlet var refreshView: UIView!
    
    var drawerBtn:UIButton!
    var aDelegate: UITabBarController!
    weak var webView: UIWebView!

    var bgTapRecognizer:UITapGestureRecognizer!

    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()
    
    var rewardDescription : [String]! = []
    var merchantName : [String]! = []
    var merchantLogo : [String]! = []
    var merchantDescription : [String]! = []
    var amount : [NSNumber]! = []
    var code : [String]! = []
    var new_flag : [NSNumber]! = []
    var pointsRequired : [NSNumber]! = []
    var rewardId : [NSNumber]! = []
    var rewardShortDescription : [String]! = []
    var rewardTypeDescription : [String]! = []
    
    var availablePoints : NSNumber = 0
    var totalPoints : NSNumber = 0
    var redeemedPoints : NSNumber = 0
    var menuVC : MenuViewController!
    
    
    @IBAction func rulesButtonClicked(_ sender: AnyObject)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RewardsTopOver") as! RulesViewController
        vc.view.backgroundColor = UIColor.clear
                view.alpha = 0.5
                navigationController?.navigationBar.alpha = 0.5
        tabBarController?.tabBar.alpha = 0.5
        tabBarController?.tabBar.isUserInteractionEnabled = false
        
        vc.rewards = self

        self.modalPresentationStyle =  UIModalPresentationStyle.currentContext
        self.present(vc, animated:false, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HELPER.setFontFamily( self.view, andSubViews: true)
        
        self.view.layoutIfNeeded()
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        RequestResponceHelper.delegate = self
        self.pointsBalanceButton.backgroundColor = rewardsPink_color
        self.pointsBalanceButton.layer.cornerRadius = self.pointsBalanceButton.frame.height / 2
        self.pointsBalanceButton.layer.masksToBounds = true
        self.pointsBalanceButton.setTitleColor(UIColor.white, for: .normal)
        self.pointsBalanceButton.layoutIfNeeded()
        
        self.pointRedeemedButton.backgroundColor = rewardsPink_color
        self.pointRedeemedButton.layer.cornerRadius = self.pointRedeemedButton.frame.height / 2
        self.pointRedeemedButton.layer.masksToBounds = true
        self.pointRedeemedButton.setTitleColor(UIColor.white, for: .normal)
        self.pointRedeemedButton.layoutIfNeeded()
        
        self.pointBalanceLbl.textColor = textGray_color
        self.pointsRedeemedLbl.textColor = textGray_color

        self.tableView.delegate = self
        self.tableView.dataSource = self
        // Do any additional setup after loading the view.
        tableView.rowHeight = 170 //UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 170
        
        tableView.register(UINib(nibName: "rewardsTableCell", bundle: nil), forCellReuseIdentifier: "RewardsCell")

        //self.tableView.reloadData()

        self.navigationController?.navigationBar.barTintColor = rewardsPink_color
        
        self.navigationController?.navigationBar.titleTextAttributes =     [NSForegroundColorAttributeName: UIColor.white]
        
        let button1: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        button1.setImage(UIImage(named: "Help_icon"), for: UIControlState())
        //add function for button
        button1.addTarget(self, action: #selector(RewardsViewController.helpTap), for: UIControlEvents.touchUpInside)
        //set frame
        button1.frame = CGRect(x: 0, y: 200, width: 16, height: 25)
        
        let barButton1 = UIBarButtonItem(customView: button1)
        
        // Pull to refresh button
        let button2: UIButton = UIButton(type: UIButtonType.custom)
        button2.setImage(UIImage(named: "ic_action_refresh"), for: UIControlState())
        button2.addTarget(self, action: #selector(self.refreshView(_:)), for: UIControlEvents.touchUpInside)
        button2.frame = CGRect(x: 0, y: 200, width: 30, height: 30)
        
        let barButtonRefresh = UIBarButtonItem(customView: button2)
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 15.0
        let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        negativeSpace.width = 0 //-7.0
        
        self.navigationItem.rightBarButtonItems = [negativeSpace, barButton1, fixedSpace, barButtonRefresh]
        
        //Drawer button start
        drawerBtn = UIButton(type: UIButtonType.custom)
        //set image for button
        drawerBtn.setImage(UIImage(named: "iosdrawericon"), for: UIControlState())
        //add function for button
        drawerBtn.addTarget(self, action: #selector(self.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        drawerBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 25)
        drawerBtn.backgroundColor = UIColor.clear
        let barButton2 = UIBarButtonItem(customView: drawerBtn)
        self.navigationItem.leftBarButtonItem = barButton2;
        
        let noRewardsText = noRewardsString
        do {
            let str = try NSAttributedString(data: noRewardsText.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
            self.NoRewardsLabel.attributedText = str

        } catch {
            //print(error)
        }
        self.NoRewardsLabel.textColor = rewardsPink_color
            
        self.NoRewardsLabel.font = UIFont.systemFont(ofSize: 15)
        self.NoRewardsLabel.numberOfLines = 0
        self.NoRewardsLabel.lineBreakMode = .byWordWrapping
        
        //Drawer button end
        
        self.tableView.isHidden = true
        self.NoRewardsLabel.isHidden = true
        
//        self.activityIndicator.showActivityIndicatory(self.view)
//        getRewardsRequest()
        
        self.refreshView.isUserInteractionEnabled = true
        let refreshingTap = UISwipeGestureRecognizer(target:self, action:#selector(ReceiptController2.refreshView(_:)))
        refreshingTap.direction = UISwipeGestureRecognizerDirection.down
        
        self.refreshView.addGestureRecognizer(refreshingTap)
        
    }
   
    // PDF reader
    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    func refreshView(_ recognizer : AnyObject){
        
        self.activityIndicator.showActivityIndicatory(self.view)
        self.activityIndicator.customlabel.isHidden = false
        self.activityIndicator.customlabel.text = rewardsRefreshString
        self.view.sendSubview(toBack: self.tableView)
        self.getNewRewards()
    }
    
    func getNewRewards(){
        if(Reachability.isConnectedToNetwork() == false){
            
                self.activityIndicator.hideActivityIndicator(self.view)
                DispatchQueue.main.async {
                HELPER.isRewardsRefreshed = false
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                
                self.navigationController!.pushViewController(secondViewController, animated: false)
                }
        }
        else{
            
            self.getRewardsRequest()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RequestResponceHelper.delegate = self

        NotificationCenter.default.post(name: Notification.Name(rawValue: "popDrawerController"), object: nil)

        if(HELPER.isDrawerOpen == true && self.menuVC != nil){
            self.dismissViewBehindWebView()
        }
        self.tabBarController?.selectedIndex = 3

        self.navigationItem.title = rewards_title
        HELPER.setFontFamily( self.view, andSubViews: true)
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 187 / 255,
                                                                        green: 92 / 255,
                                                                        blue: 222 / 255,
                                                                        alpha: 1.0)
        
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (HELPER.isRewardsRefreshed == false || (NoRewardsLabel.isHidden && tableView.isHidden)){
            rewardDescription  = []
            merchantName  = []
            merchantLogo  = []
            merchantDescription  = []
            amount  = []
            code  = []
            new_flag  = []
            pointsRequired  = []
            rewardId  = []
            rewardShortDescription  = []
            rewardTypeDescription  = []
            availablePoints  = 0
            totalPoints  = 0
            redeemedPoints  = 0
            HELPER.isRewardsRefreshed = true
            self.activityIndicator.showActivityIndicatory(self.view)
            self.getNewRewards()
        }

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if HELPER.isDrawerOpen.boolValue && self.menuVC != nil{
            dismissContainer()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func helpTap()
    {
        // print("Hi")
        if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "WebHelp") as? HelpWebViewController {
            //resultController.isFromLogin = false
            resultController.urlToVisit = rewards_helpurl
            self.navigationController?.pushViewController(resultController, animated: true)
        }
    }

    func getRewardsRequest(){

        self.GetRewards(){
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            
            if( success == true){
                self.rewardDescription      = []
                self.merchantName           = []
                self.merchantLogo           = []
                self.merchantDescription    = []
                self.amount                 = []
                self.code                   = []
                self.new_flag               = []
                self.pointsRequired         = []
                self.rewardId               = []
                self.rewardShortDescription = []
                self.rewardTypeDescription  = []
                self.availablePoints        = 0
                self.totalPoints            = 0
                self.redeemedPoints         = 0
                
                if let response = object as? HTTPURLResponse{
                    // You can print out response object
                    //print("response = \(response)")
                }
                do {
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                    //print(myJSON)
                    
                    if let parseJSON = myJSON!["data"] as! [String:Any]?{
                        
                        if (parseJSON as AnyObject).count > 0{
                            
                        if let total : NSNumber = parseJSON["availablePoints"] as? NSNumber{
                                self.availablePoints = total
                            }
                            
                        if let amt : NSNumber = parseJSON["redeemedPoints"] as? NSNumber{
                                self.redeemedPoints = amt
                            }
                            
                        if let amt : NSNumber = parseJSON["totalPoints"] as? NSNumber{
                                self.totalPoints = amt
                            }
                            
                        self.pointRedeemedButton.setTitle(String(describing: self.redeemedPoints), for: .normal)
                        self.pointsBalanceButton.setTitle(String(describing: self.availablePoints), for: .normal)
                            
                        if((parseJSON["rewards"] as! NSArray).count > 0){
                                
                            
                            for var i in 0 ..< (parseJSON["rewards"] as! NSArray).count  {
                        
                                //print(parseJSON["rewards"])
                        
                                let obj = (parseJSON["rewards"] as! NSArray)[i] as! NSDictionary

                                let rewards_desc = obj["rewardDescription"] as! String
                        
                                var merchantName = ""
                                if let dscr : String = obj["merchantName"] as? String{
                                    merchantName = dscr
                                }
                        
                                var amountStr : NSNumber = 0
                                if let amtstr : NSNumber = obj["amount"] as? NSNumber{
                                    amountStr = amtstr
                                }
                                
                                var codeStr = ""
                                if let codestr : String = obj["code"] as? String{
                                    codeStr = codestr
                                }
                        
                                var pointsRequired : NSNumber = 0
                                if let points : NSNumber = obj["pointsRequired"] as? NSNumber{
                                    pointsRequired = points
                                }
                        
                                let newFlagStr = (obj["newFlag"] as! NSNumber)
                        
                                var rewardShortDescription = ""
                        
                                if let codestr : String = obj["rewardShortDescription"] as? String{
                                    rewardShortDescription = codestr
                                }
                                
                                var rewardTypeDescription = ""
                                if let codestr : String = obj["rewardTypeDescription"] as? String{
                                    rewardTypeDescription = codestr
                                }
                                
                                var rewardId : NSNumber = 0
                                if let codestr : NSNumber = obj["rewardId"] as? NSNumber{
                                    rewardId = codestr
                                }
                                
                                var merchantLogoImg : String = ""
                                if let codestr : String = obj["merchantLogo"] as? String{
                                    merchantLogoImg = codestr
                                }
                        
                                self.rewardDescription.insert(rewards_desc, at: i)
                                self.merchantName.insert(merchantName, at : i)
                                self.amount.insert(amountStr, at : i)
                                self.code.insert(codeStr, at : i)
                                self.new_flag.insert(newFlagStr, at : i)
                                self.pointsRequired.insert(pointsRequired, at : i)
                                self.rewardShortDescription.insert(rewardShortDescription, at : i)
                                self.rewardId.insert(rewardId, at : i)
                                self.rewardTypeDescription.insert(rewardTypeDescription, at : i)
                                self.merchantLogo.insert(merchantLogoImg, at : i)
                        
                            }
                        
                            self.tableView.reloadData()
                            self.activityIndicator.hideActivityIndicator(self.view)

                            if self.tableView.contentSize.height > self.tableView.frame.height{
                                self.tableView.isScrollEnabled = true
                            }
                            else{
                                self.tableView.isScrollEnabled = false
                            }
                        }
                        else{
                            self.activityIndicator.hideActivityIndicator(self.view)
                            self.tableView.isHidden = true
                            self.NoRewardsLabel.isHidden = false
                        }
                    }
                    else{
                        self.activityIndicator.hideActivityIndicator(self.view)
                        self.tableView.isHidden = true
                        self.NoRewardsLabel.isHidden = false
                        
                    }
                    }
                }
                catch {
                    //print(error)
                }
            }
            else{
                // If rewards failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            let error = myJSON![display_msg]
                            let alertController = UIAlertController(title: alert_cashkan, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        //print(error)
                    }
                    
                }
            }
        }
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return rewardDescription.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
   
        let cell : RewardsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RewardsCell", for: indexPath) as! RewardsTableViewCell


        HELPER.setFontFamily((cell.contentView), andSubViews: true)
        self.tableView.isHidden = true
        self.NoRewardsLabel.isHidden = false
        
        if(self.rewardDescription.count > 0){
            self.tableView.isHidden = false
            self.NoRewardsLabel.isHidden = true
            
            let id = indexPath.row
            cell.descrLbl.text = rewardShortDescription[id]
            cell.pointsValueLbl.text = String(describing: pointsRequired[id])
            cell.rewardsProviderLbl.text = merchantName[id]
            cell.pointsRequiredLbl.text = "Points Required :"
            cell.redeemBtn.tag = self.rewardId[id].intValue
            //print(cell.redeemBtn.tag)
            HELPER.updateWithSpacing(lineSpacing: 2, textView: cell.descrLbl)
            cell.descrLbl.textAlignment = .center
            
            cell.isUserInteractionEnabled = true
            
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.rewardCellTapped(_:)))
            
            cell.addGestureRecognizer(tapRecognizer)
            
            if(merchantLogo[id] != ""){
            if let urlString : String = merchantLogo[id]{
                // create NSURL instance
                if let url = NSURL(string: urlString) {
                    // check if your application can open the NSURL instance
                    if UIApplication.shared.canOpenURL(url as URL){
                        self.loadImageFromUrl(urlString, btn: cell.rewardsImageButton)
                        cell.rewardsImageButton.isSelected = false
                    }
                    else{
                        cell.rewardsImageButton.setImage(nil, for: .normal)
                    }
                }
            }
            }
            else{
                cell.rewardsImageButton.setImage(nil, for: .normal)
            }

            
            if self.availablePoints.doubleValue > pointsRequired[id].doubleValue {
                
                cell.redeemBtn.isUserInteractionEnabled = true
                cell.redeemBtn.setTitleColor(rewardsPink_color, for: .normal)
                cell.redeemBtn.layer.borderColor = rewardsPink_color.cgColor
                cell.delegate = self
            }
            else{
                cell.redeemBtn.isUserInteractionEnabled = false
                cell.redeemBtn.setTitleColor(rewardsLightPink_color, for: .normal)
                cell.redeemBtn.layer.borderColor = rewardsLightPink_color.cgColor
                
            }
            
        }
        
        
//            if new_flag[(indexPath as NSIndexPath).row] == 1{
//            
//            let viewframe = self.view.frame
//            let rect = CGRect(x:viewframe.size.width-120, y: 0, width: 30, height: 30)
//            
//            
//            //dummy view for handling badges
//            let badgeView = UIView(frame: rect)
//            badgeView.layer.cornerRadius = 0.5 * badgeView.bounds.size.width
//            badgeView.layer.borderWidth = CGFloat(customButtonBorderWidth)
//            badgeView.isUserInteractionEnabled = false
//            badgeView.layer.borderColor = UIColor(red: 42 / 255,
//                                                  green: 150 / 255,
//                                                  blue: 99 / 255,
//                                                  alpha: 1.0).cgColor
//            
//
//            badgeView.isHidden = false
//            badgeView.backgroundColor =  UIColor(red: 42 / 255,
//                                                 green: 150 / 255,
//                                                 blue: 99 / 255,
//                                                 alpha: 1.0)
//            
//            let badgeLabel: UILabel = UILabel()
//            badgeLabel.frame = CGRect(x: 3, y: 3, width: 24, height: 24)
//            badgeLabel.textColor = UIColor.white
//            badgeLabel.textAlignment = NSTextAlignment.center
//            badgeLabel.text = "N"
//            badgeView.addSubview(badgeLabel)
//            
//            cell.addSubview(badgeView)
//            }
  //      }
        return cell
    }

    func rewardCellTapped(_ sender: UITapGestureRecognizer) {
        
        //isCashkanCellTapped = true
        //using sender, we can get the point in respect to the table view
        let tapLocation = sender.location(in: self.tableView)
        //using the tapLocation, we retrieve the corresponding indexPath
        let indexPath =  self.tableView.indexPathForRow(at: tapLocation)
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "RewardsDetail") as! RewardsDetailViewController
        secondViewController.merchant_name = merchantName[(indexPath! as NSIndexPath).row]
        secondViewController.required_points = self.pointsRequired[(indexPath! as NSIndexPath).row].stringValue
        secondViewController.short_descr = rewardShortDescription[(indexPath! as NSIndexPath).row]
        secondViewController.big_descr = rewardDescription[(indexPath! as NSIndexPath).row]
        secondViewController.rewards_id = rewardId[(indexPath! as NSIndexPath).row]
        secondViewController.logoImageUrl = merchantLogo[(indexPath! as NSIndexPath).row]
        secondViewController.rewardsViewController = self 
        
        if self.availablePoints.doubleValue > pointsRequired[(indexPath! as NSIndexPath).row].doubleValue {
            secondViewController.isenabledRedeembtn = true
            }
        else{
            secondViewController.isenabledRedeembtn = false
        }
        
        // Take user to SecondViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }

    
func GetRewards( _ completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
    
    let id =  HELPER.user_id.stringValue
    let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest("/users/"+id+rewards_url, params: nil )
    //print("/users/"+id+rewards_url)
    RequestResponceHelper.get(request) { ( success, object, dataObject) -> () in
        DispatchQueue.main.sync(execute: { () -> Void in
            if success {
                let successmessage = object![display_msg] as? String
                completion(true, object, dataObject ,successmessage)
            } else {
                let message = "there was an error"
                
                if let object = object, let dataObject = dataObject as? Data{
                    //message = passedMessage
                    completion(success, object ,dataObject ,message)
                }
                else{
                    
                    completion(success, nil, nil ,nil)
                }
            }
        })
    }
}
    func refreshRewards(){
        self.activityIndicator.showActivityIndicatory(self.view)
        getRewardsRequest()
    }
    
    func redeemBtnTapped(_ sender: UIButton) {
        let alertController = UIAlertController(title: alert_redeemRewrad, message: redeemRewardMessage as? String, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Redeem", style: .cancel) { (action:UIAlertAction!) in
            if(Reachability.isConnectedToNetwork() == false){
                
                DispatchQueue.main.async {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                    HELPER.isConnectedToNetwork = false
                    self.navigationController!.pushViewController(secondViewController, animated: false)
                }
            }
            else{
                //print(sender.tag)
                self.redeemPoints(rewardId: sender.tag)
            }
        }
        
        let cancelAction = UIAlertAction(title: alert_cancel, style: .default) { (action:UIAlertAction!) in
            
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
    }

    func redeemPoints(rewardId : Int){
        let user_id = HELPER.user_id.stringValue
        
        // Password Encryption
       
        self.activityIndicator.showActivityIndicatory(self.view)

        self.RedeemRewardPoints(rewardId)
        {
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            //print("got back: \(message)")
            
            if( success == true){
                
                if let response = object as? HTTPURLResponse{
                    
                    // You can print out response object
                    //print("response = \(response)")
                }
                do {
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                    
                    if let successMsg = myJSON![display_msg]{
                        //print(successMsg)
                        self.activityIndicator.hideActivityIndicator(self.view)
                        let alertController = UIAlertController(title: alert_cashkan, message: successMsg as? String, preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                            self.activityIndicator.showActivityIndicatory(self.view)
                            self.getRewardsRequest()
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
                catch {
                    //print(error)
                }
            }
                
            else{
                
                // If registration failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        var error : String = ""
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            error = myJSON![display_msg] as! String
                        }
                        else if(myJSON!["errorMessage"]! as! String).characters.count != 0{
                            error = myJSON!["errorMessage"] as! String
                        }
                        if error != ""{
                            self.activityIndicator.hideActivityIndicator(self.view)
                            
                            let alertController = UIAlertController(title: alert_cashkan, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        //print(error)
                    }
                    
                }
            }
            
            
        }
    }
    

    func RedeemRewardPoints(_ rewardId : Int, completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
        
        let id =  HELPER.user_id.stringValue
        var rewardObject: NSDictionary = ["rewardId" : String(describing: rewardId)]
        
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest("/users/"+id+redeemRewardPoints_url, params: rewardObject as! Dictionary<String, AnyObject> )
        //print("/users/"+id+redeemRewardPoints_url)
        RequestResponceHelper.post(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.sync(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? Data{
                        //message = passedMessage
                        completion(success, object ,dataObject ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }
    
    
    //MARK: left drawer method start
    func dismissViewBehindWebView(){
    
        self.view.removeGestureRecognizer(bgTapRecognizer)
        
        //        self.slideMenuItemSelectedAtIndex(-1);
        let viewMenuBack : UIView = view.subviews.last!
        
        if viewMenuBack.tag == 123 {
            self.drawerBtn.tag = 0
            HELPER.isDrawerOpen = false
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * (UIScreen.main.bounds.size.width-drawerOffset)
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                
        }
        let subViews = self.view.subviews
        for subview in subViews{
            if subview.tag == 111 {
                subview.removeFromSuperview()
            }
        }
        self.tabBarController?.tabBar.isUserInteractionEnabled = true

        return
    
    }
    func dismissContainer(){
        self.view.removeGestureRecognizer(bgTapRecognizer)

//        self.slideMenuItemSelectedAtIndex(-1);
        let viewMenuBack : UIView = view.subviews.last!
        
        if viewMenuBack.tag == 123 {
            self.drawerBtn.tag = 0
            HELPER.isDrawerOpen = false
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * (UIScreen.main.bounds.size.width-drawerOffset)
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    //viewMenuBack.removeFromSuperview()
            })
        }
        let subViews = self.view.subviews
        for subview in subViews{
            if subview.tag == 111 {
                subview.removeFromSuperview()
            }
        }
        self.tabBarController?.tabBar.isUserInteractionEnabled = true

        return
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        if index > 2 {
            self.navigationItem.title = Drawer_titles[Int(index)]
        }
    }
    
    func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
//            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            HELPER.isDrawerOpen = false
            let viewMenuBack : UIView = view.subviews.last!
            if viewMenuBack.tag == 123 {
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    var frameMenu : CGRect = viewMenuBack.frame
                    frameMenu.origin.x = -1 * (UIScreen.main.bounds.size.width-drawerOffset)
                    viewMenuBack.frame = frameMenu
                    viewMenuBack.layoutIfNeeded()
                    viewMenuBack.backgroundColor = UIColor.clear
                    }, completion: { (finished) -> Void in
                        //viewMenuBack.removeFromSuperview()
                })
            }
            
            let subViews = self.view.subviews
            for subview in subViews{
                if subview.tag == 111 {
                    subview.removeFromSuperview()
                }
            }
            self.tabBarController?.tabBar.isUserInteractionEnabled = true

            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        HELPER.isDrawerOpen = true
        if(menuVC == nil){

            menuVC  = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        }
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.view.bringSubview(toFront: menuVC.view)
        
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        let dummyDisabledView: UIView = UIView.init(frame: self.view.frame)
        dummyDisabledView.tag = 111
        dummyDisabledView.alpha = 0.5
        dummyDisabledView.backgroundColor = darkGray_color
        
        self.view.insertSubview(dummyDisabledView, belowSubview: menuVC.view)
        
        bgTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(bgTapped(_:)))
        
        self.view.addGestureRecognizer(bgTapRecognizer)

        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width-drawerOffset-40, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.menuVC.view.frame=CGRect(x: 0, y: 64, width: UIScreen.main.bounds.size.width-drawerOffset-40, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
            }, completion:nil)
        self.tabBarController?.tabBar.isUserInteractionEnabled = false

    }
    
    //MARK: left drawer method end
    
    func bgTapped(_ sender: UITapGestureRecognizer) {
        dismissContainer()
        self.tabBarController?.tabBar.isUserInteractionEnabled = true


    }
    
    func loadImageFromUrl(_ url: String, btn: UIButton){
        
        // Create Url from string
        let url = URL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.sync(execute: { () -> Void in
                    btn.setImage( UIImage(data: data), for: .normal)
                   // self.myActivityIndicator.stopAnimating()
                })
            }
        })
        // Run task
        task.resume()
    }
    
    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
            
            self.navigationController!.pushViewController(secondViewController, animated: false)
        }

    }


}

