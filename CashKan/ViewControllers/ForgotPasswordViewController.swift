//
//  ForgotPasswordViewController.swift
//  Cashkan_auotoSizing
//
//  Created by emtarang on 23/06/16.
//  Copyright © 2016 emtarang. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController,UITextFieldDelegate, NetworkFailureDelegate {

    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var sendBtn: UIButton!
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = forgot_title
        HELPER.setFontFamily( self.view, andSubViews: true)
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        let blueColor = UIColor(red: 73/255, green: 95/255, blue: 135/255, alpha: 1) //#495f87
        
        
        // Do any additional setup after loading the view.
       // self.navigationController?.title = forgot_title

        emailLabel.textColor = blueColor
        emailTextField.textColor = textGray_color
        
        let btnColor  = UIColor(red: 65/255, green: 89/255, blue: 130/255, alpha: 1) //#415982
        sendBtn.layer.borderColor = btnColor.cgColor
        sendBtn.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        sendBtn.layer.borderWidth = CGFloat(customButtonBorderWidth)
        sendBtn.setTitleColor( btnColor, for: UIControlState())
        
        
        //self.navigationItem.hidesBackButton = true
        
        
        emailTextField.delegate = self;
     
        let button: UIButton = UIButton(type: UIButtonType.custom)
        //set image for button
        button.setImage(UIImage(named: "Cashkans_Back arrow"), for: UIControlState())
        //add function for button
        button.addTarget(self, action: #selector(ForgotPasswordViewController.back(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton;
        
        
        // Change the navigation bar background color to blue.
        navigationController!.navigationBar.barTintColor = btnColor
        
        // Change the color of the navigation bar button items to white.
        navigationController!.navigationBar.tintColor = UIColor.white
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = forgot_title
        HELPER.setFontFamily( self.view, andSubViews: true)
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: customAppFont, size: navigationTitleFontSize)!,  NSForegroundColorAttributeName: UIColor.white]
        RequestResponceHelper.delegate = self
    }
    
    func back(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        //dismissViewControllerAnimated(false, completion: nil)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
        let transition = CATransition()
        transition.duration = 0.2
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        view.window!.layer.add(transition, forKey: kCATransition);
        self.present(vc, animated: false, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //print("TextField should return method called")
        emailTextField.resignFirstResponder();
        
        return true;
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
    }
    
    @IBAction func sendOtpClicked(_ sender: UIButton) {
        
        if(Reachability.isConnectedToNetwork() == false){
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
                
                self.navigationController!.pushViewController(secondViewController, animated: false)
                
            }
            
        }
        else{
        
        if (self.emailTextField.text!.isEmpty){
            let alertController = UIAlertController(title: alert_cashkan, message: alert_enterEmail, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                //print("you have pressed the Cancel button");
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }
        else if(!isValidEmail(self.emailTextField.text!)){
            let alertController = UIAlertController(title: alert_cashkan, message: alert_invalidEmail, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }
        else{
        // call sendForgotPasswordRequest
            self.activityIndicator.showActivityIndicatory(self.view)
            sendForgotPasswordRequest()
            
        }
        }
        
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    
    func sendForgotPasswordRequest(){
        

        let emailTxt = emailTextField.text! as String
        
        let params = ["email" : emailTxt]
        
        self.ForgotPassword(params)
        {
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            //print("got back: \(message)")

            if( success == true){
                
                if let response = object as? HTTPURLResponse{
                    
                    // You can print out response object
                    //print("response = \(response)")
                    
                }

                
                do {
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                    
                    if let parseJSON = myJSON![data] as! [String:Any]?{
                    
                        HELPER.otvcId                = (parseJSON["otvcId"] as! NSNumber)
                        //print(HELPER.otvcId)
                        HELPER.email = emailTxt
                    }
                    
                    if let successMsg = myJSON![display_msg]{
                        //print(successMsg)
                        self.activityIndicator.hideActivityIndicator(self.view)
                        let alertController = UIAlertController(title: alert_cashkan, message: successMsg as? String, preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                            if let resultController = self.storyboard!.instantiateViewController(withIdentifier: "OTPController") as? UIViewController {
                                //self.presentViewController(resultController, animated: true, completion: nil)
                                self.navigationController!.pushViewController(resultController, animated: true)
                            }

                        }
                        
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion:nil)
                        
                    }
                    
                    
                    
                }
                catch {
                    //print(error)
                }
                
                
                
            }
                
            else{
                
                // If registration failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            let error = myJSON![display_msg]
                            self.activityIndicator.hideActivityIndicator(self.view)

                            let alertController = UIAlertController(title: alert_cashkan, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }

                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        //print(error)
                    }
                    
                }
            }
            
            
       }
    }
    


    
    func ForgotPassword(_ dictParams: Dictionary<String, String>, completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data? , _ message: String?) -> ()) {
        
        
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest(otp_url, params: dictParams as Dictionary<String, AnyObject>? )
        
        RequestResponceHelper.post(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? NSData{
                        //message = passedMessage
                        completion(success, object ,dataObject as Data ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }
    
    func networkFailed(){
        self.activityIndicator.hideActivityIndicator(self.view)
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "NetworkFailureId") as! NetworkFailureViewController
            
            self.navigationController!.pushViewController(secondViewController, animated: false)
        }
        
    }



}
