//
//  UIColor+HexColor.h
//  BEAMOO
//
//  Created by piyush on 07/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexColor)

+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end
