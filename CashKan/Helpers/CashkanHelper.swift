//
//  CashkanHelper.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 28/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit
import Foundation
import Firebase

let _sharedInstance = CashkanHelper()

 class CashkanHelper: NSObject {
    
    // Drawer
    var isDrawerOpen : DarwinBoolean = false
    var iswebviewPresent : Bool = false // webviewIssue
    var closeDrawer : Bool = false
    
    // User details
    var fname = String()
    var authrizationHeader = String()
    var lname = String()
    var user_id : NSNumber = 0
    var sex   = String()
    var dob  = String()
    var email = String()
    var mobile = String()
    var lat = String()
    var lng = String()
    var image_url = String()
    var is_active  : NSNumber = 0
    var total_cashkans  : NSNumber = 0
    var version_obsolete  : NSNumber = 0
    var otvcId : NSNumber = 0
    var profileImage : UIImage!
        
    var password = String()
    
    // Locale info
    var localeInfo : NSArray = []
    var localeId : NSNumber = 0
    var currencySymbol = String()
    var currencySym = String()
    var distanceIn = String()
    var regionShortCode = String()
    var isLogin = String()
    
    // Search Receipt
    var search_vendorName = String()
    var search_min_amount = String()
    var search_max_amount = String()
    var search_spentOn = String()
    var search_fromDate = String()
    var search_toDate = String()
    var isSearchActive :Bool = false
    
    var isRewardsRefreshed : Bool = false
    
    // upload Receipt
    var isUploadClicked : Bool = false
    
    var searchDateFilterIndex : UInt = 0
    var isReceiptRefreshed : Bool = false
    var isJourneyRefreshed : Bool = false
    var isConnectedToNetwork : Bool = true
    var backToRoot : Bool = false
    
    var isCashDetailsOpen : Bool = false
    //showSuccessDialogue on session uploading
    var showSuccessDialogue : Bool = true
    
    // NotificationTag
    var notificationTag = String()
    
    // Instruction pages count 
    var showInstructionsCnt : Int = 0
    
    // To check whether cash vc is active or not
    var isCashAlive : Bool = false
    var selectedWebViewIndex : Int = 0
    var randomUUID = String()
    var addUuid : Bool = false

    func getRandonUUID()->String{
    
        var randomUuid = UUID().uuidString
        var deviceId = ""
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            deviceId = refreshedToken            
        }
        randomUuid.append(deviceId)
        return randomUuid
        
    }
    
    func setFontFamily(_ forView : UIView, andSubViews : Bool){
        
        let fontFamily : String = customAppFont
        
        if (forView.isKind(of: UILabel.self)){
            let lbl : UILabel = forView as! UILabel
            lbl.font = UIFont(name: fontFamily, size: lbl.font.pointSize)
        }
//        if (forView.isKind(of: UITextView.self)){
//            let vw : UITextView = forView as! UITextView
//            //vw.font = UIFont(name: fontFamily, size: vw.font?.pointSize)
//            if vw.font?.fontName != fontFamily {
//                vw.font = UIFont(name: fontFamily, size: 12)
//            }
//            
//        }
        if (forView.isKind(of: UITextField.self)){
            let vw : UITextField = forView as! UITextField
            vw.font = UIFont(name: fontFamily, size: vw.font!.pointSize)
            
        }
        if (forView.isKind(of: UIButton.self)){
            let btn : UIButton = forView as! UIButton
            btn.titleLabel!.font = UIFont(name: fontFamily, size: btn.titleLabel!.font.pointSize)
            
        }
        
        
        if(andSubViews){
            for sview : UIView in forView.subviews{
                self.setFontFamily(sview, andSubViews: true)
            }
        }
    }

    fileprivate override init() {
    }
    
    func updateWithSpacing(lineSpacing: Float, textView : UIView) {
        
        if (textView.isKind(of: UILabel.self)){
            let lbl : UILabel = textView as! UILabel
            let attributedString = NSMutableAttributedString(string: lbl.text!)
            let mutableParagraphStyle = NSMutableParagraphStyle()
            mutableParagraphStyle.lineSpacing = CGFloat(lineSpacing)
            
            if let stringLength = lbl.text?.characters.count {
                attributedString.addAttribute(NSParagraphStyleAttributeName, value: mutableParagraphStyle, range: NSMakeRange(0, stringLength))
            }
            lbl.attributedText = attributedString
        }
        if (textView.isKind(of: UIButton.self)){
            let btn : UIButton = textView as! UIButton
            let str = btn.titleLabel?.text
            let attributedString = NSMutableAttributedString(string: str!)
            let mutableParagraphStyle = NSMutableParagraphStyle()
            mutableParagraphStyle.lineSpacing = CGFloat(lineSpacing)
            
            if let stringLength = btn.titleLabel?.text?.characters.count {
                attributedString.addAttribute(NSParagraphStyleAttributeName, value: mutableParagraphStyle, range: NSMakeRange(0, stringLength))
            }
            btn.titleLabel?.attributedText = attributedString
        }
        if (textView.isKind(of: UITextView.self)){
            let lbl : UITextView = textView as! UITextView
            let attributedString = NSMutableAttributedString(string: lbl.text!)
            let mutableParagraphStyle = NSMutableParagraphStyle()
            mutableParagraphStyle.lineSpacing = CGFloat(lineSpacing)
            
            if let stringLength = lbl.text?.characters.count {
                attributedString.addAttribute(NSParagraphStyleAttributeName, value: mutableParagraphStyle, range: NSMakeRange(0, stringLength))
            }
            lbl.attributedText = attributedString
        }
        
    }
    
    //-(UIImage *)resizeImage:(UIImage *)image
    func resizeImage(_ image: UIImage, size:CGSize) -> UIImage{
        
        var actualHeight : CGFloat = image.size.height;
        var actualWidth  : CGFloat = image.size.width;
        let maxHeight    : CGFloat = size.height //800 //1080.0;
        let maxWidth     : CGFloat = size.width //800 //1920.0;
        var imgRatio     : CGFloat = actualWidth/actualHeight;
        let maxRatio     : CGFloat = maxWidth/maxHeight;
        let compressionQuality : CGFloat = 1.0;//50 percent compression
        
        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = maxHeight;
            }
            else if(imgRatio > maxRatio){
                
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = maxWidth;
            }
            else{
                
                actualHeight = maxHeight;
                actualWidth = maxWidth;
            }
        }
        
        let rect : CGRect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight);
        UIGraphicsBeginImageContext(rect.size);
        image.draw(in: rect)//[image drawInRect:rect];
        let img : UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
        let imageData : Data = UIImageJPEGRepresentation(img, compressionQuality)!;
        UIGraphicsEndImageContext();
        
        return UIImage(data: imageData)! //return [UIImage imageWithData:imageData];
        
    }

    func changeImageSize( _ image: UIImage, newSize  :CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }

    
    class var sharedInstance: CashkanHelper {
        
//        if _sharedInstance == nil{
//             _sharedInstance = CashkanHelper()
//        }
        
        return _sharedInstance
    }
}
