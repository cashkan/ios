                                                                                                                                                                                        //
//  MenuViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit
import Firebase

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
//    func signout()
    func dismissContainer()
}

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, UITabBarDelegate {
    
    let mySwitch = SevenSwitch() 
    
    /**
    *  Array to display menu options
    */
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
    *  Transparent button to hide menu
    */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
    *  Array containing menu options
    */
    var arrayMenuOptions = [Dictionary<String,String>]()
    
    /**
    *  Menu button which was tapped to display the menu
    */
    var btnMenu : UIButton!
    var userEmailAddress: String!
    var firstName: String!
    var lastName: String!
    
    
    /**
    *  Delegate of the MenuVC
    */
    var delegate : SlideMenuDelegate?
    
    var activityIndicator : CustomActivityIndicator = CustomActivityIndicator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateArrayMenuOptions()
        self.view.tag = 123
        tblMenuOptions.delegate = self
        tblMenuOptions.dataSource = self
        tblMenuOptions.separatorStyle = UITableViewCellSeparatorStyle.none
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuViewController.dismissContainer))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
        
        self.userEmailAddress = HELPER.email
        self.firstName = HELPER.fname
        self.lastName = HELPER.lname
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        HELPER.setFontFamily( self.view, andSubViews: true)
    }

    //MARK: custom method
    
    func setStyleToCircularImageView(_ img : UIImageView ) {
        
        img.layer.cornerRadius = img.frame.size.width / 2;
        img.clipsToBounds = true
        
    }
    
    func dismissContainer(){
        
        delegate?.dismissContainer()
    }
    
    func updateArrayMenuOptions(){
        arrayMenuOptions.append(["title":"SignOut", "icon":"signout"])
        arrayMenuOptions.append(["title":"Notification", "icon":"notification"])
        arrayMenuOptions.append(["title":"Help", "icon":"home"])
        arrayMenuOptions.append(["title":"What is CashKan?", "icon":"notification"])
        arrayMenuOptions.append(["title":"How it Works?", "icon":"home"])
        arrayMenuOptions.append(["title":"FAQ", "icon":"notification"])
        arrayMenuOptions.append(["title":"Rules", "icon":"notification"])
        arrayMenuOptions.append(["title":"Terms Of Use", "icon":"home"])
        arrayMenuOptions.append(["title":"Privacy", "icon":"notification"])
        arrayMenuOptions.append(["title":"Contact Us", "icon":"home"])
        tblMenuOptions.reloadData()
    }
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
         let index = Int32(button.tag)
        delegate?.slideMenuItemSelectedAtIndex(index)
    }
    
    //MARK: tableview delegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(arrayMenuOptions.count )
        let rowcnt = arrayMenuOptions.count
        return rowcnt
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var rowheight: CGFloat!
        if (indexPath as NSIndexPath).row == 0 {
            rowheight = 170.0
        }else{
            rowheight = 44.0
        }
        return rowheight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellMenu")!
        HELPER.setFontFamily(cell.contentView, andSubViews: true)

        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        cell.isUserInteractionEnabled = true
        
        if (indexPath as NSIndexPath).row==0 {
            // profile cell
            cell.addSubview(self.customCellView())
            cell.backgroundColor = darkGray_color
            cell.autoresizesSubviews = true
            
        }else if (indexPath as NSIndexPath).row == 1{
            
            // Notification cell
            mySwitch.frame = CGRect(x: cell.frame.width - 70, y: 12, width: 50, height: 20)
            mySwitch.activeColor =  darkGray_color
            mySwitch.inactiveColor = lightGray_color// darkGray_color
            mySwitch.onTintColor =  UIColor.darkGray
            mySwitch.borderColor = UIColor.clear
            mySwitch.shadowColor = UIColor.clear
            mySwitch.addTarget(self, action: #selector(MenuViewController.notificationClicked as (MenuViewController) -> () -> ()), for: UIControlEvents.valueChanged)
            cell.addSubview(mySwitch)
            mySwitch.isUserInteractionEnabled = true
            
            let defaults = UserDefaults.standard
            if(defaults.bool(forKey: "showNotifications") == true){
                mySwitch.setOn(true, animated: false)
            }
            else{
                mySwitch.setOn(false, animated: false)
            }


            let dividerView = UIView()
            dividerView.frame = CGRect(x: 0, y: cell.frame.height
                 - 1, width: cell.frame.width, height: 1)
            dividerView.backgroundColor = lightGray_color
            cell.addSubview(dividerView)

            cell.textLabel?.text = arrayMenuOptions[(indexPath as NSIndexPath).row]["title"]!
            cell.backgroundColor = UIColor.white
            cell.textLabel?.textColor = textGray_color
            
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuViewController.drawerCellTapped(_:)))
            cell.addGestureRecognizer(tapRecognizer)

        }
        
        else{
            // others cell
            if (indexPath as NSIndexPath).row != 0 {
                let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(MenuViewController.drawerCellTapped(_:)))
                cell.addGestureRecognizer(tapRecognizer)
            }

             if (indexPath as NSIndexPath).row == 2 || (indexPath as NSIndexPath).row == 5{
                
                let dividerView = UIView()
                dividerView.frame = CGRect(x: 0, y: cell.frame.height
                    - 1, width: cell.frame.width, height: 1)
                dividerView.backgroundColor = lightGray_color
                cell.addSubview(dividerView)
            }
            cell.textLabel?.text = arrayMenuOptions[(indexPath as NSIndexPath).row]["title"]!
            cell.backgroundColor = UIColor.white
            cell.textLabel?.textColor = textGray_color
        }
        
        if(tableView.contentSize.height > self.view.frame.height - 100){
            
        //((self.navigationController?.navigationBar.frame.size.height)! + (self.tabBarController?.tabBar.frame.size.height)!)){
            tableView.contentInset = UIEdgeInsetsMake(0, 0, self.bottomLayoutGuide.length + (22), 0);
            tableView.isScrollEnabled = true
        }
        else{
            tableView.contentInset = UIEdgeInsetsMake(0, 0, self.bottomLayoutGuide.length , 0);
            tableView.isScrollEnabled = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        tblMenuOptions.reloadData()
        //tblMenuOptions.cellForRow(at: 0)?.autoresizesSubviews = true
    }
    
    
    func drawerCellTapped(_ recognizer  : UITapGestureRecognizer){
        
        let tapLocation = recognizer.location(in: self.tblMenuOptions)
        //using the tapLocation, we retrieve the corresponding indexPath
        let indexPath = self.tblMenuOptions.indexPathForRow(at: tapLocation)
        
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = (indexPath! as NSIndexPath).row
        self.onCloseMenuClick(btn)
        
        if (indexPath! as NSIndexPath).row == 0 {
            //            self.doSignOut(btn)
        }
        else if (indexPath! as NSIndexPath).row == 2{
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "drawerWebView") as? DrawerWebViewController {
                resultController.urlToVisit = helpUrl
                resultController.selectedItemIndex = (indexPath! as NSIndexPath).row
                self.navigationController?.pushViewController(resultController, animated: false)
            }
        }
        else if ((indexPath as NSIndexPath?)?.row >= 3 ){
            
            if let resultController = storyboard!.instantiateViewController(withIdentifier: "drawerWebView") as? DrawerWebViewController {
                resultController.urlToVisit = webViewurls[(indexPath! as NSIndexPath).row - 3]
                resultController.selectedItemIndex = (indexPath! as NSIndexPath).row
                self.navigationController?.pushViewController(resultController, animated: false)
            }
            
        }

    
    }
    

    // #MARK: custom profile view cell
    func customCellView ()-> UIView{
         let viewFrame:CGRect = self.view.frame
        let cView:UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.btnCloseMenuOverlay.frame.width, height: 170));
        let xPos:CGFloat = 0.0
        var yPos:CGFloat =  5
        
        let image = UIImage(named: "ic_settings_power_white_36dp.png")
        
        let signOutButton = UIButton(type: .custom)
        signOutButton.frame = CGRect(x: (xPos+viewFrame.size.width+30+40)/2, y: yPos, width: 35, height: 35)
        signOutButton.setImage(image, for: UIControlState())
//        signOutButton.setTitle("Test Button", forState: .Normal)

        signOutButton.addTarget(self, action: #selector(doSignOut), for: .touchUpInside)
        cView.addSubview(signOutButton)
        
        yPos = yPos+30
        
        
        let imageName = "pro12.png"
        let img = UIImage(named: imageName)
        let imageView = UIImageView(image: img!)
        imageView.backgroundColor = UIColor.gray
        imageView.frame = CGRect(x: (cView.frame.width-90)/2, y: yPos, width: 90, height: 90)
        
        if( HELPER.image_url != ""){
            let url = NSURL(string: HELPER.image_url)
            if(HELPER.profileImage == nil &&  UIApplication.shared.canOpenURL(url! as URL)){
                self.loadImageFromUrl(HELPER.image_url, view: imageView)
            }
            else{
                imageView.image = HELPER.profileImage
            }
        }

        if((imageView.image?.size)!.equalTo(CGSize.zero)){
            imageView.image = img
        }
        cView.addSubview(imageView)
        self.perform(#selector(ReceiptController.setStyleToCircularImageView(_:)), with: imageView, afterDelay: 0)


        yPos = yPos + imageView.frame.size.height
        let label1 = UILabel(frame: CGRect(x: 0, y: yPos, width: cView.frame.width, height: 22))
        label1.textAlignment = NSTextAlignment.center
        label1.text = self.firstName + " " + self.lastName
        label1.textColor = UIColor.white
        cView.addSubview(label1)

        yPos = yPos+label1.frame.size.height
        
        let label2 = UILabel(frame: CGRect(x: 0, y: yPos, width: cView.frame.width, height: 22))
        label2.textAlignment = NSTextAlignment.center
        label2.text = self.userEmailAddress
        label2.font = UIFont(name: "Arial", size: 15)
        label2.textColor = UIColor.white
        cView.addSubview(label2)
        
        cView.backgroundColor = UIColor.gray
        return cView
    }
    
    func doSignOut(_ sender: UIButton!) {
        self.activityIndicator.showActivityIndicatory((self.view)!)
        self.signout()
    }
  
    func loadImageFromUrl(_ url: String, view: UIImageView){
        
        // Create Url from string
        let url = URL(string: url)!
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                DispatchQueue.main.sync(execute: { () -> Void in
                    HELPER.profileImage = UIImage(data: data)
                    view.image = UIImage(data: data)
                })
            }
        }) 
        // Run task
        task.resume()
    }

    func notificationClicked(){
        let defaults = UserDefaults.standard
        //print(defaults.bool(forKey: "showNotifications"))

        if(mySwitch.isOn()){
            //mySwitch.setOn(false, animated: true)
            //HELPER.notificationSetting = false
            defaults.set(true, forKey: "showNotifications")         // do not show notifications
            UIApplication.shared.registerForRemoteNotifications()

        }
        else{
            //mySwitch.setOn(true, animated: true)
            //HELPER.notificationSetting = true
            defaults.set(false, forKey: "showNotifications")         // show notifications
            UIApplication.shared.unregisterForRemoteNotifications()


        }
        defaults.synchronize()
        

    }

    func signout(){
        var deviceId = ""
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            deviceId = refreshedToken
        }
       
        self.doLogOut(deviceId)
        {
            (success :  Bool, object: AnyObject?, dataObject: Data?,message: String?) in
            
            self.activityIndicator.hideActivityIndicator((self.view)!)

            if( success == true){
                
                if let response = object as? HTTPURLResponse{
                    
                    // You can print out response object
                    
                }
                
                
                do {
                    let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                    
                    if let successMsg = myJSON![display_msg]{

                        let alertController = UIAlertController(title: alert_cashkan, message: successMsg as? String, preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                            
                            let defaults = UserDefaults.standard
                            defaults.removeObject(forKey: "userinfo")
                            defaults.synchronize()
                            HELPER.localeInfo = []
                            HELPER.image_url = ""
                            HELPER.profileImage = nil
                            HELPER.isReceiptRefreshed = false
                            HELPER.isJourneyRefreshed = false
                            HELPER.isReceiptRefreshed = false
                            HELPER.notificationTag = ""
                            HELPER.isSearchActive = false
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
                            self.present(vc, animated: true, completion: nil)
                            
                        }
                        
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion:nil)
                        
                    }
                    
                    
                    
                }
                catch {
                    //print(error)
                }
                
                
                
            }
                
            else{
                
                // If logout''' failed
                
                if( dataObject != nil){
                    
                    do {
                        let myJSON =  try JSONSerialization.jsonObject(with: dataObject!, options: .mutableContainers) as? NSDictionary
                        
                        if (myJSON![display_msg]! as! String).characters.count != 0{
                            let error = myJSON![display_msg]
                            
                            let alertController = UIAlertController(title: alert_cashkan, message: error as? String, preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: alert_ok, style: .cancel) { (action:UIAlertAction!) in
                                //print("you have pressed the Cancel button");
                            }
                            
                            alertController.addAction(cancelAction)
                            self.parent?.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                    catch {
                        //print(error)
                    }
                    
                }
            }
            
        }
    }
    
    func doLogOut(_ deviceId: String,completion: @escaping (_ success: Bool, _ object: AnyObject?,  _ dataObject: Data?, _ message: String?) -> ()) {
        
        var logoutObject: NSDictionary
        
        logoutObject = ["deviceId" : deviceId]
        let id =  HELPER.user_id.stringValue
        let logouturl = logout_url+"/"+id+"/logout"
        let request: NSMutableURLRequest = RequestResponceHelper.clientURLRequest(logouturl, params: logoutObject as? Dictionary<String, AnyObject> )
        
        RequestResponceHelper.post(request) { ( success, object, dataObject) -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                if success {
                    let successmessage = object![display_msg] as? String
                    completion(true, object, dataObject ,successmessage)
                } else {
                    let message = "there was an error"
                    
                    if let object = object, let dataObject = dataObject as? NSData{
                        //message = passedMessage
                        completion(success, object ,dataObject as Data ,message)
                    }
                    else{
                        
                        completion(success, nil, nil ,nil)
                    }
                }
            })
        }
    }
    
}
