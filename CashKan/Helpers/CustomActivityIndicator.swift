//
//  CustomActivityIndicator.swift
//  CashKan
//
//  Created by emtarang on 09/08/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class CustomActivityIndicator: NSObject {
    

    
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var actInd: UIActivityIndicatorView!
    var customlabel: UILabel = UILabel()

    
    func UIColorFromHex(_ rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    func showActivityIndicatory(_ uiView: UIView) {
    
        
        
        let xPosition = uiView.frame.origin.x
        let yPosition = uiView.frame.origin.y

        
        var rect = CGRect(x: xPosition,y: yPosition + 50,width: uiView.frame.size.width,height: uiView.frame.size.height)

        
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColorFromHex(0xffffff, alpha: 0.3)
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColorFromHex(0x444444, alpha: 0.5)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = CGFloat(customButtonCornerRadius)
        
        actInd = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.whiteLarge
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2,
                                    y: loadingView.frame.size.height / 2);

        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        
        // label for refreshing task
        customlabel.frame = CGRect(x: uiView.frame.width/4, y: uiView.frame.height - (uiView.frame.height/5), width: uiView.frame.width/2, height: 30)
        customlabel.textColor = UIColorFromHex(0xffffff, alpha: 1)
        customlabel.font = UIFont.systemFont(ofSize: 15)
        customlabel.backgroundColor = UIColorFromHex(0x444444, alpha:0.5)
        customlabel.layer.cornerRadius = customlabel.frame.height / 2
        customlabel.layer.masksToBounds = true
        customlabel.text = "Loading..."
        customlabel.textAlignment = .center
        uiView.addSubview(customlabel)
        
        customlabel.isHidden = true
        actInd.startAnimating()
    }
    
    func hideActivityIndicator(_ uiView: UIView) {

        actInd.stopAnimating()
        container.removeFromSuperview()
        customlabel.removeFromSuperview()
    }


}
