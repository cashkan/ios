//
//  RequestResponceHelper.swift
//  CashKan
//
//  Created by emtarang on 29/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit


protocol NetworkFailureDelegate {
    func networkFailed()
}

class RequestResponceHelper {
 
    static var delegate : NetworkFailureDelegate?
     class func dataTask(_ request: NSMutableURLRequest, method: String, completion: @escaping (_ success: Bool, _ object: AnyObject?, _ dataObject : Data) -> ()) {
        request.httpMethod = method
        
        //print("request = \(request)")
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 30.0;
        sessionConfig.timeoutIntervalForResource = 30.0;
        
        let session = URLSession.init(configuration: sessionConfig)  //URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest) { data, response, error in

            //print(response)

            var successObj :  Bool = true
            if let data = data {
                if let response = response as? HTTPURLResponse{
                
                    // You can print out response object
                    //print("response = \(response)")
                    
                                // Print out response body
                                let responseString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                    //print("responseString = \(responseString)")
                    
                                //Let's convert response sent from a server side script to a NSDictionary object:
                                do {
                                    let myJSON =  try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
                    //print("myjson = \(myJSON)")
                                    if let parseJSON = myJSON!["error"] as? String{

                                        if( parseJSON != "0"){
                                            successObj = false
                                        }
                                        else{
                                            successObj = true
                                        }
                                   }
                                    if let parseJSON = myJSON!["error"] as? Int{
                                        
                                        if( parseJSON != 0){
                                            successObj = false
                                        }
                                        else{
                                            successObj = true
                                        }
                                    }
                                }
                                catch {
                                    successObj = false
                    }
                    
                    completion(successObj, response, data)
                    
                } else {
//                    completion(false, response, data)
                    delegate?.networkFailed()
                }
                
            }
            else{
                delegate?.networkFailed()
            }
            
            } .resume()
    }
    
    class func post(_ request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?, _ dataObject: Data) -> ()) {
        dataTask(request, method: "POST", completion: completion)
    }
    
    class func put(_ request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?, _ dataObject: Data) -> ()) {
        dataTask(request, method: "PUT", completion: completion )
    }
    
    class func get(_ request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?, _ dataObject: Data) -> ()) {
        dataTask(request, method: "GET", completion: completion)
    }
    
    class func clientURLRequest(_ path: String,  params: Dictionary<String, AnyObject>? = nil) -> NSMutableURLRequest {
        let request = NSMutableURLRequest(url: URL(string: baseUrl+path)!)
        
        if let params = params {

            do{

                request.httpBody =   (try JSONSerialization.data(withJSONObject: params, options:[]))
                request.addValue("application/json",forHTTPHeaderField: "Content-Type")
                request.addValue("application/json",forHTTPHeaderField: "Accept")
                //print(NSString(data: request.httpBody!, encoding:String.Encoding.utf8.rawValue)!);
                //print(request.allHTTPHeaderFields);
          
        }
        catch {
            //print(error)
        }
        }

        if( HELPER.authrizationHeader.characters.count != 0){
                if let token = HELPER.authrizationHeader as? String{
                    request.addValue(token, forHTTPHeaderField: "Authorization")
                }
        }
        
        if(HELPER.addUuid){
            request.addValue(HELPER.randomUUID,forHTTPHeaderField: "transactionid")
            HELPER.addUuid = false
        }
        
        return request
    }
    
    
    
    /* .......
        To download image from Url
 
                            ..........*/
    
//    func getDataFromUrl(url: NSURL, completion: ((data: NSData?, response: NSURLResponse?, error: NSError? ) -> Void)) {
//        NSURLSession.sharedSession().dataTaskWithURL(url) {
//            (data, response, error) in
//            completion(data: data, response: response, error: error)
//            }.resume()
//    }
//    
//    func downloadImage(url: NSURL){
//        print("Download Started")
//        getDataFromUrl(url) { (data, response, error)  in
//            guard let data = data where error == nil else { return }
//            
//            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                print(response?.suggestedFilename ?? url.lastPathComponent ?? "")
//                               print("Download Finished")
//
//            })
//            
////            DispatchQueue.main.async() { () -> Void in
////                print(response?.suggestedFilename ?? url.lastPathComponent ?? "")
////                print("Download Finished")
////                //self.imageView.image = UIImage(data: data)
////            })
//        }
//    }
//    


}
