//
//  StringConstants.swift
//  CashKan
//
//  Created by emtarang on 03/08/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

let Google_api_key_location = "AIzaSyAOm9ZvlvO5o7Le4m5h6H46997z5toagSQ"

let customAppFont = "AvenirLTStd-Book"

// size constants
let navigationTitleFontSize : CGFloat = 23
let buttonFontSize : CGFloat = 20 //23
let customButtonBorderWidth = 2.0
let customButtonCornerRadius = 10.0


// IV and KEY for AES

let iv = "fdsfds85435nfdfs"
let key = "abcdefghijklmnop"
let appName = "Cashkan"

let IndianEconomy = "Indian Economy"
let USEconomy     = "U. S. Economy"
let cannadianEconomy = "Canadian Economy"


// Request Response objects
let IndianCurrency = "\u{20B9}" // Rupees symbol
let USAcurrency = "\u{0024}"
let cashkan_bullet = "\u{25CF}"



let error_msg = "errorMessage"
let display_msg = "displayMessage"
let error = "error"
let data = "data"
let response = "response"

// Months Array
let monthsArray : [(String,String)] =
    [("01", "Jan"),("02" ,"Feb"),("03","Mar"),("04","Apr"),("05","May"),("06","Jun"),("07","Jul"),("08","Aug"),("09","Sep"),("10","Oct"),("11","Nov"),("12","Dec")]

//Network failure
var network_title = "NETWORK CONNECTION"
var network_message = "Please go to settings page and enable your network connection or wifi connection"

// Login

var login_email = "  Email"
var login_password = "  Password"
var login_failed = "Login Failed"
var ourVisionUrl = ""

var alert_cashkan = "CashKan"
var alert_ok   = "OK"
var alert_cancel = "Cancel"
var alert_enterEmail = "Please enter Email..."
var alert_enterPassword = "Please enter Password..."
var alert_invalidEmail = "Please enter valid Email Address"
var alert_invalidPass = "Password must be minimum 6 characters long"


// Register
var validEmailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
var title_choose_image = "Choose Image"
var camera = "Camera"
var gallery = "Gallery"
var alert_firstName = "Please enter First Name"
var alert_lastName = "Please enter Last Name"
var alert_mobile = "Please enter Mobile Number"
var alert_password = "Please enter password"
var alert_pass_len_validation = "Password should be between 6 to 8 characters"
var alert_reEntarPassword = "Please confirm your password"
var alert_dob = "Please select Date of Birth"
var alert_gender = "Please Select Gender"
var register_failed = "Registration Failed"
var alert_gpsOff = "CashKan App requires your location information to detect scan location. Please go to settings page and enable your gps to get the location "
var alert_termsOfUseTitle = "Terms Of Use"
var alert_termsofUse = "Please accept the Terms of Use for Cashkan"
var alert_agree = "Agree"
var termsOfUse = "Terms Of Use"
var alert_notificationOff = "Cashkan app wants to send you notifications. Please go to settings and enable notifications"
var password_length_str = "(6 to 8 characters)"

// Forgot password
var forgot_title = "Forgot Password"


// OTP
var otp_title = "Verify OTVC"
var alert_otp = "Please enter the One Time Verification Code"

// Reset Password
var resetPassword_title = "Reset Password"
var alert_match_password = "Passwords do not match"
var alert_passwordValidation = "Password should be of minimum 8 characters"

// Scan Cash

var cashkanBill = "CK # : "
var scanCash_title = "Spend Amount"
var alert_spendAmount = "Spend Amount"
var alert_duplicateCurrencyValue = "Duplicate entry"
var alert_pleaseAddCurrencyValue = "Please pick a currency denomination!"
var alert_pleaseSelectDenomination = "Please select the denomination you wish to scan"

var spendString = "How do you plan to spend value ?"

var session_thankyou_dialogue = "Thanks for cashkanning. When you spend the money please upload the receipts so that the cashkans can be activated and be part of the Cashkan Journey that could win you rewards. Keep cashkanning!\n\n\n\n*The receipts can be from any form of payment - cash or non cash, after all its your money!"

var sesionSuccessfulMessage = "Thanks for cashkanning. When you spend the bills and the bills                 gets cashkanned by more users you earn more chances to win a reward. Keep Cashkanning!"

var sessionSuccessMsg1 = "Thanks for setting up a spend objective."
var sessionSuccessMsg2 = "Once you spend the money please upload receipts* so that you can play the Journey game and win rewards."

var sessionSuccessMsg3 = "* Receipts will be verified at reward redemption"
var doNotshow = "Do not show this again."

var duplicate_sessionDialogue = "We have found duplicates in the currencies you just cashkanned. \n We are dis-regarding some out of total"

var originalScannedAmount = "Original Scanned Amount"
var ActualProcessedAmount = "Actual Processed Amount"

var currency_charSet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

var slide_click_1 = "Tap on the cash denominations to specify the amount you plan to spend."
//var slide_click_1 = "1. Enter the amount you plan to spend by tapping on the currency denominations"
var slide_click_2 = "2. To earn bonus reward points pick a currency denomination, slide and scan the serial number on cash (?)"

var alert_NoScanCash = "By not scanning cash for its serial number you miss the opportunity to earn bonus reward points."

var tipTitleArray = ["Tip #1: Limit your spend",
                     "Tip #2: Know your spend habits",
                     "Tip #3: Track your cash spend"]

var tipDescArray = ["Be it cash or card, setting up spend amount in cash denominations can limit your spend.",
                    "Setup daily spend objectives. It's your daily expenses that drains your savings.",
                    "Upon withdrawing cash from ATM setup spend objective for that withdrawn amount and track how you spend it."]

var tipNext = "NEXT TIP"



// Receipts
var receipts_title = "Receipts"
var receiptsRefreshString = "Getting Receipts"
var slider_yearToDate = "12 Months"
var slider_last3months = " Last 3 Months"
var slider_lastMonth = "Last Month"
var slider_thisMonth = "This Month"

var rulesString = "Every bill CashKaneed gets an entry into the rewards program.                                                           A bill can not be CashKanned by an user more than once.Bill that generated the maximum economic value and travelled the maximum distance will be rewarded.In case of a tie the winner will be picked based on a lottery. The total economic value generated and distance travelled will be reset to 0 once a bill has been rewarded. The bill will then start a new Cashkan journey."
var sigma = "\u{03A3}"
var overSpentStr = "Over Spent"
var underSpentStr = "Saving"

var objective = sigma+" "+"Objective"
var spent = sigma+" "+"Spent"

var string_receiptHeaderLabel_plural = "You have some Spend Objectives for selectedMonth"
var string_receiptHeaderLabel = "You have some Spend Objective for selectedMonth"

var string_searchHeaderLabel_plural = "You have some actual spend items that match the search"
var string_searchHeaderLabel = "You have some actual spend item that match the search"

var headerLabelString = ""

var notificationToolTip = "You have few objectives in the current list, which have no spend recorded." + "\n\n" + "Click this message to add some spend against those"

var string_headerLabel_plurals = ["some Spend Objectives in the 12 month period",
                                  "some Spend Objectives for the last 3 months",
                                  "some Spend Objectives for the last month",
                                  "some Spend Objectives for this month"]
var string_headerLabel = ["some Spend Objective in the 12 month period",
                          "some Spend Objective for the last 3 months",
                          "some Spend Objective for the last month",
                          "some Spend Objective for this month"]



// spend Objectives - search

var searchReceipts_title = "Search Receipts"
var alert_receiptSearch = "Search Receipts"
var searchRefreshString = "Fetching search results..."
var alert_pleaseProvideSearch = "Please provide a search criteria"
var alert_amountDigitsValidation = "Please enter an amount lesser than 10,00,000. The amount should not have more than 2 digits after decimal, and should not begin with a 0"

var alert_validVendorName = "Please enter a vendor name"
var alert_validAmount = "Please enter a proper amount"
var alert_spentOn = "Please enter spent on detail"
var alert_dateCompare = "From date must be leser than To date"
var alert_amountRangeValidation = "Min amount must be lesser than Max amount"

// upload Receipt

var uploadReceipt_title = "Upload Receipt"
var alert_uploadtitle = "Upload Receipt"
var alert_beforeUploadWithoutImage = "Cash scanned will be activated for game only when a receipt is uploaded"
var alert_uploadReceipt = "Uploading Failed"


// Rewards
var rewards_title = "Rewards"
var rewardsRefreshString = "Getting Rewards"
var noRewardsString1 =  " <!DOCTYPE html> <html> <body> We are on a mission to help you save money. We provide rewards as a token of appreciation for your active participation.<br><br>Users who own the Cashkan that end up top on the Journey standings will be rewarded a gift card from a popular retailer. If you win you would see the reward details in this section."

var noRewards2 = "<br><br>Please get familiarized with the Rules of the game.</body></html>"

//var noRewardsString = noRewardsString1+noRewards2

var noRewardsString3 = " <!DOCTYPE html> <html> <body> Thanks for playing CashKan. Our mission is to help you save money. We provide rewards as a token of appreciation for your participation. We do not have any rewards to offer at this point. We will notify you when new rewards get posted. "

var noRewards4 = "<br><br>Keep playing CashKan, it's a fun way to manage your spend.</body></html>"

var noRewardsString = noRewardsString3+noRewards4

var alert_redeemRewrad = "Redeem Reward"

var redeemRewardMessage = "Do you want to redeem this reward?"

// Journey

var journey_title = "Journey"
var journeyDetail_title = "Journey Game"
var journeyRefreshString = "Getting CashKan Journey"
var journey_active = cashkan_bullet+" Active"
var journey_inactive = cashkan_bullet+" Inactive"
var journey_mapGuessStr = "Travel dist param by guessing the vendor"
//Drawer

var Drawer_titles : [String] = ["Profile","Notifications","Help","What is cashkan?","How it works?", "FAQ","Rules","Terms Of Use","Privacy","Contact Us"]


/* old arrays
// TIPS Array

var tipTitles : [String] = ["Tip #1 - Adjust camera focus","Tip #2 - For better scan","Tip #3 - Typing the serial number","Tip #4 - Click ? for help"]

var tipData : [String] = ["Tap anywhere on the camera view to adjust the focus.",
                          "Do not hold the currency note too close to camera.",
                          "If taking picture doesn't work, you can always type the serial number directly and click ☑️.",
                          "To get help on any screen, click ? on the top right corner."]

*/

// Notififcation

var cashkanSummary = "cashkanSummary"
var uploadReceipt = "uploadReceipt"

// Web view Urls

var What_is_cashkan     = "https://cashkan.com/#whatscashkan"
var How_it_works        = "https://cashkan.com/#howitworks"
var FAQ                 = "https://cashkan.com/#faqs"
var Rules               = "https://www.cashkan.com/rules.html"
var Terms_of_Use        = "https://cashkan.com/terms.html"
var Privacy             = "https://cashkan.com/privacy.html"
var contact_us_url      = "https://cashkan.com/#contactus"
var webViewurls : [String] = [What_is_cashkan, How_it_works, FAQ, Rules, Terms_of_Use, Privacy,contact_us_url]



//Login web view
var webViewUrls_title = ["How it works?", "What is CashKan?", "Contact Us", "Terms of Use", "Privacy"]
var webViewUrls_string = [How_it_works, What_is_cashkan, contact_us_url, Terms_of_Use, Privacy]


// Help urls
var scan_helpurl            = "https://cashkan.com/help.html#scan-main"
var scanObj_helpUrl         = "https://cashkan.com/help.html#scan-obj"
var scanList_helpUrl        = "https://cashkan.com/help.html#scan-list"
var scanLanding_helpUrl     = "https://cashkan.com/help.html#scan-landing"
var receipts_helpUrl        = "https://cashkan.com/help.html#receipts"
var journey_helpurl         = "https://cashkan.com/help.html#journey"
var journeyGame_helpUrl     = "https://cashkan.com/help.html#journey-game"
var rewards_helpurl         = "https://cashkan.com/help.html#rewards"
var helpUrl                 = "https://cashkan.com/help.html"
var uploadReceipt_helpUrl   = "https://cashkan.com/help.html#receipts-upload"


