//
//  URLConstants.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 28/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

//New server
let dev_server              =  "http://54.173.120.23:6005/api/v1" // main server ip dev server
let uat_server              = "https://api.cashkan.com/api/v1" //"http://52.42.176.77:6005/api/v1"


let baseUrl             =  uat_server  //"http://52.42.176.77:6001/api/v1" // main server ip

let uploadProfileImage  = "http://192.168.0.13:6001/api/v1/users/uploadProfileImage"
let log_url                         = "/login"
let register_url                    = "/users"
let otp_url                         = "/otvc"
var confirm_otp_url                 = "/otvc/confirm"
var resetPasssword_url              = "/reset"
var cashkan_summary                 = "/journey/"
var get_spendObjective_url          = "/spendObjective/"
var cashkan_session_url             = "/cashkanSession/"
var get_receipts_url_thisMonth      = "/cashkanSession?thisMonth=1"
var get_receipts_url_lastMonth      = "/cashkanSession?lastMonth=1"
var get_receipts_url_threeMonth     = "/cashkanSession?threeMonths=1"
var get_receipts_url_lastyearToDate = "/cashkanSession?"
var search_url                      = "/search/?"
var uploadReceipt_url               = "/reciepts"
var redeemRewardPoints_url          = "/rewards/redeem"
var logout_url                      = "/users"
var cashkan_journey_detail          = "/reports/"
var CashkansForMpView_url           = "/journey/game?"
var rewards_url                     = "/rewards"
var view_receipts_url               = "/reciepts/session/"
let login                           = baseUrl + log_url
let profileImageURL                 = baseUrl + "/prof_img/"


// FBs share medal Urls
var medal_base_url = "https://api.cashkan.com"

var goldMedal                   = medal_base_url + "/medals/newgold100.png";
var silverMedal                 = medal_base_url + "/medals/newsilver100.png";
var bronzeMedal                 = medal_base_url + "/medals/newbronze100.png";

var goldMedal_in                = medal_base_url + "/medals/gold-in.png";
var silverMedal_in              = medal_base_url + "/medals/silver-in.png";
var bronzeMedal_in              = medal_base_url + "/medals/bronze-in.png";

var goldMedal_us                = medal_base_url + "/medals/gold-us.png";
var silverMedal_us              = medal_base_url + "/medals/silver-us.png";
var bronzeMedal_us              = medal_base_url + "/medals/bronze-us.png";

var goldMedal_ca                = medal_base_url + "/medals/gold-ca.png";
var silverMedal_ca              = medal_base_url + "/medals/silver-ca.png";
var bronzeMedal_ca              = medal_base_url + "/medals/bronze-ca.png";
