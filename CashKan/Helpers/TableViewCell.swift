//
//  TableViewCell.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 13/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
@IBOutlet var dataLabel: UILabel!
    @IBOutlet var viewOne : UIView!
    
//    @IBOutlet var badgeView: UIView!
//    @IBOutlet var badgeNew: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.viewOne.layer.borderWidth = 2.0
        self.viewOne.layer.borderColor =  UIColor(red: 187 / 255,
            green: 92 / 255,
            blue: 222 / 255,
            alpha: 1.0).cgColor
        self.viewOne.layer.cornerRadius = 12.0
        
//        self.badgeNew.layer.cornerRadius = self.badgeNew.frame.width / 2
//        self.badgeNew.clipsToBounds = true
//        self.badgeNew.layer.masksToBounds = true
//        badgeNew.backgroundColor = UIColor(red: 42 / 255,
//            green: 150 / 255,
//            blue: 99 / 255,
//            alpha: 1.0)
//        badgeView.backgroundColor = UIColor(red: 42 / 255,
//            green: 150 / 255,
//            blue: 99 / 255,
//            alpha: 1.0)
//        self.bringSubviewToFront(badgeView)
        
        self.contentView .addSubview(viewOne)


    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
