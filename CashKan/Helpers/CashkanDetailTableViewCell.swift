//
//  CashkanDetailTableViewCell.swift
//  CashKan
//
//  Created by Emtarang TechLabs on 19/07/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit

class CashkanDetailTableViewCell: UITableViewCell {

    
    @IBOutlet var billLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    @IBOutlet var travelledLabel: UILabel!
    @IBOutlet var bulletLabel: UILabel!
    @IBOutlet var dividerView: UIView!
    @IBOutlet var ptsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        HELPER.setFontFamily( self.contentView, andSubViews: true)
        dividerView.backgroundColor = lightGray_color
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
