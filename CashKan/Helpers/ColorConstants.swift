//
//  ColorConstants.swift
//  CashKan
//
//  Created by emtarang on 18/08/16.
//  Copyright © 2016 Emtarang TechLabs. All rights reserved.
//

import UIKit


let watchMovie_color = UIColor(red: 69/255, green: 132/255, blue: 186/255, alpha: 1) //#4584ba
let music_color      = UIColor(red: 174/255, green: 103/255, blue: 204/255, alpha: 1)      //#ae67cc  rgb(174,103,204)
let cd_player_color  = UIColor(red: 239/255, green: 85/255, blue: 60/255, alpha: 1)  //#ef553c   rgb(239,85,60)
let clothes_color    = UIColor(red: 152/255, green: 190/255, blue: 79/255, alpha: 1)    //#98be4f   rgb(152,190,79)
let groceries_color  = UIColor(red: 84/255, green: 176/255, blue: 199/255, alpha: 1)  //#54b0c7   rgb(84,176,199)
let food_color       = UIColor(red: 233/255, green: 91/255, blue: 125/255, alpha: 1)       //#e95b7d   rgb(233,91,125)
let Beverages_color  = UIColor(red: 209/255, green: 139/255, blue: 65/255, alpha: 1)  //#d18b41    rgb(209,139,65)

let cashGreen_color  = UIColor(red: 42/255, green: 150/255, blue:99/255, alpha: 1.0) //#2A9663
let lightGray_color  = UIColor(red: 204/255, green: 204/255, blue:204/255, alpha: 1.0) //#cccccc
let darkGray_color   = UIColor(red: 188/255, green: 188/255, blue:188/255, alpha: 1.0) // #bcbcbc

let textGray_color   = UIColor(red: 110/255, green: 108/255, blue:108/255, alpha: 1.0) // #bcbcbc
let savingsGreen_color = UIColor(red: 68 / 255,green: 170 / 255,blue: 0 / 255,alpha: 1.0) //#44aa00
let overSpentRed_color = UIColor(red: 250 / 255,green: 56 / 255,blue: 51 / 255, alpha: 1.0)

let orangeText_color = UIColor(red: 252/255, green: 151/255, blue: 59/255, alpha: 1)// #fc973b
let darkOrange_color = UIColor(red: 191/255, green: 81/255, blue: 64/255, alpha: 1) //#bf5140
let lightOrange_color = UIColor(red: 203/255, green: 103/255, blue: 76/255, alpha: 1) //#CB674C
let orangeReceipt_color = UIColor(red: 239/255, green: 126/255, blue: 95/255, alpha: 1) //#ef7e5f

let rewardsPink_color = UIColor(red: 187 / 255,green: 92 / 255,blue: 222 / 255, alpha: 1.0)
let rewardsLightPink_color = UIColor(red: 221 / 255,green: 173 / 255,blue: 238 / 255, alpha: 1.0) //#ddadee

let journeyBlue_color = UIColor(red: 51 / 255, green: 137 / 255, blue: 195 / 255, alpha: 1.0)
let actualSpentGray_color  = UIColor(red: 138 / 255,green: 170 / 255,blue: 188 / 255,alpha: 1.0)
let facebookBlue_color = UIColor(red: 51/256, green: 79/256, blue: 141/256, alpha: 1)
let backgroundGray_color = UIColor(red: 205/255, green: 205/255, blue:205/255, alpha: 1.0)

let border_color = UIColor(red: 28/255, green: 128/255, blue:122/255, alpha: 1.0).cgColor
